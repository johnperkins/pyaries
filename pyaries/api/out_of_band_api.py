# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import re  # noqa: F401

from pydantic import validate_arguments, ValidationError
from typing_extensions import Annotated

from pydantic import Field, StrictBool, StrictStr, constr, validator

from typing import Optional

from pyaries.models.invitation_create_request import InvitationCreateRequest
from pyaries.models.invitation_message import InvitationMessage
from pyaries.models.invitation_record import InvitationRecord
from pyaries.models.oob_record import OobRecord

from pyaries.api_client import ApiClient
from pyaries.exceptions import (  # noqa: F401
    ApiTypeError,
    ApiValueError
)


class OutOfBandApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient.get_default()
        self.api_client = api_client

    @validate_arguments
    def create_invitation(self, auto_accept : Annotated[Optional[StrictBool], Field(description="Auto-accept connection (defaults to configuration)")] = None, multi_use : Annotated[Optional[StrictBool], Field(description="Create invitation for multiple use (default false)")] = None, body : Optional[InvitationCreateRequest] = None, **kwargs) -> InvitationRecord:  # noqa: E501
        """Create a new connection invitation  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_invitation(auto_accept, multi_use, body, async_req=True)
        >>> result = thread.get()

        :param auto_accept: Auto-accept connection (defaults to configuration)
        :type auto_accept: bool
        :param multi_use: Create invitation for multiple use (default false)
        :type multi_use: bool
        :param body:
        :type body: InvitationCreateRequest
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: InvitationRecord
        """
        kwargs['_return_http_data_only'] = True
        return self.create_invitation_with_http_info(auto_accept, multi_use, body, **kwargs)  # noqa: E501

    @validate_arguments
    def create_invitation_with_http_info(self, auto_accept : Annotated[Optional[StrictBool], Field(description="Auto-accept connection (defaults to configuration)")] = None, multi_use : Annotated[Optional[StrictBool], Field(description="Create invitation for multiple use (default false)")] = None, body : Optional[InvitationCreateRequest] = None, **kwargs):  # noqa: E501
        """Create a new connection invitation  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_invitation_with_http_info(auto_accept, multi_use, body, async_req=True)
        >>> result = thread.get()

        :param auto_accept: Auto-accept connection (defaults to configuration)
        :type auto_accept: bool
        :param multi_use: Create invitation for multiple use (default false)
        :type multi_use: bool
        :param body:
        :type body: InvitationCreateRequest
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(InvitationRecord, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'auto_accept',
            'multi_use',
            'body'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_invitation" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}

        # process the query parameters
        _query_params = []
        if _params.get('auto_accept') is not None:  # noqa: E501
            _query_params.append(('auto_accept', _params['auto_accept']))
        if _params.get('multi_use') is not None:  # noqa: E501
            _query_params.append(('multi_use', _params['multi_use']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['body']:
            _body_params = _params['body']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['AuthorizationHeader']  # noqa: E501

        _response_types_map = {
            200: "InvitationRecord",
        }

        return self.api_client.call_api(
            '/out-of-band/create-invitation', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))

    @validate_arguments
    def receive_invitation(self, alias : Annotated[Optional[StrictStr], Field(description="Alias for connection")] = None, auto_accept : Annotated[Optional[StrictBool], Field(description="Auto-accept connection (defaults to configuration)")] = None, mediation_id : Annotated[Optional[constr(strict=True)], Field(description="Identifier for active mediation record to be used")] = None, use_existing_connection : Annotated[Optional[StrictBool], Field(description="Use an existing connection, if possible")] = None, body : Optional[InvitationMessage] = None, **kwargs) -> OobRecord:  # noqa: E501
        """Receive a new connection invitation  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.receive_invitation(alias, auto_accept, mediation_id, use_existing_connection, body, async_req=True)
        >>> result = thread.get()

        :param alias: Alias for connection
        :type alias: str
        :param auto_accept: Auto-accept connection (defaults to configuration)
        :type auto_accept: bool
        :param mediation_id: Identifier for active mediation record to be used
        :type mediation_id: str
        :param use_existing_connection: Use an existing connection, if possible
        :type use_existing_connection: bool
        :param body:
        :type body: InvitationMessage
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: OobRecord
        """
        kwargs['_return_http_data_only'] = True
        return self.receive_invitation_with_http_info(alias, auto_accept, mediation_id, use_existing_connection, body, **kwargs)  # noqa: E501

    @validate_arguments
    def receive_invitation_with_http_info(self, alias : Annotated[Optional[StrictStr], Field(description="Alias for connection")] = None, auto_accept : Annotated[Optional[StrictBool], Field(description="Auto-accept connection (defaults to configuration)")] = None, mediation_id : Annotated[Optional[constr(strict=True)], Field(description="Identifier for active mediation record to be used")] = None, use_existing_connection : Annotated[Optional[StrictBool], Field(description="Use an existing connection, if possible")] = None, body : Optional[InvitationMessage] = None, **kwargs):  # noqa: E501
        """Receive a new connection invitation  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.receive_invitation_with_http_info(alias, auto_accept, mediation_id, use_existing_connection, body, async_req=True)
        >>> result = thread.get()

        :param alias: Alias for connection
        :type alias: str
        :param auto_accept: Auto-accept connection (defaults to configuration)
        :type auto_accept: bool
        :param mediation_id: Identifier for active mediation record to be used
        :type mediation_id: str
        :param use_existing_connection: Use an existing connection, if possible
        :type use_existing_connection: bool
        :param body:
        :type body: InvitationMessage
        :param async_req: Whether to execute the request asynchronously.
        :type async_req: bool, optional
        :param _return_http_data_only: response data without head status code
                                       and headers
        :type _return_http_data_only: bool, optional
        :param _preload_content: if False, the urllib3.HTTPResponse object will
                                 be returned without reading/decoding response
                                 data. Default is True.
        :type _preload_content: bool, optional
        :param _request_timeout: timeout setting for this request. If one
                                 number provided, it will be total request
                                 timeout. It can also be a pair (tuple) of
                                 (connection, read) timeouts.
        :param _request_auth: set to override the auth_settings for an a single
                              request; this effectively ignores the authentication
                              in the spec for a single request.
        :type _request_auth: dict, optional
        :type _content_type: string, optional: force content-type for the request
        :return: Returns the result object.
                 If the method is called asynchronously,
                 returns the request thread.
        :rtype: tuple(OobRecord, status_code(int), headers(HTTPHeaderDict))
        """

        _params = locals()

        _all_params = [
            'alias',
            'auto_accept',
            'mediation_id',
            'use_existing_connection',
            'body'
        ]
        _all_params.extend(
            [
                'async_req',
                '_return_http_data_only',
                '_preload_content',
                '_request_timeout',
                '_request_auth',
                '_content_type',
                '_headers'
            ]
        )

        # validate the arguments
        for _key, _val in _params['kwargs'].items():
            if _key not in _all_params:
                raise ApiTypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method receive_invitation" % key
                )
            _params[_key] = _val
        del _params['kwargs']

        _collection_formats = {}

        # process the path parameters
        _path_params = {}

        # process the query parameters
        _query_params = []
        if _params.get('alias') is not None:  # noqa: E501
            _query_params.append(('alias', _params['alias']))
        if _params.get('auto_accept') is not None:  # noqa: E501
            _query_params.append(('auto_accept', _params['auto_accept']))
        if _params.get('mediation_id') is not None:  # noqa: E501
            _query_params.append(('mediation_id', _params['mediation_id']))
        if _params.get('use_existing_connection') is not None:  # noqa: E501
            _query_params.append(('use_existing_connection', _params['use_existing_connection']))

        # process the header parameters
        _header_params = dict(_params.get('_headers', {}))

        # process the form parameters
        _form_params = []
        _files = {}

        # process the body parameter
        _body_params = None
        if _params['body']:
            _body_params = _params['body']

        # set the HTTP header `Accept`
        _header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # set the HTTP header `Content-Type`
        _content_types_list = _params.get('_content_type',
            self.api_client.select_header_content_type(
                ['application/json']))
        if _content_types_list:
                _header_params['Content-Type'] = _content_types_list

        # authentication setting
        _auth_settings = ['AuthorizationHeader']  # noqa: E501

        _response_types_map = {
            200: "OobRecord",
        }

        return self.api_client.call_api(
            '/out-of-band/receive-invitation', 'POST',
            _path_params,
            _query_params,
            _header_params,
            body=_body_params,
            post_params=_form_params,
            files=_files,
            response_types_map=_response_types_map,
            auth_settings=_auth_settings,
            async_req=_params.get('async_req'),
            _return_http_data_only=_params.get('_return_http_data_only'),  # noqa: E501
            _preload_content=_params.get('_preload_content', True),
            _request_timeout=_params.get('_request_timeout'),
            collection_formats=_collection_formats,
            _request_auth=_params.get('_request_auth'))
