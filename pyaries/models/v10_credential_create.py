# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictBool, StrictStr, constr, validator
from pyaries.models.credential_preview import CredentialPreview
from pydantic import ValidationError

class V10CredentialCreate(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    auto_remove: Optional[StrictBool] = Field(None, description="Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting)")
    comment: Optional[StrictStr] = Field(None, description="Human-readable comment")
    cred_def_id: Optional[constr(strict=True)] = Field(None, description="Credential definition identifier")
    credential_proposal: CredentialPreview = ...
    issuer_did: Optional[constr(strict=True)] = Field(None, description="Credential issuer DID")
    schema_id: Optional[constr(strict=True)] = Field(None, description="Schema identifier")
    schema_issuer_did: Optional[constr(strict=True)] = Field(None, description="Schema issuer DID")
    schema_name: Optional[StrictStr] = Field(None, description="Schema name")
    schema_version: Optional[constr(strict=True)] = Field(None, description="Schema version")
    trace: Optional[StrictBool] = Field(None, description="Record trace information, based on agent configuration")

    @validator('cred_def_id')
    def cred_def_id_validate_regular_expression(cls, v):
        if not re.match(r"^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$", v):
            raise ValueError(r"must validate the regular expression /^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+)):(.+)?$/")
        return v

    @validator('issuer_did')
    def issuer_did_validate_regular_expression(cls, v):
        if not re.match(r"^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$", v):
            raise ValueError(r"must validate the regular expression /^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$/")
        return v

    @validator('schema_id')
    def schema_id_validate_regular_expression(cls, v):
        if not re.match(r"^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$", v):
            raise ValueError(r"must validate the regular expression /^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+$/")
        return v

    @validator('schema_issuer_did')
    def schema_issuer_did_validate_regular_expression(cls, v):
        if not re.match(r"^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$", v):
            raise ValueError(r"must validate the regular expression /^(did:sov:)?[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}$/")
        return v

    @validator('schema_version')
    def schema_version_validate_regular_expression(cls, v):
        if not re.match(r"^[0-9.]+$", v):
            raise ValueError(r"must validate the regular expression /^[0-9.]+$/")
        return v

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V10CredentialCreate:
        """Create an instance of V10CredentialCreate from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of credential_proposal
        if self.credential_proposal:
            _dict['credential_proposal'] = self.credential_proposal.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V10CredentialCreate:
        """Create an instance of V10CredentialCreate from a dict"""
        if type(obj) is not dict:
            return V10CredentialCreate.parse_obj(obj)

        return V10CredentialCreate.parse_obj({
            "auto_remove": obj.get("auto_remove"),
            "comment": obj.get("comment"),
            "cred_def_id": obj.get("cred_def_id"),
            "credential_proposal": CredentialPreview.from_dict(obj.get("credential_proposal")),
            "issuer_did": obj.get("issuer_did"),
            "schema_id": obj.get("schema_id"),
            "schema_issuer_did": obj.get("schema_issuer_did"),
            "schema_name": obj.get("schema_name"),
            "schema_version": obj.get("schema_version"),
            "trace": obj.get("trace")
        })


