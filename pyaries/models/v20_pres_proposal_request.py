# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictBool, StrictStr
from pyaries.models.v20_pres_proposal_by_format import V20PresProposalByFormat
from pydantic import ValidationError

class V20PresProposalRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    auto_present: Optional[StrictBool] = Field(None, description="Whether to respond automatically to presentation requests, building and presenting requested proof")
    comment: Optional[StrictStr] = Field(None, description="Human-readable comment")
    connection_id: StrictStr = Field(..., description="Connection identifier")
    presentation_proposal: V20PresProposalByFormat = ...
    trace: Optional[StrictBool] = Field(None, description="Whether to trace event (default false)")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V20PresProposalRequest:
        """Create an instance of V20PresProposalRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of presentation_proposal
        if self.presentation_proposal:
            _dict['presentation_proposal'] = self.presentation_proposal.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V20PresProposalRequest:
        """Create an instance of V20PresProposalRequest from a dict"""
        if type(obj) is not dict:
            return V20PresProposalRequest.parse_obj(obj)

        return V20PresProposalRequest.parse_obj({
            "auto_present": obj.get("auto_present"),
            "comment": obj.get("comment"),
            "connection_id": obj.get("connection_id"),
            "presentation_proposal": V20PresProposalByFormat.from_dict(obj.get("presentation_proposal")),
            "trace": obj.get("trace")
        })


