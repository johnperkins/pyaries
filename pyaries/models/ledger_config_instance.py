# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictBool, StrictStr
from pydantic import ValidationError

class LedgerConfigInstance(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    genesis_file: Optional[StrictStr] = Field(None, description="genesis_file")
    genesis_transactions: Optional[StrictStr] = Field(None, description="genesis_transactions")
    genesis_url: Optional[StrictStr] = Field(None, description="genesis_url")
    id: Optional[StrictStr] = Field(None, description="ledger_id")
    is_production: Optional[StrictBool] = Field(None, description="is_production")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> LedgerConfigInstance:
        """Create an instance of LedgerConfigInstance from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> LedgerConfigInstance:
        """Create an instance of LedgerConfigInstance from a dict"""
        if type(obj) is not dict:
            return LedgerConfigInstance.parse_obj(obj)

        return LedgerConfigInstance.parse_obj({
            "genesis_file": obj.get("genesis_file"),
            "genesis_transactions": obj.get("genesis_transactions"),
            "genesis_url": obj.get("genesis_url"),
            "id": obj.get("id"),
            "is_production": obj.get("is_production")
        })


