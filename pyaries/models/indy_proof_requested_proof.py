# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Any, Dict, Optional
from pydantic import BaseModel, Field
from pyaries.models.indy_proof_requested_proof_predicate import IndyProofRequestedProofPredicate
from pyaries.models.indy_proof_requested_proof_revealed_attr import IndyProofRequestedProofRevealedAttr
from pyaries.models.indy_proof_requested_proof_revealed_attr_group import IndyProofRequestedProofRevealedAttrGroup
from pydantic import ValidationError

class IndyProofRequestedProof(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    predicates: Optional[Dict[str, IndyProofRequestedProofPredicate]] = Field(None, description="Proof requested proof predicates.")
    revealed_attr_groups: Optional[Dict[str, IndyProofRequestedProofRevealedAttrGroup]] = Field(None, description="Proof requested proof revealed attribute groups")
    revealed_attrs: Optional[Dict[str, IndyProofRequestedProofRevealedAttr]] = Field(None, description="Proof requested proof revealed attributes")
    self_attested_attrs: Optional[Dict[str, Any]] = Field(None, description="Proof requested proof self-attested attributes")
    unrevealed_attrs: Optional[Dict[str, Any]] = Field(None, description="Unrevealed attributes")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> IndyProofRequestedProof:
        """Create an instance of IndyProofRequestedProof from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each value in predicates (dict)
        _field_dict = {}
        if self.predicates:
            for _key in self.predicates:
                if self.predicates[_key]:
                    _field_dict[_key] = self.predicates[_key].to_dict()
            _dict['predicates'] = _field_dict
        # override the default output from pydantic by calling `to_dict()` of each value in revealed_attr_groups (dict)
        _field_dict = {}
        if self.revealed_attr_groups:
            for _key in self.revealed_attr_groups:
                if self.revealed_attr_groups[_key]:
                    _field_dict[_key] = self.revealed_attr_groups[_key].to_dict()
            _dict['revealed_attr_groups'] = _field_dict
        # override the default output from pydantic by calling `to_dict()` of each value in revealed_attrs (dict)
        _field_dict = {}
        if self.revealed_attrs:
            for _key in self.revealed_attrs:
                if self.revealed_attrs[_key]:
                    _field_dict[_key] = self.revealed_attrs[_key].to_dict()
            _dict['revealed_attrs'] = _field_dict

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> IndyProofRequestedProof:
        """Create an instance of IndyProofRequestedProof from a dict"""
        if type(obj) is not dict:
            return IndyProofRequestedProof.parse_obj(obj)

        return IndyProofRequestedProof.parse_obj({
            "predicates": dict((_k, Dict[str, IndyProofRequestedProofPredicate].from_dict(_v)) for _k, _v in obj.get("predicates").items()),
            "revealed_attr_groups": dict((_k, Dict[str, IndyProofRequestedProofRevealedAttrGroup].from_dict(_v)) for _k, _v in obj.get("revealed_attr_groups").items()),
            "revealed_attrs": dict((_k, Dict[str, IndyProofRequestedProofRevealedAttr].from_dict(_v)) for _k, _v in obj.get("revealed_attrs").items()),
            "self_attested_attrs": obj.get("self_attested_attrs"),
            "unrevealed_attrs": obj.get("unrevealed_attrs")
        })


