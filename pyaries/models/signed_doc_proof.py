# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictStr
from pydantic import ValidationError

class SignedDocProof(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    challenge: Optional[StrictStr] = None
    domain: Optional[StrictStr] = None
    proof_purpose: StrictStr = Field(..., alias="proofPurpose")
    type: Optional[StrictStr] = None
    verification_method: StrictStr = Field(..., alias="verificationMethod")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> SignedDocProof:
        """Create an instance of SignedDocProof from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> SignedDocProof:
        """Create an instance of SignedDocProof from a dict"""
        if type(obj) is not dict:
            return SignedDocProof.parse_obj(obj)

        return SignedDocProof.parse_obj({
            "challenge": obj.get("challenge"),
            "domain": obj.get("domain"),
            "proof_purpose": obj.get("proofPurpose"),
            "type": obj.get("type"),
            "verification_method": obj.get("verificationMethod")
        })


