# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel, Field, StrictBool, StrictStr, constr, validator
from pydantic import ValidationError

class RevokeRequest(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    comment: Optional[StrictStr] = Field(None, description="Optional comment to include in revocation notification")
    connection_id: Optional[constr(strict=True)] = Field(None, description="Connection ID to which the revocation notification will be sent; required if notify is true")
    cred_ex_id: Optional[constr(strict=True)] = Field(None, description="Credential exchange identifier")
    cred_rev_id: Optional[constr(strict=True)] = Field(None, description="Credential revocation identifier")
    notify: Optional[StrictBool] = Field(None, description="Send a notification to the credential recipient")
    notify_version: Optional[StrictStr] = Field(None, description="Specify which version of the revocation notification should be sent")
    publish: Optional[StrictBool] = Field(None, description="(True) publish revocation to ledger immediately, or (default, False) mark it pending")
    rev_reg_id: Optional[constr(strict=True)] = Field(None, description="Revocation registry identifier")
    thread_id: Optional[StrictStr] = Field(None, description="Thread ID of the credential exchange message thread resulting in the credential now being revoked; required if notify is true")

    @validator('connection_id')
    def connection_id_validate_regular_expression(cls, v):
        if not re.match(r"[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}", v):
            raise ValueError(r"must validate the regular expression /[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}/")
        return v

    @validator('cred_ex_id')
    def cred_ex_id_validate_regular_expression(cls, v):
        if not re.match(r"[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}", v):
            raise ValueError(r"must validate the regular expression /[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}/")
        return v

    @validator('cred_rev_id')
    def cred_rev_id_validate_regular_expression(cls, v):
        if not re.match(r"^[1-9][0-9]*$", v):
            raise ValueError(r"must validate the regular expression /^[1-9][0-9]*$/")
        return v

    @validator('notify_version')
    def notify_version_validate_enum(cls, v):
        if v not in ('v1_0', 'v2_0'):
            raise ValueError("must validate the enum values ('v1_0', 'v2_0')")
        return v

    @validator('rev_reg_id')
    def rev_reg_id_validate_regular_expression(cls, v):
        if not re.match(r"^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)", v):
            raise ValueError(r"must validate the regular expression /^([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):4:([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}):3:CL:(([1-9][0-9]*)|([123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{21,22}:2:.+:[0-9.]+))(:.+)?:CL_ACCUM:(.+$)/")
        return v

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> RevokeRequest:
        """Create an instance of RevokeRequest from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> RevokeRequest:
        """Create an instance of RevokeRequest from a dict"""
        if type(obj) is not dict:
            return RevokeRequest.parse_obj(obj)

        return RevokeRequest.parse_obj({
            "comment": obj.get("comment"),
            "connection_id": obj.get("connection_id"),
            "cred_ex_id": obj.get("cred_ex_id"),
            "cred_rev_id": obj.get("cred_rev_id"),
            "notify": obj.get("notify"),
            "notify_version": obj.get("notify_version"),
            "publish": obj.get("publish"),
            "rev_reg_id": obj.get("rev_reg_id"),
            "thread_id": obj.get("thread_id")
        })


