# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Optional
from pydantic import BaseModel
from pyaries.models.v10_discovery_record import V10DiscoveryRecord
from pydantic import ValidationError

class V10DiscoveryExchangeResult(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    results: Optional[V10DiscoveryRecord] = None

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> V10DiscoveryExchangeResult:
        """Create an instance of V10DiscoveryExchangeResult from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of results
        if self.results:
            _dict['results'] = self.results.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> V10DiscoveryExchangeResult:
        """Create an instance of V10DiscoveryExchangeResult from a dict"""
        if type(obj) is not dict:
            return V10DiscoveryExchangeResult.parse_obj(obj)

        return V10DiscoveryExchangeResult.parse_obj({
            "results": V10DiscoveryRecord.from_dict(obj.get("results"))
        })


