# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import Dict, List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyaries.models.indy_proof_req_attr_spec_non_revoked import IndyProofReqAttrSpecNonRevoked
from pydantic import ValidationError

class IndyProofReqAttrSpec(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    name: Optional[StrictStr] = Field(None, description="Attribute name")
    names: Optional[List[StrictStr]] = Field(None, description="Attribute name group")
    non_revoked: Optional[IndyProofReqAttrSpecNonRevoked] = None
    restrictions: Optional[List[Dict[str, StrictStr]]] = Field(None, description="If present, credential must satisfy one of given restrictions: specify schema_id, schema_issuer_did, schema_name, schema_version, issuer_did, cred_def_id, and/or attr::<attribute-name>::value where <attribute-name> represents a credential attribute name")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> IndyProofReqAttrSpec:
        """Create an instance of IndyProofReqAttrSpec from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of non_revoked
        if self.non_revoked:
            _dict['non_revoked'] = self.non_revoked.to_dict()

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> IndyProofReqAttrSpec:
        """Create an instance of IndyProofReqAttrSpec from a dict"""
        if type(obj) is not dict:
            return IndyProofReqAttrSpec.parse_obj(obj)

        return IndyProofReqAttrSpec.parse_obj({
            "name": obj.get("name"),
            "names": obj.get("names"),
            "non_revoked": IndyProofReqAttrSpecNonRevoked.from_dict(obj.get("non_revoked")),
            "restrictions": obj.get("restrictions")
        })


