# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictStr
from pyaries.models.keylist_update_rule import KeylistUpdateRule
from pydantic import ValidationError

class KeylistUpdate(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    id: Optional[StrictStr] = Field(None, alias="@id", description="Message identifier")
    type: Optional[StrictStr] = Field(None, alias="@type", description="Message type")
    updates: Optional[List[KeylistUpdateRule]] = Field(None, description="List of update rules")

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> KeylistUpdate:
        """Create an instance of KeylistUpdate from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True, exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in updates (list)
        _items = []
        if self.updates:
            for _item in self.updates:
                if _item:
                    _items.append(_item.to_dict())
            _dict['updates'] = _items

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> KeylistUpdate:
        """Create an instance of KeylistUpdate from a dict"""
        if type(obj) is not dict:
            return KeylistUpdate.parse_obj(obj)

        return KeylistUpdate.parse_obj({
            "id": obj.get("@id"),
            "type": obj.get("@type"),
            "updates": [KeylistUpdateRule.from_dict(_item) for _item in obj.get("updates")]
        })


