# pyaries.MultitenancyApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_wallet**](MultitenancyApi.md#create_wallet) | **POST** /multitenancy/wallet | Create a subwallet
[**delete_wallet**](MultitenancyApi.md#delete_wallet) | **POST** /multitenancy/wallet/{wallet_id}/remove | Remove a subwallet
[**get_auth_token**](MultitenancyApi.md#get_auth_token) | **POST** /multitenancy/wallet/{wallet_id}/token | Get auth token for a subwallet
[**get_wallet**](MultitenancyApi.md#get_wallet) | **GET** /multitenancy/wallet/{wallet_id} | Get a single subwallet
[**get_wallets**](MultitenancyApi.md#get_wallets) | **GET** /multitenancy/wallets | Query subwallets
[**update_wallet**](MultitenancyApi.md#update_wallet) | **PUT** /multitenancy/wallet/{wallet_id} | Update a subwallet


# **create_wallet**
> CreateWalletResponse create_wallet(body=body)

Create a subwallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MultitenancyApi(api_client)
    body = pyaries.CreateWalletRequest() # CreateWalletRequest |  (optional)

    try:
        # Create a subwallet
        api_response = api_instance.create_wallet(body=body)
        print("The response of MultitenancyApi->create_wallet:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MultitenancyApi->create_wallet: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CreateWalletRequest**](CreateWalletRequest.md)|  | [optional] 

### Return type

[**CreateWalletResponse**](CreateWalletResponse.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_wallet**
> object delete_wallet(wallet_id, body=body)

Remove a subwallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MultitenancyApi(api_client)
    wallet_id = 'wallet_id_example' # str | Subwallet identifier
    body = pyaries.RemoveWalletRequest() # RemoveWalletRequest |  (optional)

    try:
        # Remove a subwallet
        api_response = api_instance.delete_wallet(wallet_id, body=body)
        print("The response of MultitenancyApi->delete_wallet:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MultitenancyApi->delete_wallet: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wallet_id** | **str**| Subwallet identifier | 
 **body** | [**RemoveWalletRequest**](RemoveWalletRequest.md)|  | [optional] 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_auth_token**
> CreateWalletTokenResponse get_auth_token(wallet_id, body=body)

Get auth token for a subwallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MultitenancyApi(api_client)
    wallet_id = 'wallet_id_example' # str | 
    body = pyaries.CreateWalletTokenRequest() # CreateWalletTokenRequest |  (optional)

    try:
        # Get auth token for a subwallet
        api_response = api_instance.get_auth_token(wallet_id, body=body)
        print("The response of MultitenancyApi->get_auth_token:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MultitenancyApi->get_auth_token: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wallet_id** | **str**|  | 
 **body** | [**CreateWalletTokenRequest**](CreateWalletTokenRequest.md)|  | [optional] 

### Return type

[**CreateWalletTokenResponse**](CreateWalletTokenResponse.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_wallet**
> WalletRecord get_wallet(wallet_id)

Get a single subwallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MultitenancyApi(api_client)
    wallet_id = 'wallet_id_example' # str | Subwallet identifier

    try:
        # Get a single subwallet
        api_response = api_instance.get_wallet(wallet_id)
        print("The response of MultitenancyApi->get_wallet:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MultitenancyApi->get_wallet: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wallet_id** | **str**| Subwallet identifier | 

### Return type

[**WalletRecord**](WalletRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_wallets**
> WalletList get_wallets(wallet_name=wallet_name)

Query subwallets

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MultitenancyApi(api_client)
    wallet_name = 'wallet_name_example' # str | Wallet name (optional)

    try:
        # Query subwallets
        api_response = api_instance.get_wallets(wallet_name=wallet_name)
        print("The response of MultitenancyApi->get_wallets:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MultitenancyApi->get_wallets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wallet_name** | **str**| Wallet name | [optional] 

### Return type

[**WalletList**](WalletList.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_wallet**
> WalletRecord update_wallet(wallet_id, body=body)

Update a subwallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MultitenancyApi(api_client)
    wallet_id = 'wallet_id_example' # str | Subwallet identifier
    body = pyaries.UpdateWalletRequest() # UpdateWalletRequest |  (optional)

    try:
        # Update a subwallet
        api_response = api_instance.update_wallet(wallet_id, body=body)
        print("The response of MultitenancyApi->update_wallet:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MultitenancyApi->update_wallet: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wallet_id** | **str**| Subwallet identifier | 
 **body** | [**UpdateWalletRequest**](UpdateWalletRequest.md)|  | [optional] 

### Return type

[**WalletRecord**](WalletRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

