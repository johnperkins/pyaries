# V20CredExRecordByFormat


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_issue** | **object** |  | [optional] 
**cred_offer** | **object** |  | [optional] 
**cred_proposal** | **object** |  | [optional] 
**cred_request** | **object** |  | [optional] 

## Example

```python
from pyaries.models.v20_cred_ex_record_by_format import V20CredExRecordByFormat

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExRecordByFormat from a JSON string
v20_cred_ex_record_by_format_instance = V20CredExRecordByFormat.from_json(json)
# print the JSON string representation of the object
print V20CredExRecordByFormat.to_json()

# convert the object into a dict
v20_cred_ex_record_by_format_dict = v20_cred_ex_record_by_format_instance.to_dict()
# create an instance of V20CredExRecordByFormat from a dict
v20_cred_ex_record_by_format_form_dict = v20_cred_ex_record_by_format.from_dict(v20_cred_ex_record_by_format_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


