# DIDList


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[DID]**](DID.md) | DID list | [optional] 

## Example

```python
from pyaries.models.did_list import DIDList

# TODO update the JSON string below
json = "{}"
# create an instance of DIDList from a JSON string
did_list_instance = DIDList.from_json(json)
# print the JSON string representation of the object
print DIDList.to_json()

# convert the object into a dict
did_list_dict = did_list_instance.to_dict()
# create an instance of DIDList from a dict
did_list_form_dict = did_list.from_dict(did_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


