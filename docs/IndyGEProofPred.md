# IndyGEProofPred


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attr_name** | **str** | Attribute name, indy-canonicalized | [optional] 
**p_type** | **str** | Predicate type | [optional] 
**value** | **int** | Predicate threshold value | [optional] 

## Example

```python
from pyaries.models.indy_ge_proof_pred import IndyGEProofPred

# TODO update the JSON string below
json = "{}"
# create an instance of IndyGEProofPred from a JSON string
indy_ge_proof_pred_instance = IndyGEProofPred.from_json(json)
# print the JSON string representation of the object
print IndyGEProofPred.to_json()

# convert the object into a dict
indy_ge_proof_pred_dict = indy_ge_proof_pred_instance.to_dict()
# create an instance of IndyGEProofPred from a dict
indy_ge_proof_pred_form_dict = indy_ge_proof_pred.from_dict(indy_ge_proof_pred_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


