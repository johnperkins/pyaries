# V20CredRequestFree


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_remove** | **bool** | Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) | [optional] 
**comment** | **str** | Human-readable comment | [optional] 
**connection_id** | **str** | Connection identifier | 
**filter** | [**V20CredFilterLDProof**](V20CredFilterLDProof.md) |  | 
**holder_did** | **str** | Holder DID to substitute for the credentialSubject.id | [optional] 
**trace** | **bool** | Whether to trace event (default false) | [optional] 

## Example

```python
from pyaries.models.v20_cred_request_free import V20CredRequestFree

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredRequestFree from a JSON string
v20_cred_request_free_instance = V20CredRequestFree.from_json(json)
# print the JSON string representation of the object
print V20CredRequestFree.to_json()

# convert the object into a dict
v20_cred_request_free_dict = v20_cred_request_free_instance.to_dict()
# create an instance of V20CredRequestFree from a dict
v20_cred_request_free_form_dict = v20_cred_request_free.from_dict(v20_cred_request_free_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


