# CredentialDefinitionSendRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revocation_registry_size** | **int** | Revocation registry size | [optional] 
**schema_id** | **str** | Schema identifier | [optional] 
**support_revocation** | **bool** | Revocation supported flag | [optional] 
**tag** | **str** | Credential definition identifier tag | [optional] 

## Example

```python
from pyaries.models.credential_definition_send_request import CredentialDefinitionSendRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialDefinitionSendRequest from a JSON string
credential_definition_send_request_instance = CredentialDefinitionSendRequest.from_json(json)
# print the JSON string representation of the object
print CredentialDefinitionSendRequest.to_json()

# convert the object into a dict
credential_definition_send_request_dict = credential_definition_send_request_instance.to_dict()
# create an instance of CredentialDefinitionSendRequest from a dict
credential_definition_send_request_form_dict = credential_definition_send_request.from_dict(credential_definition_send_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


