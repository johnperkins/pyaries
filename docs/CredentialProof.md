# CredentialProof

The proof of the credential

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**challenge** | **str** | Associates a challenge with a proof, for use with a proofPurpose such as authentication | [optional] 
**created** | **str** | The string value of an ISO8601 combined date and time string generated by the Signature Algorithm | 
**domain** | **str** | A string value specifying the restricted domain of the signature. | [optional] 
**jws** | **str** | Associates a Detached Json Web Signature with a proof | [optional] 
**nonce** | **str** | The nonce | [optional] 
**proof_purpose** | **str** | Proof purpose | 
**proof_value** | **str** | The proof value of a proof | [optional] 
**type** | **str** | Identifies the digital signature suite that was used to create the signature | 
**verification_method** | **str** | Information used for proof verification | 

## Example

```python
from pyaries.models.credential_proof import CredentialProof

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialProof from a JSON string
credential_proof_instance = CredentialProof.from_json(json)
# print the JSON string representation of the object
print CredentialProof.to_json()

# convert the object into a dict
credential_proof_dict = credential_proof_instance.to_dict()
# create an instance of CredentialProof from a dict
credential_proof_form_dict = credential_proof.from_dict(credential_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


