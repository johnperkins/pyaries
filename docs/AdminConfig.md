# AdminConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | **object** | Configuration settings | [optional] 

## Example

```python
from pyaries.models.admin_config import AdminConfig

# TODO update the JSON string below
json = "{}"
# create an instance of AdminConfig from a JSON string
admin_config_instance = AdminConfig.from_json(json)
# print the JSON string representation of the object
print AdminConfig.to_json()

# convert the object into a dict
admin_config_dict = admin_config_instance.to_dict()
# create an instance of AdminConfig from a dict
admin_config_form_dict = admin_config.from_dict(admin_config_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


