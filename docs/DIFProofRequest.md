# DIFProofRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**options** | [**DIFOptions**](DIFOptions.md) |  | [optional] 
**presentation_definition** | [**PresentationDefinition**](PresentationDefinition.md) |  | 

## Example

```python
from pyaries.models.dif_proof_request import DIFProofRequest

# TODO update the JSON string below
json = "{}"
# create an instance of DIFProofRequest from a JSON string
dif_proof_request_instance = DIFProofRequest.from_json(json)
# print the JSON string representation of the object
print DIFProofRequest.to_json()

# convert the object into a dict
dif_proof_request_dict = dif_proof_request_instance.to_dict()
# create an instance of DIFProofRequest from a dict
dif_proof_request_form_dict = dif_proof_request.from_dict(dif_proof_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


