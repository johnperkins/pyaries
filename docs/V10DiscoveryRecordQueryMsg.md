# V10DiscoveryRecordQueryMsg

Query message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** |  | [optional] 
**query** | **str** |  | 

## Example

```python
from pyaries.models.v10_discovery_record_query_msg import V10DiscoveryRecordQueryMsg

# TODO update the JSON string below
json = "{}"
# create an instance of V10DiscoveryRecordQueryMsg from a JSON string
v10_discovery_record_query_msg_instance = V10DiscoveryRecordQueryMsg.from_json(json)
# print the JSON string representation of the object
print V10DiscoveryRecordQueryMsg.to_json()

# convert the object into a dict
v10_discovery_record_query_msg_dict = v10_discovery_record_query_msg_instance.to_dict()
# create an instance of V10DiscoveryRecordQueryMsg from a dict
v10_discovery_record_query_msg_form_dict = v10_discovery_record_query_msg.from_dict(v10_discovery_record_query_msg_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


