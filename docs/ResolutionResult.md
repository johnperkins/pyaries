# ResolutionResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**did_doc** | **object** | DID Document | 
**metadata** | **object** | Resolution metadata | 

## Example

```python
from pyaries.models.resolution_result import ResolutionResult

# TODO update the JSON string below
json = "{}"
# create an instance of ResolutionResult from a JSON string
resolution_result_instance = ResolutionResult.from_json(json)
# print the JSON string representation of the object
print ResolutionResult.to_json()

# convert the object into a dict
resolution_result_dict = resolution_result_instance.to_dict()
# create an instance of ResolutionResult from a dict
resolution_result_form_dict = resolution_result.from_dict(resolution_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


