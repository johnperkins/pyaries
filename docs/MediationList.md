# MediationList


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[MediationRecord]**](MediationRecord.md) | List of mediation records | [optional] 

## Example

```python
from pyaries.models.mediation_list import MediationList

# TODO update the JSON string below
json = "{}"
# create an instance of MediationList from a JSON string
mediation_list_instance = MediationList.from_json(json)
# print the JSON string representation of the object
print MediationList.to_json()

# convert the object into a dict
mediation_list_dict = mediation_list_instance.to_dict()
# create an instance of MediationList from a dict
mediation_list_form_dict = mediation_list.from_dict(mediation_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


