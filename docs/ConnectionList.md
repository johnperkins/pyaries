# ConnectionList


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[ConnRecord]**](ConnRecord.md) | List of connection records | [optional] 

## Example

```python
from pyaries.models.connection_list import ConnectionList

# TODO update the JSON string below
json = "{}"
# create an instance of ConnectionList from a JSON string
connection_list_instance = ConnectionList.from_json(json)
# print the JSON string representation of the object
print ConnectionList.to_json()

# convert the object into a dict
connection_list_dict = connection_list_instance.to_dict()
# create an instance of ConnectionList from a dict
connection_list_form_dict = connection_list.from_dict(connection_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


