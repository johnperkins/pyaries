# V10DiscoveryExchangeListResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[V10DiscoveryRecord]**](V10DiscoveryRecord.md) |  | [optional] 

## Example

```python
from pyaries.models.v10_discovery_exchange_list_result import V10DiscoveryExchangeListResult

# TODO update the JSON string below
json = "{}"
# create an instance of V10DiscoveryExchangeListResult from a JSON string
v10_discovery_exchange_list_result_instance = V10DiscoveryExchangeListResult.from_json(json)
# print the JSON string representation of the object
print V10DiscoveryExchangeListResult.to_json()

# convert the object into a dict
v10_discovery_exchange_list_result_dict = v10_discovery_exchange_list_result_instance.to_dict()
# create an instance of V10DiscoveryExchangeListResult from a dict
v10_discovery_exchange_list_result_form_dict = v10_discovery_exchange_list_result.from_dict(v10_discovery_exchange_list_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


