# KeylistQueryFilterRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter** | **object** | Filter for keylist query | [optional] 

## Example

```python
from pyaries.models.keylist_query_filter_request import KeylistQueryFilterRequest

# TODO update the JSON string below
json = "{}"
# create an instance of KeylistQueryFilterRequest from a JSON string
keylist_query_filter_request_instance = KeylistQueryFilterRequest.from_json(json)
# print the JSON string representation of the object
print KeylistQueryFilterRequest.to_json()

# convert the object into a dict
keylist_query_filter_request_dict = keylist_query_filter_request_instance.to_dict()
# create an instance of KeylistQueryFilterRequest from a dict
keylist_query_filter_request_form_dict = keylist_query_filter_request.from_dict(keylist_query_filter_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


