# ConnectionStaticRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alias** | **str** | Alias to assign to this connection | [optional] 
**my_did** | **str** | Local DID | [optional] 
**my_seed** | **str** | Seed to use for the local DID | [optional] 
**their_did** | **str** | Remote DID | [optional] 
**their_endpoint** | **str** | URL endpoint for other party | [optional] 
**their_label** | **str** | Other party&#39;s label for this connection | [optional] 
**their_seed** | **str** | Seed to use for the remote DID | [optional] 
**their_verkey** | **str** | Remote verification key | [optional] 

## Example

```python
from pyaries.models.connection_static_request import ConnectionStaticRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ConnectionStaticRequest from a JSON string
connection_static_request_instance = ConnectionStaticRequest.from_json(json)
# print the JSON string representation of the object
print ConnectionStaticRequest.to_json()

# convert the object into a dict
connection_static_request_dict = connection_static_request_instance.to_dict()
# create an instance of ConnectionStaticRequest from a dict
connection_static_request_form_dict = connection_static_request.from_dict(connection_static_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


