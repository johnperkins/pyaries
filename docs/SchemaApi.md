# pyaries.SchemaApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_created_schemas**](SchemaApi.md#get_created_schemas) | **GET** /schemas/created | Search for matching schema that agent originated
[**get_schema**](SchemaApi.md#get_schema) | **GET** /schemas/{schema_id} | Gets a schema from the ledger
[**publish_schema**](SchemaApi.md#publish_schema) | **POST** /schemas | Sends a schema to the ledger
[**write_record**](SchemaApi.md#write_record) | **POST** /schemas/{schema_id}/write_record | Writes a schema non-secret record to the wallet


# **get_created_schemas**
> SchemasCreatedResult get_created_schemas(schema_id=schema_id, schema_issuer_did=schema_issuer_did, schema_name=schema_name, schema_version=schema_version)

Search for matching schema that agent originated

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.SchemaApi(api_client)
    schema_id = 'schema_id_example' # str | Schema identifier (optional)
    schema_issuer_did = 'schema_issuer_did_example' # str | Schema issuer DID (optional)
    schema_name = 'schema_name_example' # str | Schema name (optional)
    schema_version = 'schema_version_example' # str | Schema version (optional)

    try:
        # Search for matching schema that agent originated
        api_response = api_instance.get_created_schemas(schema_id=schema_id, schema_issuer_did=schema_issuer_did, schema_name=schema_name, schema_version=schema_version)
        print("The response of SchemaApi->get_created_schemas:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SchemaApi->get_created_schemas: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **schema_id** | **str**| Schema identifier | [optional] 
 **schema_issuer_did** | **str**| Schema issuer DID | [optional] 
 **schema_name** | **str**| Schema name | [optional] 
 **schema_version** | **str**| Schema version | [optional] 

### Return type

[**SchemasCreatedResult**](SchemasCreatedResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_schema**
> SchemaGetResult get_schema(schema_id)

Gets a schema from the ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.SchemaApi(api_client)
    schema_id = 'schema_id_example' # str | Schema identifier

    try:
        # Gets a schema from the ledger
        api_response = api_instance.get_schema(schema_id)
        print("The response of SchemaApi->get_schema:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SchemaApi->get_schema: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **schema_id** | **str**| Schema identifier | 

### Return type

[**SchemaGetResult**](SchemaGetResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **publish_schema**
> TxnOrSchemaSendResult publish_schema(conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser, body=body)

Sends a schema to the ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.SchemaApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier (optional)
    create_transaction_for_endorser = True # bool | Create Transaction For Endorser's signature (optional)
    body = pyaries.SchemaSendRequest() # SchemaSendRequest |  (optional)

    try:
        # Sends a schema to the ledger
        api_response = api_instance.publish_schema(conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser, body=body)
        print("The response of SchemaApi->publish_schema:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SchemaApi->publish_schema: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | [optional] 
 **create_transaction_for_endorser** | **bool**| Create Transaction For Endorser&#39;s signature | [optional] 
 **body** | [**SchemaSendRequest**](SchemaSendRequest.md)|  | [optional] 

### Return type

[**TxnOrSchemaSendResult**](TxnOrSchemaSendResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **write_record**
> SchemaGetResult write_record(schema_id)

Writes a schema non-secret record to the wallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.SchemaApi(api_client)
    schema_id = 'schema_id_example' # str | Schema identifier

    try:
        # Writes a schema non-secret record to the wallet
        api_response = api_instance.write_record(schema_id)
        print("The response of SchemaApi->write_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SchemaApi->write_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **schema_id** | **str**| Schema identifier | 

### Return type

[**SchemaGetResult**](SchemaGetResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

