# V10PresentationExchangePresentationRequest

(Indy) presentation request (also known as proof request)

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Proof request name | [optional] 
**non_revoked** | [**IndyProofRequestNonRevoked**](IndyProofRequestNonRevoked.md) |  | [optional] 
**nonce** | **str** | Nonce | [optional] 
**requested_attributes** | [**Dict[str, IndyProofReqAttrSpec]**](IndyProofReqAttrSpec.md) | Requested attribute specifications of proof request | 
**requested_predicates** | [**Dict[str, IndyProofReqPredSpec]**](IndyProofReqPredSpec.md) | Requested predicate specifications of proof request | 
**version** | **str** | Proof request version | [optional] 

## Example

```python
from pyaries.models.v10_presentation_exchange_presentation_request import V10PresentationExchangePresentationRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationExchangePresentationRequest from a JSON string
v10_presentation_exchange_presentation_request_instance = V10PresentationExchangePresentationRequest.from_json(json)
# print the JSON string representation of the object
print V10PresentationExchangePresentationRequest.to_json()

# convert the object into a dict
v10_presentation_exchange_presentation_request_dict = v10_presentation_exchange_presentation_request_instance.to_dict()
# create an instance of V10PresentationExchangePresentationRequest from a dict
v10_presentation_exchange_presentation_request_form_dict = v10_presentation_exchange_presentation_request.from_dict(v10_presentation_exchange_presentation_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


