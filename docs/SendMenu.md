# SendMenu


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**menu** | [**MenuJson**](MenuJson.md) |  | 

## Example

```python
from pyaries.models.send_menu import SendMenu

# TODO update the JSON string below
json = "{}"
# create an instance of SendMenu from a JSON string
send_menu_instance = SendMenu.from_json(json)
# print the JSON string representation of the object
print SendMenu.to_json()

# convert the object into a dict
send_menu_dict = send_menu_instance.to_dict()
# create an instance of SendMenu from a dict
send_menu_form_dict = send_menu.from_dict(send_menu_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


