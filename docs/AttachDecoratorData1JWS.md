# AttachDecoratorData1JWS


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**AttachDecoratorDataJWSHeader**](AttachDecoratorDataJWSHeader.md) |  | 
**protected** | **str** | protected JWS header | [optional] 
**signature** | **str** | signature | 

## Example

```python
from pyaries.models.attach_decorator_data1_jws import AttachDecoratorData1JWS

# TODO update the JSON string below
json = "{}"
# create an instance of AttachDecoratorData1JWS from a JSON string
attach_decorator_data1_jws_instance = AttachDecoratorData1JWS.from_json(json)
# print the JSON string representation of the object
print AttachDecoratorData1JWS.to_json()

# convert the object into a dict
attach_decorator_data1_jws_dict = attach_decorator_data1_jws_instance.to_dict()
# create an instance of AttachDecoratorData1JWS from a dict
attach_decorator_data1_jws_form_dict = attach_decorator_data1_jws.from_dict(attach_decorator_data1_jws_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


