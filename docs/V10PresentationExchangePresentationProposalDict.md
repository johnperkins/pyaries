# V10PresentationExchangePresentationProposalDict

Presentation proposal message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**presentation_proposal** | [**IndyPresPreview**](IndyPresPreview.md) |  | 

## Example

```python
from pyaries.models.v10_presentation_exchange_presentation_proposal_dict import V10PresentationExchangePresentationProposalDict

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationExchangePresentationProposalDict from a JSON string
v10_presentation_exchange_presentation_proposal_dict_instance = V10PresentationExchangePresentationProposalDict.from_json(json)
# print the JSON string representation of the object
print V10PresentationExchangePresentationProposalDict.to_json()

# convert the object into a dict
v10_presentation_exchange_presentation_proposal_dict_dict = v10_presentation_exchange_presentation_proposal_dict_instance.to_dict()
# create an instance of V10PresentationExchangePresentationProposalDict from a dict
v10_presentation_exchange_presentation_proposal_dict_form_dict = v10_presentation_exchange_presentation_proposal_dict.from_dict(v10_presentation_exchange_presentation_proposal_dict_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


