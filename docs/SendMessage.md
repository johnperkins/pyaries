# SendMessage


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **str** | Message content | [optional] 

## Example

```python
from pyaries.models.send_message import SendMessage

# TODO update the JSON string below
json = "{}"
# create an instance of SendMessage from a JSON string
send_message_instance = SendMessage.from_json(json)
# print the JSON string representation of the object
print SendMessage.to_json()

# convert the object into a dict
send_message_dict = send_message_instance.to_dict()
# create an instance of SendMessage from a dict
send_message_form_dict = send_message.from_dict(send_message_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


