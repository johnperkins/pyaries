# ProtocolDescriptor


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pid** | **str** |  | 
**roles** | **List[str]** | List of roles | [optional] 

## Example

```python
from pyaries.models.protocol_descriptor import ProtocolDescriptor

# TODO update the JSON string below
json = "{}"
# create an instance of ProtocolDescriptor from a JSON string
protocol_descriptor_instance = ProtocolDescriptor.from_json(json)
# print the JSON string representation of the object
print ProtocolDescriptor.to_json()

# convert the object into a dict
protocol_descriptor_dict = protocol_descriptor_instance.to_dict()
# create an instance of ProtocolDescriptor from a dict
protocol_descriptor_form_dict = protocol_descriptor.from_dict(protocol_descriptor_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


