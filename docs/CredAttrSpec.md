# CredAttrSpec


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mime_type** | **str** | MIME type: omit for (null) default | [optional] 
**name** | **str** | Attribute name | 
**value** | **str** | Attribute value: base64-encode if MIME type is present | 

## Example

```python
from pyaries.models.cred_attr_spec import CredAttrSpec

# TODO update the JSON string below
json = "{}"
# create an instance of CredAttrSpec from a JSON string
cred_attr_spec_instance = CredAttrSpec.from_json(json)
# print the JSON string representation of the object
print CredAttrSpec.to_json()

# convert the object into a dict
cred_attr_spec_dict = cred_attr_spec_instance.to_dict()
# create an instance of CredAttrSpec from a dict
cred_attr_spec_form_dict = cred_attr_spec.from_dict(cred_attr_spec_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


