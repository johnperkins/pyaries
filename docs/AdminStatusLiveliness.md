# AdminStatusLiveliness


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alive** | **bool** | Liveliness status | [optional] 

## Example

```python
from pyaries.models.admin_status_liveliness import AdminStatusLiveliness

# TODO update the JSON string below
json = "{}"
# create an instance of AdminStatusLiveliness from a JSON string
admin_status_liveliness_instance = AdminStatusLiveliness.from_json(json)
# print the JSON string representation of the object
print AdminStatusLiveliness.to_json()

# convert the object into a dict
admin_status_liveliness_dict = admin_status_liveliness_instance.to_dict()
# create an instance of AdminStatusLiveliness from a dict
admin_status_liveliness_form_dict = admin_status_liveliness.from_dict(admin_status_liveliness_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


