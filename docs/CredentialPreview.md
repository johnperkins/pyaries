# CredentialPreview


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Message type identifier | [optional] 
**attributes** | [**List[CredAttrSpec]**](CredAttrSpec.md) |  | 

## Example

```python
from pyaries.models.credential_preview import CredentialPreview

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialPreview from a JSON string
credential_preview_instance = CredentialPreview.from_json(json)
# print the JSON string representation of the object
print CredentialPreview.to_json()

# convert the object into a dict
credential_preview_dict = credential_preview_instance.to_dict()
# create an instance of CredentialPreview from a dict
credential_preview_form_dict = credential_preview.from_dict(credential_preview_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


