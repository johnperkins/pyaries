# DIDXRequestDidDocAttach

As signed attachment, DID Doc associated with DID

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Attachment identifier | [optional] 
**byte_count** | **int** | Byte count of data included by reference | [optional] 
**data** | [**AttachDecoratorData**](AttachDecoratorData.md) |  | 
**description** | **str** | Human-readable description of content | [optional] 
**filename** | **str** | File name | [optional] 
**lastmod_time** | **str** | Hint regarding last modification datetime, in ISO-8601 format | [optional] 
**mime_type** | **str** | MIME type | [optional] 

## Example

```python
from pyaries.models.didx_request_did_doc_attach import DIDXRequestDidDocAttach

# TODO update the JSON string below
json = "{}"
# create an instance of DIDXRequestDidDocAttach from a JSON string
didx_request_did_doc_attach_instance = DIDXRequestDidDocAttach.from_json(json)
# print the JSON string representation of the object
print DIDXRequestDidDocAttach.to_json()

# convert the object into a dict
didx_request_did_doc_attach_dict = didx_request_did_doc_attach_instance.to_dict()
# create an instance of DIDXRequestDidDocAttach from a dict
didx_request_did_doc_attach_form_dict = didx_request_did_doc_attach.from_dict(didx_request_did_doc_attach_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


