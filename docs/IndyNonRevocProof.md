# IndyNonRevocProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**c_list** | **Dict[str, str]** |  | [optional] 
**x_list** | **Dict[str, str]** |  | [optional] 

## Example

```python
from pyaries.models.indy_non_revoc_proof import IndyNonRevocProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyNonRevocProof from a JSON string
indy_non_revoc_proof_instance = IndyNonRevocProof.from_json(json)
# print the JSON string representation of the object
print IndyNonRevocProof.to_json()

# convert the object into a dict
indy_non_revoc_proof_dict = indy_non_revoc_proof_instance.to_dict()
# create an instance of IndyNonRevocProof from a dict
indy_non_revoc_proof_form_dict = indy_non_revoc_proof.from_dict(indy_non_revoc_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


