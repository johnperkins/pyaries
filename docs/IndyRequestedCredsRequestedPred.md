# IndyRequestedCredsRequestedPred


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_id** | **str** | Wallet credential identifier (typically but not necessarily a UUID) | 
**timestamp** | **int** | Epoch timestamp of interest for non-revocation proof | [optional] 

## Example

```python
from pyaries.models.indy_requested_creds_requested_pred import IndyRequestedCredsRequestedPred

# TODO update the JSON string below
json = "{}"
# create an instance of IndyRequestedCredsRequestedPred from a JSON string
indy_requested_creds_requested_pred_instance = IndyRequestedCredsRequestedPred.from_json(json)
# print the JSON string representation of the object
print IndyRequestedCredsRequestedPred.to_json()

# convert the object into a dict
indy_requested_creds_requested_pred_dict = indy_requested_creds_requested_pred_instance.to_dict()
# create an instance of IndyRequestedCredsRequestedPred from a dict
indy_requested_creds_requested_pred_form_dict = indy_requested_creds_requested_pred.from_dict(indy_requested_creds_requested_pred_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


