# IssuerRevRegRecordRevocRegEntry

Revocation registry entry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | [**IndyRevRegEntryValue**](IndyRevRegEntryValue.md) |  | [optional] 
**ver** | **str** | Version of revocation registry entry | [optional] 

## Example

```python
from pyaries.models.issuer_rev_reg_record_revoc_reg_entry import IssuerRevRegRecordRevocRegEntry

# TODO update the JSON string below
json = "{}"
# create an instance of IssuerRevRegRecordRevocRegEntry from a JSON string
issuer_rev_reg_record_revoc_reg_entry_instance = IssuerRevRegRecordRevocRegEntry.from_json(json)
# print the JSON string representation of the object
print IssuerRevRegRecordRevocRegEntry.to_json()

# convert the object into a dict
issuer_rev_reg_record_revoc_reg_entry_dict = issuer_rev_reg_record_revoc_reg_entry_instance.to_dict()
# create an instance of IssuerRevRegRecordRevocRegEntry from a dict
issuer_rev_reg_record_revoc_reg_entry_form_dict = issuer_rev_reg_record_revoc_reg_entry.from_dict(issuer_rev_reg_record_revoc_reg_entry_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


