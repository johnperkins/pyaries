# CredentialDefinitionGetResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credential_definition** | [**CredentialDefinition**](CredentialDefinition.md) |  | [optional] 

## Example

```python
from pyaries.models.credential_definition_get_result import CredentialDefinitionGetResult

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialDefinitionGetResult from a JSON string
credential_definition_get_result_instance = CredentialDefinitionGetResult.from_json(json)
# print the JSON string representation of the object
print CredentialDefinitionGetResult.to_json()

# convert the object into a dict
credential_definition_get_result_dict = credential_definition_get_result_instance.to_dict()
# create an instance of CredentialDefinitionGetResult from a dict
credential_definition_get_result_form_dict = credential_definition_get_result.from_dict(credential_definition_get_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


