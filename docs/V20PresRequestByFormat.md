# V20PresRequestByFormat


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dif** | [**DIFProofRequest**](DIFProofRequest.md) |  | [optional] 
**indy** | [**IndyProofRequest**](IndyProofRequest.md) |  | [optional] 

## Example

```python
from pyaries.models.v20_pres_request_by_format import V20PresRequestByFormat

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresRequestByFormat from a JSON string
v20_pres_request_by_format_instance = V20PresRequestByFormat.from_json(json)
# print the JSON string representation of the object
print V20PresRequestByFormat.to_json()

# convert the object into a dict
v20_pres_request_by_format_dict = v20_pres_request_by_format_instance.to_dict()
# create an instance of V20PresRequestByFormat from a dict
v20_pres_request_by_format_form_dict = v20_pres_request_by_format.from_dict(v20_pres_request_by_format_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


