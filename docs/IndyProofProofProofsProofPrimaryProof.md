# IndyProofProofProofsProofPrimaryProof

Indy primary proof

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eq_proof** | [**IndyPrimaryProofEqProof**](IndyPrimaryProofEqProof.md) |  | [optional] 
**ge_proofs** | [**List[IndyGEProof]**](IndyGEProof.md) | Indy GE proofs | [optional] 

## Example

```python
from pyaries.models.indy_proof_proof_proofs_proof_primary_proof import IndyProofProofProofsProofPrimaryProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofProofProofsProofPrimaryProof from a JSON string
indy_proof_proof_proofs_proof_primary_proof_instance = IndyProofProofProofsProofPrimaryProof.from_json(json)
# print the JSON string representation of the object
print IndyProofProofProofsProofPrimaryProof.to_json()

# convert the object into a dict
indy_proof_proof_proofs_proof_primary_proof_dict = indy_proof_proof_proofs_proof_primary_proof_instance.to_dict()
# create an instance of IndyProofProofProofsProofPrimaryProof from a dict
indy_proof_proof_proofs_proof_primary_proof_form_dict = indy_proof_proof_proofs_proof_primary_proof.from_dict(indy_proof_proof_proofs_proof_primary_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


