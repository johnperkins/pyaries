# IndyProofReqPredSpec


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Attribute name | 
**non_revoked** | [**IndyProofReqPredSpecNonRevoked**](IndyProofReqPredSpecNonRevoked.md) |  | [optional] 
**p_type** | **str** | Predicate type (&#39;&lt;&#39;, &#39;&lt;&#x3D;&#39;, &#39;&gt;&#x3D;&#39;, or &#39;&gt;&#39;) | 
**p_value** | **int** | Threshold value | 
**restrictions** | **List[Dict[str, str]]** | If present, credential must satisfy one of given restrictions: specify schema_id, schema_issuer_did, schema_name, schema_version, issuer_did, cred_def_id, and/or attr::&lt;attribute-name&gt;::value where &lt;attribute-name&gt; represents a credential attribute name | [optional] 

## Example

```python
from pyaries.models.indy_proof_req_pred_spec import IndyProofReqPredSpec

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofReqPredSpec from a JSON string
indy_proof_req_pred_spec_instance = IndyProofReqPredSpec.from_json(json)
# print the JSON string representation of the object
print IndyProofReqPredSpec.to_json()

# convert the object into a dict
indy_proof_req_pred_spec_dict = indy_proof_req_pred_spec_instance.to_dict()
# create an instance of IndyProofReqPredSpec from a dict
indy_proof_req_pred_spec_form_dict = indy_proof_req_pred_spec.from_dict(indy_proof_req_pred_spec_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


