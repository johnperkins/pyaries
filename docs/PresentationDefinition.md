# PresentationDefinition


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | [**ClaimFormat**](ClaimFormat.md) |  | [optional] 
**id** | **str** | Unique Resource Identifier | [optional] 
**input_descriptors** | [**List[InputDescriptors]**](InputDescriptors.md) |  | [optional] 
**name** | **str** | Human-friendly name that describes what the presentation definition pertains to | [optional] 
**purpose** | **str** | Describes the purpose for which the Presentation Definition&#39;s inputs are being requested | [optional] 
**submission_requirements** | [**List[SubmissionRequirements]**](SubmissionRequirements.md) |  | [optional] 

## Example

```python
from pyaries.models.presentation_definition import PresentationDefinition

# TODO update the JSON string below
json = "{}"
# create an instance of PresentationDefinition from a JSON string
presentation_definition_instance = PresentationDefinition.from_json(json)
# print the JSON string representation of the object
print PresentationDefinition.to_json()

# convert the object into a dict
presentation_definition_dict = presentation_definition_instance.to_dict()
# create an instance of PresentationDefinition from a dict
presentation_definition_form_dict = presentation_definition.from_dict(presentation_definition_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


