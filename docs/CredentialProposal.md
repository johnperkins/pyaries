# CredentialProposal


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**cred_def_id** | **str** |  | [optional] 
**credential_proposal** | [**CredentialPreview**](CredentialPreview.md) |  | [optional] 
**issuer_did** | **str** |  | [optional] 
**schema_id** | **str** |  | [optional] 
**schema_issuer_did** | **str** |  | [optional] 
**schema_name** | **str** |  | [optional] 
**schema_version** | **str** |  | [optional] 

## Example

```python
from pyaries.models.credential_proposal import CredentialProposal

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialProposal from a JSON string
credential_proposal_instance = CredentialProposal.from_json(json)
# print the JSON string representation of the object
print CredentialProposal.to_json()

# convert the object into a dict
credential_proposal_dict = credential_proposal_instance.to_dict()
# create an instance of CredentialProposal from a dict
credential_proposal_form_dict = credential_proposal.from_dict(credential_proposal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


