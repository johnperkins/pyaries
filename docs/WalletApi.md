# pyaries.WalletApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_did**](WalletApi.md#create_did) | **POST** /wallet/did/create | Create a local DID
[**get_did_endpoint**](WalletApi.md#get_did_endpoint) | **GET** /wallet/get-did-endpoint | Query DID endpoint in wallet
[**get_dids**](WalletApi.md#get_dids) | **GET** /wallet/did | List wallet DIDs
[**get_public_did**](WalletApi.md#get_public_did) | **GET** /wallet/did/public | Fetch the current public DID
[**rotate_keypair**](WalletApi.md#rotate_keypair) | **PATCH** /wallet/did/local/rotate-keypair | Rotate keypair for a DID not posted to the ledger
[**set_did_endpoint**](WalletApi.md#set_did_endpoint) | **POST** /wallet/set-did-endpoint | Update endpoint in wallet and on ledger if posted to it
[**set_public_did**](WalletApi.md#set_public_did) | **POST** /wallet/did/public | Assign the current public DID


# **create_did**
> DIDResult create_did(body=body)

Create a local DID

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.WalletApi(api_client)
    body = pyaries.DIDCreate() # DIDCreate |  (optional)

    try:
        # Create a local DID
        api_response = api_instance.create_did(body=body)
        print("The response of WalletApi->create_did:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling WalletApi->create_did: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DIDCreate**](DIDCreate.md)|  | [optional] 

### Return type

[**DIDResult**](DIDResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_did_endpoint**
> DIDEndpoint get_did_endpoint(did)

Query DID endpoint in wallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.WalletApi(api_client)
    did = 'did_example' # str | DID of interest

    try:
        # Query DID endpoint in wallet
        api_response = api_instance.get_did_endpoint(did)
        print("The response of WalletApi->get_did_endpoint:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling WalletApi->get_did_endpoint: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **str**| DID of interest | 

### Return type

[**DIDEndpoint**](DIDEndpoint.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_dids**
> DIDList get_dids(did=did, key_type=key_type, method=method, posture=posture, verkey=verkey)

List wallet DIDs

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.WalletApi(api_client)
    did = 'did_example' # str | DID of interest (optional)
    key_type = 'key_type_example' # str | Key type to query for. (optional)
    method = 'method_example' # str | DID method to query for. e.g. sov to only fetch indy/sov DIDs (optional)
    posture = 'posture_example' # str | Whether DID is current public DID, posted to ledger but current public DID, or local to the wallet (optional)
    verkey = 'verkey_example' # str | Verification key of interest (optional)

    try:
        # List wallet DIDs
        api_response = api_instance.get_dids(did=did, key_type=key_type, method=method, posture=posture, verkey=verkey)
        print("The response of WalletApi->get_dids:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling WalletApi->get_dids: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **str**| DID of interest | [optional] 
 **key_type** | **str**| Key type to query for. | [optional] 
 **method** | **str**| DID method to query for. e.g. sov to only fetch indy/sov DIDs | [optional] 
 **posture** | **str**| Whether DID is current public DID, posted to ledger but current public DID, or local to the wallet | [optional] 
 **verkey** | **str**| Verification key of interest | [optional] 

### Return type

[**DIDList**](DIDList.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_public_did**
> DIDResult get_public_did()

Fetch the current public DID

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.WalletApi(api_client)

    try:
        # Fetch the current public DID
        api_response = api_instance.get_public_did()
        print("The response of WalletApi->get_public_did:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling WalletApi->get_public_did: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DIDResult**](DIDResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rotate_keypair**
> object rotate_keypair(did)

Rotate keypair for a DID not posted to the ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.WalletApi(api_client)
    did = 'did_example' # str | DID of interest

    try:
        # Rotate keypair for a DID not posted to the ledger
        api_response = api_instance.rotate_keypair(did)
        print("The response of WalletApi->rotate_keypair:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling WalletApi->rotate_keypair: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **str**| DID of interest | 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_did_endpoint**
> object set_did_endpoint(conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser, body=body)

Update endpoint in wallet and on ledger if posted to it

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.WalletApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier (optional)
    create_transaction_for_endorser = True # bool | Create Transaction For Endorser's signature (optional)
    body = pyaries.DIDEndpointWithType() # DIDEndpointWithType |  (optional)

    try:
        # Update endpoint in wallet and on ledger if posted to it
        api_response = api_instance.set_did_endpoint(conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser, body=body)
        print("The response of WalletApi->set_did_endpoint:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling WalletApi->set_did_endpoint: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | [optional] 
 **create_transaction_for_endorser** | **bool**| Create Transaction For Endorser&#39;s signature | [optional] 
 **body** | [**DIDEndpointWithType**](DIDEndpointWithType.md)|  | [optional] 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_public_did**
> DIDResult set_public_did(did, conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser)

Assign the current public DID

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.WalletApi(api_client)
    did = 'did_example' # str | DID of interest
    conn_id = 'conn_id_example' # str | Connection identifier (optional)
    create_transaction_for_endorser = True # bool | Create Transaction For Endorser's signature (optional)

    try:
        # Assign the current public DID
        api_response = api_instance.set_public_did(did, conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser)
        print("The response of WalletApi->set_public_did:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling WalletApi->set_public_did: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **str**| DID of interest | 
 **conn_id** | **str**| Connection identifier | [optional] 
 **create_transaction_for_endorser** | **bool**| Create Transaction For Endorser&#39;s signature | [optional] 

### Return type

[**DIDResult**](DIDResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

