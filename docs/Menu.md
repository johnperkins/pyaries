# Menu


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**description** | **str** | Introductory text for the menu | [optional] 
**errormsg** | **str** | An optional error message to display in menu header | [optional] 
**options** | [**List[MenuOption]**](MenuOption.md) | List of menu options | 
**title** | **str** | Menu title | [optional] 

## Example

```python
from pyaries.models.menu import Menu

# TODO update the JSON string below
json = "{}"
# create an instance of Menu from a JSON string
menu_instance = Menu.from_json(json)
# print the JSON string representation of the object
print Menu.to_json()

# convert the object into a dict
menu_dict = menu_instance.to_dict()
# create an instance of Menu from a dict
menu_form_dict = menu.from_dict(menu_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


