# QueryItem


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**feature_type** | **str** | feature type | 
**match** | **str** | match | 

## Example

```python
from pyaries.models.query_item import QueryItem

# TODO update the JSON string below
json = "{}"
# create an instance of QueryItem from a JSON string
query_item_instance = QueryItem.from_json(json)
# print the JSON string representation of the object
print QueryItem.to_json()

# convert the object into a dict
query_item_dict = query_item_instance.to_dict()
# create an instance of QueryItem from a dict
query_item_form_dict = query_item.from_dict(query_item_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


