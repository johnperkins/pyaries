# CredRevokedResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revoked** | **bool** | Whether credential is revoked on the ledger | [optional] 

## Example

```python
from pyaries.models.cred_revoked_result import CredRevokedResult

# TODO update the JSON string below
json = "{}"
# create an instance of CredRevokedResult from a JSON string
cred_revoked_result_instance = CredRevokedResult.from_json(json)
# print the JSON string representation of the object
print CredRevokedResult.to_json()

# convert the object into a dict
cred_revoked_result_dict = cred_revoked_result_instance.to_dict()
# create an instance of CredRevokedResult from a dict
cred_revoked_result_form_dict = cred_revoked_result.from_dict(cred_revoked_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


