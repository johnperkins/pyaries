# ClearPendingRevocationsRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purge** | **Dict[str, List[str]]** | Credential revocation ids by revocation registry id: omit for all, specify null or empty list for all pending per revocation registry | [optional] 

## Example

```python
from pyaries.models.clear_pending_revocations_request import ClearPendingRevocationsRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ClearPendingRevocationsRequest from a JSON string
clear_pending_revocations_request_instance = ClearPendingRevocationsRequest.from_json(json)
# print the JSON string representation of the object
print ClearPendingRevocationsRequest.to_json()

# convert the object into a dict
clear_pending_revocations_request_dict = clear_pending_revocations_request_instance.to_dict()
# create an instance of ClearPendingRevocationsRequest from a dict
clear_pending_revocations_request_form_dict = clear_pending_revocations_request.from_dict(clear_pending_revocations_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


