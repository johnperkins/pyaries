# CredDefValueRevocation


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**g** | **str** |  | [optional] 
**g_dash** | **str** |  | [optional] 
**h** | **str** |  | [optional] 
**h0** | **str** |  | [optional] 
**h1** | **str** |  | [optional] 
**h2** | **str** |  | [optional] 
**h_cap** | **str** |  | [optional] 
**htilde** | **str** |  | [optional] 
**pk** | **str** |  | [optional] 
**u** | **str** |  | [optional] 
**y** | **str** |  | [optional] 

## Example

```python
from pyaries.models.cred_def_value_revocation import CredDefValueRevocation

# TODO update the JSON string below
json = "{}"
# create an instance of CredDefValueRevocation from a JSON string
cred_def_value_revocation_instance = CredDefValueRevocation.from_json(json)
# print the JSON string representation of the object
print CredDefValueRevocation.to_json()

# convert the object into a dict
cred_def_value_revocation_dict = cred_def_value_revocation_instance.to_dict()
# create an instance of CredDefValueRevocation from a dict
cred_def_value_revocation_form_dict = cred_def_value_revocation.from_dict(cred_def_value_revocation_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


