# IndyRevRegEntry


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | [**IndyRevRegEntryValue**](IndyRevRegEntryValue.md) |  | [optional] 
**ver** | **str** | Version of revocation registry entry | [optional] 

## Example

```python
from pyaries.models.indy_rev_reg_entry import IndyRevRegEntry

# TODO update the JSON string below
json = "{}"
# create an instance of IndyRevRegEntry from a JSON string
indy_rev_reg_entry_instance = IndyRevRegEntry.from_json(json)
# print the JSON string representation of the object
print IndyRevRegEntry.to_json()

# convert the object into a dict
indy_rev_reg_entry_dict = indy_rev_reg_entry_instance.to_dict()
# create an instance of IndyRevRegEntry from a dict
indy_rev_reg_entry_form_dict = indy_rev_reg_entry.from_dict(indy_rev_reg_entry_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


