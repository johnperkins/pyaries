# CreateWalletTokenRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wallet_key** | **str** | Master key used for key derivation. Only required for             unamanged wallets. | [optional] 

## Example

```python
from pyaries.models.create_wallet_token_request import CreateWalletTokenRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CreateWalletTokenRequest from a JSON string
create_wallet_token_request_instance = CreateWalletTokenRequest.from_json(json)
# print the JSON string representation of the object
print CreateWalletTokenRequest.to_json()

# convert the object into a dict
create_wallet_token_request_dict = create_wallet_token_request_instance.to_dict()
# create an instance of CreateWalletTokenRequest from a dict
create_wallet_token_request_form_dict = create_wallet_token_request.from_dict(create_wallet_token_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


