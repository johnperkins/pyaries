# V20CredFilterLDProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ld_proof** | [**LDProofVCDetail**](LDProofVCDetail.md) |  | 

## Example

```python
from pyaries.models.v20_cred_filter_ld_proof import V20CredFilterLDProof

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredFilterLDProof from a JSON string
v20_cred_filter_ld_proof_instance = V20CredFilterLDProof.from_json(json)
# print the JSON string representation of the object
print V20CredFilterLDProof.to_json()

# convert the object into a dict
v20_cred_filter_ld_proof_dict = v20_cred_filter_ld_proof_instance.to_dict()
# create an instance of V20CredFilterLDProof from a dict
v20_cred_filter_ld_proof_form_dict = v20_cred_filter_ld_proof.from_dict(v20_cred_filter_ld_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


