# pyaries.JsonldApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sign**](JsonldApi.md#sign) | **POST** /jsonld/sign | Sign a JSON-LD structure and return it
[**verify**](JsonldApi.md#verify) | **POST** /jsonld/verify | Verify a JSON-LD structure.


# **sign**
> SignResponse sign(body=body)

Sign a JSON-LD structure and return it

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.JsonldApi(api_client)
    body = pyaries.SignRequest() # SignRequest |  (optional)

    try:
        # Sign a JSON-LD structure and return it
        api_response = api_instance.sign(body=body)
        print("The response of JsonldApi->sign:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JsonldApi->sign: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**SignRequest**](SignRequest.md)|  | [optional] 

### Return type

[**SignResponse**](SignResponse.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify**
> VerifyResponse verify(body=body)

Verify a JSON-LD structure.

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.JsonldApi(api_client)
    body = pyaries.VerifyRequest() # VerifyRequest |  (optional)

    try:
        # Verify a JSON-LD structure.
        api_response = api_instance.verify(body=body)
        print("The response of JsonldApi->verify:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling JsonldApi->verify: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**VerifyRequest**](VerifyRequest.md)|  | [optional] 

### Return type

[**VerifyResponse**](VerifyResponse.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

