# pyaries.ResolverApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_did**](ResolverApi.md#get_did) | **GET** /resolver/resolve/{did} | Retrieve doc for requested did


# **get_did**
> ResolutionResult get_did(did)

Retrieve doc for requested did

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ResolverApi(api_client)
    did = 'did_example' # str | DID

    try:
        # Retrieve doc for requested did
        api_response = api_instance.get_did(did)
        print("The response of ResolverApi->get_did:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ResolverApi->get_did: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **str**| DID | 

### Return type

[**ResolutionResult**](ResolutionResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

