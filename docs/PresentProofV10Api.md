# pyaries.PresentProofV10Api

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_proof_request**](PresentProofV10Api.md#create_proof_request) | **POST** /present-proof/create-request | Creates a presentation request not bound to any proposal or connection
[**delete_record**](PresentProofV10Api.md#delete_record) | **DELETE** /present-proof/records/{pres_ex_id} | Remove an existing presentation exchange record
[**get_matching_credentials**](PresentProofV10Api.md#get_matching_credentials) | **GET** /present-proof/records/{pres_ex_id}/credentials | Fetch credentials for a presentation request from wallet
[**get_record**](PresentProofV10Api.md#get_record) | **GET** /present-proof/records/{pres_ex_id} | Fetch a single presentation exchange record
[**get_records**](PresentProofV10Api.md#get_records) | **GET** /present-proof/records | Fetch all present-proof exchange records
[**report_problem**](PresentProofV10Api.md#report_problem) | **POST** /present-proof/records/{pres_ex_id}/problem-report | Send a problem report for presentation exchange
[**send_presentation**](PresentProofV10Api.md#send_presentation) | **POST** /present-proof/records/{pres_ex_id}/send-presentation | Sends a proof presentation
[**send_proposal**](PresentProofV10Api.md#send_proposal) | **POST** /present-proof/send-proposal | Sends a presentation proposal
[**send_request**](PresentProofV10Api.md#send_request) | **POST** /present-proof/records/{pres_ex_id}/send-request | Sends a presentation request in reference to a proposal
[**send_request_free**](PresentProofV10Api.md#send_request_free) | **POST** /present-proof/send-request | Sends a free presentation request not bound to any proposal
[**verify_presentation**](PresentProofV10Api.md#verify_presentation) | **POST** /present-proof/records/{pres_ex_id}/verify-presentation | Verify a received presentation


# **create_proof_request**
> V10PresentationExchange create_proof_request(body=body)

Creates a presentation request not bound to any proposal or connection

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    body = pyaries.V10PresentationCreateRequestRequest() # V10PresentationCreateRequestRequest |  (optional)

    try:
        # Creates a presentation request not bound to any proposal or connection
        api_response = api_instance.create_proof_request(body=body)
        print("The response of PresentProofV10Api->create_proof_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->create_proof_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V10PresentationCreateRequestRequest**](V10PresentationCreateRequestRequest.md)|  | [optional] 

### Return type

[**V10PresentationExchange**](V10PresentationExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_record**
> object delete_record(pres_ex_id)

Remove an existing presentation exchange record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier

    try:
        # Remove an existing presentation exchange record
        api_response = api_instance.delete_record(pres_ex_id)
        print("The response of PresentProofV10Api->delete_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->delete_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_matching_credentials**
> List[IndyCredPrecis] get_matching_credentials(pres_ex_id, count=count, extra_query=extra_query, referent=referent, start=start)

Fetch credentials for a presentation request from wallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier
    count = 'count_example' # str | Maximum number to retrieve (optional)
    extra_query = 'extra_query_example' # str | (JSON) object mapping referents to extra WQL queries (optional)
    referent = 'referent_example' # str | Proof request referents of interest, comma-separated (optional)
    start = 'start_example' # str | Start index (optional)

    try:
        # Fetch credentials for a presentation request from wallet
        api_response = api_instance.get_matching_credentials(pres_ex_id, count=count, extra_query=extra_query, referent=referent, start=start)
        print("The response of PresentProofV10Api->get_matching_credentials:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->get_matching_credentials: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 
 **count** | **str**| Maximum number to retrieve | [optional] 
 **extra_query** | **str**| (JSON) object mapping referents to extra WQL queries | [optional] 
 **referent** | **str**| Proof request referents of interest, comma-separated | [optional] 
 **start** | **str**| Start index | [optional] 

### Return type

[**List[IndyCredPrecis]**](IndyCredPrecis.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_record**
> V10PresentationExchange get_record(pres_ex_id)

Fetch a single presentation exchange record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier

    try:
        # Fetch a single presentation exchange record
        api_response = api_instance.get_record(pres_ex_id)
        print("The response of PresentProofV10Api->get_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->get_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 

### Return type

[**V10PresentationExchange**](V10PresentationExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_records**
> V10PresentationExchangeList get_records(connection_id=connection_id, role=role, state=state, thread_id=thread_id)

Fetch all present-proof exchange records

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    connection_id = 'connection_id_example' # str | Connection identifier (optional)
    role = 'role_example' # str | Role assigned in presentation exchange (optional)
    state = 'state_example' # str | Presentation exchange state (optional)
    thread_id = 'thread_id_example' # str | Thread identifier (optional)

    try:
        # Fetch all present-proof exchange records
        api_response = api_instance.get_records(connection_id=connection_id, role=role, state=state, thread_id=thread_id)
        print("The response of PresentProofV10Api->get_records:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->get_records: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connection_id** | **str**| Connection identifier | [optional] 
 **role** | **str**| Role assigned in presentation exchange | [optional] 
 **state** | **str**| Presentation exchange state | [optional] 
 **thread_id** | **str**| Thread identifier | [optional] 

### Return type

[**V10PresentationExchangeList**](V10PresentationExchangeList.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **report_problem**
> object report_problem(pres_ex_id, body=body)

Send a problem report for presentation exchange

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier
    body = pyaries.V10PresentationProblemReportRequest() # V10PresentationProblemReportRequest |  (optional)

    try:
        # Send a problem report for presentation exchange
        api_response = api_instance.report_problem(pres_ex_id, body=body)
        print("The response of PresentProofV10Api->report_problem:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->report_problem: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 
 **body** | [**V10PresentationProblemReportRequest**](V10PresentationProblemReportRequest.md)|  | [optional] 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_presentation**
> V10PresentationExchange send_presentation(pres_ex_id, body=body)

Sends a proof presentation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier
    body = pyaries.IndyPresSpec() # IndyPresSpec |  (optional)

    try:
        # Sends a proof presentation
        api_response = api_instance.send_presentation(pres_ex_id, body=body)
        print("The response of PresentProofV10Api->send_presentation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->send_presentation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 
 **body** | [**IndyPresSpec**](IndyPresSpec.md)|  | [optional] 

### Return type

[**V10PresentationExchange**](V10PresentationExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_proposal**
> V10PresentationExchange send_proposal(body=body)

Sends a presentation proposal

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    body = pyaries.V10PresentationProposalRequest() # V10PresentationProposalRequest |  (optional)

    try:
        # Sends a presentation proposal
        api_response = api_instance.send_proposal(body=body)
        print("The response of PresentProofV10Api->send_proposal:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->send_proposal: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V10PresentationProposalRequest**](V10PresentationProposalRequest.md)|  | [optional] 

### Return type

[**V10PresentationExchange**](V10PresentationExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_request**
> V10PresentationExchange send_request(pres_ex_id, body=body)

Sends a presentation request in reference to a proposal

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier
    body = pyaries.V10PresentationSendRequestToProposal() # V10PresentationSendRequestToProposal |  (optional)

    try:
        # Sends a presentation request in reference to a proposal
        api_response = api_instance.send_request(pres_ex_id, body=body)
        print("The response of PresentProofV10Api->send_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->send_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 
 **body** | [**V10PresentationSendRequestToProposal**](V10PresentationSendRequestToProposal.md)|  | [optional] 

### Return type

[**V10PresentationExchange**](V10PresentationExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_request_free**
> V10PresentationExchange send_request_free(body=body)

Sends a free presentation request not bound to any proposal

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    body = pyaries.V10PresentationSendRequestRequest() # V10PresentationSendRequestRequest |  (optional)

    try:
        # Sends a free presentation request not bound to any proposal
        api_response = api_instance.send_request_free(body=body)
        print("The response of PresentProofV10Api->send_request_free:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->send_request_free: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V10PresentationSendRequestRequest**](V10PresentationSendRequestRequest.md)|  | [optional] 

### Return type

[**V10PresentationExchange**](V10PresentationExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_presentation**
> V10PresentationExchange verify_presentation(pres_ex_id)

Verify a received presentation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV10Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier

    try:
        # Verify a received presentation
        api_response = api_instance.verify_presentation(pres_ex_id)
        print("The response of PresentProofV10Api->verify_presentation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV10Api->verify_presentation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 

### Return type

[**V10PresentationExchange**](V10PresentationExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

