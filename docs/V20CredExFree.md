# V20CredExFree


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_remove** | **bool** | Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) | [optional] 
**comment** | **str** | Human-readable comment | [optional] 
**connection_id** | **str** | Connection identifier | 
**credential_preview** | [**V20CredPreview**](V20CredPreview.md) |  | [optional] 
**filter** | [**V20CredFilter**](V20CredFilter.md) |  | 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 

## Example

```python
from pyaries.models.v20_cred_ex_free import V20CredExFree

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExFree from a JSON string
v20_cred_ex_free_instance = V20CredExFree.from_json(json)
# print the JSON string representation of the object
print V20CredExFree.to_json()

# convert the object into a dict
v20_cred_ex_free_dict = v20_cred_ex_free_instance.to_dict()
# create an instance of V20CredExFree from a dict
v20_cred_ex_free_form_dict = v20_cred_ex_free.from_dict(v20_cred_ex_free_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


