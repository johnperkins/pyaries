# SubmissionRequirements


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | Count Value | [optional] 
**var_from** | **str** | From | [optional] 
**from_nested** | [**List[SubmissionRequirements]**](SubmissionRequirements.md) |  | [optional] 
**max** | **int** | Max Value | [optional] 
**min** | **int** | Min Value | [optional] 
**name** | **str** | Name | [optional] 
**purpose** | **str** | Purpose | [optional] 
**rule** | **str** | Selection | [optional] 

## Example

```python
from pyaries.models.submission_requirements import SubmissionRequirements

# TODO update the JSON string below
json = "{}"
# create an instance of SubmissionRequirements from a JSON string
submission_requirements_instance = SubmissionRequirements.from_json(json)
# print the JSON string representation of the object
print SubmissionRequirements.to_json()

# convert the object into a dict
submission_requirements_dict = submission_requirements_instance.to_dict()
# create an instance of SubmissionRequirements from a dict
submission_requirements_form_dict = submission_requirements.from_dict(submission_requirements_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


