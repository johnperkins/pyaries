# SignatureOptions


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**challenge** | **str** |  | [optional] 
**domain** | **str** |  | [optional] 
**proof_purpose** | **str** |  | 
**type** | **str** |  | [optional] 
**verification_method** | **str** |  | 

## Example

```python
from pyaries.models.signature_options import SignatureOptions

# TODO update the JSON string below
json = "{}"
# create an instance of SignatureOptions from a JSON string
signature_options_instance = SignatureOptions.from_json(json)
# print the JSON string representation of the object
print SignatureOptions.to_json()

# convert the object into a dict
signature_options_dict = signature_options_instance.to_dict()
# create an instance of SignatureOptions from a dict
signature_options_form_dict = signature_options.from_dict(signature_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


