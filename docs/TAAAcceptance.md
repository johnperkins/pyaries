# TAAAcceptance


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mechanism** | **str** |  | [optional] 
**time** | **int** |  | [optional] 

## Example

```python
from pyaries.models.taa_acceptance import TAAAcceptance

# TODO update the JSON string below
json = "{}"
# create an instance of TAAAcceptance from a JSON string
taa_acceptance_instance = TAAAcceptance.from_json(json)
# print the JSON string representation of the object
print TAAAcceptance.to_json()

# convert the object into a dict
taa_acceptance_dict = taa_acceptance_instance.to_dict()
# create an instance of TAAAcceptance from a dict
taa_acceptance_form_dict = taa_acceptance.from_dict(taa_acceptance_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


