# IssuerRevRegRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | Time of record creation | [optional] 
**cred_def_id** | **str** | Credential definition identifier | [optional] 
**error_msg** | **str** | Error message | [optional] 
**issuer_did** | **str** | Issuer DID | [optional] 
**max_cred_num** | **int** | Maximum number of credentials for revocation registry | [optional] 
**pending_pub** | **List[str]** | Credential revocation identifier for credential revoked and pending publication to ledger | [optional] 
**record_id** | **str** | Issuer revocation registry record identifier | [optional] 
**revoc_def_type** | **str** | Revocation registry type (specify CL_ACCUM) | [optional] 
**revoc_reg_def** | [**IndyRevRegDef**](IndyRevRegDef.md) |  | [optional] 
**revoc_reg_entry** | [**IndyRevRegEntry**](IndyRevRegEntry.md) |  | [optional] 
**revoc_reg_id** | **str** | Revocation registry identifier | [optional] 
**state** | **str** | Issue revocation registry record state | [optional] 
**tag** | **str** | Tag within issuer revocation registry identifier | [optional] 
**tails_hash** | **str** | Tails hash | [optional] 
**tails_local_path** | **str** | Local path to tails file | [optional] 
**tails_public_uri** | **str** | Public URI for tails file | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.issuer_rev_reg_record import IssuerRevRegRecord

# TODO update the JSON string below
json = "{}"
# create an instance of IssuerRevRegRecord from a JSON string
issuer_rev_reg_record_instance = IssuerRevRegRecord.from_json(json)
# print the JSON string representation of the object
print IssuerRevRegRecord.to_json()

# convert the object into a dict
issuer_rev_reg_record_dict = issuer_rev_reg_record_instance.to_dict()
# create an instance of IssuerRevRegRecord from a dict
issuer_rev_reg_record_form_dict = issuer_rev_reg_record.from_dict(issuer_rev_reg_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


