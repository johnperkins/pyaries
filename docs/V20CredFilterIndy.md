# V20CredFilterIndy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_def_id** | **str** | Credential definition identifier | [optional] 
**issuer_did** | **str** | Credential issuer DID | [optional] 
**schema_id** | **str** | Schema identifier | [optional] 
**schema_issuer_did** | **str** | Schema issuer DID | [optional] 
**schema_name** | **str** | Schema name | [optional] 
**schema_version** | **str** | Schema version | [optional] 

## Example

```python
from pyaries.models.v20_cred_filter_indy import V20CredFilterIndy

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredFilterIndy from a JSON string
v20_cred_filter_indy_instance = V20CredFilterIndy.from_json(json)
# print the JSON string representation of the object
print V20CredFilterIndy.to_json()

# convert the object into a dict
v20_cred_filter_indy_dict = v20_cred_filter_indy_instance.to_dict()
# create an instance of V20CredFilterIndy from a dict
v20_cred_filter_indy_form_dict = v20_cred_filter_indy.from_dict(v20_cred_filter_indy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


