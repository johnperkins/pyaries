# GetDIDEndpointResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**endpoint** | **str** | Full verification key | [optional] 

## Example

```python
from pyaries.models.get_did_endpoint_response import GetDIDEndpointResponse

# TODO update the JSON string below
json = "{}"
# create an instance of GetDIDEndpointResponse from a JSON string
get_did_endpoint_response_instance = GetDIDEndpointResponse.from_json(json)
# print the JSON string representation of the object
print GetDIDEndpointResponse.to_json()

# convert the object into a dict
get_did_endpoint_response_dict = get_did_endpoint_response_instance.to_dict()
# create an instance of GetDIDEndpointResponse from a dict
get_did_endpoint_response_form_dict = get_did_endpoint_response.from_dict(get_did_endpoint_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


