# V20PresExRecordPresProposal

Presentation proposal message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**formats** | [**List[V20PresFormat]**](V20PresFormat.md) |  | 
**proposalsattach** | [**List[AttachDecorator]**](AttachDecorator.md) | Attachment per acceptable format on corresponding identifier | 

## Example

```python
from pyaries.models.v20_pres_ex_record_pres_proposal import V20PresExRecordPresProposal

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresExRecordPresProposal from a JSON string
v20_pres_ex_record_pres_proposal_instance = V20PresExRecordPresProposal.from_json(json)
# print the JSON string representation of the object
print V20PresExRecordPresProposal.to_json()

# convert the object into a dict
v20_pres_ex_record_pres_proposal_dict = v20_pres_ex_record_pres_proposal_instance.to_dict()
# create an instance of V20PresExRecordPresProposal from a dict
v20_pres_ex_record_pres_proposal_form_dict = v20_pres_ex_record_pres_proposal.from_dict(v20_pres_ex_record_pres_proposal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


