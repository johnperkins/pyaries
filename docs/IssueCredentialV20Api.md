# pyaries.IssueCredentialV20Api

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_credential**](IssueCredentialV20Api.md#create_credential) | **POST** /issue-credential-2.0/create | Create credential from attribute values
[**delete_record**](IssueCredentialV20Api.md#delete_record) | **DELETE** /issue-credential-2.0/records/{cred_ex_id} | Remove an existing credential exchange record
[**get_record**](IssueCredentialV20Api.md#get_record) | **GET** /issue-credential-2.0/records/{cred_ex_id} | Fetch a single credential exchange record
[**get_records**](IssueCredentialV20Api.md#get_records) | **GET** /issue-credential-2.0/records | Fetch all credential exchange records
[**issue_credential**](IssueCredentialV20Api.md#issue_credential) | **POST** /issue-credential-2.0/records/{cred_ex_id}/issue | Send holder a credential
[**issue_credential20_create_offer_post**](IssueCredentialV20Api.md#issue_credential20_create_offer_post) | **POST** /issue-credential-2.0/create-offer | Create a credential offer, independent of any proposal or connection
[**issue_credential_automated**](IssueCredentialV20Api.md#issue_credential_automated) | **POST** /issue-credential-2.0/send | Send holder a credential, automating entire flow
[**report_problem**](IssueCredentialV20Api.md#report_problem) | **POST** /issue-credential-2.0/records/{cred_ex_id}/problem-report | Send a problem report for credential exchange
[**send_offer**](IssueCredentialV20Api.md#send_offer) | **POST** /issue-credential-2.0/records/{cred_ex_id}/send-offer | Send holder a credential offer in reference to a proposal with preview
[**send_offer_free**](IssueCredentialV20Api.md#send_offer_free) | **POST** /issue-credential-2.0/send-offer | Send holder a credential offer, independent of any proposal
[**send_proposal**](IssueCredentialV20Api.md#send_proposal) | **POST** /issue-credential-2.0/send-proposal | Send issuer a credential proposal
[**send_request**](IssueCredentialV20Api.md#send_request) | **POST** /issue-credential-2.0/records/{cred_ex_id}/send-request | Send issuer a credential request
[**send_request_free**](IssueCredentialV20Api.md#send_request_free) | **POST** /issue-credential-2.0/send-request | Send issuer a credential request not bound to an existing thread. Indy credentials cannot start at a request
[**store_credential**](IssueCredentialV20Api.md#store_credential) | **POST** /issue-credential-2.0/records/{cred_ex_id}/store | Store a received credential


# **create_credential**
> V20CredExRecord create_credential(body=body)

Create credential from attribute values

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    body = pyaries.V20IssueCredSchemaCore() # V20IssueCredSchemaCore |  (optional)

    try:
        # Create credential from attribute values
        api_response = api_instance.create_credential(body=body)
        print("The response of IssueCredentialV20Api->create_credential:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->create_credential: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V20IssueCredSchemaCore**](V20IssueCredSchemaCore.md)|  | [optional] 

### Return type

[**V20CredExRecord**](V20CredExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_record**
> object delete_record(cred_ex_id)

Remove an existing credential exchange record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier

    try:
        # Remove an existing credential exchange record
        api_response = api_instance.delete_record(cred_ex_id)
        print("The response of IssueCredentialV20Api->delete_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->delete_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_record**
> V20CredExRecordDetail get_record(cred_ex_id)

Fetch a single credential exchange record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier

    try:
        # Fetch a single credential exchange record
        api_response = api_instance.get_record(cred_ex_id)
        print("The response of IssueCredentialV20Api->get_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->get_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 

### Return type

[**V20CredExRecordDetail**](V20CredExRecordDetail.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_records**
> V20CredExRecordListResult get_records(connection_id=connection_id, role=role, state=state, thread_id=thread_id)

Fetch all credential exchange records

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    connection_id = 'connection_id_example' # str | Connection identifier (optional)
    role = 'role_example' # str | Role assigned in credential exchange (optional)
    state = 'state_example' # str | Credential exchange state (optional)
    thread_id = 'thread_id_example' # str | Thread identifier (optional)

    try:
        # Fetch all credential exchange records
        api_response = api_instance.get_records(connection_id=connection_id, role=role, state=state, thread_id=thread_id)
        print("The response of IssueCredentialV20Api->get_records:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->get_records: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connection_id** | **str**| Connection identifier | [optional] 
 **role** | **str**| Role assigned in credential exchange | [optional] 
 **state** | **str**| Credential exchange state | [optional] 
 **thread_id** | **str**| Thread identifier | [optional] 

### Return type

[**V20CredExRecordListResult**](V20CredExRecordListResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **issue_credential**
> V20CredExRecordDetail issue_credential(cred_ex_id, body=body)

Send holder a credential

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier
    body = pyaries.V20CredIssueRequest() # V20CredIssueRequest |  (optional)

    try:
        # Send holder a credential
        api_response = api_instance.issue_credential(cred_ex_id, body=body)
        print("The response of IssueCredentialV20Api->issue_credential:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->issue_credential: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 
 **body** | [**V20CredIssueRequest**](V20CredIssueRequest.md)|  | [optional] 

### Return type

[**V20CredExRecordDetail**](V20CredExRecordDetail.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **issue_credential20_create_offer_post**
> V20CredExRecord issue_credential20_create_offer_post(body=body)

Create a credential offer, independent of any proposal or connection

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    body = pyaries.V20CredOfferConnFreeRequest() # V20CredOfferConnFreeRequest |  (optional)

    try:
        # Create a credential offer, independent of any proposal or connection
        api_response = api_instance.issue_credential20_create_offer_post(body=body)
        print("The response of IssueCredentialV20Api->issue_credential20_create_offer_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->issue_credential20_create_offer_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V20CredOfferConnFreeRequest**](V20CredOfferConnFreeRequest.md)|  | [optional] 

### Return type

[**V20CredExRecord**](V20CredExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **issue_credential_automated**
> V20CredExRecord issue_credential_automated(body=body)

Send holder a credential, automating entire flow

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    body = pyaries.V20CredExFree() # V20CredExFree |  (optional)

    try:
        # Send holder a credential, automating entire flow
        api_response = api_instance.issue_credential_automated(body=body)
        print("The response of IssueCredentialV20Api->issue_credential_automated:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->issue_credential_automated: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V20CredExFree**](V20CredExFree.md)|  | [optional] 

### Return type

[**V20CredExRecord**](V20CredExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **report_problem**
> object report_problem(cred_ex_id, body=body)

Send a problem report for credential exchange

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier
    body = pyaries.V20CredIssueProblemReportRequest() # V20CredIssueProblemReportRequest |  (optional)

    try:
        # Send a problem report for credential exchange
        api_response = api_instance.report_problem(cred_ex_id, body=body)
        print("The response of IssueCredentialV20Api->report_problem:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->report_problem: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 
 **body** | [**V20CredIssueProblemReportRequest**](V20CredIssueProblemReportRequest.md)|  | [optional] 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_offer**
> V20CredExRecord send_offer(cred_ex_id, body=body)

Send holder a credential offer in reference to a proposal with preview

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier
    body = pyaries.V20CredBoundOfferRequest() # V20CredBoundOfferRequest |  (optional)

    try:
        # Send holder a credential offer in reference to a proposal with preview
        api_response = api_instance.send_offer(cred_ex_id, body=body)
        print("The response of IssueCredentialV20Api->send_offer:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->send_offer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 
 **body** | [**V20CredBoundOfferRequest**](V20CredBoundOfferRequest.md)|  | [optional] 

### Return type

[**V20CredExRecord**](V20CredExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_offer_free**
> V20CredExRecord send_offer_free(body=body)

Send holder a credential offer, independent of any proposal

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    body = pyaries.V20CredOfferRequest() # V20CredOfferRequest |  (optional)

    try:
        # Send holder a credential offer, independent of any proposal
        api_response = api_instance.send_offer_free(body=body)
        print("The response of IssueCredentialV20Api->send_offer_free:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->send_offer_free: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V20CredOfferRequest**](V20CredOfferRequest.md)|  | [optional] 

### Return type

[**V20CredExRecord**](V20CredExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_proposal**
> V20CredExRecord send_proposal(body=body)

Send issuer a credential proposal

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    body = pyaries.V20CredExFree() # V20CredExFree |  (optional)

    try:
        # Send issuer a credential proposal
        api_response = api_instance.send_proposal(body=body)
        print("The response of IssueCredentialV20Api->send_proposal:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->send_proposal: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V20CredExFree**](V20CredExFree.md)|  | [optional] 

### Return type

[**V20CredExRecord**](V20CredExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_request**
> V20CredExRecord send_request(cred_ex_id, body=body)

Send issuer a credential request

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier
    body = pyaries.V20CredRequestRequest() # V20CredRequestRequest |  (optional)

    try:
        # Send issuer a credential request
        api_response = api_instance.send_request(cred_ex_id, body=body)
        print("The response of IssueCredentialV20Api->send_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->send_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 
 **body** | [**V20CredRequestRequest**](V20CredRequestRequest.md)|  | [optional] 

### Return type

[**V20CredExRecord**](V20CredExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_request_free**
> V20CredExRecord send_request_free(body=body)

Send issuer a credential request not bound to an existing thread. Indy credentials cannot start at a request

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    body = pyaries.V20CredRequestFree() # V20CredRequestFree |  (optional)

    try:
        # Send issuer a credential request not bound to an existing thread. Indy credentials cannot start at a request
        api_response = api_instance.send_request_free(body=body)
        print("The response of IssueCredentialV20Api->send_request_free:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->send_request_free: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V20CredRequestFree**](V20CredRequestFree.md)|  | [optional] 

### Return type

[**V20CredExRecord**](V20CredExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **store_credential**
> V20CredExRecordDetail store_credential(cred_ex_id, body=body)

Store a received credential

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV20Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier
    body = pyaries.V20CredStoreRequest() # V20CredStoreRequest |  (optional)

    try:
        # Store a received credential
        api_response = api_instance.store_credential(cred_ex_id, body=body)
        print("The response of IssueCredentialV20Api->store_credential:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV20Api->store_credential: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 
 **body** | [**V20CredStoreRequest**](V20CredStoreRequest.md)|  | [optional] 

### Return type

[**V20CredExRecordDetail**](V20CredExRecordDetail.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

