# IndyProofRequestedProofRevealedAttrGroup


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sub_proof_index** | **int** | Sub-proof index | [optional] 
**values** | [**Dict[str, RawEncoded]**](RawEncoded.md) | Indy proof requested proof revealed attr groups group value | [optional] 

## Example

```python
from pyaries.models.indy_proof_requested_proof_revealed_attr_group import IndyProofRequestedProofRevealedAttrGroup

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofRequestedProofRevealedAttrGroup from a JSON string
indy_proof_requested_proof_revealed_attr_group_instance = IndyProofRequestedProofRevealedAttrGroup.from_json(json)
# print the JSON string representation of the object
print IndyProofRequestedProofRevealedAttrGroup.to_json()

# convert the object into a dict
indy_proof_requested_proof_revealed_attr_group_dict = indy_proof_requested_proof_revealed_attr_group_instance.to_dict()
# create an instance of IndyProofRequestedProofRevealedAttrGroup from a dict
indy_proof_requested_proof_revealed_attr_group_form_dict = indy_proof_requested_proof_revealed_attr_group.from_dict(indy_proof_requested_proof_revealed_attr_group_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


