# IndyPresPreview


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Message type identifier | [optional] 
**attributes** | [**List[IndyPresAttrSpec]**](IndyPresAttrSpec.md) |  | 
**predicates** | [**List[IndyPresPredSpec]**](IndyPresPredSpec.md) |  | 

## Example

```python
from pyaries.models.indy_pres_preview import IndyPresPreview

# TODO update the JSON string below
json = "{}"
# create an instance of IndyPresPreview from a JSON string
indy_pres_preview_instance = IndyPresPreview.from_json(json)
# print the JSON string representation of the object
print IndyPresPreview.to_json()

# convert the object into a dict
indy_pres_preview_dict = indy_pres_preview_instance.to_dict()
# create an instance of IndyPresPreview from a dict
indy_pres_preview_form_dict = indy_pres_preview.from_dict(indy_pres_preview_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


