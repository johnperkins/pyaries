# IndyProofProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aggregated_proof** | [**IndyProofProofAggregatedProof**](IndyProofProofAggregatedProof.md) |  | [optional] 
**proofs** | [**List[IndyProofProofProofsProof]**](IndyProofProofProofsProof.md) | Indy proof proofs | [optional] 

## Example

```python
from pyaries.models.indy_proof_proof import IndyProofProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofProof from a JSON string
indy_proof_proof_instance = IndyProofProof.from_json(json)
# print the JSON string representation of the object
print IndyProofProof.to_json()

# convert the object into a dict
indy_proof_proof_dict = indy_proof_proof_instance.to_dict()
# create an instance of IndyProofProof from a dict
indy_proof_proof_form_dict = indy_proof_proof.from_dict(indy_proof_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


