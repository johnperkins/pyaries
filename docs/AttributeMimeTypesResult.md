# AttributeMimeTypesResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | **Dict[str, str]** |  | [optional] 

## Example

```python
from pyaries.models.attribute_mime_types_result import AttributeMimeTypesResult

# TODO update the JSON string below
json = "{}"
# create an instance of AttributeMimeTypesResult from a JSON string
attribute_mime_types_result_instance = AttributeMimeTypesResult.from_json(json)
# print the JSON string representation of the object
print AttributeMimeTypesResult.to_json()

# convert the object into a dict
attribute_mime_types_result_dict = attribute_mime_types_result_instance.to_dict()
# create an instance of AttributeMimeTypesResult from a dict
attribute_mime_types_result_form_dict = attribute_mime_types_result.from_dict(attribute_mime_types_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


