# InputDescriptorsSchema

Accepts a list of schema or a dict containing filters like oneof_filter.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**oneof_filter** | **bool** | oneOf | [optional] 
**uri_groups** | **List[List[SchemaInputDescriptor]]** |  | [optional] 

## Example

```python
from pyaries.models.input_descriptors_schema import InputDescriptorsSchema

# TODO update the JSON string below
json = "{}"
# create an instance of InputDescriptorsSchema from a JSON string
input_descriptors_schema_instance = InputDescriptorsSchema.from_json(json)
# print the JSON string representation of the object
print InputDescriptorsSchema.to_json()

# convert the object into a dict
input_descriptors_schema_dict = input_descriptors_schema_instance.to_dict()
# create an instance of InputDescriptorsSchema from a dict
input_descriptors_schema_form_dict = input_descriptors_schema.from_dict(input_descriptors_schema_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


