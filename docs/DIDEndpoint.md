# DIDEndpoint


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**did** | **str** | DID of interest | 
**endpoint** | **str** | Endpoint to set (omit to delete) | [optional] 

## Example

```python
from pyaries.models.did_endpoint import DIDEndpoint

# TODO update the JSON string below
json = "{}"
# create an instance of DIDEndpoint from a JSON string
did_endpoint_instance = DIDEndpoint.from_json(json)
# print the JSON string representation of the object
print DIDEndpoint.to_json()

# convert the object into a dict
did_endpoint_dict = did_endpoint_instance.to_dict()
# create an instance of DIDEndpoint from a dict
did_endpoint_form_dict = did_endpoint.from_dict(did_endpoint_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


