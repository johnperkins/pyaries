# CredentialStatusOptions


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Credential status method type to use for the credential. Should match status method registered in the Verifiable Credential Extension Registry | 

## Example

```python
from pyaries.models.credential_status_options import CredentialStatusOptions

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialStatusOptions from a JSON string
credential_status_options_instance = CredentialStatusOptions.from_json(json)
# print the JSON string representation of the object
print CredentialStatusOptions.to_json()

# convert the object into a dict
credential_status_options_dict = credential_status_options_instance.to_dict()
# create an instance of CredentialStatusOptions from a dict
credential_status_options_form_dict = credential_status_options.from_dict(credential_status_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


