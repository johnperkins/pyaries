# LDProofVCDetailOptionsCredentialStatus

The credential status mechanism to use for the credential. Omitting the property indicates the issued credential will not include a credential status

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Credential status method type to use for the credential. Should match status method registered in the Verifiable Credential Extension Registry | 

## Example

```python
from pyaries.models.ld_proof_vc_detail_options_credential_status import LDProofVCDetailOptionsCredentialStatus

# TODO update the JSON string below
json = "{}"
# create an instance of LDProofVCDetailOptionsCredentialStatus from a JSON string
ld_proof_vc_detail_options_credential_status_instance = LDProofVCDetailOptionsCredentialStatus.from_json(json)
# print the JSON string representation of the object
print LDProofVCDetailOptionsCredentialStatus.to_json()

# convert the object into a dict
ld_proof_vc_detail_options_credential_status_dict = ld_proof_vc_detail_options_credential_status_instance.to_dict()
# create an instance of LDProofVCDetailOptionsCredentialStatus from a dict
ld_proof_vc_detail_options_credential_status_form_dict = ld_proof_vc_detail_options_credential_status.from_dict(ld_proof_vc_detail_options_credential_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


