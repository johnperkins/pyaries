# IndyProofRequestedProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**predicates** | [**Dict[str, IndyProofRequestedProofPredicate]**](IndyProofRequestedProofPredicate.md) | Proof requested proof predicates. | [optional] 
**revealed_attr_groups** | [**Dict[str, IndyProofRequestedProofRevealedAttrGroup]**](IndyProofRequestedProofRevealedAttrGroup.md) | Proof requested proof revealed attribute groups | [optional] 
**revealed_attrs** | [**Dict[str, IndyProofRequestedProofRevealedAttr]**](IndyProofRequestedProofRevealedAttr.md) | Proof requested proof revealed attributes | [optional] 
**self_attested_attrs** | **object** | Proof requested proof self-attested attributes | [optional] 
**unrevealed_attrs** | **object** | Unrevealed attributes | [optional] 

## Example

```python
from pyaries.models.indy_proof_requested_proof import IndyProofRequestedProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofRequestedProof from a JSON string
indy_proof_requested_proof_instance = IndyProofRequestedProof.from_json(json)
# print the JSON string representation of the object
print IndyProofRequestedProof.to_json()

# convert the object into a dict
indy_proof_requested_proof_dict = indy_proof_requested_proof_instance.to_dict()
# create an instance of IndyProofRequestedProof from a dict
indy_proof_requested_proof_form_dict = indy_proof_requested_proof.from_dict(indy_proof_requested_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


