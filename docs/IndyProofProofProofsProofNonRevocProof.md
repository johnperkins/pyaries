# IndyProofProofProofsProofNonRevocProof

Indy non-revocation proof

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**c_list** | **Dict[str, str]** |  | [optional] 
**x_list** | **Dict[str, str]** |  | [optional] 

## Example

```python
from pyaries.models.indy_proof_proof_proofs_proof_non_revoc_proof import IndyProofProofProofsProofNonRevocProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofProofProofsProofNonRevocProof from a JSON string
indy_proof_proof_proofs_proof_non_revoc_proof_instance = IndyProofProofProofsProofNonRevocProof.from_json(json)
# print the JSON string representation of the object
print IndyProofProofProofsProofNonRevocProof.to_json()

# convert the object into a dict
indy_proof_proof_proofs_proof_non_revoc_proof_dict = indy_proof_proof_proofs_proof_non_revoc_proof_instance.to_dict()
# create an instance of IndyProofProofProofsProofNonRevocProof from a dict
indy_proof_proof_proofs_proof_non_revoc_proof_form_dict = indy_proof_proof_proofs_proof_non_revoc_proof.from_dict(indy_proof_proof_proofs_proof_non_revoc_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


