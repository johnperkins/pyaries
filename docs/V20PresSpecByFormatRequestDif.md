# V20PresSpecByFormatRequestDif

Optional Presentation specification for DIF, overrides the PresentionExchange record's PresRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issuer_id** | **str** | Issuer identifier to sign the presentation, if different from current public DID | [optional] 
**presentation_definition** | [**PresentationDefinition**](PresentationDefinition.md) |  | [optional] 
**record_ids** | **object** | Mapping of input_descriptor id to list of stored W3C credential record_id | [optional] 
**reveal_doc** | **object** | reveal doc [JSON-LD frame] dict used to derive the credential when selective disclosure is required | [optional] 

## Example

```python
from pyaries.models.v20_pres_spec_by_format_request_dif import V20PresSpecByFormatRequestDif

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresSpecByFormatRequestDif from a JSON string
v20_pres_spec_by_format_request_dif_instance = V20PresSpecByFormatRequestDif.from_json(json)
# print the JSON string representation of the object
print V20PresSpecByFormatRequestDif.to_json()

# convert the object into a dict
v20_pres_spec_by_format_request_dif_dict = v20_pres_spec_by_format_request_dif_instance.to_dict()
# create an instance of V20PresSpecByFormatRequestDif from a dict
v20_pres_spec_by_format_request_dif_form_dict = v20_pres_spec_by_format_request_dif.from_dict(v20_pres_spec_by_format_request_dif_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


