# V20CredProposalCredentialPreview

Credential preview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Message type identifier | [optional] 
**attributes** | [**List[V20CredAttrSpec]**](V20CredAttrSpec.md) |  | 

## Example

```python
from pyaries.models.v20_cred_proposal_credential_preview import V20CredProposalCredentialPreview

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredProposalCredentialPreview from a JSON string
v20_cred_proposal_credential_preview_instance = V20CredProposalCredentialPreview.from_json(json)
# print the JSON string representation of the object
print V20CredProposalCredentialPreview.to_json()

# convert the object into a dict
v20_cred_proposal_credential_preview_dict = v20_cred_proposal_credential_preview_instance.to_dict()
# create an instance of V20CredProposalCredentialPreview from a dict
v20_cred_proposal_credential_preview_form_dict = v20_cred_proposal_credential_preview.from_dict(v20_cred_proposal_credential_preview_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


