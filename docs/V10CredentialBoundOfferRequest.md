# V10CredentialBoundOfferRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**counter_proposal** | [**CredentialProposal**](CredentialProposal.md) |  | [optional] 

## Example

```python
from pyaries.models.v10_credential_bound_offer_request import V10CredentialBoundOfferRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialBoundOfferRequest from a JSON string
v10_credential_bound_offer_request_instance = V10CredentialBoundOfferRequest.from_json(json)
# print the JSON string representation of the object
print V10CredentialBoundOfferRequest.to_json()

# convert the object into a dict
v10_credential_bound_offer_request_dict = v10_credential_bound_offer_request_instance.to_dict()
# create an instance of V10CredentialBoundOfferRequest from a dict
v10_credential_bound_offer_request_form_dict = v10_credential_bound_offer_request.from_dict(v10_credential_bound_offer_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


