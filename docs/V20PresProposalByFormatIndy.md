# V20PresProposalByFormatIndy

Presentation proposal for indy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Proof request name | [optional] 
**non_revoked** | [**IndyProofRequestNonRevoked**](IndyProofRequestNonRevoked.md) |  | [optional] 
**nonce** | **str** | Nonce | [optional] 
**requested_attributes** | [**Dict[str, IndyProofReqAttrSpec]**](IndyProofReqAttrSpec.md) | Requested attribute specifications of proof request | 
**requested_predicates** | [**Dict[str, IndyProofReqPredSpec]**](IndyProofReqPredSpec.md) | Requested predicate specifications of proof request | 
**version** | **str** | Proof request version | [optional] 

## Example

```python
from pyaries.models.v20_pres_proposal_by_format_indy import V20PresProposalByFormatIndy

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresProposalByFormatIndy from a JSON string
v20_pres_proposal_by_format_indy_instance = V20PresProposalByFormatIndy.from_json(json)
# print the JSON string representation of the object
print V20PresProposalByFormatIndy.to_json()

# convert the object into a dict
v20_pres_proposal_by_format_indy_dict = v20_pres_proposal_by_format_indy_instance.to_dict()
# create an instance of V20PresProposalByFormatIndy from a dict
v20_pres_proposal_by_format_indy_form_dict = v20_pres_proposal_by_format_indy.from_dict(v20_pres_proposal_by_format_indy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


