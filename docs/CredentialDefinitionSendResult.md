# CredentialDefinitionSendResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credential_definition_id** | **str** | Credential definition identifier | [optional] 

## Example

```python
from pyaries.models.credential_definition_send_result import CredentialDefinitionSendResult

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialDefinitionSendResult from a JSON string
credential_definition_send_result_instance = CredentialDefinitionSendResult.from_json(json)
# print the JSON string representation of the object
print CredentialDefinitionSendResult.to_json()

# convert the object into a dict
credential_definition_send_result_dict = credential_definition_send_result_instance.to_dict()
# create an instance of CredentialDefinitionSendResult from a dict
credential_definition_send_result_form_dict = credential_definition_send_result.from_dict(credential_definition_send_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


