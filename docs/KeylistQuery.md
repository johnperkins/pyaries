# KeylistQuery


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**filter** | **object** | Query dictionary object | [optional] 
**paginate** | [**KeylistQueryPaginate**](KeylistQueryPaginate.md) |  | [optional] 

## Example

```python
from pyaries.models.keylist_query import KeylistQuery

# TODO update the JSON string below
json = "{}"
# create an instance of KeylistQuery from a JSON string
keylist_query_instance = KeylistQuery.from_json(json)
# print the JSON string representation of the object
print KeylistQuery.to_json()

# convert the object into a dict
keylist_query_dict = keylist_query_instance.to_dict()
# create an instance of KeylistQuery from a dict
keylist_query_form_dict = keylist_query.from_dict(keylist_query_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


