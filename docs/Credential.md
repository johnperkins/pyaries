# Credential


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **List[object]** | The JSON-LD context of the credential | 
**credential_subject** | **object** |  | 
**expiration_date** | **str** | The expiration date | [optional] 
**id** | **str** |  | [optional] 
**issuance_date** | **str** | The issuance date | 
**issuer** | **object** | The JSON-LD Verifiable Credential Issuer. Either string of object with id field. | 
**proof** | [**LinkedDataProof**](LinkedDataProof.md) |  | [optional] 
**type** | **List[str]** | The JSON-LD type of the credential | 

## Example

```python
from pyaries.models.credential import Credential

# TODO update the JSON string below
json = "{}"
# create an instance of Credential from a JSON string
credential_instance = Credential.from_json(json)
# print the JSON string representation of the object
print Credential.to_json()

# convert the object into a dict
credential_dict = credential_instance.to_dict()
# create an instance of Credential from a dict
credential_form_dict = credential.from_dict(credential_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


