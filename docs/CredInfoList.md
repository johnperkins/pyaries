# CredInfoList


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[IndyCredInfo]**](IndyCredInfo.md) |  | [optional] 

## Example

```python
from pyaries.models.cred_info_list import CredInfoList

# TODO update the JSON string below
json = "{}"
# create an instance of CredInfoList from a JSON string
cred_info_list_instance = CredInfoList.from_json(json)
# print the JSON string representation of the object
print CredInfoList.to_json()

# convert the object into a dict
cred_info_list_dict = cred_info_list_instance.to_dict()
# create an instance of CredInfoList from a dict
cred_info_list_form_dict = cred_info_list.from_dict(cred_info_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


