# IndyPresSpec


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requested_attributes** | [**Dict[str, IndyRequestedCredsRequestedAttr]**](IndyRequestedCredsRequestedAttr.md) | Nested object mapping proof request attribute referents to requested-attribute specifiers | 
**requested_predicates** | [**Dict[str, IndyRequestedCredsRequestedPred]**](IndyRequestedCredsRequestedPred.md) | Nested object mapping proof request predicate referents to requested-predicate specifiers | 
**self_attested_attributes** | **Dict[str, str]** | Self-attested attributes to build into proof | 
**trace** | **bool** | Whether to trace event (default false) | [optional] 

## Example

```python
from pyaries.models.indy_pres_spec import IndyPresSpec

# TODO update the JSON string below
json = "{}"
# create an instance of IndyPresSpec from a JSON string
indy_pres_spec_instance = IndyPresSpec.from_json(json)
# print the JSON string representation of the object
print IndyPresSpec.to_json()

# convert the object into a dict
indy_pres_spec_dict = indy_pres_spec_instance.to_dict()
# create an instance of IndyPresSpec from a dict
indy_pres_spec_form_dict = indy_pres_spec.from_dict(indy_pres_spec_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


