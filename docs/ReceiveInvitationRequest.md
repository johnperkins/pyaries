# ReceiveInvitationRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**did** | **str** | DID for connection invitation | [optional] 
**image_url** | **str** | Optional image URL for connection invitation | [optional] 
**label** | **str** | Optional label for connection invitation | [optional] 
**recipient_keys** | **List[str]** | List of recipient keys | [optional] 
**routing_keys** | **List[str]** | List of routing keys | [optional] 
**service_endpoint** | **str** | Service endpoint at which to reach this agent | [optional] 

## Example

```python
from pyaries.models.receive_invitation_request import ReceiveInvitationRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ReceiveInvitationRequest from a JSON string
receive_invitation_request_instance = ReceiveInvitationRequest.from_json(json)
# print the JSON string representation of the object
print ReceiveInvitationRequest.to_json()

# convert the object into a dict
receive_invitation_request_dict = receive_invitation_request_instance.to_dict()
# create an instance of ReceiveInvitationRequest from a dict
receive_invitation_request_form_dict = receive_invitation_request.from_dict(receive_invitation_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


