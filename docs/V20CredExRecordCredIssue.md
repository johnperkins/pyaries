# V20CredExRecordCredIssue

Serialized credential issue message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**credentialsattach** | [**List[AttachDecorator]**](AttachDecorator.md) | Credential attachments | 
**formats** | [**List[V20CredFormat]**](V20CredFormat.md) | Acceptable attachment formats | 
**replacement_id** | **str** | Issuer-unique identifier to coordinate credential replacement | [optional] 

## Example

```python
from pyaries.models.v20_cred_ex_record_cred_issue import V20CredExRecordCredIssue

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExRecordCredIssue from a JSON string
v20_cred_ex_record_cred_issue_instance = V20CredExRecordCredIssue.from_json(json)
# print the JSON string representation of the object
print V20CredExRecordCredIssue.to_json()

# convert the object into a dict
v20_cred_ex_record_cred_issue_dict = v20_cred_ex_record_cred_issue_instance.to_dict()
# create an instance of V20CredExRecordCredIssue from a dict
v20_cred_ex_record_cred_issue_form_dict = v20_cred_ex_record_cred_issue.from_dict(v20_cred_ex_record_cred_issue_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


