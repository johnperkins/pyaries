# V20PresSpecByFormatRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dif** | [**DIFPresSpec**](DIFPresSpec.md) |  | [optional] 
**indy** | [**IndyPresSpec**](IndyPresSpec.md) |  | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 

## Example

```python
from pyaries.models.v20_pres_spec_by_format_request import V20PresSpecByFormatRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresSpecByFormatRequest from a JSON string
v20_pres_spec_by_format_request_instance = V20PresSpecByFormatRequest.from_json(json)
# print the JSON string representation of the object
print V20PresSpecByFormatRequest.to_json()

# convert the object into a dict
v20_pres_spec_by_format_request_dict = v20_pres_spec_by_format_request_instance.to_dict()
# create an instance of V20PresSpecByFormatRequest from a dict
v20_pres_spec_by_format_request_form_dict = v20_pres_spec_by_format_request.from_dict(v20_pres_spec_by_format_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


