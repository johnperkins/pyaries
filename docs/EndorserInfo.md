# EndorserInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**endorser_did** | **str** | Endorser DID | 
**endorser_name** | **str** | Endorser Name | [optional] 

## Example

```python
from pyaries.models.endorser_info import EndorserInfo

# TODO update the JSON string below
json = "{}"
# create an instance of EndorserInfo from a JSON string
endorser_info_instance = EndorserInfo.from_json(json)
# print the JSON string representation of the object
print EndorserInfo.to_json()

# convert the object into a dict
endorser_info_dict = endorser_info_instance.to_dict()
# create an instance of EndorserInfo from a dict
endorser_info_form_dict = endorser_info.from_dict(endorser_info_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


