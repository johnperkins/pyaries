# IndyPresAttrSpec


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_def_id** | **str** |  | [optional] 
**mime_type** | **str** | MIME type (default null) | [optional] 
**name** | **str** | Attribute name | 
**referent** | **str** | Credential referent | [optional] 
**value** | **str** | Attribute value | [optional] 

## Example

```python
from pyaries.models.indy_pres_attr_spec import IndyPresAttrSpec

# TODO update the JSON string below
json = "{}"
# create an instance of IndyPresAttrSpec from a JSON string
indy_pres_attr_spec_instance = IndyPresAttrSpec.from_json(json)
# print the JSON string representation of the object
print IndyPresAttrSpec.to_json()

# convert the object into a dict
indy_pres_attr_spec_dict = indy_pres_attr_spec_instance.to_dict()
# create an instance of IndyPresAttrSpec from a dict
indy_pres_attr_spec_form_dict = indy_pres_attr_spec.from_dict(indy_pres_attr_spec_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


