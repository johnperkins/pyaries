# CreateWalletRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_url** | **str** | Image url for this wallet. This image url is publicized            (self-attested) to other agents as part of forming a connection. | [optional] 
**key_management_mode** | **str** | Key management method to use for this wallet. | [optional] 
**label** | **str** | Label for this wallet. This label is publicized            (self-attested) to other agents as part of forming a connection. | [optional] 
**wallet_dispatch_type** | **str** | Webhook target dispatch type for this wallet.             default - Dispatch only to webhooks associated with this wallet.             base - Dispatch only to webhooks associated with the base wallet.             both - Dispatch to both webhook targets. | [optional] 
**wallet_key** | **str** | Master key used for key derivation. | [optional] 
**wallet_key_derivation** | **str** | Key derivation | [optional] 
**wallet_name** | **str** | Wallet name | [optional] 
**wallet_type** | **str** | Type of the wallet to create | [optional] 
**wallet_webhook_urls** | **List[str]** | List of Webhook URLs associated with this subwallet | [optional] 

## Example

```python
from pyaries.models.create_wallet_request import CreateWalletRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CreateWalletRequest from a JSON string
create_wallet_request_instance = CreateWalletRequest.from_json(json)
# print the JSON string representation of the object
print CreateWalletRequest.to_json()

# convert the object into a dict
create_wallet_request_dict = create_wallet_request_instance.to_dict()
# create an instance of CreateWalletRequest from a dict
create_wallet_request_form_dict = create_wallet_request.from_dict(create_wallet_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


