# V10DiscoveryRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connection_id** | **str** | Connection identifier | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**disclose** | [**Disclose**](Disclose.md) |  | [optional] 
**discovery_exchange_id** | **str** | Credential exchange identifier | [optional] 
**query_msg** | [**Query**](Query.md) |  | [optional] 
**state** | **str** | Current record state | [optional] 
**thread_id** | **str** | Thread identifier | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.v10_discovery_record import V10DiscoveryRecord

# TODO update the JSON string below
json = "{}"
# create an instance of V10DiscoveryRecord from a JSON string
v10_discovery_record_instance = V10DiscoveryRecord.from_json(json)
# print the JSON string representation of the object
print V10DiscoveryRecord.to_json()

# convert the object into a dict
v10_discovery_record_dict = v10_discovery_record_instance.to_dict()
# create an instance of V10DiscoveryRecord from a dict
v10_discovery_record_form_dict = v10_discovery_record.from_dict(v10_discovery_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


