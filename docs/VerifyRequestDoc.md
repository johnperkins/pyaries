# VerifyRequestDoc

Signed document

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**proof** | [**SignedDocProof**](SignedDocProof.md) |  | 

## Example

```python
from pyaries.models.verify_request_doc import VerifyRequestDoc

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyRequestDoc from a JSON string
verify_request_doc_instance = VerifyRequestDoc.from_json(json)
# print the JSON string representation of the object
print VerifyRequestDoc.to_json()

# convert the object into a dict
verify_request_doc_dict = verify_request_doc_instance.to_dict()
# create an instance of VerifyRequestDoc from a dict
verify_request_doc_form_dict = verify_request_doc.from_dict(verify_request_doc_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


