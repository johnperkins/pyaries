# DIDXRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**did** | **str** | DID of exchange | [optional] 
**did_docattach** | [**AttachDecorator**](AttachDecorator.md) |  | [optional] 
**label** | **str** | Label for DID exchange request | 

## Example

```python
from pyaries.models.didx_request import DIDXRequest

# TODO update the JSON string below
json = "{}"
# create an instance of DIDXRequest from a JSON string
didx_request_instance = DIDXRequest.from_json(json)
# print the JSON string representation of the object
print DIDXRequest.to_json()

# convert the object into a dict
didx_request_dict = didx_request_instance.to_dict()
# create an instance of DIDXRequest from a dict
didx_request_form_dict = didx_request.from_dict(didx_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


