# IndyProofReqPredSpecNonRevoked


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**var_from** | **int** | Earliest time of interest in non-revocation interval | [optional] 
**to** | **int** | Latest time of interest in non-revocation interval | [optional] 

## Example

```python
from pyaries.models.indy_proof_req_pred_spec_non_revoked import IndyProofReqPredSpecNonRevoked

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofReqPredSpecNonRevoked from a JSON string
indy_proof_req_pred_spec_non_revoked_instance = IndyProofReqPredSpecNonRevoked.from_json(json)
# print the JSON string representation of the object
print IndyProofReqPredSpecNonRevoked.to_json()

# convert the object into a dict
indy_proof_req_pred_spec_non_revoked_dict = indy_proof_req_pred_spec_non_revoked_instance.to_dict()
# create an instance of IndyProofReqPredSpecNonRevoked from a dict
indy_proof_req_pred_spec_non_revoked_form_dict = indy_proof_req_pred_spec_non_revoked.from_dict(indy_proof_req_pred_spec_non_revoked_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


