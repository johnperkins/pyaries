# TxnOrSchemaSendResultSent

Content sent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**var_schema** | [**SchemaSendResultSchema**](SchemaSendResultSchema.md) |  | [optional] 
**schema_id** | **str** | Schema identifier | 

## Example

```python
from pyaries.models.txn_or_schema_send_result_sent import TxnOrSchemaSendResultSent

# TODO update the JSON string below
json = "{}"
# create an instance of TxnOrSchemaSendResultSent from a JSON string
txn_or_schema_send_result_sent_instance = TxnOrSchemaSendResultSent.from_json(json)
# print the JSON string representation of the object
print TxnOrSchemaSendResultSent.to_json()

# convert the object into a dict
txn_or_schema_send_result_sent_dict = txn_or_schema_send_result_sent_instance.to_dict()
# create an instance of TxnOrSchemaSendResultSent from a dict
txn_or_schema_send_result_sent_form_dict = txn_or_schema_send_result_sent.from_dict(txn_or_schema_send_result_sent_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


