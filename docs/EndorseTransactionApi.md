# pyaries.EndorseTransactionApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancel_transaction**](EndorseTransactionApi.md#cancel_transaction) | **POST** /transactions/{tran_id}/cancel | For Author to cancel a particular transaction request
[**create_request**](EndorseTransactionApi.md#create_request) | **POST** /transactions/create-request | For author to send a transaction request
[**endorse_transaction**](EndorseTransactionApi.md#endorse_transaction) | **POST** /transactions/{tran_id}/endorse | For Endorser to endorse a particular transaction record
[**get_records**](EndorseTransactionApi.md#get_records) | **GET** /transactions | Query transactions
[**get_transaction**](EndorseTransactionApi.md#get_transaction) | **GET** /transactions/{tran_id} | Fetch a single transaction record
[**refuse_transaction**](EndorseTransactionApi.md#refuse_transaction) | **POST** /transactions/{tran_id}/refuse | For Endorser to refuse a particular transaction record
[**resend_transaction_request**](EndorseTransactionApi.md#resend_transaction_request) | **POST** /transaction/{tran_id}/resend | For Author to resend a particular transaction request
[**set_endorser_info**](EndorseTransactionApi.md#set_endorser_info) | **POST** /transactions/{conn_id}/set-endorser-info | Set Endorser Info
[**set_endorser_role**](EndorseTransactionApi.md#set_endorser_role) | **POST** /transactions/{conn_id}/set-endorser-role | Set transaction jobs
[**write_transaction**](EndorseTransactionApi.md#write_transaction) | **POST** /transactions/{tran_id}/write | For Author / Endorser to write an endorsed transaction to the ledger


# **cancel_transaction**
> TransactionRecord cancel_transaction(tran_id)

For Author to cancel a particular transaction request

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.EndorseTransactionApi(api_client)
    tran_id = 'tran_id_example' # str | Transaction identifier

    try:
        # For Author to cancel a particular transaction request
        api_response = api_instance.cancel_transaction(tran_id)
        print("The response of EndorseTransactionApi->cancel_transaction:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EndorseTransactionApi->cancel_transaction: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tran_id** | **str**| Transaction identifier | 

### Return type

[**TransactionRecord**](TransactionRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_request**
> TransactionRecord create_request(tran_id, endorser_write_txn=endorser_write_txn, body=body)

For author to send a transaction request

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.EndorseTransactionApi(api_client)
    tran_id = 'tran_id_example' # str | Transaction identifier
    endorser_write_txn = True # bool | Endorser will write the transaction after endorsing it (optional)
    body = pyaries.ModelDate() # ModelDate |  (optional)

    try:
        # For author to send a transaction request
        api_response = api_instance.create_request(tran_id, endorser_write_txn=endorser_write_txn, body=body)
        print("The response of EndorseTransactionApi->create_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EndorseTransactionApi->create_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tran_id** | **str**| Transaction identifier | 
 **endorser_write_txn** | **bool**| Endorser will write the transaction after endorsing it | [optional] 
 **body** | [**ModelDate**](ModelDate.md)|  | [optional] 

### Return type

[**TransactionRecord**](TransactionRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **endorse_transaction**
> TransactionRecord endorse_transaction(tran_id, endorser_did=endorser_did)

For Endorser to endorse a particular transaction record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.EndorseTransactionApi(api_client)
    tran_id = 'tran_id_example' # str | Transaction identifier
    endorser_did = 'endorser_did_example' # str | Endorser DID (optional)

    try:
        # For Endorser to endorse a particular transaction record
        api_response = api_instance.endorse_transaction(tran_id, endorser_did=endorser_did)
        print("The response of EndorseTransactionApi->endorse_transaction:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EndorseTransactionApi->endorse_transaction: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tran_id** | **str**| Transaction identifier | 
 **endorser_did** | **str**| Endorser DID | [optional] 

### Return type

[**TransactionRecord**](TransactionRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_records**
> TransactionList get_records()

Query transactions

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.EndorseTransactionApi(api_client)

    try:
        # Query transactions
        api_response = api_instance.get_records()
        print("The response of EndorseTransactionApi->get_records:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EndorseTransactionApi->get_records: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**TransactionList**](TransactionList.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transaction**
> TransactionRecord get_transaction(tran_id)

Fetch a single transaction record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.EndorseTransactionApi(api_client)
    tran_id = 'tran_id_example' # str | Transaction identifier

    try:
        # Fetch a single transaction record
        api_response = api_instance.get_transaction(tran_id)
        print("The response of EndorseTransactionApi->get_transaction:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EndorseTransactionApi->get_transaction: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tran_id** | **str**| Transaction identifier | 

### Return type

[**TransactionRecord**](TransactionRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **refuse_transaction**
> TransactionRecord refuse_transaction(tran_id)

For Endorser to refuse a particular transaction record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.EndorseTransactionApi(api_client)
    tran_id = 'tran_id_example' # str | Transaction identifier

    try:
        # For Endorser to refuse a particular transaction record
        api_response = api_instance.refuse_transaction(tran_id)
        print("The response of EndorseTransactionApi->refuse_transaction:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EndorseTransactionApi->refuse_transaction: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tran_id** | **str**| Transaction identifier | 

### Return type

[**TransactionRecord**](TransactionRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **resend_transaction_request**
> TransactionRecord resend_transaction_request(tran_id)

For Author to resend a particular transaction request

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.EndorseTransactionApi(api_client)
    tran_id = 'tran_id_example' # str | Transaction identifier

    try:
        # For Author to resend a particular transaction request
        api_response = api_instance.resend_transaction_request(tran_id)
        print("The response of EndorseTransactionApi->resend_transaction_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EndorseTransactionApi->resend_transaction_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tran_id** | **str**| Transaction identifier | 

### Return type

[**TransactionRecord**](TransactionRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_endorser_info**
> EndorserInfo set_endorser_info(conn_id, endorser_did, endorser_name=endorser_name)

Set Endorser Info

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.EndorseTransactionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    endorser_did = 'endorser_did_example' # str | Endorser DID
    endorser_name = 'endorser_name_example' # str | Endorser Name (optional)

    try:
        # Set Endorser Info
        api_response = api_instance.set_endorser_info(conn_id, endorser_did, endorser_name=endorser_name)
        print("The response of EndorseTransactionApi->set_endorser_info:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EndorseTransactionApi->set_endorser_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **endorser_did** | **str**| Endorser DID | 
 **endorser_name** | **str**| Endorser Name | [optional] 

### Return type

[**EndorserInfo**](EndorserInfo.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_endorser_role**
> TransactionJobs set_endorser_role(conn_id, transaction_my_job=transaction_my_job)

Set transaction jobs

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.EndorseTransactionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    transaction_my_job = 'transaction_my_job_example' # str | Transaction related jobs (optional)

    try:
        # Set transaction jobs
        api_response = api_instance.set_endorser_role(conn_id, transaction_my_job=transaction_my_job)
        print("The response of EndorseTransactionApi->set_endorser_role:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EndorseTransactionApi->set_endorser_role: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **transaction_my_job** | **str**| Transaction related jobs | [optional] 

### Return type

[**TransactionJobs**](TransactionJobs.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **write_transaction**
> TransactionRecord write_transaction(tran_id)

For Author / Endorser to write an endorsed transaction to the ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.EndorseTransactionApi(api_client)
    tran_id = 'tran_id_example' # str | Transaction identifier

    try:
        # For Author / Endorser to write an endorsed transaction to the ledger
        api_response = api_instance.write_transaction(tran_id)
        print("The response of EndorseTransactionApi->write_transaction:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EndorseTransactionApi->write_transaction: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tran_id** | **str**| Transaction identifier | 

### Return type

[**TransactionRecord**](TransactionRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

