# V10CredentialExchangeRawCredential

Credential as received, prior to storage in holder wallet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_def_id** | **str** | Credential definition identifier | 
**rev_reg** | **object** | Revocation registry state | [optional] 
**rev_reg_id** | **str** | Revocation registry identifier | [optional] 
**schema_id** | **str** | Schema identifier | 
**signature** | **object** | Credential signature | 
**signature_correctness_proof** | **object** | Credential signature correctness proof | 
**values** | [**Dict[str, IndyCredentialValuesValue]**](IndyCredentialValuesValue.md) | Credential attributes | 
**witness** | **object** | Witness for revocation proof | [optional] 

## Example

```python
from pyaries.models.v10_credential_exchange_raw_credential import V10CredentialExchangeRawCredential

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialExchangeRawCredential from a JSON string
v10_credential_exchange_raw_credential_instance = V10CredentialExchangeRawCredential.from_json(json)
# print the JSON string representation of the object
print V10CredentialExchangeRawCredential.to_json()

# convert the object into a dict
v10_credential_exchange_raw_credential_dict = v10_credential_exchange_raw_credential_instance.to_dict()
# create an instance of V10CredentialExchangeRawCredential from a dict
v10_credential_exchange_raw_credential_form_dict = v10_credential_exchange_raw_credential.from_dict(v10_credential_exchange_raw_credential_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


