# SchemaGetResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**var_schema** | [**ModelSchema**](ModelSchema.md) |  | [optional] 

## Example

```python
from pyaries.models.schema_get_result import SchemaGetResult

# TODO update the JSON string below
json = "{}"
# create an instance of SchemaGetResult from a JSON string
schema_get_result_instance = SchemaGetResult.from_json(json)
# print the JSON string representation of the object
print SchemaGetResult.to_json()

# convert the object into a dict
schema_get_result_dict = schema_get_result_instance.to_dict()
# create an instance of SchemaGetResult from a dict
schema_get_result_form_dict = schema_get_result.from_dict(schema_get_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


