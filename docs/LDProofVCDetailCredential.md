# LDProofVCDetailCredential

Detail of the JSON-LD Credential to be issued

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **List[object]** | The JSON-LD context of the credential | 
**credential_subject** | **object** |  | 
**expiration_date** | **str** | The expiration date | [optional] 
**id** | **str** |  | [optional] 
**issuance_date** | **str** | The issuance date | 
**issuer** | **object** | The JSON-LD Verifiable Credential Issuer. Either string of object with id field. | 
**proof** | [**CredentialProof**](CredentialProof.md) |  | [optional] 
**type** | **List[str]** | The JSON-LD type of the credential | 

## Example

```python
from pyaries.models.ld_proof_vc_detail_credential import LDProofVCDetailCredential

# TODO update the JSON string below
json = "{}"
# create an instance of LDProofVCDetailCredential from a JSON string
ld_proof_vc_detail_credential_instance = LDProofVCDetailCredential.from_json(json)
# print the JSON string representation of the object
print LDProofVCDetailCredential.to_json()

# convert the object into a dict
ld_proof_vc_detail_credential_dict = ld_proof_vc_detail_credential_instance.to_dict()
# create an instance of LDProofVCDetailCredential from a dict
ld_proof_vc_detail_credential_form_dict = ld_proof_vc_detail_credential.from_dict(ld_proof_vc_detail_credential_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


