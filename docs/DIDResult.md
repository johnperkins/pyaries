# DIDResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**DID**](DID.md) |  | [optional] 

## Example

```python
from pyaries.models.did_result import DIDResult

# TODO update the JSON string below
json = "{}"
# create an instance of DIDResult from a JSON string
did_result_instance = DIDResult.from_json(json)
# print the JSON string representation of the object
print DIDResult.to_json()

# convert the object into a dict
did_result_dict = did_result_instance.to_dict()
# create an instance of DIDResult from a dict
did_result_form_dict = did_result.from_dict(did_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


