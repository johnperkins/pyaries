# IndyPresPredSpec


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_def_id** | **str** | Credential definition identifier | [optional] 
**name** | **str** | Attribute name | 
**predicate** | **str** | Predicate type (&#39;&lt;&#39;, &#39;&lt;&#x3D;&#39;, &#39;&gt;&#x3D;&#39;, or &#39;&gt;&#39;) | 
**threshold** | **int** | Threshold value | 

## Example

```python
from pyaries.models.indy_pres_pred_spec import IndyPresPredSpec

# TODO update the JSON string below
json = "{}"
# create an instance of IndyPresPredSpec from a JSON string
indy_pres_pred_spec_instance = IndyPresPredSpec.from_json(json)
# print the JSON string representation of the object
print IndyPresPredSpec.to_json()

# convert the object into a dict
indy_pres_pred_spec_dict = indy_pres_pred_spec_instance.to_dict()
# create an instance of IndyPresPredSpec from a dict
indy_pres_pred_spec_form_dict = indy_pres_pred_spec.from_dict(indy_pres_pred_spec_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


