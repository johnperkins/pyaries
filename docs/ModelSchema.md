# ModelSchema


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attr_names** | **List[str]** | Schema attribute names | [optional] 
**id** | **str** | Schema identifier | [optional] 
**name** | **str** | Schema name | [optional] 
**seq_no** | **int** | Schema sequence number | [optional] 
**ver** | **str** | Node protocol version | [optional] 
**version** | **str** | Schema version | [optional] 

## Example

```python
from pyaries.models.model_schema import ModelSchema

# TODO update the JSON string below
json = "{}"
# create an instance of ModelSchema from a JSON string
model_schema_instance = ModelSchema.from_json(json)
# print the JSON string representation of the object
print ModelSchema.to_json()

# convert the object into a dict
model_schema_dict = model_schema_instance.to_dict()
# create an instance of ModelSchema from a dict
model_schema_form_dict = model_schema.from_dict(model_schema_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


