# CredentialDefinitionsCreatedResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credential_definition_ids** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.credential_definitions_created_result import CredentialDefinitionsCreatedResult

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialDefinitionsCreatedResult from a JSON string
credential_definitions_created_result_instance = CredentialDefinitionsCreatedResult.from_json(json)
# print the JSON string representation of the object
print CredentialDefinitionsCreatedResult.to_json()

# convert the object into a dict
credential_definitions_created_result_dict = credential_definitions_created_result_instance.to_dict()
# create an instance of CredentialDefinitionsCreatedResult from a dict
credential_definitions_created_result_form_dict = credential_definitions_created_result.from_dict(credential_definitions_created_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


