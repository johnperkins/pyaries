# TAARecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**digest** | **str** |  | [optional] 
**text** | **str** |  | [optional] 
**version** | **str** |  | [optional] 

## Example

```python
from pyaries.models.taa_record import TAARecord

# TODO update the JSON string below
json = "{}"
# create an instance of TAARecord from a JSON string
taa_record_instance = TAARecord.from_json(json)
# print the JSON string representation of the object
print TAARecord.to_json()

# convert the object into a dict
taa_record_dict = taa_record_instance.to_dict()
# create an instance of TAARecord from a dict
taa_record_form_dict = taa_record.from_dict(taa_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


