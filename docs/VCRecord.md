# VCRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contexts** | **List[str]** |  | [optional] 
**cred_tags** | **Dict[str, str]** |  | [optional] 
**cred_value** | **object** | (JSON-serializable) credential value | [optional] 
**expanded_types** | **List[str]** |  | [optional] 
**given_id** | **str** | Credential identifier | [optional] 
**issuer_id** | **str** | Issuer identifier | [optional] 
**proof_types** | **List[str]** |  | [optional] 
**record_id** | **str** | Record identifier | [optional] 
**schema_ids** | **List[str]** |  | [optional] 
**subject_ids** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.vc_record import VCRecord

# TODO update the JSON string below
json = "{}"
# create an instance of VCRecord from a JSON string
vc_record_instance = VCRecord.from_json(json)
# print the JSON string representation of the object
print VCRecord.to_json()

# convert the object into a dict
vc_record_dict = vc_record_instance.to_dict()
# create an instance of VCRecord from a dict
vc_record_form_dict = vc_record.from_dict(vc_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


