# TxnOrRegisterLedgerNymResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **bool** | Success of nym registration operation | [optional] 
**txn** | [**TransactionRecord**](TransactionRecord.md) |  | [optional] 

## Example

```python
from pyaries.models.txn_or_register_ledger_nym_response import TxnOrRegisterLedgerNymResponse

# TODO update the JSON string below
json = "{}"
# create an instance of TxnOrRegisterLedgerNymResponse from a JSON string
txn_or_register_ledger_nym_response_instance = TxnOrRegisterLedgerNymResponse.from_json(json)
# print the JSON string representation of the object
print TxnOrRegisterLedgerNymResponse.to_json()

# convert the object into a dict
txn_or_register_ledger_nym_response_dict = txn_or_register_ledger_nym_response_instance.to_dict()
# create an instance of TxnOrRegisterLedgerNymResponse from a dict
txn_or_register_ledger_nym_response_form_dict = txn_or_register_ledger_nym_response.from_dict(txn_or_register_ledger_nym_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


