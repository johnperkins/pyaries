# MenuForm


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | Additional descriptive text for menu form | [optional] 
**params** | [**List[MenuFormParam]**](MenuFormParam.md) | List of form parameters | [optional] 
**submit_label** | **str** | Alternative label for form submit button | [optional] 
**title** | **str** | Menu form title | [optional] 

## Example

```python
from pyaries.models.menu_form import MenuForm

# TODO update the JSON string below
json = "{}"
# create an instance of MenuForm from a JSON string
menu_form_instance = MenuForm.from_json(json)
# print the JSON string representation of the object
print MenuForm.to_json()

# convert the object into a dict
menu_form_dict = menu_form_instance.to_dict()
# create an instance of MenuForm from a dict
menu_form_form_dict = menu_form.from_dict(menu_form_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


