# ServiceDecorator


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipient_keys** | **List[str]** | List of recipient keys | 
**routing_keys** | **List[str]** | List of routing keys | [optional] 
**service_endpoint** | **str** | Service endpoint at which to reach this agent | 

## Example

```python
from pyaries.models.service_decorator import ServiceDecorator

# TODO update the JSON string below
json = "{}"
# create an instance of ServiceDecorator from a JSON string
service_decorator_instance = ServiceDecorator.from_json(json)
# print the JSON string representation of the object
print ServiceDecorator.to_json()

# convert the object into a dict
service_decorator_dict = service_decorator_instance.to_dict()
# create an instance of ServiceDecorator from a dict
service_decorator_form_dict = service_decorator.from_dict(service_decorator_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


