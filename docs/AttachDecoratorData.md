# AttachDecoratorData


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**var_base64** | **str** | Base64-encoded data | [optional] 
**var_json** | **object** | JSON-serialized data | [optional] 
**jws** | [**AttachDecoratorDataJWS**](AttachDecoratorDataJWS.md) |  | [optional] 
**links** | **List[str]** | List of hypertext links to data | [optional] 
**sha256** | **str** | SHA256 hash (binhex encoded) of content | [optional] 

## Example

```python
from pyaries.models.attach_decorator_data import AttachDecoratorData

# TODO update the JSON string below
json = "{}"
# create an instance of AttachDecoratorData from a JSON string
attach_decorator_data_instance = AttachDecoratorData.from_json(json)
# print the JSON string representation of the object
print AttachDecoratorData.to_json()

# convert the object into a dict
attach_decorator_data_dict = attach_decorator_data_instance.to_dict()
# create an instance of AttachDecoratorData from a dict
attach_decorator_data_form_dict = attach_decorator_data.from_dict(attach_decorator_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


