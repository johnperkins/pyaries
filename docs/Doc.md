# Doc


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credential** | **object** | Credential to sign | 
**options** | [**SignatureOptions**](SignatureOptions.md) |  | 

## Example

```python
from pyaries.models.doc import Doc

# TODO update the JSON string below
json = "{}"
# create an instance of Doc from a JSON string
doc_instance = Doc.from_json(json)
# print the JSON string representation of the object
print Doc.to_json()

# convert the object into a dict
doc_dict = doc_instance.to_dict()
# create an instance of Doc from a dict
doc_form_dict = doc.from_dict(doc_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


