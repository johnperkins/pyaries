# PingRequestResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**thread_id** | **str** | Thread ID of the ping message | [optional] 

## Example

```python
from pyaries.models.ping_request_response import PingRequestResponse

# TODO update the JSON string below
json = "{}"
# create an instance of PingRequestResponse from a JSON string
ping_request_response_instance = PingRequestResponse.from_json(json)
# print the JSON string representation of the object
print PingRequestResponse.to_json()

# convert the object into a dict
ping_request_response_dict = ping_request_response_instance.to_dict()
# create an instance of PingRequestResponse from a dict
ping_request_response_form_dict = ping_request_response.from_dict(ping_request_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


