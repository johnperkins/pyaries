# InvitationRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | Time of record creation | [optional] 
**invi_msg_id** | **str** | Invitation message identifier | [optional] 
**invitation** | [**InvitationMessage**](InvitationMessage.md) |  | [optional] 
**invitation_id** | **str** | Invitation record identifier | [optional] 
**invitation_url** | **str** | Invitation message URL | [optional] 
**oob_id** | **str** | Out of band record identifier | [optional] 
**state** | **str** | Out of band message exchange state | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.invitation_record import InvitationRecord

# TODO update the JSON string below
json = "{}"
# create an instance of InvitationRecord from a JSON string
invitation_record_instance = InvitationRecord.from_json(json)
# print the JSON string representation of the object
print InvitationRecord.to_json()

# convert the object into a dict
invitation_record_dict = invitation_record_instance.to_dict()
# create an instance of InvitationRecord from a dict
invitation_record_form_dict = invitation_record.from_dict(invitation_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


