# MediationCreateRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mediator_terms** | **List[str]** | List of mediator rules for recipient | [optional] 
**recipient_terms** | **List[str]** | List of recipient rules for mediation | [optional] 

## Example

```python
from pyaries.models.mediation_create_request import MediationCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of MediationCreateRequest from a JSON string
mediation_create_request_instance = MediationCreateRequest.from_json(json)
# print the JSON string representation of the object
print MediationCreateRequest.to_json()

# convert the object into a dict
mediation_create_request_dict = mediation_create_request_instance.to_dict()
# create an instance of MediationCreateRequest from a dict
mediation_create_request_form_dict = mediation_create_request.from_dict(mediation_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


