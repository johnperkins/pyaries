# V20PresRequestByFormatDif

Presentation request for DIF

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**options** | [**DIFOptions**](DIFOptions.md) |  | [optional] 
**presentation_definition** | [**PresentationDefinition**](PresentationDefinition.md) |  | 

## Example

```python
from pyaries.models.v20_pres_request_by_format_dif import V20PresRequestByFormatDif

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresRequestByFormatDif from a JSON string
v20_pres_request_by_format_dif_instance = V20PresRequestByFormatDif.from_json(json)
# print the JSON string representation of the object
print V20PresRequestByFormatDif.to_json()

# convert the object into a dict
v20_pres_request_by_format_dif_dict = v20_pres_request_by_format_dif_instance.to_dict()
# create an instance of V20PresRequestByFormatDif from a dict
v20_pres_request_by_format_dif_form_dict = v20_pres_request_by_format_dif.from_dict(v20_pres_request_by_format_dif_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


