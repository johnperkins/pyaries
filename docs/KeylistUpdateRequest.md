# KeylistUpdateRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**updates** | [**List[KeylistUpdateRule]**](KeylistUpdateRule.md) |  | [optional] 

## Example

```python
from pyaries.models.keylist_update_request import KeylistUpdateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of KeylistUpdateRequest from a JSON string
keylist_update_request_instance = KeylistUpdateRequest.from_json(json)
# print the JSON string representation of the object
print KeylistUpdateRequest.to_json()

# convert the object into a dict
keylist_update_request_dict = keylist_update_request_instance.to_dict()
# create an instance of KeylistUpdateRequest from a dict
keylist_update_request_form_dict = keylist_update_request.from_dict(keylist_update_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


