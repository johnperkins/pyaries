# IndyCredentialValuesValue

Attribute value

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encoded** | **str** | Attribute encoded value | 
**raw** | **str** | Attribute raw value | 

## Example

```python
from pyaries.models.indy_credential_values_value import IndyCredentialValuesValue

# TODO update the JSON string below
json = "{}"
# create an instance of IndyCredentialValuesValue from a JSON string
indy_credential_values_value_instance = IndyCredentialValuesValue.from_json(json)
# print the JSON string representation of the object
print IndyCredentialValuesValue.to_json()

# convert the object into a dict
indy_credential_values_value_dict = indy_credential_values_value_instance.to_dict()
# create an instance of IndyCredentialValuesValue from a dict
indy_credential_values_value_form_dict = indy_credential_values_value.from_dict(indy_credential_values_value_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


