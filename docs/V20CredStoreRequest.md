# V20CredStoreRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credential_id** | **str** |  | [optional] 

## Example

```python
from pyaries.models.v20_cred_store_request import V20CredStoreRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredStoreRequest from a JSON string
v20_cred_store_request_instance = V20CredStoreRequest.from_json(json)
# print the JSON string representation of the object
print V20CredStoreRequest.to_json()

# convert the object into a dict
v20_cred_store_request_dict = v20_cred_store_request_instance.to_dict()
# create an instance of V20CredStoreRequest from a dict
v20_cred_store_request_form_dict = v20_cred_store_request.from_dict(v20_cred_store_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


