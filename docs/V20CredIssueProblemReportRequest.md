# V20CredIssueProblemReportRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | 

## Example

```python
from pyaries.models.v20_cred_issue_problem_report_request import V20CredIssueProblemReportRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredIssueProblemReportRequest from a JSON string
v20_cred_issue_problem_report_request_instance = V20CredIssueProblemReportRequest.from_json(json)
# print the JSON string representation of the object
print V20CredIssueProblemReportRequest.to_json()

# convert the object into a dict
v20_cred_issue_problem_report_request_dict = v20_cred_issue_problem_report_request_instance.to_dict()
# create an instance of V20CredIssueProblemReportRequest from a dict
v20_cred_issue_problem_report_request_form_dict = v20_cred_issue_problem_report_request.from_dict(v20_cred_issue_problem_report_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


