# V20PresProposalByFormat


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dif** | [**DIFProofProposal**](DIFProofProposal.md) |  | [optional] 
**indy** | [**IndyProofRequest**](IndyProofRequest.md) |  | [optional] 

## Example

```python
from pyaries.models.v20_pres_proposal_by_format import V20PresProposalByFormat

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresProposalByFormat from a JSON string
v20_pres_proposal_by_format_instance = V20PresProposalByFormat.from_json(json)
# print the JSON string representation of the object
print V20PresProposalByFormat.to_json()

# convert the object into a dict
v20_pres_proposal_by_format_dict = v20_pres_proposal_by_format_instance.to_dict()
# create an instance of V20PresProposalByFormat from a dict
v20_pres_proposal_by_format_form_dict = v20_pres_proposal_by_format.from_dict(v20_pres_proposal_by_format_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


