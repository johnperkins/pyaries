# IndyPrimaryProofEqProof

Indy equality proof

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**a_prime** | **str** |  | [optional] 
**e** | **str** |  | [optional] 
**m** | **Dict[str, str]** |  | [optional] 
**m2** | **str** |  | [optional] 
**revealed_attrs** | **Dict[str, str]** |  | [optional] 
**v** | **str** |  | [optional] 

## Example

```python
from pyaries.models.indy_primary_proof_eq_proof import IndyPrimaryProofEqProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyPrimaryProofEqProof from a JSON string
indy_primary_proof_eq_proof_instance = IndyPrimaryProofEqProof.from_json(json)
# print the JSON string representation of the object
print IndyPrimaryProofEqProof.to_json()

# convert the object into a dict
indy_primary_proof_eq_proof_dict = indy_primary_proof_eq_proof_instance.to_dict()
# create an instance of IndyPrimaryProofEqProof from a dict
indy_primary_proof_eq_proof_form_dict = indy_primary_proof_eq_proof.from_dict(indy_primary_proof_eq_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


