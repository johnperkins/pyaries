# V20CredOfferConnFreeRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_issue** | **bool** | Whether to respond automatically to credential requests, creating and issuing requested credentials | [optional] 
**auto_remove** | **bool** | Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) | [optional] 
**comment** | **str** | Human-readable comment | [optional] 
**credential_preview** | [**V20CredPreview**](V20CredPreview.md) |  | [optional] 
**filter** | [**V20CredFilter**](V20CredFilter.md) |  | 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 

## Example

```python
from pyaries.models.v20_cred_offer_conn_free_request import V20CredOfferConnFreeRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredOfferConnFreeRequest from a JSON string
v20_cred_offer_conn_free_request_instance = V20CredOfferConnFreeRequest.from_json(json)
# print the JSON string representation of the object
print V20CredOfferConnFreeRequest.to_json()

# convert the object into a dict
v20_cred_offer_conn_free_request_dict = v20_cred_offer_conn_free_request_instance.to_dict()
# create an instance of V20CredOfferConnFreeRequest from a dict
v20_cred_offer_conn_free_request_form_dict = v20_cred_offer_conn_free_request.from_dict(v20_cred_offer_conn_free_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


