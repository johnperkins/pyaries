# V20PresentationSendRequestToProposal


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_verify** | **bool** | Verifier choice to auto-verify proof presentation | [optional] 
**trace** | **bool** | Whether to trace event (default false) | [optional] 

## Example

```python
from pyaries.models.v20_presentation_send_request_to_proposal import V20PresentationSendRequestToProposal

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresentationSendRequestToProposal from a JSON string
v20_presentation_send_request_to_proposal_instance = V20PresentationSendRequestToProposal.from_json(json)
# print the JSON string representation of the object
print V20PresentationSendRequestToProposal.to_json()

# convert the object into a dict
v20_presentation_send_request_to_proposal_dict = v20_presentation_send_request_to_proposal_instance.to_dict()
# create an instance of V20PresentationSendRequestToProposal from a dict
v20_presentation_send_request_to_proposal_form_dict = v20_presentation_send_request_to_proposal.from_dict(v20_presentation_send_request_to_proposal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


