# CredDefValuePrimary


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**n** | **str** |  | [optional] 
**r** | [**Generated**](Generated.md) |  | [optional] 
**rctxt** | **str** |  | [optional] 
**s** | **str** |  | [optional] 
**z** | **str** |  | [optional] 

## Example

```python
from pyaries.models.cred_def_value_primary import CredDefValuePrimary

# TODO update the JSON string below
json = "{}"
# create an instance of CredDefValuePrimary from a JSON string
cred_def_value_primary_instance = CredDefValuePrimary.from_json(json)
# print the JSON string representation of the object
print CredDefValuePrimary.to_json()

# convert the object into a dict
cred_def_value_primary_dict = cred_def_value_primary_instance.to_dict()
# create an instance of CredDefValuePrimary from a dict
cred_def_value_primary_form_dict = cred_def_value_primary.from_dict(cred_def_value_primary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


