# V10PresentationSendRequestRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_verify** | **bool** | Verifier choice to auto-verify proof presentation | [optional] 
**comment** | **str** |  | [optional] 
**connection_id** | **str** | Connection identifier | 
**proof_request** | [**IndyProofRequest**](IndyProofRequest.md) |  | 
**trace** | **bool** | Whether to trace event (default false) | [optional] 

## Example

```python
from pyaries.models.v10_presentation_send_request_request import V10PresentationSendRequestRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationSendRequestRequest from a JSON string
v10_presentation_send_request_request_instance = V10PresentationSendRequestRequest.from_json(json)
# print the JSON string representation of the object
print V10PresentationSendRequestRequest.to_json()

# convert the object into a dict
v10_presentation_send_request_request_dict = v10_presentation_send_request_request_instance.to_dict()
# create an instance of V10PresentationSendRequestRequest from a dict
v10_presentation_send_request_request_form_dict = v10_presentation_send_request_request.from_dict(v10_presentation_send_request_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


