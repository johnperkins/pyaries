# MediationDeny


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**mediator_terms** | **List[str]** |  | [optional] 
**recipient_terms** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.mediation_deny import MediationDeny

# TODO update the JSON string below
json = "{}"
# create an instance of MediationDeny from a JSON string
mediation_deny_instance = MediationDeny.from_json(json)
# print the JSON string representation of the object
print MediationDeny.to_json()

# convert the object into a dict
mediation_deny_dict = mediation_deny_instance.to_dict()
# create an instance of MediationDeny from a dict
mediation_deny_form_dict = mediation_deny.from_dict(mediation_deny_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


