# V20CredRequestFreeFilter

Credential specification criteria by format

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ld_proof** | [**V20CredFilterLdProof**](V20CredFilterLdProof.md) |  | 

## Example

```python
from pyaries.models.v20_cred_request_free_filter import V20CredRequestFreeFilter

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredRequestFreeFilter from a JSON string
v20_cred_request_free_filter_instance = V20CredRequestFreeFilter.from_json(json)
# print the JSON string representation of the object
print V20CredRequestFreeFilter.to_json()

# convert the object into a dict
v20_cred_request_free_filter_dict = v20_cred_request_free_filter_instance.to_dict()
# create an instance of V20CredRequestFreeFilter from a dict
v20_cred_request_free_filter_form_dict = v20_cred_request_free_filter.from_dict(v20_cred_request_free_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


