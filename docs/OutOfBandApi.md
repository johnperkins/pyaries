# pyaries.OutOfBandApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_invitation**](OutOfBandApi.md#create_invitation) | **POST** /out-of-band/create-invitation | Create a new connection invitation
[**receive_invitation**](OutOfBandApi.md#receive_invitation) | **POST** /out-of-band/receive-invitation | Receive a new connection invitation


# **create_invitation**
> InvitationRecord create_invitation(auto_accept=auto_accept, multi_use=multi_use, body=body)

Create a new connection invitation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.OutOfBandApi(api_client)
    auto_accept = True # bool | Auto-accept connection (defaults to configuration) (optional)
    multi_use = True # bool | Create invitation for multiple use (default false) (optional)
    body = pyaries.InvitationCreateRequest() # InvitationCreateRequest |  (optional)

    try:
        # Create a new connection invitation
        api_response = api_instance.create_invitation(auto_accept=auto_accept, multi_use=multi_use, body=body)
        print("The response of OutOfBandApi->create_invitation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OutOfBandApi->create_invitation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **auto_accept** | **bool**| Auto-accept connection (defaults to configuration) | [optional] 
 **multi_use** | **bool**| Create invitation for multiple use (default false) | [optional] 
 **body** | [**InvitationCreateRequest**](InvitationCreateRequest.md)|  | [optional] 

### Return type

[**InvitationRecord**](InvitationRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **receive_invitation**
> OobRecord receive_invitation(alias=alias, auto_accept=auto_accept, mediation_id=mediation_id, use_existing_connection=use_existing_connection, body=body)

Receive a new connection invitation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.OutOfBandApi(api_client)
    alias = 'alias_example' # str | Alias for connection (optional)
    auto_accept = True # bool | Auto-accept connection (defaults to configuration) (optional)
    mediation_id = 'mediation_id_example' # str | Identifier for active mediation record to be used (optional)
    use_existing_connection = True # bool | Use an existing connection, if possible (optional)
    body = pyaries.InvitationMessage() # InvitationMessage |  (optional)

    try:
        # Receive a new connection invitation
        api_response = api_instance.receive_invitation(alias=alias, auto_accept=auto_accept, mediation_id=mediation_id, use_existing_connection=use_existing_connection, body=body)
        print("The response of OutOfBandApi->receive_invitation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OutOfBandApi->receive_invitation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alias** | **str**| Alias for connection | [optional] 
 **auto_accept** | **bool**| Auto-accept connection (defaults to configuration) | [optional] 
 **mediation_id** | **str**| Identifier for active mediation record to be used | [optional] 
 **use_existing_connection** | **bool**| Use an existing connection, if possible | [optional] 
 **body** | [**InvitationMessage**](InvitationMessage.md)|  | [optional] 

### Return type

[**OobRecord**](OobRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

