# DIFField


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter** | [**Filter**](Filter.md) |  | [optional] 
**id** | **str** | ID | [optional] 
**path** | **List[str]** |  | [optional] 
**predicate** | **str** | Preference | [optional] 
**purpose** | **str** | Purpose | [optional] 

## Example

```python
from pyaries.models.dif_field import DIFField

# TODO update the JSON string below
json = "{}"
# create an instance of DIFField from a JSON string
dif_field_instance = DIFField.from_json(json)
# print the JSON string representation of the object
print DIFField.to_json()

# convert the object into a dict
dif_field_dict = dif_field_instance.to_dict()
# create an instance of DIFField from a dict
dif_field_form_dict = dif_field.from_dict(dif_field_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


