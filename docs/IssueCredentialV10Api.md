# pyaries.IssueCredentialV10Api

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_credential**](IssueCredentialV10Api.md#create_credential) | **POST** /issue-credential/create | Send holder a credential, automating entire flow
[**create_offer**](IssueCredentialV10Api.md#create_offer) | **POST** /issue-credential/create-offer | Create a credential offer, independent of any proposal or connection
[**delete_record**](IssueCredentialV10Api.md#delete_record) | **DELETE** /issue-credential/records/{cred_ex_id} | Remove an existing credential exchange record
[**get_record**](IssueCredentialV10Api.md#get_record) | **GET** /issue-credential/records/{cred_ex_id} | Fetch a single credential exchange record
[**get_records**](IssueCredentialV10Api.md#get_records) | **GET** /issue-credential/records | Fetch all credential exchange records
[**issue_credential**](IssueCredentialV10Api.md#issue_credential) | **POST** /issue-credential/records/{cred_ex_id}/issue | Send holder a credential
[**issue_credential_automated**](IssueCredentialV10Api.md#issue_credential_automated) | **POST** /issue-credential/send | Send holder a credential, automating entire flow
[**report_problem**](IssueCredentialV10Api.md#report_problem) | **POST** /issue-credential/records/{cred_ex_id}/problem-report | Send a problem report for credential exchange
[**send_offer**](IssueCredentialV10Api.md#send_offer) | **POST** /issue-credential/records/{cred_ex_id}/send-offer | Send holder a credential offer in reference to a proposal with preview
[**send_offer_free**](IssueCredentialV10Api.md#send_offer_free) | **POST** /issue-credential/send-offer | Send holder a credential offer, independent of any proposal
[**send_proposal**](IssueCredentialV10Api.md#send_proposal) | **POST** /issue-credential/send-proposal | Send issuer a credential proposal
[**send_request**](IssueCredentialV10Api.md#send_request) | **POST** /issue-credential/records/{cred_ex_id}/send-request | Send issuer a credential request
[**store_credential**](IssueCredentialV10Api.md#store_credential) | **POST** /issue-credential/records/{cred_ex_id}/store | Store a received credential


# **create_credential**
> V10CredentialExchange create_credential(body=body)

Send holder a credential, automating entire flow

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    body = pyaries.V10CredentialCreate() # V10CredentialCreate |  (optional)

    try:
        # Send holder a credential, automating entire flow
        api_response = api_instance.create_credential(body=body)
        print("The response of IssueCredentialV10Api->create_credential:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->create_credential: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V10CredentialCreate**](V10CredentialCreate.md)|  | [optional] 

### Return type

[**V10CredentialExchange**](V10CredentialExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_offer**
> V10CredentialExchange create_offer(body=body)

Create a credential offer, independent of any proposal or connection

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    body = pyaries.V10CredentialConnFreeOfferRequest() # V10CredentialConnFreeOfferRequest |  (optional)

    try:
        # Create a credential offer, independent of any proposal or connection
        api_response = api_instance.create_offer(body=body)
        print("The response of IssueCredentialV10Api->create_offer:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->create_offer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V10CredentialConnFreeOfferRequest**](V10CredentialConnFreeOfferRequest.md)|  | [optional] 

### Return type

[**V10CredentialExchange**](V10CredentialExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_record**
> object delete_record(cred_ex_id)

Remove an existing credential exchange record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier

    try:
        # Remove an existing credential exchange record
        api_response = api_instance.delete_record(cred_ex_id)
        print("The response of IssueCredentialV10Api->delete_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->delete_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_record**
> V10CredentialExchange get_record(cred_ex_id)

Fetch a single credential exchange record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier

    try:
        # Fetch a single credential exchange record
        api_response = api_instance.get_record(cred_ex_id)
        print("The response of IssueCredentialV10Api->get_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->get_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 

### Return type

[**V10CredentialExchange**](V10CredentialExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_records**
> V10CredentialExchangeListResult get_records(connection_id=connection_id, role=role, state=state, thread_id=thread_id)

Fetch all credential exchange records

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    connection_id = 'connection_id_example' # str | Connection identifier (optional)
    role = 'role_example' # str | Role assigned in credential exchange (optional)
    state = 'state_example' # str | Credential exchange state (optional)
    thread_id = 'thread_id_example' # str | Thread identifier (optional)

    try:
        # Fetch all credential exchange records
        api_response = api_instance.get_records(connection_id=connection_id, role=role, state=state, thread_id=thread_id)
        print("The response of IssueCredentialV10Api->get_records:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->get_records: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connection_id** | **str**| Connection identifier | [optional] 
 **role** | **str**| Role assigned in credential exchange | [optional] 
 **state** | **str**| Credential exchange state | [optional] 
 **thread_id** | **str**| Thread identifier | [optional] 

### Return type

[**V10CredentialExchangeListResult**](V10CredentialExchangeListResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **issue_credential**
> V10CredentialExchange issue_credential(cred_ex_id, body=body)

Send holder a credential

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier
    body = pyaries.V10CredentialIssueRequest() # V10CredentialIssueRequest |  (optional)

    try:
        # Send holder a credential
        api_response = api_instance.issue_credential(cred_ex_id, body=body)
        print("The response of IssueCredentialV10Api->issue_credential:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->issue_credential: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 
 **body** | [**V10CredentialIssueRequest**](V10CredentialIssueRequest.md)|  | [optional] 

### Return type

[**V10CredentialExchange**](V10CredentialExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **issue_credential_automated**
> V10CredentialExchange issue_credential_automated(body=body)

Send holder a credential, automating entire flow

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    body = pyaries.V10CredentialProposalRequestMand() # V10CredentialProposalRequestMand |  (optional)

    try:
        # Send holder a credential, automating entire flow
        api_response = api_instance.issue_credential_automated(body=body)
        print("The response of IssueCredentialV10Api->issue_credential_automated:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->issue_credential_automated: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V10CredentialProposalRequestMand**](V10CredentialProposalRequestMand.md)|  | [optional] 

### Return type

[**V10CredentialExchange**](V10CredentialExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **report_problem**
> object report_problem(cred_ex_id, body=body)

Send a problem report for credential exchange

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier
    body = pyaries.V10CredentialProblemReportRequest() # V10CredentialProblemReportRequest |  (optional)

    try:
        # Send a problem report for credential exchange
        api_response = api_instance.report_problem(cred_ex_id, body=body)
        print("The response of IssueCredentialV10Api->report_problem:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->report_problem: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 
 **body** | [**V10CredentialProblemReportRequest**](V10CredentialProblemReportRequest.md)|  | [optional] 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_offer**
> V10CredentialExchange send_offer(cred_ex_id, body=body)

Send holder a credential offer in reference to a proposal with preview

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier
    body = pyaries.V10CredentialBoundOfferRequest() # V10CredentialBoundOfferRequest |  (optional)

    try:
        # Send holder a credential offer in reference to a proposal with preview
        api_response = api_instance.send_offer(cred_ex_id, body=body)
        print("The response of IssueCredentialV10Api->send_offer:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->send_offer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 
 **body** | [**V10CredentialBoundOfferRequest**](V10CredentialBoundOfferRequest.md)|  | [optional] 

### Return type

[**V10CredentialExchange**](V10CredentialExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_offer_free**
> V10CredentialExchange send_offer_free(body=body)

Send holder a credential offer, independent of any proposal

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    body = pyaries.V10CredentialFreeOfferRequest() # V10CredentialFreeOfferRequest |  (optional)

    try:
        # Send holder a credential offer, independent of any proposal
        api_response = api_instance.send_offer_free(body=body)
        print("The response of IssueCredentialV10Api->send_offer_free:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->send_offer_free: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V10CredentialFreeOfferRequest**](V10CredentialFreeOfferRequest.md)|  | [optional] 

### Return type

[**V10CredentialExchange**](V10CredentialExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_proposal**
> V10CredentialExchange send_proposal(body=body)

Send issuer a credential proposal

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    body = pyaries.V10CredentialProposalRequestOpt() # V10CredentialProposalRequestOpt |  (optional)

    try:
        # Send issuer a credential proposal
        api_response = api_instance.send_proposal(body=body)
        print("The response of IssueCredentialV10Api->send_proposal:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->send_proposal: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V10CredentialProposalRequestOpt**](V10CredentialProposalRequestOpt.md)|  | [optional] 

### Return type

[**V10CredentialExchange**](V10CredentialExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_request**
> V10CredentialExchange send_request(cred_ex_id)

Send issuer a credential request

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier

    try:
        # Send issuer a credential request
        api_response = api_instance.send_request(cred_ex_id)
        print("The response of IssueCredentialV10Api->send_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->send_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 

### Return type

[**V10CredentialExchange**](V10CredentialExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **store_credential**
> V10CredentialExchange store_credential(cred_ex_id, body=body)

Store a received credential

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.IssueCredentialV10Api(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier
    body = pyaries.V10CredentialStoreRequest() # V10CredentialStoreRequest |  (optional)

    try:
        # Store a received credential
        api_response = api_instance.store_credential(cred_ex_id, body=body)
        print("The response of IssueCredentialV10Api->store_credential:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling IssueCredentialV10Api->store_credential: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | 
 **body** | [**V10CredentialStoreRequest**](V10CredentialStoreRequest.md)|  | [optional] 

### Return type

[**V10CredentialExchange**](V10CredentialExchange.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

