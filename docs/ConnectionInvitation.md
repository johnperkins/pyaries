# ConnectionInvitation


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**did** | **str** | DID for connection invitation | [optional] 
**image_url** | **str** | Optional image URL for connection invitation | [optional] 
**label** | **str** | Optional label for connection invitation | [optional] 
**recipient_keys** | **List[str]** | List of recipient keys | [optional] 
**routing_keys** | **List[str]** | List of routing keys | [optional] 
**service_endpoint** | **str** | Service endpoint at which to reach this agent | [optional] 

## Example

```python
from pyaries.models.connection_invitation import ConnectionInvitation

# TODO update the JSON string below
json = "{}"
# create an instance of ConnectionInvitation from a JSON string
connection_invitation_instance = ConnectionInvitation.from_json(json)
# print the JSON string representation of the object
print ConnectionInvitation.to_json()

# convert the object into a dict
connection_invitation_dict = connection_invitation_instance.to_dict()
# create an instance of ConnectionInvitation from a dict
connection_invitation_form_dict = connection_invitation.from_dict(connection_invitation_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


