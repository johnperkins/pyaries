# IssuerRevRegRecordRevocRegDef

Revocation registry definition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_def_id** | **str** | Credential definition identifier | [optional] 
**id** | **str** | Indy revocation registry identifier | [optional] 
**revoc_def_type** | **str** | Revocation registry type (specify CL_ACCUM) | [optional] 
**tag** | **str** | Revocation registry tag | [optional] 
**value** | [**IndyRevRegDefValue**](IndyRevRegDefValue.md) |  | [optional] 
**ver** | **str** | Version of revocation registry definition | [optional] 

## Example

```python
from pyaries.models.issuer_rev_reg_record_revoc_reg_def import IssuerRevRegRecordRevocRegDef

# TODO update the JSON string below
json = "{}"
# create an instance of IssuerRevRegRecordRevocRegDef from a JSON string
issuer_rev_reg_record_revoc_reg_def_instance = IssuerRevRegRecordRevocRegDef.from_json(json)
# print the JSON string representation of the object
print IssuerRevRegRecordRevocRegDef.to_json()

# convert the object into a dict
issuer_rev_reg_record_revoc_reg_def_dict = issuer_rev_reg_record_revoc_reg_def_instance.to_dict()
# create an instance of IssuerRevRegRecordRevocRegDef from a dict
issuer_rev_reg_record_revoc_reg_def_form_dict = issuer_rev_reg_record_revoc_reg_def.from_dict(issuer_rev_reg_record_revoc_reg_def_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


