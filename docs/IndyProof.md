# IndyProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifiers** | [**List[IndyProofIdentifier]**](IndyProofIdentifier.md) | Indy proof.identifiers content | [optional] 
**proof** | [**IndyProofProof**](IndyProofProof.md) |  | [optional] 
**requested_proof** | [**IndyProofRequestedProof**](IndyProofRequestedProof.md) |  | [optional] 

## Example

```python
from pyaries.models.indy_proof import IndyProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProof from a JSON string
indy_proof_instance = IndyProof.from_json(json)
# print the JSON string representation of the object
print IndyProof.to_json()

# convert the object into a dict
indy_proof_dict = indy_proof_instance.to_dict()
# create an instance of IndyProof from a dict
indy_proof_form_dict = indy_proof.from_dict(indy_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


