# IndyProofRequestedProofPredicate


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sub_proof_index** | **int** | Sub-proof index | [optional] 

## Example

```python
from pyaries.models.indy_proof_requested_proof_predicate import IndyProofRequestedProofPredicate

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofRequestedProofPredicate from a JSON string
indy_proof_requested_proof_predicate_instance = IndyProofRequestedProofPredicate.from_json(json)
# print the JSON string representation of the object
print IndyProofRequestedProofPredicate.to_json()

# convert the object into a dict
indy_proof_requested_proof_predicate_dict = indy_proof_requested_proof_predicate_instance.to_dict()
# create an instance of IndyProofRequestedProofPredicate from a dict
indy_proof_requested_proof_predicate_form_dict = indy_proof_requested_proof_predicate.from_dict(indy_proof_requested_proof_predicate_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


