# DIFOptions


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**challenge** | **str** | Challenge protect against replay attack | [optional] 
**domain** | **str** | Domain protect against replay attack | [optional] 

## Example

```python
from pyaries.models.dif_options import DIFOptions

# TODO update the JSON string below
json = "{}"
# create an instance of DIFOptions from a JSON string
dif_options_instance = DIFOptions.from_json(json)
# print the JSON string representation of the object
print DIFOptions.to_json()

# convert the object into a dict
dif_options_dict = dif_options_instance.to_dict()
# create an instance of DIFOptions from a dict
dif_options_form_dict = dif_options.from_dict(dif_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


