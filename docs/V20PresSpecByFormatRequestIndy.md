# V20PresSpecByFormatRequestIndy

Presentation specification for indy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requested_attributes** | [**Dict[str, IndyRequestedCredsRequestedAttr]**](IndyRequestedCredsRequestedAttr.md) | Nested object mapping proof request attribute referents to requested-attribute specifiers | 
**requested_predicates** | [**Dict[str, IndyRequestedCredsRequestedPred]**](IndyRequestedCredsRequestedPred.md) | Nested object mapping proof request predicate referents to requested-predicate specifiers | 
**self_attested_attributes** | **Dict[str, str]** | Self-attested attributes to build into proof | 
**trace** | **bool** | Whether to trace event (default false) | [optional] 

## Example

```python
from pyaries.models.v20_pres_spec_by_format_request_indy import V20PresSpecByFormatRequestIndy

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresSpecByFormatRequestIndy from a JSON string
v20_pres_spec_by_format_request_indy_instance = V20PresSpecByFormatRequestIndy.from_json(json)
# print the JSON string representation of the object
print V20PresSpecByFormatRequestIndy.to_json()

# convert the object into a dict
v20_pres_spec_by_format_request_indy_dict = v20_pres_spec_by_format_request_indy_instance.to_dict()
# create an instance of V20PresSpecByFormatRequestIndy from a dict
v20_pres_spec_by_format_request_indy_form_dict = v20_pres_spec_by_format_request_indy.from_dict(v20_pres_spec_by_format_request_indy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


