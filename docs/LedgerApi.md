# pyaries.LedgerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accept_taa**](LedgerApi.md#accept_taa) | **POST** /ledger/taa/accept | Accept the transaction author agreement
[**fetch_taa**](LedgerApi.md#fetch_taa) | **GET** /ledger/taa | Fetch the current transaction author agreement, if any
[**get_did_endpoint**](LedgerApi.md#get_did_endpoint) | **GET** /ledger/did-endpoint | Get the endpoint for a DID from the ledger.
[**get_did_nym_role**](LedgerApi.md#get_did_nym_role) | **GET** /ledger/get-nym-role | Get the role from the NYM registration of a public DID.
[**get_did_verkey**](LedgerApi.md#get_did_verkey) | **GET** /ledger/did-verkey | Get the verkey for a DID from the ledger.
[**ledger_multiple_config_get**](LedgerApi.md#ledger_multiple_config_get) | **GET** /ledger/multiple/config | Fetch the multiple ledger configuration currently in use
[**ledger_multiple_get_write_ledger_get**](LedgerApi.md#ledger_multiple_get_write_ledger_get) | **GET** /ledger/multiple/get-write-ledger | Fetch the current write ledger
[**register_nym**](LedgerApi.md#register_nym) | **POST** /ledger/register-nym | Send a NYM registration to the ledger.
[**rotate_public_did_keypair**](LedgerApi.md#rotate_public_did_keypair) | **PATCH** /ledger/rotate-public-did-keypair | Rotate key pair for public DID.


# **accept_taa**
> object accept_taa(body=body)

Accept the transaction author agreement

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.LedgerApi(api_client)
    body = pyaries.TAAAccept() # TAAAccept |  (optional)

    try:
        # Accept the transaction author agreement
        api_response = api_instance.accept_taa(body=body)
        print("The response of LedgerApi->accept_taa:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LedgerApi->accept_taa: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TAAAccept**](TAAAccept.md)|  | [optional] 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_taa**
> TAAResult fetch_taa()

Fetch the current transaction author agreement, if any

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.LedgerApi(api_client)

    try:
        # Fetch the current transaction author agreement, if any
        api_response = api_instance.fetch_taa()
        print("The response of LedgerApi->fetch_taa:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LedgerApi->fetch_taa: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**TAAResult**](TAAResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_did_endpoint**
> GetDIDEndpointResponse get_did_endpoint(did, endpoint_type=endpoint_type)

Get the endpoint for a DID from the ledger.

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.LedgerApi(api_client)
    did = 'did_example' # str | DID of interest
    endpoint_type = 'endpoint_type_example' # str | Endpoint type of interest (default 'Endpoint') (optional)

    try:
        # Get the endpoint for a DID from the ledger.
        api_response = api_instance.get_did_endpoint(did, endpoint_type=endpoint_type)
        print("The response of LedgerApi->get_did_endpoint:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LedgerApi->get_did_endpoint: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **str**| DID of interest | 
 **endpoint_type** | **str**| Endpoint type of interest (default &#39;Endpoint&#39;) | [optional] 

### Return type

[**GetDIDEndpointResponse**](GetDIDEndpointResponse.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_did_nym_role**
> GetNymRoleResponse get_did_nym_role(did)

Get the role from the NYM registration of a public DID.

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.LedgerApi(api_client)
    did = 'did_example' # str | DID of interest

    try:
        # Get the role from the NYM registration of a public DID.
        api_response = api_instance.get_did_nym_role(did)
        print("The response of LedgerApi->get_did_nym_role:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LedgerApi->get_did_nym_role: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **str**| DID of interest | 

### Return type

[**GetNymRoleResponse**](GetNymRoleResponse.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_did_verkey**
> GetDIDVerkeyResponse get_did_verkey(did)

Get the verkey for a DID from the ledger.

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.LedgerApi(api_client)
    did = 'did_example' # str | DID of interest

    try:
        # Get the verkey for a DID from the ledger.
        api_response = api_instance.get_did_verkey(did)
        print("The response of LedgerApi->get_did_verkey:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LedgerApi->get_did_verkey: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **str**| DID of interest | 

### Return type

[**GetDIDVerkeyResponse**](GetDIDVerkeyResponse.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ledger_multiple_config_get**
> LedgerConfigList ledger_multiple_config_get()

Fetch the multiple ledger configuration currently in use

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.LedgerApi(api_client)

    try:
        # Fetch the multiple ledger configuration currently in use
        api_response = api_instance.ledger_multiple_config_get()
        print("The response of LedgerApi->ledger_multiple_config_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LedgerApi->ledger_multiple_config_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LedgerConfigList**](LedgerConfigList.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ledger_multiple_get_write_ledger_get**
> WriteLedgerRequest ledger_multiple_get_write_ledger_get()

Fetch the current write ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.LedgerApi(api_client)

    try:
        # Fetch the current write ledger
        api_response = api_instance.ledger_multiple_get_write_ledger_get()
        print("The response of LedgerApi->ledger_multiple_get_write_ledger_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LedgerApi->ledger_multiple_get_write_ledger_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**WriteLedgerRequest**](WriteLedgerRequest.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **register_nym**
> TxnOrRegisterLedgerNymResponse register_nym(did, verkey, alias=alias, conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser, role=role)

Send a NYM registration to the ledger.

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.LedgerApi(api_client)
    did = 'did_example' # str | DID to register
    verkey = 'verkey_example' # str | Verification key
    alias = 'alias_example' # str | Alias (optional)
    conn_id = 'conn_id_example' # str | Connection identifier (optional)
    create_transaction_for_endorser = True # bool | Create Transaction For Endorser's signature (optional)
    role = 'role_example' # str | Role (optional)

    try:
        # Send a NYM registration to the ledger.
        api_response = api_instance.register_nym(did, verkey, alias=alias, conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser, role=role)
        print("The response of LedgerApi->register_nym:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LedgerApi->register_nym: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **str**| DID to register | 
 **verkey** | **str**| Verification key | 
 **alias** | **str**| Alias | [optional] 
 **conn_id** | **str**| Connection identifier | [optional] 
 **create_transaction_for_endorser** | **bool**| Create Transaction For Endorser&#39;s signature | [optional] 
 **role** | **str**| Role | [optional] 

### Return type

[**TxnOrRegisterLedgerNymResponse**](TxnOrRegisterLedgerNymResponse.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rotate_public_did_keypair**
> object rotate_public_did_keypair()

Rotate key pair for public DID.

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.LedgerApi(api_client)

    try:
        # Rotate key pair for public DID.
        api_response = api_instance.rotate_public_did_keypair()
        print("The response of LedgerApi->rotate_public_did_keypair:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LedgerApi->rotate_public_did_keypair: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

