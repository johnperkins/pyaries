# V10PresentationExchangeList


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[V10PresentationExchange]**](V10PresentationExchange.md) | Aries RFC 37 v1.0 presentation exchange records | [optional] 

## Example

```python
from pyaries.models.v10_presentation_exchange_list import V10PresentationExchangeList

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationExchangeList from a JSON string
v10_presentation_exchange_list_instance = V10PresentationExchangeList.from_json(json)
# print the JSON string representation of the object
print V10PresentationExchangeList.to_json()

# convert the object into a dict
v10_presentation_exchange_list_dict = v10_presentation_exchange_list_instance.to_dict()
# create an instance of V10PresentationExchangeList from a dict
v10_presentation_exchange_list_form_dict = v10_presentation_exchange_list.from_dict(v10_presentation_exchange_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


