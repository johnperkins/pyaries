# ConnectionMetadataSetRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | **object** | Dictionary of metadata to set for connection. | 

## Example

```python
from pyaries.models.connection_metadata_set_request import ConnectionMetadataSetRequest

# TODO update the JSON string below
json = "{}"
# create an instance of ConnectionMetadataSetRequest from a JSON string
connection_metadata_set_request_instance = ConnectionMetadataSetRequest.from_json(json)
# print the JSON string representation of the object
print ConnectionMetadataSetRequest.to_json()

# convert the object into a dict
connection_metadata_set_request_dict = connection_metadata_set_request_instance.to_dict()
# create an instance of ConnectionMetadataSetRequest from a dict
connection_metadata_set_request_form_dict = connection_metadata_set_request.from_dict(connection_metadata_set_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


