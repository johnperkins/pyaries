# IndyCredential


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_def_id** | **str** | Credential definition identifier | 
**rev_reg** | **object** | Revocation registry state | [optional] 
**rev_reg_id** | **str** | Revocation registry identifier | [optional] 
**schema_id** | **str** | Schema identifier | 
**signature** | **object** | Credential signature | 
**signature_correctness_proof** | **object** | Credential signature correctness proof | 
**values** | [**Dict[str, IndyAttrValue]**](IndyAttrValue.md) | Credential attributes | 
**witness** | **object** | Witness for revocation proof | [optional] 

## Example

```python
from pyaries.models.indy_credential import IndyCredential

# TODO update the JSON string below
json = "{}"
# create an instance of IndyCredential from a JSON string
indy_credential_instance = IndyCredential.from_json(json)
# print the JSON string representation of the object
print IndyCredential.to_json()

# convert the object into a dict
indy_credential_dict = indy_credential_instance.to_dict()
# create an instance of IndyCredential from a dict
indy_credential_form_dict = indy_credential.from_dict(indy_credential_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


