# ConnectionStaticResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**my_did** | **str** | Local DID | 
**my_endpoint** | **str** | My URL endpoint | 
**my_verkey** | **str** | My verification key | 
**record** | [**ConnRecord**](ConnRecord.md) |  | 
**their_did** | **str** | Remote DID | 
**their_verkey** | **str** | Remote verification key | 

## Example

```python
from pyaries.models.connection_static_result import ConnectionStaticResult

# TODO update the JSON string below
json = "{}"
# create an instance of ConnectionStaticResult from a JSON string
connection_static_result_instance = ConnectionStaticResult.from_json(json)
# print the JSON string representation of the object
print ConnectionStaticResult.to_json()

# convert the object into a dict
connection_static_result_dict = connection_static_result_instance.to_dict()
# create an instance of ConnectionStaticResult from a dict
connection_static_result_form_dict = connection_static_result.from_dict(connection_static_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


