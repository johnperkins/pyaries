# pyaries.RevocationApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clear_pending_revocations**](RevocationApi.md#clear_pending_revocations) | **POST** /revocation/clear-pending-revocations | Clear pending revocations
[**create_registry**](RevocationApi.md#create_registry) | **POST** /revocation/create-registry | Creates a new revocation registry
[**download_tails_file**](RevocationApi.md#download_tails_file) | **GET** /revocation/registry/{rev_reg_id}/tails-file | Download tails file
[**get_active_registry_for_cred_def**](RevocationApi.md#get_active_registry_for_cred_def) | **GET** /revocation/active-registry/{cred_def_id} | Get current active revocation registry by credential definition id
[**get_created_registries**](RevocationApi.md#get_created_registries) | **GET** /revocation/registries/created | Search for matching revocation registries that current agent created
[**get_registry**](RevocationApi.md#get_registry) | **GET** /revocation/registry/{rev_reg_id} | Get revocation registry by revocation registry id
[**get_registry_issued_credentials_count**](RevocationApi.md#get_registry_issued_credentials_count) | **GET** /revocation/registry/{rev_reg_id}/issued | Get number of credentials issued against revocation registry
[**get_revocation_status**](RevocationApi.md#get_revocation_status) | **GET** /revocation/credential-record | Get credential revocation status
[**publish_rev_reg_def**](RevocationApi.md#publish_rev_reg_def) | **POST** /revocation/registry/{rev_reg_id}/definition | Send revocation registry definition to ledger
[**publish_rev_reg_entry**](RevocationApi.md#publish_rev_reg_entry) | **POST** /revocation/registry/{rev_reg_id}/entry | Send revocation registry entry to ledger
[**publish_revocations**](RevocationApi.md#publish_revocations) | **POST** /revocation/publish-revocations | Publish pending revocations to ledger
[**revocation_registry_rev_reg_id_fix_revocation_entry_state_put**](RevocationApi.md#revocation_registry_rev_reg_id_fix_revocation_entry_state_put) | **PUT** /revocation/registry/{rev_reg_id}/fix-revocation-entry-state | Fix revocation state in wallet and return number of updated entries
[**revocation_registry_rev_reg_id_issued_details_get**](RevocationApi.md#revocation_registry_rev_reg_id_issued_details_get) | **GET** /revocation/registry/{rev_reg_id}/issued/details | Get details of credentials issued against revocation registry
[**revocation_registry_rev_reg_id_issued_indy_recs_get**](RevocationApi.md#revocation_registry_rev_reg_id_issued_indy_recs_get) | **GET** /revocation/registry/{rev_reg_id}/issued/indy_recs | Get details of revoked credentials from ledger
[**revoke_credential**](RevocationApi.md#revoke_credential) | **POST** /revocation/revoke | Revoke an issued credential
[**set_registry_state**](RevocationApi.md#set_registry_state) | **PATCH** /revocation/registry/{rev_reg_id}/set-state | Set revocation registry state manually
[**update_registry**](RevocationApi.md#update_registry) | **PATCH** /revocation/registry/{rev_reg_id} | Update revocation registry with new public URI to its tails file
[**upload_tails_file**](RevocationApi.md#upload_tails_file) | **PUT** /revocation/registry/{rev_reg_id}/tails-file | Upload local tails file to server


# **clear_pending_revocations**
> PublishRevocations clear_pending_revocations(body=body)

Clear pending revocations

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    body = pyaries.ClearPendingRevocationsRequest() # ClearPendingRevocationsRequest |  (optional)

    try:
        # Clear pending revocations
        api_response = api_instance.clear_pending_revocations(body=body)
        print("The response of RevocationApi->clear_pending_revocations:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->clear_pending_revocations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ClearPendingRevocationsRequest**](ClearPendingRevocationsRequest.md)|  | [optional] 

### Return type

[**PublishRevocations**](PublishRevocations.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_registry**
> RevRegResult create_registry(body=body)

Creates a new revocation registry

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    body = pyaries.RevRegCreateRequest() # RevRegCreateRequest |  (optional)

    try:
        # Creates a new revocation registry
        api_response = api_instance.create_registry(body=body)
        print("The response of RevocationApi->create_registry:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->create_registry: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RevRegCreateRequest**](RevRegCreateRequest.md)|  | [optional] 

### Return type

[**RevRegResult**](RevRegResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **download_tails_file**
> str download_tails_file(rev_reg_id)

Download tails file

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier

    try:
        # Download tails file
        api_response = api_instance.download_tails_file(rev_reg_id)
        print("The response of RevocationApi->download_tails_file:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->download_tails_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 

### Return type

**str**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | tails file |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_active_registry_for_cred_def**
> RevRegResult get_active_registry_for_cred_def(cred_def_id)

Get current active revocation registry by credential definition id

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    cred_def_id = 'cred_def_id_example' # str | Credential definition identifier

    try:
        # Get current active revocation registry by credential definition id
        api_response = api_instance.get_active_registry_for_cred_def(cred_def_id)
        print("The response of RevocationApi->get_active_registry_for_cred_def:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->get_active_registry_for_cred_def: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_def_id** | **str**| Credential definition identifier | 

### Return type

[**RevRegResult**](RevRegResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_created_registries**
> RevRegsCreated get_created_registries(cred_def_id=cred_def_id, state=state)

Search for matching revocation registries that current agent created

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    cred_def_id = 'cred_def_id_example' # str | Credential definition identifier (optional)
    state = 'state_example' # str | Revocation registry state (optional)

    try:
        # Search for matching revocation registries that current agent created
        api_response = api_instance.get_created_registries(cred_def_id=cred_def_id, state=state)
        print("The response of RevocationApi->get_created_registries:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->get_created_registries: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_def_id** | **str**| Credential definition identifier | [optional] 
 **state** | **str**| Revocation registry state | [optional] 

### Return type

[**RevRegsCreated**](RevRegsCreated.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_registry**
> RevRegResult get_registry(rev_reg_id)

Get revocation registry by revocation registry id

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier

    try:
        # Get revocation registry by revocation registry id
        api_response = api_instance.get_registry(rev_reg_id)
        print("The response of RevocationApi->get_registry:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->get_registry: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 

### Return type

[**RevRegResult**](RevRegResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_registry_issued_credentials_count**
> RevRegIssuedResult get_registry_issued_credentials_count(rev_reg_id)

Get number of credentials issued against revocation registry

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier

    try:
        # Get number of credentials issued against revocation registry
        api_response = api_instance.get_registry_issued_credentials_count(rev_reg_id)
        print("The response of RevocationApi->get_registry_issued_credentials_count:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->get_registry_issued_credentials_count: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 

### Return type

[**RevRegIssuedResult**](RevRegIssuedResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_revocation_status**
> CredRevRecordResult get_revocation_status(cred_ex_id=cred_ex_id, cred_rev_id=cred_rev_id, rev_reg_id=rev_reg_id)

Get credential revocation status

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    cred_ex_id = 'cred_ex_id_example' # str | Credential exchange identifier (optional)
    cred_rev_id = 'cred_rev_id_example' # str | Credential revocation identifier (optional)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation registry identifier (optional)

    try:
        # Get credential revocation status
        api_response = api_instance.get_revocation_status(cred_ex_id=cred_ex_id, cred_rev_id=cred_rev_id, rev_reg_id=rev_reg_id)
        print("The response of RevocationApi->get_revocation_status:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->get_revocation_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_ex_id** | **str**| Credential exchange identifier | [optional] 
 **cred_rev_id** | **str**| Credential revocation identifier | [optional] 
 **rev_reg_id** | **str**| Revocation registry identifier | [optional] 

### Return type

[**CredRevRecordResult**](CredRevRecordResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **publish_rev_reg_def**
> TxnOrRevRegResult publish_rev_reg_def(rev_reg_id, conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser)

Send revocation registry definition to ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier
    conn_id = 'conn_id_example' # str | Connection identifier (optional)
    create_transaction_for_endorser = True # bool | Create Transaction For Endorser's signature (optional)

    try:
        # Send revocation registry definition to ledger
        api_response = api_instance.publish_rev_reg_def(rev_reg_id, conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser)
        print("The response of RevocationApi->publish_rev_reg_def:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->publish_rev_reg_def: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 
 **conn_id** | **str**| Connection identifier | [optional] 
 **create_transaction_for_endorser** | **bool**| Create Transaction For Endorser&#39;s signature | [optional] 

### Return type

[**TxnOrRevRegResult**](TxnOrRevRegResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **publish_rev_reg_entry**
> RevRegResult publish_rev_reg_entry(rev_reg_id, conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser)

Send revocation registry entry to ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier
    conn_id = 'conn_id_example' # str | Connection identifier (optional)
    create_transaction_for_endorser = True # bool | Create Transaction For Endorser's signature (optional)

    try:
        # Send revocation registry entry to ledger
        api_response = api_instance.publish_rev_reg_entry(rev_reg_id, conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser)
        print("The response of RevocationApi->publish_rev_reg_entry:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->publish_rev_reg_entry: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 
 **conn_id** | **str**| Connection identifier | [optional] 
 **create_transaction_for_endorser** | **bool**| Create Transaction For Endorser&#39;s signature | [optional] 

### Return type

[**RevRegResult**](RevRegResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **publish_revocations**
> TxnOrPublishRevocationsResult publish_revocations(body=body)

Publish pending revocations to ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    body = pyaries.PublishRevocations() # PublishRevocations |  (optional)

    try:
        # Publish pending revocations to ledger
        api_response = api_instance.publish_revocations(body=body)
        print("The response of RevocationApi->publish_revocations:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->publish_revocations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PublishRevocations**](PublishRevocations.md)|  | [optional] 

### Return type

[**TxnOrPublishRevocationsResult**](TxnOrPublishRevocationsResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revocation_registry_rev_reg_id_fix_revocation_entry_state_put**
> RevRegWalletUpdatedResult revocation_registry_rev_reg_id_fix_revocation_entry_state_put(rev_reg_id, apply_ledger_update)

Fix revocation state in wallet and return number of updated entries

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier
    apply_ledger_update = True # bool | Apply updated accumulator transaction to ledger

    try:
        # Fix revocation state in wallet and return number of updated entries
        api_response = api_instance.revocation_registry_rev_reg_id_fix_revocation_entry_state_put(rev_reg_id, apply_ledger_update)
        print("The response of RevocationApi->revocation_registry_rev_reg_id_fix_revocation_entry_state_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->revocation_registry_rev_reg_id_fix_revocation_entry_state_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 
 **apply_ledger_update** | **bool**| Apply updated accumulator transaction to ledger | 

### Return type

[**RevRegWalletUpdatedResult**](RevRegWalletUpdatedResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revocation_registry_rev_reg_id_issued_details_get**
> CredRevRecordDetailsResult revocation_registry_rev_reg_id_issued_details_get(rev_reg_id)

Get details of credentials issued against revocation registry

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier

    try:
        # Get details of credentials issued against revocation registry
        api_response = api_instance.revocation_registry_rev_reg_id_issued_details_get(rev_reg_id)
        print("The response of RevocationApi->revocation_registry_rev_reg_id_issued_details_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->revocation_registry_rev_reg_id_issued_details_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 

### Return type

[**CredRevRecordDetailsResult**](CredRevRecordDetailsResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revocation_registry_rev_reg_id_issued_indy_recs_get**
> CredRevIndyRecordsResult revocation_registry_rev_reg_id_issued_indy_recs_get(rev_reg_id)

Get details of revoked credentials from ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier

    try:
        # Get details of revoked credentials from ledger
        api_response = api_instance.revocation_registry_rev_reg_id_issued_indy_recs_get(rev_reg_id)
        print("The response of RevocationApi->revocation_registry_rev_reg_id_issued_indy_recs_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->revocation_registry_rev_reg_id_issued_indy_recs_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 

### Return type

[**CredRevIndyRecordsResult**](CredRevIndyRecordsResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revoke_credential**
> object revoke_credential(body=body)

Revoke an issued credential

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    body = pyaries.RevokeRequest() # RevokeRequest |  (optional)

    try:
        # Revoke an issued credential
        api_response = api_instance.revoke_credential(body=body)
        print("The response of RevocationApi->revoke_credential:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->revoke_credential: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RevokeRequest**](RevokeRequest.md)|  | [optional] 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_registry_state**
> RevRegResult set_registry_state(rev_reg_id, state)

Set revocation registry state manually

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier
    state = 'state_example' # str | Revocation registry state to set

    try:
        # Set revocation registry state manually
        api_response = api_instance.set_registry_state(rev_reg_id, state)
        print("The response of RevocationApi->set_registry_state:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->set_registry_state: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 
 **state** | **str**| Revocation registry state to set | 

### Return type

[**RevRegResult**](RevRegResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_registry**
> RevRegResult update_registry(rev_reg_id, body=body)

Update revocation registry with new public URI to its tails file

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier
    body = pyaries.RevRegUpdateTailsFileUri() # RevRegUpdateTailsFileUri |  (optional)

    try:
        # Update revocation registry with new public URI to its tails file
        api_response = api_instance.update_registry(rev_reg_id, body=body)
        print("The response of RevocationApi->update_registry:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->update_registry: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 
 **body** | [**RevRegUpdateTailsFileUri**](RevRegUpdateTailsFileUri.md)|  | [optional] 

### Return type

[**RevRegResult**](RevRegResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_tails_file**
> object upload_tails_file(rev_reg_id)

Upload local tails file to server

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.RevocationApi(api_client)
    rev_reg_id = 'rev_reg_id_example' # str | Revocation Registry identifier

    try:
        # Upload local tails file to server
        api_response = api_instance.upload_tails_file(rev_reg_id)
        print("The response of RevocationApi->upload_tails_file:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RevocationApi->upload_tails_file: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rev_reg_id** | **str**| Revocation Registry identifier | 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

