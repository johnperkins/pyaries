# IndyProofProofAggregatedProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**c_hash** | **str** | c_hash value | [optional] 
**c_list** | **List[List[int]]** | c_list value | [optional] 

## Example

```python
from pyaries.models.indy_proof_proof_aggregated_proof import IndyProofProofAggregatedProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofProofAggregatedProof from a JSON string
indy_proof_proof_aggregated_proof_instance = IndyProofProofAggregatedProof.from_json(json)
# print the JSON string representation of the object
print IndyProofProofAggregatedProof.to_json()

# convert the object into a dict
indy_proof_proof_aggregated_proof_dict = indy_proof_proof_aggregated_proof_instance.to_dict()
# create an instance of IndyProofProofAggregatedProof from a dict
indy_proof_proof_aggregated_proof_form_dict = indy_proof_proof_aggregated_proof.from_dict(indy_proof_proof_aggregated_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


