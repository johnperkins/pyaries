# V20PresExRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_present** | **bool** | Prover choice to auto-present proof as verifier requests | [optional] 
**auto_verify** | **bool** | Verifier choice to auto-verify proof presentation | [optional] 
**by_format** | [**V20PresExRecordByFormat**](V20PresExRecordByFormat.md) |  | [optional] 
**connection_id** | **str** | Connection identifier | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**error_msg** | **str** | Error message | [optional] 
**initiator** | **str** | Present-proof exchange initiator: self or external | [optional] 
**pres** | [**V20Pres**](V20Pres.md) |  | [optional] 
**pres_ex_id** | **str** | Presentation exchange identifier | [optional] 
**pres_proposal** | [**V20PresProposal**](V20PresProposal.md) |  | [optional] 
**pres_request** | [**V20PresRequest**](V20PresRequest.md) |  | [optional] 
**role** | **str** | Present-proof exchange role: prover or verifier | [optional] 
**state** | **str** | Present-proof exchange state | [optional] 
**thread_id** | **str** | Thread identifier | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 
**verified** | **str** | Whether presentation is verified: &#39;true&#39; or &#39;false&#39; | [optional] 
**verified_msgs** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.v20_pres_ex_record import V20PresExRecord

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresExRecord from a JSON string
v20_pres_ex_record_instance = V20PresExRecord.from_json(json)
# print the JSON string representation of the object
print V20PresExRecord.to_json()

# convert the object into a dict
v20_pres_ex_record_dict = v20_pres_ex_record_instance.to_dict()
# create an instance of V20PresExRecord from a dict
v20_pres_ex_record_form_dict = v20_pres_ex_record.from_dict(v20_pres_ex_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


