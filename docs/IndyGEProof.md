# IndyGEProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alpha** | **str** |  | [optional] 
**mj** | **str** |  | [optional] 
**predicate** | [**IndyGEProofPred**](IndyGEProofPred.md) |  | [optional] 
**r** | **Dict[str, str]** |  | [optional] 
**t** | **Dict[str, str]** |  | [optional] 
**u** | **Dict[str, str]** |  | [optional] 

## Example

```python
from pyaries.models.indy_ge_proof import IndyGEProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyGEProof from a JSON string
indy_ge_proof_instance = IndyGEProof.from_json(json)
# print the JSON string representation of the object
print IndyGEProof.to_json()

# convert the object into a dict
indy_ge_proof_dict = indy_ge_proof_instance.to_dict()
# create an instance of IndyGEProof from a dict
indy_ge_proof_form_dict = indy_ge_proof.from_dict(indy_ge_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


