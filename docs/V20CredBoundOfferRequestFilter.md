# V20CredBoundOfferRequestFilter

Credential specification criteria by format

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**indy** | [**V20CredFilterIndy**](V20CredFilterIndy.md) |  | [optional] 
**ld_proof** | [**V20CredFilterLdProof**](V20CredFilterLdProof.md) |  | [optional] 

## Example

```python
from pyaries.models.v20_cred_bound_offer_request_filter import V20CredBoundOfferRequestFilter

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredBoundOfferRequestFilter from a JSON string
v20_cred_bound_offer_request_filter_instance = V20CredBoundOfferRequestFilter.from_json(json)
# print the JSON string representation of the object
print V20CredBoundOfferRequestFilter.to_json()

# convert the object into a dict
v20_cred_bound_offer_request_filter_dict = v20_cred_bound_offer_request_filter_instance.to_dict()
# create an instance of V20CredBoundOfferRequestFilter from a dict
v20_cred_bound_offer_request_filter_form_dict = v20_cred_bound_offer_request_filter.from_dict(v20_cred_bound_offer_request_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


