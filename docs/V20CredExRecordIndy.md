# V20CredExRecordIndy


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | Time of record creation | [optional] 
**cred_ex_id** | **str** | Corresponding v2.0 credential exchange record identifier | [optional] 
**cred_ex_indy_id** | **str** | Record identifier | [optional] 
**cred_id_stored** | **str** | Credential identifier stored in wallet | [optional] 
**cred_request_metadata** | **object** | Credential request metadata for indy holder | [optional] 
**cred_rev_id** | **str** | Credential revocation identifier within revocation registry | [optional] 
**rev_reg_id** | **str** | Revocation registry identifier | [optional] 
**state** | **str** | Current record state | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.v20_cred_ex_record_indy import V20CredExRecordIndy

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExRecordIndy from a JSON string
v20_cred_ex_record_indy_instance = V20CredExRecordIndy.from_json(json)
# print the JSON string representation of the object
print V20CredExRecordIndy.to_json()

# convert the object into a dict
v20_cred_ex_record_indy_dict = v20_cred_ex_record_indy_instance.to_dict()
# create an instance of V20CredExRecordIndy from a dict
v20_cred_ex_record_indy_form_dict = v20_cred_ex_record_indy.from_dict(v20_cred_ex_record_indy_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


