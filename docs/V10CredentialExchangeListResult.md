# V10CredentialExchangeListResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[V10CredentialExchange]**](V10CredentialExchange.md) | Aries#0036 v1.0 credential exchange records | [optional] 

## Example

```python
from pyaries.models.v10_credential_exchange_list_result import V10CredentialExchangeListResult

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialExchangeListResult from a JSON string
v10_credential_exchange_list_result_instance = V10CredentialExchangeListResult.from_json(json)
# print the JSON string representation of the object
print V10CredentialExchangeListResult.to_json()

# convert the object into a dict
v10_credential_exchange_list_result_dict = v10_credential_exchange_list_result_instance.to_dict()
# create an instance of V10CredentialExchangeListResult from a dict
v10_credential_exchange_list_result_form_dict = v10_credential_exchange_list_result.from_dict(v10_credential_exchange_list_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


