# V10CredentialExchangeCredentialProposalDict

Credential proposal message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**cred_def_id** | **str** |  | [optional] 
**credential_proposal** | [**CredentialPreview**](CredentialPreview.md) |  | [optional] 
**issuer_did** | **str** |  | [optional] 
**schema_id** | **str** |  | [optional] 
**schema_issuer_did** | **str** |  | [optional] 
**schema_name** | **str** |  | [optional] 
**schema_version** | **str** |  | [optional] 

## Example

```python
from pyaries.models.v10_credential_exchange_credential_proposal_dict import V10CredentialExchangeCredentialProposalDict

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialExchangeCredentialProposalDict from a JSON string
v10_credential_exchange_credential_proposal_dict_instance = V10CredentialExchangeCredentialProposalDict.from_json(json)
# print the JSON string representation of the object
print V10CredentialExchangeCredentialProposalDict.to_json()

# convert the object into a dict
v10_credential_exchange_credential_proposal_dict_dict = v10_credential_exchange_credential_proposal_dict_instance.to_dict()
# create an instance of V10CredentialExchangeCredentialProposalDict from a dict
v10_credential_exchange_credential_proposal_dict_form_dict = v10_credential_exchange_credential_proposal_dict.from_dict(v10_credential_exchange_credential_proposal_dict_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


