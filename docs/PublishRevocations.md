# PublishRevocations


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rrid2crid** | **Dict[str, List[str]]** | Credential revocation ids by revocation registry id | [optional] 

## Example

```python
from pyaries.models.publish_revocations import PublishRevocations

# TODO update the JSON string below
json = "{}"
# create an instance of PublishRevocations from a JSON string
publish_revocations_instance = PublishRevocations.from_json(json)
# print the JSON string representation of the object
print PublishRevocations.to_json()

# convert the object into a dict
publish_revocations_dict = publish_revocations_instance.to_dict()
# create an instance of PublishRevocations from a dict
publish_revocations_form_dict = publish_revocations.from_dict(publish_revocations_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


