# AttachDecoratorDataJWS


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**AttachDecoratorDataJWSHeader**](AttachDecoratorDataJWSHeader.md) |  | [optional] 
**protected** | **str** | protected JWS header | [optional] 
**signature** | **str** | signature | [optional] 
**signatures** | [**List[AttachDecoratorData1JWS]**](AttachDecoratorData1JWS.md) | List of signatures | [optional] 

## Example

```python
from pyaries.models.attach_decorator_data_jws import AttachDecoratorDataJWS

# TODO update the JSON string below
json = "{}"
# create an instance of AttachDecoratorDataJWS from a JSON string
attach_decorator_data_jws_instance = AttachDecoratorDataJWS.from_json(json)
# print the JSON string representation of the object
print AttachDecoratorDataJWS.to_json()

# convert the object into a dict
attach_decorator_data_jws_dict = attach_decorator_data_jws_instance.to_dict()
# create an instance of AttachDecoratorDataJWS from a dict
attach_decorator_data_jws_form_dict = attach_decorator_data_jws.from_dict(attach_decorator_data_jws_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


