# V20DiscoveryExchangeResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**V20DiscoveryRecord**](V20DiscoveryRecord.md) |  | [optional] 

## Example

```python
from pyaries.models.v20_discovery_exchange_result import V20DiscoveryExchangeResult

# TODO update the JSON string below
json = "{}"
# create an instance of V20DiscoveryExchangeResult from a JSON string
v20_discovery_exchange_result_instance = V20DiscoveryExchangeResult.from_json(json)
# print the JSON string representation of the object
print V20DiscoveryExchangeResult.to_json()

# convert the object into a dict
v20_discovery_exchange_result_dict = v20_discovery_exchange_result_instance.to_dict()
# create an instance of V20DiscoveryExchangeResult from a dict
v20_discovery_exchange_result_form_dict = v20_discovery_exchange_result.from_dict(v20_discovery_exchange_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


