# V10PresentationSendRequestToProposal


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_verify** | **bool** | Verifier choice to auto-verify proof presentation | [optional] 
**trace** | **bool** | Whether to trace event (default false) | [optional] 

## Example

```python
from pyaries.models.v10_presentation_send_request_to_proposal import V10PresentationSendRequestToProposal

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationSendRequestToProposal from a JSON string
v10_presentation_send_request_to_proposal_instance = V10PresentationSendRequestToProposal.from_json(json)
# print the JSON string representation of the object
print V10PresentationSendRequestToProposal.to_json()

# convert the object into a dict
v10_presentation_send_request_to_proposal_dict = v10_presentation_send_request_to_proposal_instance.to_dict()
# create an instance of V10PresentationSendRequestToProposal from a dict
v10_presentation_send_request_to_proposal_form_dict = v10_presentation_send_request_to_proposal.from_dict(v10_presentation_send_request_to_proposal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


