# CredentialDefinition


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Credential definition identifier | [optional] 
**schema_id** | **str** | Schema identifier within credential definition identifier | [optional] 
**tag** | **str** | Tag within credential definition identifier | [optional] 
**type** | **object** | Signature type: CL for Camenisch-Lysyanskaya | [optional] 
**value** | [**CredDefValue**](CredDefValue.md) |  | [optional] 
**ver** | **str** | Node protocol version | [optional] 

## Example

```python
from pyaries.models.credential_definition import CredentialDefinition

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialDefinition from a JSON string
credential_definition_instance = CredentialDefinition.from_json(json)
# print the JSON string representation of the object
print CredentialDefinition.to_json()

# convert the object into a dict
credential_definition_dict = credential_definition_instance.to_dict()
# create an instance of CredentialDefinition from a dict
credential_definition_form_dict = credential_definition.from_dict(credential_definition_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


