# IndyRequestedCredsRequestedAttr


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_id** | **str** | Wallet credential identifier (typically but not necessarily a UUID) | 
**revealed** | **bool** | Whether to reveal attribute in proof (default true) | [optional] 

## Example

```python
from pyaries.models.indy_requested_creds_requested_attr import IndyRequestedCredsRequestedAttr

# TODO update the JSON string below
json = "{}"
# create an instance of IndyRequestedCredsRequestedAttr from a JSON string
indy_requested_creds_requested_attr_instance = IndyRequestedCredsRequestedAttr.from_json(json)
# print the JSON string representation of the object
print IndyRequestedCredsRequestedAttr.to_json()

# convert the object into a dict
indy_requested_creds_requested_attr_dict = indy_requested_creds_requested_attr_instance.to_dict()
# create an instance of IndyRequestedCredsRequestedAttr from a dict
indy_requested_creds_requested_attr_form_dict = indy_requested_creds_requested_attr.from_dict(indy_requested_creds_requested_attr_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


