# V20CredExRecordCredProposal

Credential proposal message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**credential_preview** | [**V20CredProposalCredentialPreview**](V20CredProposalCredentialPreview.md) |  | [optional] 
**filtersattach** | [**List[AttachDecorator]**](AttachDecorator.md) | Credential filter per acceptable format on corresponding identifier | 
**formats** | [**List[V20CredFormat]**](V20CredFormat.md) | Attachment formats | 

## Example

```python
from pyaries.models.v20_cred_ex_record_cred_proposal import V20CredExRecordCredProposal

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExRecordCredProposal from a JSON string
v20_cred_ex_record_cred_proposal_instance = V20CredExRecordCredProposal.from_json(json)
# print the JSON string representation of the object
print V20CredExRecordCredProposal.to_json()

# convert the object into a dict
v20_cred_ex_record_cred_proposal_dict = v20_cred_ex_record_cred_proposal_instance.to_dict()
# create an instance of V20CredExRecordCredProposal from a dict
v20_cred_ex_record_cred_proposal_form_dict = v20_cred_ex_record_cred_proposal.from_dict(v20_cred_ex_record_cred_proposal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


