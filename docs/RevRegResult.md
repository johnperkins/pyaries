# RevRegResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**IssuerRevRegRecord**](IssuerRevRegRecord.md) |  | [optional] 

## Example

```python
from pyaries.models.rev_reg_result import RevRegResult

# TODO update the JSON string below
json = "{}"
# create an instance of RevRegResult from a JSON string
rev_reg_result_instance = RevRegResult.from_json(json)
# print the JSON string representation of the object
print RevRegResult.to_json()

# convert the object into a dict
rev_reg_result_dict = rev_reg_result_instance.to_dict()
# create an instance of RevRegResult from a dict
rev_reg_result_form_dict = rev_reg_result.from_dict(rev_reg_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


