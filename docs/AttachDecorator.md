# AttachDecorator


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Attachment identifier | [optional] 
**byte_count** | **int** | Byte count of data included by reference | [optional] 
**data** | [**AttachDecoratorData**](AttachDecoratorData.md) |  | 
**description** | **str** | Human-readable description of content | [optional] 
**filename** | **str** | File name | [optional] 
**lastmod_time** | **str** | Hint regarding last modification datetime, in ISO-8601 format | [optional] 
**mime_type** | **str** | MIME type | [optional] 

## Example

```python
from pyaries.models.attach_decorator import AttachDecorator

# TODO update the JSON string below
json = "{}"
# create an instance of AttachDecorator from a JSON string
attach_decorator_instance = AttachDecorator.from_json(json)
# print the JSON string representation of the object
print AttachDecorator.to_json()

# convert the object into a dict
attach_decorator_dict = attach_decorator_instance.to_dict()
# create an instance of AttachDecorator from a dict
attach_decorator_form_dict = attach_decorator.from_dict(attach_decorator_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


