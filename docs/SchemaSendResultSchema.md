# SchemaSendResultSchema

Schema definition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attr_names** | **List[str]** | Schema attribute names | [optional] 
**id** | **str** | Schema identifier | [optional] 
**name** | **str** | Schema name | [optional] 
**seq_no** | **int** | Schema sequence number | [optional] 
**ver** | **str** | Node protocol version | [optional] 
**version** | **str** | Schema version | [optional] 

## Example

```python
from pyaries.models.schema_send_result_schema import SchemaSendResultSchema

# TODO update the JSON string below
json = "{}"
# create an instance of SchemaSendResultSchema from a JSON string
schema_send_result_schema_instance = SchemaSendResultSchema.from_json(json)
# print the JSON string representation of the object
print SchemaSendResultSchema.to_json()

# convert the object into a dict
schema_send_result_schema_dict = schema_send_result_schema_instance.to_dict()
# create an instance of SchemaSendResultSchema from a dict
schema_send_result_schema_form_dict = schema_send_result_schema.from_dict(schema_send_result_schema_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


