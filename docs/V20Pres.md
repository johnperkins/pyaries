# V20Pres


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**formats** | [**List[V20PresFormat]**](V20PresFormat.md) | Acceptable attachment formats | 
**presentationsattach** | [**List[AttachDecorator]**](AttachDecorator.md) |  | 

## Example

```python
from pyaries.models.v20_pres import V20Pres

# TODO update the JSON string below
json = "{}"
# create an instance of V20Pres from a JSON string
v20_pres_instance = V20Pres.from_json(json)
# print the JSON string representation of the object
print V20Pres.to_json()

# convert the object into a dict
v20_pres_dict = v20_pres_instance.to_dict()
# create an instance of V20Pres from a dict
v20_pres_form_dict = v20_pres.from_dict(v20_pres_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


