# V20CredBoundOfferRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**counter_preview** | [**V20CredPreview**](V20CredPreview.md) |  | [optional] 
**filter** | [**V20CredFilter**](V20CredFilter.md) |  | [optional] 

## Example

```python
from pyaries.models.v20_cred_bound_offer_request import V20CredBoundOfferRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredBoundOfferRequest from a JSON string
v20_cred_bound_offer_request_instance = V20CredBoundOfferRequest.from_json(json)
# print the JSON string representation of the object
print V20CredBoundOfferRequest.to_json()

# convert the object into a dict
v20_cred_bound_offer_request_dict = v20_cred_bound_offer_request_instance.to_dict()
# create an instance of V20CredBoundOfferRequest from a dict
v20_cred_bound_offer_request_form_dict = v20_cred_bound_offer_request.from_dict(v20_cred_bound_offer_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


