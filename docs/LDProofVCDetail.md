# LDProofVCDetail


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credential** | [**Credential**](Credential.md) |  | 
**options** | [**LDProofVCDetailOptions**](LDProofVCDetailOptions.md) |  | 

## Example

```python
from pyaries.models.ld_proof_vc_detail import LDProofVCDetail

# TODO update the JSON string below
json = "{}"
# create an instance of LDProofVCDetail from a JSON string
ld_proof_vc_detail_instance = LDProofVCDetail.from_json(json)
# print the JSON string representation of the object
print LDProofVCDetail.to_json()

# convert the object into a dict
ld_proof_vc_detail_dict = ld_proof_vc_detail_instance.to_dict()
# create an instance of LDProofVCDetail from a dict
ld_proof_vc_detail_form_dict = ld_proof_vc_detail.from_dict(ld_proof_vc_detail_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


