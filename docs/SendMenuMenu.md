# SendMenuMenu

Menu to send to connection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | Introductory text for the menu | [optional] 
**errormsg** | **str** | Optional error message to display in menu header | [optional] 
**options** | [**List[MenuOption]**](MenuOption.md) | List of menu options | 
**title** | **str** | Menu title | [optional] 

## Example

```python
from pyaries.models.send_menu_menu import SendMenuMenu

# TODO update the JSON string below
json = "{}"
# create an instance of SendMenuMenu from a JSON string
send_menu_menu_instance = SendMenuMenu.from_json(json)
# print the JSON string representation of the object
print SendMenuMenu.to_json()

# convert the object into a dict
send_menu_menu_dict = send_menu_menu_instance.to_dict()
# create an instance of SendMenuMenu from a dict
send_menu_menu_form_dict = send_menu_menu.from_dict(send_menu_menu_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


