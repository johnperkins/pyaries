# IssuerCredRevRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | Time of record creation | [optional] 
**cred_def_id** | **str** | Credential definition identifier | [optional] 
**cred_ex_id** | **str** | Credential exchange record identifier at credential issue | [optional] 
**cred_rev_id** | **str** | Credential revocation identifier | [optional] 
**record_id** | **str** | Issuer credential revocation record identifier | [optional] 
**rev_reg_id** | **str** | Revocation registry identifier | [optional] 
**state** | **str** | Issue credential revocation record state | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.issuer_cred_rev_record import IssuerCredRevRecord

# TODO update the JSON string below
json = "{}"
# create an instance of IssuerCredRevRecord from a JSON string
issuer_cred_rev_record_instance = IssuerCredRevRecord.from_json(json)
# print the JSON string representation of the object
print IssuerCredRevRecord.to_json()

# convert the object into a dict
issuer_cred_rev_record_dict = issuer_cred_rev_record_instance.to_dict()
# create an instance of IssuerCredRevRecord from a dict
issuer_cred_rev_record_form_dict = issuer_cred_rev_record.from_dict(issuer_cred_rev_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


