# V10PresentationProposalRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_present** | **bool** | Whether to respond automatically to presentation requests, building and presenting requested proof | [optional] 
**comment** | **str** | Human-readable comment | [optional] 
**connection_id** | **str** | Connection identifier | 
**presentation_proposal** | [**IndyPresPreview**](IndyPresPreview.md) |  | 
**trace** | **bool** | Whether to trace event (default false) | [optional] 

## Example

```python
from pyaries.models.v10_presentation_proposal_request import V10PresentationProposalRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationProposalRequest from a JSON string
v10_presentation_proposal_request_instance = V10PresentationProposalRequest.from_json(json)
# print the JSON string representation of the object
print V10PresentationProposalRequest.to_json()

# convert the object into a dict
v10_presentation_proposal_request_dict = v10_presentation_proposal_request_instance.to_dict()
# create an instance of V10PresentationProposalRequest from a dict
v10_presentation_proposal_request_form_dict = v10_presentation_proposal_request.from_dict(v10_presentation_proposal_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


