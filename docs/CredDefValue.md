# CredDefValue


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**primary** | [**CredDefValuePrimary**](CredDefValuePrimary.md) |  | [optional] 
**revocation** | [**CredDefValueRevocation**](CredDefValueRevocation.md) |  | [optional] 

## Example

```python
from pyaries.models.cred_def_value import CredDefValue

# TODO update the JSON string below
json = "{}"
# create an instance of CredDefValue from a JSON string
cred_def_value_instance = CredDefValue.from_json(json)
# print the JSON string representation of the object
print CredDefValue.to_json()

# convert the object into a dict
cred_def_value_dict = cred_def_value_instance.to_dict()
# create an instance of CredDefValue from a dict
cred_def_value_form_dict = cred_def_value.from_dict(cred_def_value_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


