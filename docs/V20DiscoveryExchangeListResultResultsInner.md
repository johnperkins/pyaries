# V20DiscoveryExchangeListResultResultsInner

Discover Features v2.0 exchange record

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connection_id** | **str** | Connection identifier | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**disclosures** | [**V20DiscoveryRecordDisclosures**](V20DiscoveryRecordDisclosures.md) |  | [optional] 
**discovery_exchange_id** | **str** | Credential exchange identifier | [optional] 
**queries_msg** | [**V20DiscoveryRecordQueriesMsg**](V20DiscoveryRecordQueriesMsg.md) |  | [optional] 
**state** | **str** | Current record state | [optional] 
**thread_id** | **str** | Thread identifier | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.v20_discovery_exchange_list_result_results_inner import V20DiscoveryExchangeListResultResultsInner

# TODO update the JSON string below
json = "{}"
# create an instance of V20DiscoveryExchangeListResultResultsInner from a JSON string
v20_discovery_exchange_list_result_results_inner_instance = V20DiscoveryExchangeListResultResultsInner.from_json(json)
# print the JSON string representation of the object
print V20DiscoveryExchangeListResultResultsInner.to_json()

# convert the object into a dict
v20_discovery_exchange_list_result_results_inner_dict = v20_discovery_exchange_list_result_results_inner_instance.to_dict()
# create an instance of V20DiscoveryExchangeListResultResultsInner from a dict
v20_discovery_exchange_list_result_results_inner_form_dict = v20_discovery_exchange_list_result_results_inner.from_dict(v20_discovery_exchange_list_result_results_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


