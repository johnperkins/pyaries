# IndyKeyCorrectnessProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**c** | **str** | c in key correctness proof | 
**xr_cap** | **List[List[str]]** | xr_cap in key correctness proof | 
**xz_cap** | **str** | xz_cap in key correctness proof | 

## Example

```python
from pyaries.models.indy_key_correctness_proof import IndyKeyCorrectnessProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyKeyCorrectnessProof from a JSON string
indy_key_correctness_proof_instance = IndyKeyCorrectnessProof.from_json(json)
# print the JSON string representation of the object
print IndyKeyCorrectnessProof.to_json()

# convert the object into a dict
indy_key_correctness_proof_dict = indy_key_correctness_proof_instance.to_dict()
# create an instance of IndyKeyCorrectnessProof from a dict
indy_key_correctness_proof_form_dict = indy_key_correctness_proof.from_dict(indy_key_correctness_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


