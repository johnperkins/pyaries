# VCRecordList


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[VCRecord]**](VCRecord.md) |  | [optional] 

## Example

```python
from pyaries.models.vc_record_list import VCRecordList

# TODO update the JSON string below
json = "{}"
# create an instance of VCRecordList from a JSON string
vc_record_list_instance = VCRecordList.from_json(json)
# print the JSON string representation of the object
print VCRecordList.to_json()

# convert the object into a dict
vc_record_list_dict = vc_record_list_instance.to_dict()
# create an instance of VCRecordList from a dict
vc_record_list_form_dict = vc_record_list.from_dict(vc_record_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


