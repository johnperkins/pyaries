# ConnRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accept** | **str** | Connection acceptance: manual or auto | [optional] 
**alias** | **str** | Optional alias to apply to connection for later use | [optional] 
**connection_id** | **str** | Connection identifier | [optional] 
**connection_protocol** | **str** | Connection protocol used | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**error_msg** | **str** | Error message | [optional] 
**inbound_connection_id** | **str** | Inbound routing connection id to use | [optional] 
**invitation_key** | **str** | Public key for connection | [optional] 
**invitation_mode** | **str** | Invitation mode | [optional] 
**invitation_msg_id** | **str** | ID of out-of-band invitation message | [optional] 
**my_did** | **str** | Our DID for connection | [optional] 
**request_id** | **str** | Connection request identifier | [optional] 
**rfc23_state** | **str** | State per RFC 23 | [optional] [readonly] 
**routing_state** | **str** | Routing state of connection | [optional] 
**state** | **str** | Current record state | [optional] 
**their_did** | **str** | Their DID for connection | [optional] 
**their_label** | **str** | Their label for connection | [optional] 
**their_public_did** | **str** | Other agent&#39;s public DID for connection | [optional] 
**their_role** | **str** | Their role in the connection protocol | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.conn_record import ConnRecord

# TODO update the JSON string below
json = "{}"
# create an instance of ConnRecord from a JSON string
conn_record_instance = ConnRecord.from_json(json)
# print the JSON string representation of the object
print ConnRecord.to_json()

# convert the object into a dict
conn_record_dict = conn_record_instance.to_dict()
# create an instance of ConnRecord from a dict
conn_record_form_dict = conn_record.from_dict(conn_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


