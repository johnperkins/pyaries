# PresentationProposal


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**presentation_proposal** | [**IndyPresPreview**](IndyPresPreview.md) |  | 

## Example

```python
from pyaries.models.presentation_proposal import PresentationProposal

# TODO update the JSON string below
json = "{}"
# create an instance of PresentationProposal from a JSON string
presentation_proposal_instance = PresentationProposal.from_json(json)
# print the JSON string representation of the object
print PresentationProposal.to_json()

# convert the object into a dict
presentation_proposal_dict = presentation_proposal_instance.to_dict()
# create an instance of PresentationProposal from a dict
presentation_proposal_form_dict = presentation_proposal.from_dict(presentation_proposal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


