# Disclosures


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**disclosures** | **List[object]** | List of protocol or goal_code descriptors | 

## Example

```python
from pyaries.models.disclosures import Disclosures

# TODO update the JSON string below
json = "{}"
# create an instance of Disclosures from a JSON string
disclosures_instance = Disclosures.from_json(json)
# print the JSON string representation of the object
print Disclosures.to_json()

# convert the object into a dict
disclosures_dict = disclosures_instance.to_dict()
# create an instance of Disclosures from a dict
disclosures_form_dict = disclosures.from_dict(disclosures_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


