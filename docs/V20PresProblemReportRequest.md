# V20PresProblemReportRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | 

## Example

```python
from pyaries.models.v20_pres_problem_report_request import V20PresProblemReportRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresProblemReportRequest from a JSON string
v20_pres_problem_report_request_instance = V20PresProblemReportRequest.from_json(json)
# print the JSON string representation of the object
print V20PresProblemReportRequest.to_json()

# convert the object into a dict
v20_pres_problem_report_request_dict = v20_pres_problem_report_request_instance.to_dict()
# create an instance of V20PresProblemReportRequest from a dict
v20_pres_problem_report_request_form_dict = v20_pres_problem_report_request.from_dict(v20_pres_problem_report_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


