# V20CredExRecordCredOffer

Credential offer message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**credential_preview** | [**V20CredPreview**](V20CredPreview.md) |  | [optional] 
**formats** | [**List[V20CredFormat]**](V20CredFormat.md) | Acceptable credential formats | 
**offersattach** | [**List[AttachDecorator]**](AttachDecorator.md) | Offer attachments | 
**replacement_id** | **str** | Issuer-unique identifier to coordinate credential replacement | [optional] 

## Example

```python
from pyaries.models.v20_cred_ex_record_cred_offer import V20CredExRecordCredOffer

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExRecordCredOffer from a JSON string
v20_cred_ex_record_cred_offer_instance = V20CredExRecordCredOffer.from_json(json)
# print the JSON string representation of the object
print V20CredExRecordCredOffer.to_json()

# convert the object into a dict
v20_cred_ex_record_cred_offer_dict = v20_cred_ex_record_cred_offer_instance.to_dict()
# create an instance of V20CredExRecordCredOffer from a dict
v20_cred_ex_record_cred_offer_form_dict = v20_cred_ex_record_cred_offer.from_dict(v20_cred_ex_record_cred_offer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


