# SchemasInputDescriptorFilter


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**oneof_filter** | **bool** | oneOf | [optional] 
**uri_groups** | **List[List[SchemaInputDescriptor]]** |  | [optional] 

## Example

```python
from pyaries.models.schemas_input_descriptor_filter import SchemasInputDescriptorFilter

# TODO update the JSON string below
json = "{}"
# create an instance of SchemasInputDescriptorFilter from a JSON string
schemas_input_descriptor_filter_instance = SchemasInputDescriptorFilter.from_json(json)
# print the JSON string representation of the object
print SchemasInputDescriptorFilter.to_json()

# convert the object into a dict
schemas_input_descriptor_filter_dict = schemas_input_descriptor_filter_instance.to_dict()
# create an instance of SchemasInputDescriptorFilter from a dict
schemas_input_descriptor_filter_form_dict = schemas_input_descriptor_filter.from_dict(schemas_input_descriptor_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


