# V20CredFilter


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**indy** | [**V20CredFilterIndy**](V20CredFilterIndy.md) |  | [optional] 
**ld_proof** | [**LDProofVCDetail**](LDProofVCDetail.md) |  | [optional] 

## Example

```python
from pyaries.models.v20_cred_filter import V20CredFilter

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredFilter from a JSON string
v20_cred_filter_instance = V20CredFilter.from_json(json)
# print the JSON string representation of the object
print V20CredFilter.to_json()

# convert the object into a dict
v20_cred_filter_dict = v20_cred_filter_instance.to_dict()
# create an instance of V20CredFilter from a dict
v20_cred_filter_form_dict = v20_cred_filter.from_dict(v20_cred_filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


