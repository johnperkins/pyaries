# CreateWalletTokenResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **str** | Authorization token to authenticate wallet requests | [optional] 

## Example

```python
from pyaries.models.create_wallet_token_response import CreateWalletTokenResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreateWalletTokenResponse from a JSON string
create_wallet_token_response_instance = CreateWalletTokenResponse.from_json(json)
# print the JSON string representation of the object
print CreateWalletTokenResponse.to_json()

# convert the object into a dict
create_wallet_token_response_dict = create_wallet_token_response_instance.to_dict()
# create an instance of CreateWalletTokenResponse from a dict
create_wallet_token_response_form_dict = create_wallet_token_response.from_dict(create_wallet_token_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


