# V10CredentialExchangeCredential

Credential as stored

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attrs** | **Dict[str, str]** | Attribute names and value | [optional] 
**cred_def_id** | **str** | Credential definition identifier | [optional] 
**cred_rev_id** | **str** | Credential revocation identifier | [optional] 
**referent** | **str** | Wallet referent | [optional] 
**rev_reg_id** | **str** | Revocation registry identifier | [optional] 
**schema_id** | **str** | Schema identifier | [optional] 

## Example

```python
from pyaries.models.v10_credential_exchange_credential import V10CredentialExchangeCredential

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialExchangeCredential from a JSON string
v10_credential_exchange_credential_instance = V10CredentialExchangeCredential.from_json(json)
# print the JSON string representation of the object
print V10CredentialExchangeCredential.to_json()

# convert the object into a dict
v10_credential_exchange_credential_dict = v10_credential_exchange_credential_instance.to_dict()
# create an instance of V10CredentialExchangeCredential from a dict
v10_credential_exchange_credential_form_dict = v10_credential_exchange_credential.from_dict(v10_credential_exchange_credential_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


