# AMLRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aml** | **Dict[str, str]** |  | [optional] 
**aml_context** | **str** |  | [optional] 
**version** | **str** |  | [optional] 

## Example

```python
from pyaries.models.aml_record import AMLRecord

# TODO update the JSON string below
json = "{}"
# create an instance of AMLRecord from a JSON string
aml_record_instance = AMLRecord.from_json(json)
# print the JSON string representation of the object
print AMLRecord.to_json()

# convert the object into a dict
aml_record_dict = aml_record_instance.to_dict()
# create an instance of AMLRecord from a dict
aml_record_form_dict = aml_record.from_dict(aml_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


