# ActionMenuFetchResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**Menu**](Menu.md) |  | [optional] 

## Example

```python
from pyaries.models.action_menu_fetch_result import ActionMenuFetchResult

# TODO update the JSON string below
json = "{}"
# create an instance of ActionMenuFetchResult from a JSON string
action_menu_fetch_result_instance = ActionMenuFetchResult.from_json(json)
# print the JSON string representation of the object
print ActionMenuFetchResult.to_json()

# convert the object into a dict
action_menu_fetch_result_dict = action_menu_fetch_result_instance.to_dict()
# create an instance of ActionMenuFetchResult from a dict
action_menu_fetch_result_form_dict = action_menu_fetch_result.from_dict(action_menu_fetch_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


