# IndyProofRequestedProofRevealedAttr


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encoded** | **str** | Encoded value | [optional] 
**raw** | **str** | Raw value | [optional] 
**sub_proof_index** | **int** | Sub-proof index | [optional] 

## Example

```python
from pyaries.models.indy_proof_requested_proof_revealed_attr import IndyProofRequestedProofRevealedAttr

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofRequestedProofRevealedAttr from a JSON string
indy_proof_requested_proof_revealed_attr_instance = IndyProofRequestedProofRevealedAttr.from_json(json)
# print the JSON string representation of the object
print IndyProofRequestedProofRevealedAttr.to_json()

# convert the object into a dict
indy_proof_requested_proof_revealed_attr_dict = indy_proof_requested_proof_revealed_attr_instance.to_dict()
# create an instance of IndyProofRequestedProofRevealedAttr from a dict
indy_proof_requested_proof_revealed_attr_form_dict = indy_proof_requested_proof_revealed_attr.from_dict(indy_proof_requested_proof_revealed_attr_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


