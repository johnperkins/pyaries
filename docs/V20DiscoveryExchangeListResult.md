# V20DiscoveryExchangeListResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[V20DiscoveryRecord]**](V20DiscoveryRecord.md) |  | [optional] 

## Example

```python
from pyaries.models.v20_discovery_exchange_list_result import V20DiscoveryExchangeListResult

# TODO update the JSON string below
json = "{}"
# create an instance of V20DiscoveryExchangeListResult from a JSON string
v20_discovery_exchange_list_result_instance = V20DiscoveryExchangeListResult.from_json(json)
# print the JSON string representation of the object
print V20DiscoveryExchangeListResult.to_json()

# convert the object into a dict
v20_discovery_exchange_list_result_dict = v20_discovery_exchange_list_result_instance.to_dict()
# create an instance of V20DiscoveryExchangeListResult from a dict
v20_discovery_exchange_list_result_form_dict = v20_discovery_exchange_list_result.from_dict(v20_discovery_exchange_list_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


