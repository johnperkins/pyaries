# Queries


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**queries** | [**List[QueryItem]**](QueryItem.md) |  | [optional] 

## Example

```python
from pyaries.models.queries import Queries

# TODO update the JSON string below
json = "{}"
# create an instance of Queries from a JSON string
queries_instance = Queries.from_json(json)
# print the JSON string representation of the object
print Queries.to_json()

# convert the object into a dict
queries_dict = queries_instance.to_dict()
# create an instance of Queries from a dict
queries_form_dict = queries.from_dict(queries_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


