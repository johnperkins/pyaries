# V20CredProposal


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**credential_preview** | [**V20CredPreview**](V20CredPreview.md) |  | [optional] 
**filtersattach** | [**List[AttachDecorator]**](AttachDecorator.md) | Credential filter per acceptable format on corresponding identifier | 
**formats** | [**List[V20CredFormat]**](V20CredFormat.md) | Attachment formats | 

## Example

```python
from pyaries.models.v20_cred_proposal import V20CredProposal

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredProposal from a JSON string
v20_cred_proposal_instance = V20CredProposal.from_json(json)
# print the JSON string representation of the object
print V20CredProposal.to_json()

# convert the object into a dict
v20_cred_proposal_dict = v20_cred_proposal_instance.to_dict()
# create an instance of V20CredProposal from a dict
v20_cred_proposal_form_dict = v20_cred_proposal.from_dict(v20_cred_proposal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


