# SignedDocProof

Linked data proof

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**challenge** | **str** |  | [optional] 
**domain** | **str** |  | [optional] 
**proof_purpose** | **str** |  | 
**type** | **str** |  | [optional] 
**verification_method** | **str** |  | 

## Example

```python
from pyaries.models.signed_doc_proof import SignedDocProof

# TODO update the JSON string below
json = "{}"
# create an instance of SignedDocProof from a JSON string
signed_doc_proof_instance = SignedDocProof.from_json(json)
# print the JSON string representation of the object
print SignedDocProof.to_json()

# convert the object into a dict
signed_doc_proof_dict = signed_doc_proof_instance.to_dict()
# create an instance of SignedDocProof from a dict
signed_doc_proof_form_dict = signed_doc_proof.from_dict(signed_doc_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


