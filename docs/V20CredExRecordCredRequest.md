# V20CredExRecordCredRequest

Serialized credential request message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**formats** | [**List[V20CredFormat]**](V20CredFormat.md) | Acceptable attachment formats | 
**requestsattach** | [**List[AttachDecorator]**](AttachDecorator.md) | Request attachments | 

## Example

```python
from pyaries.models.v20_cred_ex_record_cred_request import V20CredExRecordCredRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExRecordCredRequest from a JSON string
v20_cred_ex_record_cred_request_instance = V20CredExRecordCredRequest.from_json(json)
# print the JSON string representation of the object
print V20CredExRecordCredRequest.to_json()

# convert the object into a dict
v20_cred_ex_record_cred_request_dict = v20_cred_ex_record_cred_request_instance.to_dict()
# create an instance of V20CredExRecordCredRequest from a dict
v20_cred_ex_record_cred_request_form_dict = v20_cred_ex_record_cred_request.from_dict(v20_cred_ex_record_cred_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


