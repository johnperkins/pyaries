# IndyRevRegDefValuePublicKeys


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accum_key** | [**IndyRevRegDefValuePublicKeysAccumKey**](IndyRevRegDefValuePublicKeysAccumKey.md) |  | [optional] 

## Example

```python
from pyaries.models.indy_rev_reg_def_value_public_keys import IndyRevRegDefValuePublicKeys

# TODO update the JSON string below
json = "{}"
# create an instance of IndyRevRegDefValuePublicKeys from a JSON string
indy_rev_reg_def_value_public_keys_instance = IndyRevRegDefValuePublicKeys.from_json(json)
# print the JSON string representation of the object
print IndyRevRegDefValuePublicKeys.to_json()

# convert the object into a dict
indy_rev_reg_def_value_public_keys_dict = indy_rev_reg_def_value_public_keys_instance.to_dict()
# create an instance of IndyRevRegDefValuePublicKeys from a dict
indy_rev_reg_def_value_public_keys_form_dict = indy_rev_reg_def_value_public_keys.from_dict(indy_rev_reg_def_value_public_keys_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


