# V10PresentationCreateRequestRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_verify** | **bool** | Verifier choice to auto-verify proof presentation | [optional] 
**comment** | **str** |  | [optional] 
**proof_request** | [**IndyProofRequest**](IndyProofRequest.md) |  | 
**trace** | **bool** | Whether to trace event (default false) | [optional] 

## Example

```python
from pyaries.models.v10_presentation_create_request_request import V10PresentationCreateRequestRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationCreateRequestRequest from a JSON string
v10_presentation_create_request_request_instance = V10PresentationCreateRequestRequest.from_json(json)
# print the JSON string representation of the object
print V10PresentationCreateRequestRequest.to_json()

# convert the object into a dict
v10_presentation_create_request_request_dict = v10_presentation_create_request_request_instance.to_dict()
# create an instance of V10PresentationCreateRequestRequest from a dict
v10_presentation_create_request_request_form_dict = v10_presentation_create_request_request.from_dict(v10_presentation_create_request_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


