# IndyAttrValue


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encoded** | **str** | Attribute encoded value | 
**raw** | **str** | Attribute raw value | 

## Example

```python
from pyaries.models.indy_attr_value import IndyAttrValue

# TODO update the JSON string below
json = "{}"
# create an instance of IndyAttrValue from a JSON string
indy_attr_value_instance = IndyAttrValue.from_json(json)
# print the JSON string representation of the object
print IndyAttrValue.to_json()

# convert the object into a dict
indy_attr_value_dict = indy_attr_value_instance.to_dict()
# create an instance of IndyAttrValue from a dict
indy_attr_value_form_dict = indy_attr_value.from_dict(indy_attr_value_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


