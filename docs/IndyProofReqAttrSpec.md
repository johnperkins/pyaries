# IndyProofReqAttrSpec


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Attribute name | [optional] 
**names** | **List[str]** | Attribute name group | [optional] 
**non_revoked** | [**IndyProofReqAttrSpecNonRevoked**](IndyProofReqAttrSpecNonRevoked.md) |  | [optional] 
**restrictions** | **List[Dict[str, str]]** | If present, credential must satisfy one of given restrictions: specify schema_id, schema_issuer_did, schema_name, schema_version, issuer_did, cred_def_id, and/or attr::&lt;attribute-name&gt;::value where &lt;attribute-name&gt; represents a credential attribute name | [optional] 

## Example

```python
from pyaries.models.indy_proof_req_attr_spec import IndyProofReqAttrSpec

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofReqAttrSpec from a JSON string
indy_proof_req_attr_spec_instance = IndyProofReqAttrSpec.from_json(json)
# print the JSON string representation of the object
print IndyProofReqAttrSpec.to_json()

# convert the object into a dict
indy_proof_req_attr_spec_dict = indy_proof_req_attr_spec_instance.to_dict()
# create an instance of IndyProofReqAttrSpec from a dict
indy_proof_req_attr_spec_form_dict = indy_proof_req_attr_spec.from_dict(indy_proof_req_attr_spec_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


