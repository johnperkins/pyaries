# V20PresSendRequestRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_verify** | **bool** | Verifier choice to auto-verify proof presentation | [optional] 
**comment** | **str** |  | [optional] 
**connection_id** | **str** | Connection identifier | 
**presentation_request** | [**V20PresRequestByFormat**](V20PresRequestByFormat.md) |  | 
**trace** | **bool** | Whether to trace event (default false) | [optional] 

## Example

```python
from pyaries.models.v20_pres_send_request_request import V20PresSendRequestRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresSendRequestRequest from a JSON string
v20_pres_send_request_request_instance = V20PresSendRequestRequest.from_json(json)
# print the JSON string representation of the object
print V20PresSendRequestRequest.to_json()

# convert the object into a dict
v20_pres_send_request_request_dict = v20_pres_send_request_request_instance.to_dict()
# create an instance of V20PresSendRequestRequest from a dict
v20_pres_send_request_request_form_dict = v20_pres_send_request_request.from_dict(v20_pres_send_request_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


