# CredRevRecordResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**IssuerCredRevRecord**](IssuerCredRevRecord.md) |  | [optional] 

## Example

```python
from pyaries.models.cred_rev_record_result import CredRevRecordResult

# TODO update the JSON string below
json = "{}"
# create an instance of CredRevRecordResult from a JSON string
cred_rev_record_result_instance = CredRevRecordResult.from_json(json)
# print the JSON string representation of the object
print CredRevRecordResult.to_json()

# convert the object into a dict
cred_rev_record_result_dict = cred_rev_record_result_instance.to_dict()
# create an instance of CredRevRecordResult from a dict
cred_rev_record_result_form_dict = cred_rev_record_result.from_dict(cred_rev_record_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


