# V20CredExRecordLDProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | Time of record creation | [optional] 
**cred_ex_id** | **str** | Corresponding v2.0 credential exchange record identifier | [optional] 
**cred_ex_ld_proof_id** | **str** | Record identifier | [optional] 
**cred_id_stored** | **str** | Credential identifier stored in wallet | [optional] 
**state** | **str** | Current record state | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.v20_cred_ex_record_ld_proof import V20CredExRecordLDProof

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExRecordLDProof from a JSON string
v20_cred_ex_record_ld_proof_instance = V20CredExRecordLDProof.from_json(json)
# print the JSON string representation of the object
print V20CredExRecordLDProof.to_json()

# convert the object into a dict
v20_cred_ex_record_ld_proof_dict = v20_cred_ex_record_ld_proof_instance.to_dict()
# create an instance of V20CredExRecordLDProof from a dict
v20_cred_ex_record_ld_proof_form_dict = v20_cred_ex_record_ld_proof.from_dict(v20_cred_ex_record_ld_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


