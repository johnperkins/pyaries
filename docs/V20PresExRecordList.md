# V20PresExRecordList


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[V20PresExRecord]**](V20PresExRecord.md) | Presentation exchange records | [optional] 

## Example

```python
from pyaries.models.v20_pres_ex_record_list import V20PresExRecordList

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresExRecordList from a JSON string
v20_pres_ex_record_list_instance = V20PresExRecordList.from_json(json)
# print the JSON string representation of the object
print V20PresExRecordList.to_json()

# convert the object into a dict
v20_pres_ex_record_list_dict = v20_pres_ex_record_list_instance.to_dict()
# create an instance of V20PresExRecordList from a dict
v20_pres_ex_record_list_form_dict = v20_pres_ex_record_list.from_dict(v20_pres_ex_record_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


