# IndyProofRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Proof request name | [optional] 
**non_revoked** | [**IndyProofRequestNonRevoked**](IndyProofRequestNonRevoked.md) |  | [optional] 
**nonce** | **str** | Nonce | [optional] 
**requested_attributes** | [**Dict[str, IndyProofReqAttrSpec]**](IndyProofReqAttrSpec.md) | Requested attribute specifications of proof request | 
**requested_predicates** | [**Dict[str, IndyProofReqPredSpec]**](IndyProofReqPredSpec.md) | Requested predicate specifications of proof request | 
**version** | **str** | Proof request version | [optional] 

## Example

```python
from pyaries.models.indy_proof_request import IndyProofRequest

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofRequest from a JSON string
indy_proof_request_instance = IndyProofRequest.from_json(json)
# print the JSON string representation of the object
print IndyProofRequest.to_json()

# convert the object into a dict
indy_proof_request_dict = indy_proof_request_instance.to_dict()
# create an instance of IndyProofRequest from a dict
indy_proof_request_form_dict = indy_proof_request.from_dict(indy_proof_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


