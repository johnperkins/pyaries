# AttachmentDef


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Attachment identifier | [optional] 
**type** | **str** | Attachment type | [optional] 

## Example

```python
from pyaries.models.attachment_def import AttachmentDef

# TODO update the JSON string below
json = "{}"
# create an instance of AttachmentDef from a JSON string
attachment_def_instance = AttachmentDef.from_json(json)
# print the JSON string representation of the object
print AttachmentDef.to_json()

# convert the object into a dict
attachment_def_dict = attachment_def_instance.to_dict()
# create an instance of AttachmentDef from a dict
attachment_def_form_dict = attachment_def.from_dict(attachment_def_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


