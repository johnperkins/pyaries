# IndyCredAbstract


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_def_id** | **str** | Credential definition identifier | 
**key_correctness_proof** | [**IndyKeyCorrectnessProof**](IndyKeyCorrectnessProof.md) |  | 
**nonce** | **str** | Nonce in credential abstract | 
**schema_id** | **str** | Schema identifier | 

## Example

```python
from pyaries.models.indy_cred_abstract import IndyCredAbstract

# TODO update the JSON string below
json = "{}"
# create an instance of IndyCredAbstract from a JSON string
indy_cred_abstract_instance = IndyCredAbstract.from_json(json)
# print the JSON string representation of the object
print IndyCredAbstract.to_json()

# convert the object into a dict
indy_cred_abstract_dict = indy_cred_abstract_instance.to_dict()
# create an instance of IndyCredAbstract from a dict
indy_cred_abstract_form_dict = indy_cred_abstract.from_dict(indy_cred_abstract_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


