# MenuJson


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | Introductory text for the menu | [optional] 
**errormsg** | **str** | Optional error message to display in menu header | [optional] 
**options** | [**List[MenuOption]**](MenuOption.md) | List of menu options | 
**title** | **str** | Menu title | [optional] 

## Example

```python
from pyaries.models.menu_json import MenuJson

# TODO update the JSON string below
json = "{}"
# create an instance of MenuJson from a JSON string
menu_json_instance = MenuJson.from_json(json)
# print the JSON string representation of the object
print MenuJson.to_json()

# convert the object into a dict
menu_json_dict = menu_json_instance.to_dict()
# create an instance of MenuJson from a dict
menu_json_form_dict = menu_json.from_dict(menu_json_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


