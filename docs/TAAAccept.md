# TAAAccept


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mechanism** | **str** |  | [optional] 
**text** | **str** |  | [optional] 
**version** | **str** |  | [optional] 

## Example

```python
from pyaries.models.taa_accept import TAAAccept

# TODO update the JSON string below
json = "{}"
# create an instance of TAAAccept from a JSON string
taa_accept_instance = TAAAccept.from_json(json)
# print the JSON string representation of the object
print TAAAccept.to_json()

# convert the object into a dict
taa_accept_dict = taa_accept_instance.to_dict()
# create an instance of TAAAccept from a dict
taa_accept_form_dict = taa_accept.from_dict(taa_accept_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


