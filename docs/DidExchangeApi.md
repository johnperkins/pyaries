# pyaries.DidExchangeApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accept_invitation**](DidExchangeApi.md#accept_invitation) | **POST** /didexchange/{conn_id}/accept-invitation | Accept a stored connection invitation
[**accept_request**](DidExchangeApi.md#accept_request) | **POST** /didexchange/{conn_id}/accept-request | Accept a stored connection request
[**create_request**](DidExchangeApi.md#create_request) | **POST** /didexchange/create-request | Create and send a request against public DID&#39;s implicit invitation
[**receive_request**](DidExchangeApi.md#receive_request) | **POST** /didexchange/receive-request | Receive request against public DID&#39;s implicit invitation


# **accept_invitation**
> ConnRecord accept_invitation(conn_id, my_endpoint=my_endpoint, my_label=my_label)

Accept a stored connection invitation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.DidExchangeApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    my_endpoint = 'my_endpoint_example' # str | My URL endpoint (optional)
    my_label = 'my_label_example' # str | Label for connection request (optional)

    try:
        # Accept a stored connection invitation
        api_response = api_instance.accept_invitation(conn_id, my_endpoint=my_endpoint, my_label=my_label)
        print("The response of DidExchangeApi->accept_invitation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DidExchangeApi->accept_invitation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **my_endpoint** | **str**| My URL endpoint | [optional] 
 **my_label** | **str**| Label for connection request | [optional] 

### Return type

[**ConnRecord**](ConnRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **accept_request**
> ConnRecord accept_request(conn_id, mediation_id=mediation_id, my_endpoint=my_endpoint)

Accept a stored connection request

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.DidExchangeApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    mediation_id = 'mediation_id_example' # str | Identifier for active mediation record to be used (optional)
    my_endpoint = 'my_endpoint_example' # str | My URL endpoint (optional)

    try:
        # Accept a stored connection request
        api_response = api_instance.accept_request(conn_id, mediation_id=mediation_id, my_endpoint=my_endpoint)
        print("The response of DidExchangeApi->accept_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DidExchangeApi->accept_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **mediation_id** | **str**| Identifier for active mediation record to be used | [optional] 
 **my_endpoint** | **str**| My URL endpoint | [optional] 

### Return type

[**ConnRecord**](ConnRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_request**
> ConnRecord create_request(their_public_did, alias=alias, mediation_id=mediation_id, my_endpoint=my_endpoint, my_label=my_label, use_public_did=use_public_did)

Create and send a request against public DID's implicit invitation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.DidExchangeApi(api_client)
    their_public_did = 'their_public_did_example' # str | Qualified public DID to which to request connection
    alias = 'alias_example' # str | Alias for connection (optional)
    mediation_id = 'mediation_id_example' # str | Identifier for active mediation record to be used (optional)
    my_endpoint = 'my_endpoint_example' # str | My URL endpoint (optional)
    my_label = 'my_label_example' # str | Label for connection request (optional)
    use_public_did = True # bool | Use public DID for this connection (optional)

    try:
        # Create and send a request against public DID's implicit invitation
        api_response = api_instance.create_request(their_public_did, alias=alias, mediation_id=mediation_id, my_endpoint=my_endpoint, my_label=my_label, use_public_did=use_public_did)
        print("The response of DidExchangeApi->create_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DidExchangeApi->create_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **their_public_did** | **str**| Qualified public DID to which to request connection | 
 **alias** | **str**| Alias for connection | [optional] 
 **mediation_id** | **str**| Identifier for active mediation record to be used | [optional] 
 **my_endpoint** | **str**| My URL endpoint | [optional] 
 **my_label** | **str**| Label for connection request | [optional] 
 **use_public_did** | **bool**| Use public DID for this connection | [optional] 

### Return type

[**ConnRecord**](ConnRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **receive_request**
> ConnRecord receive_request(alias=alias, auto_accept=auto_accept, mediation_id=mediation_id, my_endpoint=my_endpoint, body=body)

Receive request against public DID's implicit invitation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.DidExchangeApi(api_client)
    alias = 'alias_example' # str | Alias for connection (optional)
    auto_accept = True # bool | Auto-accept connection (defaults to configuration) (optional)
    mediation_id = 'mediation_id_example' # str | Identifier for active mediation record to be used (optional)
    my_endpoint = 'my_endpoint_example' # str | My URL endpoint (optional)
    body = pyaries.DIDXRequest() # DIDXRequest |  (optional)

    try:
        # Receive request against public DID's implicit invitation
        api_response = api_instance.receive_request(alias=alias, auto_accept=auto_accept, mediation_id=mediation_id, my_endpoint=my_endpoint, body=body)
        print("The response of DidExchangeApi->receive_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DidExchangeApi->receive_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alias** | **str**| Alias for connection | [optional] 
 **auto_accept** | **bool**| Auto-accept connection (defaults to configuration) | [optional] 
 **mediation_id** | **str**| Identifier for active mediation record to be used | [optional] 
 **my_endpoint** | **str**| My URL endpoint | [optional] 
 **body** | [**DIDXRequest**](DIDXRequest.md)|  | [optional] 

### Return type

[**ConnRecord**](ConnRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

