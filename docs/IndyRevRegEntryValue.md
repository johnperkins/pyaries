# IndyRevRegEntryValue


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accum** | **str** | Accumulator value | [optional] 
**prev_accum** | **str** | Previous accumulator value | [optional] 
**revoked** | **List[int]** | Revoked credential revocation identifiers | [optional] 

## Example

```python
from pyaries.models.indy_rev_reg_entry_value import IndyRevRegEntryValue

# TODO update the JSON string below
json = "{}"
# create an instance of IndyRevRegEntryValue from a JSON string
indy_rev_reg_entry_value_instance = IndyRevRegEntryValue.from_json(json)
# print the JSON string representation of the object
print IndyRevRegEntryValue.to_json()

# convert the object into a dict
indy_rev_reg_entry_value_dict = indy_rev_reg_entry_value_instance.to_dict()
# create an instance of IndyRevRegEntryValue from a dict
indy_rev_reg_entry_value_form_dict = indy_rev_reg_entry_value.from_dict(indy_rev_reg_entry_value_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


