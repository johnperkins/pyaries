# V10CredentialExchangeCredentialOffer

(Indy) credential offer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_def_id** | **str** | Credential definition identifier | 
**key_correctness_proof** | [**IndyCredAbstractKeyCorrectnessProof**](IndyCredAbstractKeyCorrectnessProof.md) |  | 
**nonce** | **str** | Nonce in credential abstract | 
**schema_id** | **str** | Schema identifier | 

## Example

```python
from pyaries.models.v10_credential_exchange_credential_offer import V10CredentialExchangeCredentialOffer

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialExchangeCredentialOffer from a JSON string
v10_credential_exchange_credential_offer_instance = V10CredentialExchangeCredentialOffer.from_json(json)
# print the JSON string representation of the object
print V10CredentialExchangeCredentialOffer.to_json()

# convert the object into a dict
v10_credential_exchange_credential_offer_dict = v10_credential_exchange_credential_offer_instance.to_dict()
# create an instance of V10CredentialExchangeCredentialOffer from a dict
v10_credential_exchange_credential_offer_form_dict = v10_credential_exchange_credential_offer.from_dict(v10_credential_exchange_credential_offer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


