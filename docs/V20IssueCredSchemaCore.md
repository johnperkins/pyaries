# V20IssueCredSchemaCore


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_remove** | **bool** | Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) | [optional] 
**comment** | **str** | Human-readable comment | [optional] 
**credential_preview** | [**V20CredPreview**](V20CredPreview.md) |  | [optional] 
**filter** | [**V20CredFilter**](V20CredFilter.md) |  | 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 

## Example

```python
from pyaries.models.v20_issue_cred_schema_core import V20IssueCredSchemaCore

# TODO update the JSON string below
json = "{}"
# create an instance of V20IssueCredSchemaCore from a JSON string
v20_issue_cred_schema_core_instance = V20IssueCredSchemaCore.from_json(json)
# print the JSON string representation of the object
print V20IssueCredSchemaCore.to_json()

# convert the object into a dict
v20_issue_cred_schema_core_dict = v20_issue_cred_schema_core_instance.to_dict()
# create an instance of V20IssueCredSchemaCore from a dict
v20_issue_cred_schema_core_form_dict = v20_issue_cred_schema_core.from_dict(v20_issue_cred_schema_core_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


