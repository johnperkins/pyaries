# DocOptions

Signature options

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**challenge** | **str** |  | [optional] 
**domain** | **str** |  | [optional] 
**proof_purpose** | **str** |  | 
**type** | **str** |  | [optional] 
**verification_method** | **str** |  | 

## Example

```python
from pyaries.models.doc_options import DocOptions

# TODO update the JSON string below
json = "{}"
# create an instance of DocOptions from a JSON string
doc_options_instance = DocOptions.from_json(json)
# print the JSON string representation of the object
print DocOptions.to_json()

# convert the object into a dict
doc_options_dict = doc_options_instance.to_dict()
# create an instance of DocOptions from a dict
doc_options_form_dict = doc_options.from_dict(doc_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


