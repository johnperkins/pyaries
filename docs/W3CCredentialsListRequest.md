# W3CCredentialsListRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contexts** | **List[str]** |  | [optional] 
**given_id** | **str** | Given credential id to match | [optional] 
**issuer_id** | **str** | Credential issuer identifier to match | [optional] 
**max_results** | **int** | Maximum number of results to return | [optional] 
**proof_types** | **List[str]** |  | [optional] 
**schema_ids** | **List[str]** | Schema identifiers, all of which to match | [optional] 
**subject_ids** | **List[str]** | Subject identifiers, all of which to match | [optional] 
**tag_query** | **Dict[str, str]** | Tag filter | [optional] 
**types** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.w3_c_credentials_list_request import W3CCredentialsListRequest

# TODO update the JSON string below
json = "{}"
# create an instance of W3CCredentialsListRequest from a JSON string
w3_c_credentials_list_request_instance = W3CCredentialsListRequest.from_json(json)
# print the JSON string representation of the object
print W3CCredentialsListRequest.to_json()

# convert the object into a dict
w3_c_credentials_list_request_dict = w3_c_credentials_list_request_instance.to_dict()
# create an instance of W3CCredentialsListRequest from a dict
w3_c_credentials_list_request_form_dict = w3_c_credentials_list_request.from_dict(w3_c_credentials_list_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


