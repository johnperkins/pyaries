# DIDCreate


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**method** | **str** |  | [optional] 
**options** | [**DIDCreateOptions**](DIDCreateOptions.md) |  | [optional] 

## Example

```python
from pyaries.models.did_create import DIDCreate

# TODO update the JSON string below
json = "{}"
# create an instance of DIDCreate from a JSON string
did_create_instance = DIDCreate.from_json(json)
# print the JSON string representation of the object
print DIDCreate.to_json()

# convert the object into a dict
did_create_dict = did_create_instance.to_dict()
# create an instance of DIDCreate from a dict
did_create_form_dict = did_create.from_dict(did_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


