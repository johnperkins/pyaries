# CreateInvitationRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mediation_id** | **str** | Identifier for active mediation record to be used | [optional] 
**metadata** | **object** | Optional metadata to attach to the connection created with the invitation | [optional] 
**my_label** | **str** | Optional label for connection invitation | [optional] 
**recipient_keys** | **List[str]** | List of recipient keys | [optional] 
**routing_keys** | **List[str]** | List of routing keys | [optional] 
**service_endpoint** | **str** | Connection endpoint | [optional] 

## Example

```python
from pyaries.models.create_invitation_request import CreateInvitationRequest

# TODO update the JSON string below
json = "{}"
# create an instance of CreateInvitationRequest from a JSON string
create_invitation_request_instance = CreateInvitationRequest.from_json(json)
# print the JSON string representation of the object
print CreateInvitationRequest.to_json()

# convert the object into a dict
create_invitation_request_dict = create_invitation_request_instance.to_dict()
# create an instance of CreateInvitationRequest from a dict
create_invitation_request_form_dict = create_invitation_request.from_dict(create_invitation_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


