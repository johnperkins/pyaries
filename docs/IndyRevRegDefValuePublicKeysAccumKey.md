# IndyRevRegDefValuePublicKeysAccumKey


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**z** | **str** | Value for z | [optional] 

## Example

```python
from pyaries.models.indy_rev_reg_def_value_public_keys_accum_key import IndyRevRegDefValuePublicKeysAccumKey

# TODO update the JSON string below
json = "{}"
# create an instance of IndyRevRegDefValuePublicKeysAccumKey from a JSON string
indy_rev_reg_def_value_public_keys_accum_key_instance = IndyRevRegDefValuePublicKeysAccumKey.from_json(json)
# print the JSON string representation of the object
print IndyRevRegDefValuePublicKeysAccumKey.to_json()

# convert the object into a dict
indy_rev_reg_def_value_public_keys_accum_key_dict = indy_rev_reg_def_value_public_keys_accum_key_instance.to_dict()
# create an instance of IndyRevRegDefValuePublicKeysAccumKey from a dict
indy_rev_reg_def_value_public_keys_accum_key_form_dict = indy_rev_reg_def_value_public_keys_accum_key.from_dict(indy_rev_reg_def_value_public_keys_accum_key_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


