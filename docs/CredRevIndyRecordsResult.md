# CredRevIndyRecordsResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rev_reg_delta** | **object** | Indy revocation registry delta | [optional] 

## Example

```python
from pyaries.models.cred_rev_indy_records_result import CredRevIndyRecordsResult

# TODO update the JSON string below
json = "{}"
# create an instance of CredRevIndyRecordsResult from a JSON string
cred_rev_indy_records_result_instance = CredRevIndyRecordsResult.from_json(json)
# print the JSON string representation of the object
print CredRevIndyRecordsResult.to_json()

# convert the object into a dict
cred_rev_indy_records_result_dict = cred_rev_indy_records_result_instance.to_dict()
# create an instance of CredRevIndyRecordsResult from a dict
cred_rev_indy_records_result_form_dict = cred_rev_indy_records_result.from_dict(cred_rev_indy_records_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


