# InvitationMessage


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] 
**accept** | **List[str]** | List of mime type in order of preference | [optional] 
**handshake_protocols** | **List[str]** |  | [optional] 
**label** | **str** | Optional label | [optional] 
**requestsattach** | [**List[AttachDecorator]**](AttachDecorator.md) | Optional request attachment | [optional] 
**services** | **List[object]** |  | [optional] 

## Example

```python
from pyaries.models.invitation_message import InvitationMessage

# TODO update the JSON string below
json = "{}"
# create an instance of InvitationMessage from a JSON string
invitation_message_instance = InvitationMessage.from_json(json)
# print the JSON string representation of the object
print InvitationMessage.to_json()

# convert the object into a dict
invitation_message_dict = invitation_message_instance.to_dict()
# create an instance of InvitationMessage from a dict
invitation_message_form_dict = invitation_message.from_dict(invitation_message_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


