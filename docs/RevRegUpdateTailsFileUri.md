# RevRegUpdateTailsFileUri


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tails_public_uri** | **str** | Public URI to the tails file | 

## Example

```python
from pyaries.models.rev_reg_update_tails_file_uri import RevRegUpdateTailsFileUri

# TODO update the JSON string below
json = "{}"
# create an instance of RevRegUpdateTailsFileUri from a JSON string
rev_reg_update_tails_file_uri_instance = RevRegUpdateTailsFileUri.from_json(json)
# print the JSON string representation of the object
print RevRegUpdateTailsFileUri.to_json()

# convert the object into a dict
rev_reg_update_tails_file_uri_dict = rev_reg_update_tails_file_uri_instance.to_dict()
# create an instance of RevRegUpdateTailsFileUri from a dict
rev_reg_update_tails_file_uri_form_dict = rev_reg_update_tails_file_uri.from_dict(rev_reg_update_tails_file_uri_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


