# RevRegCreateRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credential_definition_id** | **str** | Credential definition identifier | [optional] 
**max_cred_num** | **int** | Revocation registry size | [optional] 

## Example

```python
from pyaries.models.rev_reg_create_request import RevRegCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of RevRegCreateRequest from a JSON string
rev_reg_create_request_instance = RevRegCreateRequest.from_json(json)
# print the JSON string representation of the object
print RevRegCreateRequest.to_json()

# convert the object into a dict
rev_reg_create_request_dict = rev_reg_create_request_instance.to_dict()
# create an instance of RevRegCreateRequest from a dict
rev_reg_create_request_form_dict = rev_reg_create_request.from_dict(rev_reg_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


