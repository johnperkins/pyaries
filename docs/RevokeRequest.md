# RevokeRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | **str** | Optional comment to include in revocation notification | [optional] 
**connection_id** | **str** | Connection ID to which the revocation notification will be sent; required if notify is true | [optional] 
**cred_ex_id** | **str** | Credential exchange identifier | [optional] 
**cred_rev_id** | **str** | Credential revocation identifier | [optional] 
**notify** | **bool** | Send a notification to the credential recipient | [optional] 
**notify_version** | **str** | Specify which version of the revocation notification should be sent | [optional] 
**publish** | **bool** | (True) publish revocation to ledger immediately, or (default, False) mark it pending | [optional] 
**rev_reg_id** | **str** | Revocation registry identifier | [optional] 
**thread_id** | **str** | Thread ID of the credential exchange message thread resulting in the credential now being revoked; required if notify is true | [optional] 

## Example

```python
from pyaries.models.revoke_request import RevokeRequest

# TODO update the JSON string below
json = "{}"
# create an instance of RevokeRequest from a JSON string
revoke_request_instance = RevokeRequest.from_json(json)
# print the JSON string representation of the object
print RevokeRequest.to_json()

# convert the object into a dict
revoke_request_dict = revoke_request_instance.to_dict()
# create an instance of RevokeRequest from a dict
revoke_request_form_dict = revoke_request.from_dict(revoke_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


