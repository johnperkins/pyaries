# MediationGrant


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**endpoint** | **str** | endpoint on which messages destined for the recipient are received. | [optional] 
**routing_keys** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.mediation_grant import MediationGrant

# TODO update the JSON string below
json = "{}"
# create an instance of MediationGrant from a JSON string
mediation_grant_instance = MediationGrant.from_json(json)
# print the JSON string representation of the object
print MediationGrant.to_json()

# convert the object into a dict
mediation_grant_dict = mediation_grant_instance.to_dict()
# create an instance of MediationGrant from a dict
mediation_grant_form_dict = mediation_grant.from_dict(mediation_grant_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


