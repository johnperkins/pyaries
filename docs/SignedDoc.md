# SignedDoc


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**proof** | [**SignatureOptions**](SignatureOptions.md) |  | 

## Example

```python
from pyaries.models.signed_doc import SignedDoc

# TODO update the JSON string below
json = "{}"
# create an instance of SignedDoc from a JSON string
signed_doc_instance = SignedDoc.from_json(json)
# print the JSON string representation of the object
print SignedDoc.to_json()

# convert the object into a dict
signed_doc_dict = signed_doc_instance.to_dict()
# create an instance of SignedDoc from a dict
signed_doc_form_dict = signed_doc.from_dict(signed_doc_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


