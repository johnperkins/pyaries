# DIDCreateOptions


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key_type** | **str** |  | 

## Example

```python
from pyaries.models.did_create_options import DIDCreateOptions

# TODO update the JSON string below
json = "{}"
# create an instance of DIDCreateOptions from a JSON string
did_create_options_instance = DIDCreateOptions.from_json(json)
# print the JSON string representation of the object
print DIDCreateOptions.to_json()

# convert the object into a dict
did_create_options_dict = did_create_options_instance.to_dict()
# create an instance of DIDCreateOptions from a dict
did_create_options_form_dict = did_create_options.from_dict(did_create_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


