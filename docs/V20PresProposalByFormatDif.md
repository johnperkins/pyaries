# V20PresProposalByFormatDif

Presentation proposal for DIF

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**input_descriptors** | [**List[InputDescriptors]**](InputDescriptors.md) |  | [optional] 
**options** | [**DIFOptions**](DIFOptions.md) |  | [optional] 

## Example

```python
from pyaries.models.v20_pres_proposal_by_format_dif import V20PresProposalByFormatDif

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresProposalByFormatDif from a JSON string
v20_pres_proposal_by_format_dif_instance = V20PresProposalByFormatDif.from_json(json)
# print the JSON string representation of the object
print V20PresProposalByFormatDif.to_json()

# convert the object into a dict
v20_pres_proposal_by_format_dif_dict = v20_pres_proposal_by_format_dif_instance.to_dict()
# create an instance of V20PresProposalByFormatDif from a dict
v20_pres_proposal_by_format_dif_form_dict = v20_pres_proposal_by_format_dif.from_dict(v20_pres_proposal_by_format_dif_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


