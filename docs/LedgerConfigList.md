# LedgerConfigList


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ledger_config_list** | [**List[LedgerConfigInstance]**](LedgerConfigInstance.md) |  | 

## Example

```python
from pyaries.models.ledger_config_list import LedgerConfigList

# TODO update the JSON string below
json = "{}"
# create an instance of LedgerConfigList from a JSON string
ledger_config_list_instance = LedgerConfigList.from_json(json)
# print the JSON string representation of the object
print LedgerConfigList.to_json()

# convert the object into a dict
ledger_config_list_dict = ledger_config_list_instance.to_dict()
# create an instance of LedgerConfigList from a dict
ledger_config_list_form_dict = ledger_config_list.from_dict(ledger_config_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


