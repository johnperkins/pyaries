# Keylist


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[RouteRecord]**](RouteRecord.md) | List of keylist records | [optional] 

## Example

```python
from pyaries.models.keylist import Keylist

# TODO update the JSON string below
json = "{}"
# create an instance of Keylist from a JSON string
keylist_instance = Keylist.from_json(json)
# print the JSON string representation of the object
print Keylist.to_json()

# convert the object into a dict
keylist_dict = keylist_instance.to_dict()
# create an instance of Keylist from a dict
keylist_form_dict = keylist.from_dict(keylist_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


