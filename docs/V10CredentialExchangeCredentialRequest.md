# V10CredentialExchangeCredentialRequest

(Indy) credential request

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**blinded_ms** | **object** | Blinded master secret | 
**blinded_ms_correctness_proof** | **object** | Blinded master secret correctness proof | 
**cred_def_id** | **str** | Credential definition identifier | 
**nonce** | **str** | Nonce in credential request | 
**prover_did** | **str** | Prover DID | [optional] 

## Example

```python
from pyaries.models.v10_credential_exchange_credential_request import V10CredentialExchangeCredentialRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialExchangeCredentialRequest from a JSON string
v10_credential_exchange_credential_request_instance = V10CredentialExchangeCredentialRequest.from_json(json)
# print the JSON string representation of the object
print V10CredentialExchangeCredentialRequest.to_json()

# convert the object into a dict
v10_credential_exchange_credential_request_dict = v10_credential_exchange_credential_request_instance.to_dict()
# create an instance of V10CredentialExchangeCredentialRequest from a dict
v10_credential_exchange_credential_request_form_dict = v10_credential_exchange_credential_request.from_dict(v10_credential_exchange_credential_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


