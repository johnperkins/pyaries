# SchemasCreatedResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schema_ids** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.schemas_created_result import SchemasCreatedResult

# TODO update the JSON string below
json = "{}"
# create an instance of SchemasCreatedResult from a JSON string
schemas_created_result_instance = SchemasCreatedResult.from_json(json)
# print the JSON string representation of the object
print SchemasCreatedResult.to_json()

# convert the object into a dict
schemas_created_result_dict = schemas_created_result_instance.to_dict()
# create an instance of SchemasCreatedResult from a dict
schemas_created_result_form_dict = schemas_created_result.from_dict(schemas_created_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


