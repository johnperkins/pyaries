# IndyCredAbstractKeyCorrectnessProof

Key correctness proof

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**c** | **str** | c in key correctness proof | 
**xr_cap** | **List[List[str]]** | xr_cap in key correctness proof | 
**xz_cap** | **str** | xz_cap in key correctness proof | 

## Example

```python
from pyaries.models.indy_cred_abstract_key_correctness_proof import IndyCredAbstractKeyCorrectnessProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyCredAbstractKeyCorrectnessProof from a JSON string
indy_cred_abstract_key_correctness_proof_instance = IndyCredAbstractKeyCorrectnessProof.from_json(json)
# print the JSON string representation of the object
print IndyCredAbstractKeyCorrectnessProof.to_json()

# convert the object into a dict
indy_cred_abstract_key_correctness_proof_dict = indy_cred_abstract_key_correctness_proof_instance.to_dict()
# create an instance of IndyCredAbstractKeyCorrectnessProof from a dict
indy_cred_abstract_key_correctness_proof_form_dict = indy_cred_abstract_key_correctness_proof.from_dict(indy_cred_abstract_key_correctness_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


