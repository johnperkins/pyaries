# V10CredentialFreeOfferRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_issue** | **bool** | Whether to respond automatically to credential requests, creating and issuing requested credentials | [optional] 
**auto_remove** | **bool** | Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) | [optional] 
**comment** | **str** | Human-readable comment | [optional] 
**connection_id** | **str** | Connection identifier | 
**cred_def_id** | **str** | Credential definition identifier | 
**credential_preview** | [**CredentialPreview**](CredentialPreview.md) |  | 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 

## Example

```python
from pyaries.models.v10_credential_free_offer_request import V10CredentialFreeOfferRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialFreeOfferRequest from a JSON string
v10_credential_free_offer_request_instance = V10CredentialFreeOfferRequest.from_json(json)
# print the JSON string representation of the object
print V10CredentialFreeOfferRequest.to_json()

# convert the object into a dict
v10_credential_free_offer_request_dict = v10_credential_free_offer_request_instance.to_dict()
# create an instance of V10CredentialFreeOfferRequest from a dict
v10_credential_free_offer_request_form_dict = v10_credential_free_offer_request.from_dict(v10_credential_free_offer_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


