# V10CredentialStoreRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credential_id** | **str** |  | [optional] 

## Example

```python
from pyaries.models.v10_credential_store_request import V10CredentialStoreRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialStoreRequest from a JSON string
v10_credential_store_request_instance = V10CredentialStoreRequest.from_json(json)
# print the JSON string representation of the object
print V10CredentialStoreRequest.to_json()

# convert the object into a dict
v10_credential_store_request_dict = v10_credential_store_request_instance.to_dict()
# create an instance of V10CredentialStoreRequest from a dict
v10_credential_store_request_form_dict = v10_credential_store_request.from_dict(v10_credential_store_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


