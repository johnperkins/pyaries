# CredentialDefinitionValue

Credential definition primary and revocation values

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**primary** | [**CredDefValuePrimary**](CredDefValuePrimary.md) |  | [optional] 
**revocation** | [**CredDefValueRevocation**](CredDefValueRevocation.md) |  | [optional] 

## Example

```python
from pyaries.models.credential_definition_value import CredentialDefinitionValue

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialDefinitionValue from a JSON string
credential_definition_value_instance = CredentialDefinitionValue.from_json(json)
# print the JSON string representation of the object
print CredentialDefinitionValue.to_json()

# convert the object into a dict
credential_definition_value_dict = credential_definition_value_instance.to_dict()
# create an instance of CredentialDefinitionValue from a dict
credential_definition_value_form_dict = credential_definition_value.from_dict(credential_definition_value_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


