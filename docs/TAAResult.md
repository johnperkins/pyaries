# TAAResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**TAAInfo**](TAAInfo.md) |  | [optional] 

## Example

```python
from pyaries.models.taa_result import TAAResult

# TODO update the JSON string below
json = "{}"
# create an instance of TAAResult from a JSON string
taa_result_instance = TAAResult.from_json(json)
# print the JSON string representation of the object
print TAAResult.to_json()

# convert the object into a dict
taa_result_dict = taa_result_instance.to_dict()
# create an instance of TAAResult from a dict
taa_result_form_dict = taa_result.from_dict(taa_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


