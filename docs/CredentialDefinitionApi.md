# pyaries.CredentialDefinitionApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**credential_definitions_cred_def_id_write_record_post**](CredentialDefinitionApi.md#credential_definitions_cred_def_id_write_record_post) | **POST** /credential-definitions/{cred_def_id}/write_record | Writes a credential definition non-secret record to the wallet
[**get_created_cred_defs**](CredentialDefinitionApi.md#get_created_cred_defs) | **GET** /credential-definitions/created | Search for matching credential definitions that agent originated
[**get_cred_def**](CredentialDefinitionApi.md#get_cred_def) | **GET** /credential-definitions/{cred_def_id} | Gets a credential definition from the ledger
[**publish_cred_def**](CredentialDefinitionApi.md#publish_cred_def) | **POST** /credential-definitions | Sends a credential definition to the ledger


# **credential_definitions_cred_def_id_write_record_post**
> CredentialDefinitionGetResult credential_definitions_cred_def_id_write_record_post(cred_def_id)

Writes a credential definition non-secret record to the wallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialDefinitionApi(api_client)
    cred_def_id = 'cred_def_id_example' # str | Credential definition identifier

    try:
        # Writes a credential definition non-secret record to the wallet
        api_response = api_instance.credential_definitions_cred_def_id_write_record_post(cred_def_id)
        print("The response of CredentialDefinitionApi->credential_definitions_cred_def_id_write_record_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialDefinitionApi->credential_definitions_cred_def_id_write_record_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_def_id** | **str**| Credential definition identifier | 

### Return type

[**CredentialDefinitionGetResult**](CredentialDefinitionGetResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_created_cred_defs**
> CredentialDefinitionsCreatedResult get_created_cred_defs(cred_def_id=cred_def_id, issuer_did=issuer_did, schema_id=schema_id, schema_issuer_did=schema_issuer_did, schema_name=schema_name, schema_version=schema_version)

Search for matching credential definitions that agent originated

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialDefinitionApi(api_client)
    cred_def_id = 'cred_def_id_example' # str | Credential definition id (optional)
    issuer_did = 'issuer_did_example' # str | Issuer DID (optional)
    schema_id = 'schema_id_example' # str | Schema identifier (optional)
    schema_issuer_did = 'schema_issuer_did_example' # str | Schema issuer DID (optional)
    schema_name = 'schema_name_example' # str | Schema name (optional)
    schema_version = 'schema_version_example' # str | Schema version (optional)

    try:
        # Search for matching credential definitions that agent originated
        api_response = api_instance.get_created_cred_defs(cred_def_id=cred_def_id, issuer_did=issuer_did, schema_id=schema_id, schema_issuer_did=schema_issuer_did, schema_name=schema_name, schema_version=schema_version)
        print("The response of CredentialDefinitionApi->get_created_cred_defs:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialDefinitionApi->get_created_cred_defs: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_def_id** | **str**| Credential definition id | [optional] 
 **issuer_did** | **str**| Issuer DID | [optional] 
 **schema_id** | **str**| Schema identifier | [optional] 
 **schema_issuer_did** | **str**| Schema issuer DID | [optional] 
 **schema_name** | **str**| Schema name | [optional] 
 **schema_version** | **str**| Schema version | [optional] 

### Return type

[**CredentialDefinitionsCreatedResult**](CredentialDefinitionsCreatedResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_cred_def**
> CredentialDefinitionGetResult get_cred_def(cred_def_id)

Gets a credential definition from the ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialDefinitionApi(api_client)
    cred_def_id = 'cred_def_id_example' # str | Credential definition identifier

    try:
        # Gets a credential definition from the ledger
        api_response = api_instance.get_cred_def(cred_def_id)
        print("The response of CredentialDefinitionApi->get_cred_def:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialDefinitionApi->get_cred_def: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cred_def_id** | **str**| Credential definition identifier | 

### Return type

[**CredentialDefinitionGetResult**](CredentialDefinitionGetResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **publish_cred_def**
> TxnOrCredentialDefinitionSendResult publish_cred_def(conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser, body=body)

Sends a credential definition to the ledger

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialDefinitionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier (optional)
    create_transaction_for_endorser = True # bool | Create Transaction For Endorser's signature (optional)
    body = pyaries.CredentialDefinitionSendRequest() # CredentialDefinitionSendRequest |  (optional)

    try:
        # Sends a credential definition to the ledger
        api_response = api_instance.publish_cred_def(conn_id=conn_id, create_transaction_for_endorser=create_transaction_for_endorser, body=body)
        print("The response of CredentialDefinitionApi->publish_cred_def:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialDefinitionApi->publish_cred_def: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | [optional] 
 **create_transaction_for_endorser** | **bool**| Create Transaction For Endorser&#39;s signature | [optional] 
 **body** | [**CredentialDefinitionSendRequest**](CredentialDefinitionSendRequest.md)|  | [optional] 

### Return type

[**TxnOrCredentialDefinitionSendResult**](TxnOrCredentialDefinitionSendResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

