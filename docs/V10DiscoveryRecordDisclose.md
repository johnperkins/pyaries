# V10DiscoveryRecordDisclose

Disclose message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**protocols** | [**List[ProtocolDescriptor]**](ProtocolDescriptor.md) | List of protocol descriptors | 

## Example

```python
from pyaries.models.v10_discovery_record_disclose import V10DiscoveryRecordDisclose

# TODO update the JSON string below
json = "{}"
# create an instance of V10DiscoveryRecordDisclose from a JSON string
v10_discovery_record_disclose_instance = V10DiscoveryRecordDisclose.from_json(json)
# print the JSON string representation of the object
print V10DiscoveryRecordDisclose.to_json()

# convert the object into a dict
v10_discovery_record_disclose_dict = v10_discovery_record_disclose_instance.to_dict()
# create an instance of V10DiscoveryRecordDisclose from a dict
v10_discovery_record_disclose_form_dict = v10_discovery_record_disclose.from_dict(v10_discovery_record_disclose_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


