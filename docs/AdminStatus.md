# AdminStatus


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**conductor** | **object** | Conductor statistics | [optional] 
**label** | **str** | Default label | [optional] 
**timing** | **object** | Timing results | [optional] 
**version** | **str** | Version code | [optional] 

## Example

```python
from pyaries.models.admin_status import AdminStatus

# TODO update the JSON string below
json = "{}"
# create an instance of AdminStatus from a JSON string
admin_status_instance = AdminStatus.from_json(json)
# print the JSON string representation of the object
print AdminStatus.to_json()

# convert the object into a dict
admin_status_dict = admin_status_instance.to_dict()
# create an instance of AdminStatus from a dict
admin_status_form_dict = admin_status.from_dict(admin_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


