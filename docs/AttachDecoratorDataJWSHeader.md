# AttachDecoratorDataJWSHeader


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kid** | **str** | Key identifier, in W3C did:key or DID URL format | 

## Example

```python
from pyaries.models.attach_decorator_data_jws_header import AttachDecoratorDataJWSHeader

# TODO update the JSON string below
json = "{}"
# create an instance of AttachDecoratorDataJWSHeader from a JSON string
attach_decorator_data_jws_header_instance = AttachDecoratorDataJWSHeader.from_json(json)
# print the JSON string representation of the object
print AttachDecoratorDataJWSHeader.to_json()

# convert the object into a dict
attach_decorator_data_jws_header_dict = attach_decorator_data_jws_header_instance.to_dict()
# create an instance of AttachDecoratorDataJWSHeader from a dict
attach_decorator_data_jws_header_form_dict = attach_decorator_data_jws_header.from_dict(attach_decorator_data_jws_header_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


