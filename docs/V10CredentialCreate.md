# V10CredentialCreate


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_remove** | **bool** | Whether to remove the credential exchange record on completion (overrides --preserve-exchange-records configuration setting) | [optional] 
**comment** | **str** | Human-readable comment | [optional] 
**cred_def_id** | **str** | Credential definition identifier | [optional] 
**credential_proposal** | [**CredentialPreview**](CredentialPreview.md) |  | 
**issuer_did** | **str** | Credential issuer DID | [optional] 
**schema_id** | **str** | Schema identifier | [optional] 
**schema_issuer_did** | **str** | Schema issuer DID | [optional] 
**schema_name** | **str** | Schema name | [optional] 
**schema_version** | **str** | Schema version | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 

## Example

```python
from pyaries.models.v10_credential_create import V10CredentialCreate

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialCreate from a JSON string
v10_credential_create_instance = V10CredentialCreate.from_json(json)
# print the JSON string representation of the object
print V10CredentialCreate.to_json()

# convert the object into a dict
v10_credential_create_dict = v10_credential_create_instance.to_dict()
# create an instance of V10CredentialCreate from a dict
v10_credential_create_form_dict = v10_credential_create.from_dict(v10_credential_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


