# pyaries.MediationApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clear_default_mediator**](MediationApi.md#clear_default_mediator) | **DELETE** /mediation/default-mediator | Clear default mediator
[**delete_record**](MediationApi.md#delete_record) | **DELETE** /mediation/requests/{mediation_id} | Delete mediation request by ID
[**deny_mediation_request**](MediationApi.md#deny_mediation_request) | **POST** /mediation/requests/{mediation_id}/deny | Deny a stored mediation request
[**get_default_mediator**](MediationApi.md#get_default_mediator) | **GET** /mediation/default-mediator | Get default mediator
[**get_record**](MediationApi.md#get_record) | **GET** /mediation/requests/{mediation_id} | Retrieve mediation request record
[**get_records**](MediationApi.md#get_records) | **GET** /mediation/requests | Query mediation requests, returns list of all mediation records
[**grant_mediation_request**](MediationApi.md#grant_mediation_request) | **POST** /mediation/requests/{mediation_id}/grant | Grant received mediation
[**request_mediation**](MediationApi.md#request_mediation) | **POST** /mediation/request/{conn_id} | Request mediation from connection
[**retrieve_keylists**](MediationApi.md#retrieve_keylists) | **GET** /mediation/keylists | Retrieve keylists by connection or role
[**send_keylist_query**](MediationApi.md#send_keylist_query) | **POST** /mediation/keylists/{mediation_id}/send-keylist-query | Send keylist query to mediator
[**send_keylist_update**](MediationApi.md#send_keylist_update) | **POST** /mediation/keylists/{mediation_id}/send-keylist-update | Send keylist update to mediator
[**set_default_mediator**](MediationApi.md#set_default_mediator) | **PUT** /mediation/{mediation_id}/default-mediator | Set default mediator


# **clear_default_mediator**
> MediationRecord clear_default_mediator()

Clear default mediator

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)

    try:
        # Clear default mediator
        api_response = api_instance.clear_default_mediator()
        print("The response of MediationApi->clear_default_mediator:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->clear_default_mediator: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**MediationRecord**](MediationRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_record**
> MediationRecord delete_record(mediation_id)

Delete mediation request by ID

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)
    mediation_id = 'mediation_id_example' # str | Mediation record identifier

    try:
        # Delete mediation request by ID
        api_response = api_instance.delete_record(mediation_id)
        print("The response of MediationApi->delete_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->delete_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mediation_id** | **str**| Mediation record identifier | 

### Return type

[**MediationRecord**](MediationRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deny_mediation_request**
> MediationDeny deny_mediation_request(mediation_id, body=body)

Deny a stored mediation request

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)
    mediation_id = 'mediation_id_example' # str | Mediation record identifier
    body = pyaries.AdminMediationDeny() # AdminMediationDeny |  (optional)

    try:
        # Deny a stored mediation request
        api_response = api_instance.deny_mediation_request(mediation_id, body=body)
        print("The response of MediationApi->deny_mediation_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->deny_mediation_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mediation_id** | **str**| Mediation record identifier | 
 **body** | [**AdminMediationDeny**](AdminMediationDeny.md)|  | [optional] 

### Return type

[**MediationDeny**](MediationDeny.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_default_mediator**
> MediationRecord get_default_mediator()

Get default mediator

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)

    try:
        # Get default mediator
        api_response = api_instance.get_default_mediator()
        print("The response of MediationApi->get_default_mediator:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->get_default_mediator: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**MediationRecord**](MediationRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_record**
> MediationRecord get_record(mediation_id)

Retrieve mediation request record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)
    mediation_id = 'mediation_id_example' # str | Mediation record identifier

    try:
        # Retrieve mediation request record
        api_response = api_instance.get_record(mediation_id)
        print("The response of MediationApi->get_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->get_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mediation_id** | **str**| Mediation record identifier | 

### Return type

[**MediationRecord**](MediationRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_records**
> MediationList get_records(conn_id=conn_id, mediator_terms=mediator_terms, recipient_terms=recipient_terms, state=state)

Query mediation requests, returns list of all mediation records

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier (optional) (optional)
    mediator_terms = ['mediator_terms_example'] # List[str] | List of mediator rules for recipient (optional)
    recipient_terms = ['recipient_terms_example'] # List[str] | List of recipient rules for mediation (optional)
    state = 'state_example' # str | Mediation state (optional) (optional)

    try:
        # Query mediation requests, returns list of all mediation records
        api_response = api_instance.get_records(conn_id=conn_id, mediator_terms=mediator_terms, recipient_terms=recipient_terms, state=state)
        print("The response of MediationApi->get_records:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->get_records: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier (optional) | [optional] 
 **mediator_terms** | [**List[str]**](str.md)| List of mediator rules for recipient | [optional] 
 **recipient_terms** | [**List[str]**](str.md)| List of recipient rules for mediation | [optional] 
 **state** | **str**| Mediation state (optional) | [optional] 

### Return type

[**MediationList**](MediationList.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **grant_mediation_request**
> MediationGrant grant_mediation_request(mediation_id)

Grant received mediation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)
    mediation_id = 'mediation_id_example' # str | Mediation record identifier

    try:
        # Grant received mediation
        api_response = api_instance.grant_mediation_request(mediation_id)
        print("The response of MediationApi->grant_mediation_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->grant_mediation_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mediation_id** | **str**| Mediation record identifier | 

### Return type

[**MediationGrant**](MediationGrant.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **request_mediation**
> MediationRecord request_mediation(conn_id, body=body)

Request mediation from connection

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    body = pyaries.MediationCreateRequest() # MediationCreateRequest |  (optional)

    try:
        # Request mediation from connection
        api_response = api_instance.request_mediation(conn_id, body=body)
        print("The response of MediationApi->request_mediation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->request_mediation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **body** | [**MediationCreateRequest**](MediationCreateRequest.md)|  | [optional] 

### Return type

[**MediationRecord**](MediationRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **retrieve_keylists**
> Keylist retrieve_keylists(conn_id=conn_id, role=role)

Retrieve keylists by connection or role

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier (optional) (optional)
    role = 'server' # str | Filer on role, 'client' for keys         mediated by other agents, 'server' for keys         mediated by this agent (optional) (default to 'server')

    try:
        # Retrieve keylists by connection or role
        api_response = api_instance.retrieve_keylists(conn_id=conn_id, role=role)
        print("The response of MediationApi->retrieve_keylists:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->retrieve_keylists: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier (optional) | [optional] 
 **role** | **str**| Filer on role, &#39;client&#39; for keys         mediated by other agents, &#39;server&#39; for keys         mediated by this agent | [optional] [default to &#39;server&#39;]

### Return type

[**Keylist**](Keylist.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_keylist_query**
> KeylistQuery send_keylist_query(mediation_id, paginate_limit=paginate_limit, paginate_offset=paginate_offset, body=body)

Send keylist query to mediator

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)
    mediation_id = 'mediation_id_example' # str | Mediation record identifier
    paginate_limit = -1 # int | limit number of results (optional) (default to -1)
    paginate_offset = 0 # int | offset to use in pagination (optional) (default to 0)
    body = pyaries.KeylistQueryFilterRequest() # KeylistQueryFilterRequest |  (optional)

    try:
        # Send keylist query to mediator
        api_response = api_instance.send_keylist_query(mediation_id, paginate_limit=paginate_limit, paginate_offset=paginate_offset, body=body)
        print("The response of MediationApi->send_keylist_query:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->send_keylist_query: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mediation_id** | **str**| Mediation record identifier | 
 **paginate_limit** | **int**| limit number of results | [optional] [default to -1]
 **paginate_offset** | **int**| offset to use in pagination | [optional] [default to 0]
 **body** | [**KeylistQueryFilterRequest**](KeylistQueryFilterRequest.md)|  | [optional] 

### Return type

[**KeylistQuery**](KeylistQuery.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_keylist_update**
> KeylistUpdate send_keylist_update(mediation_id, body=body)

Send keylist update to mediator

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)
    mediation_id = 'mediation_id_example' # str | Mediation record identifier
    body = pyaries.KeylistUpdateRequest() # KeylistUpdateRequest |  (optional)

    try:
        # Send keylist update to mediator
        api_response = api_instance.send_keylist_update(mediation_id, body=body)
        print("The response of MediationApi->send_keylist_update:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->send_keylist_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mediation_id** | **str**| Mediation record identifier | 
 **body** | [**KeylistUpdateRequest**](KeylistUpdateRequest.md)|  | [optional] 

### Return type

[**KeylistUpdate**](KeylistUpdate.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_default_mediator**
> MediationRecord set_default_mediator(mediation_id)

Set default mediator

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.MediationApi(api_client)
    mediation_id = 'mediation_id_example' # str | Mediation record identifier

    try:
        # Set default mediator
        api_response = api_instance.set_default_mediator(mediation_id)
        print("The response of MediationApi->set_default_mediator:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MediationApi->set_default_mediator: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mediation_id** | **str**| Mediation record identifier | 

### Return type

[**MediationRecord**](MediationRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

