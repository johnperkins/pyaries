# DID


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**did** | **str** | DID of interest | [optional] 
**key_type** | **str** | Key type associated with the DID | [optional] 
**method** | **str** | Did method associated with the DID | [optional] 
**posture** | **str** | Whether DID is current public DID, posted to ledger but not current public DID, or local to the wallet | [optional] 
**verkey** | **str** | Public verification key | [optional] 

## Example

```python
from pyaries.models.did import DID

# TODO update the JSON string below
json = "{}"
# create an instance of DID from a JSON string
did_instance = DID.from_json(json)
# print the JSON string representation of the object
print DID.to_json()

# convert the object into a dict
did_dict = did_instance.to_dict()
# create an instance of DID from a dict
did_form_dict = did.from_dict(did_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


