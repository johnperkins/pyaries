# IndyRevRegDefValue


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issuance_type** | **str** | Issuance type | [optional] 
**max_cred_num** | **int** | Maximum number of credentials; registry size | [optional] 
**public_keys** | [**IndyRevRegDefValuePublicKeys**](IndyRevRegDefValuePublicKeys.md) |  | [optional] 
**tails_hash** | **str** | Tails hash value | [optional] 
**tails_location** | **str** | Tails file location | [optional] 

## Example

```python
from pyaries.models.indy_rev_reg_def_value import IndyRevRegDefValue

# TODO update the JSON string below
json = "{}"
# create an instance of IndyRevRegDefValue from a JSON string
indy_rev_reg_def_value_instance = IndyRevRegDefValue.from_json(json)
# print the JSON string representation of the object
print IndyRevRegDefValue.to_json()

# convert the object into a dict
indy_rev_reg_def_value_dict = indy_rev_reg_def_value_instance.to_dict()
# create an instance of IndyRevRegDefValue from a dict
indy_rev_reg_def_value_form_dict = indy_rev_reg_def_value.from_dict(indy_rev_reg_def_value_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


