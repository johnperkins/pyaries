# AdminModules


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **List[str]** | List of admin modules | [optional] 

## Example

```python
from pyaries.models.admin_modules import AdminModules

# TODO update the JSON string below
json = "{}"
# create an instance of AdminModules from a JSON string
admin_modules_instance = AdminModules.from_json(json)
# print the JSON string representation of the object
print AdminModules.to_json()

# convert the object into a dict
admin_modules_dict = admin_modules_instance.to_dict()
# create an instance of AdminModules from a dict
admin_modules_form_dict = admin_modules.from_dict(admin_modules_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


