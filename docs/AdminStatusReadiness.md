# AdminStatusReadiness


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ready** | **bool** | Readiness status | [optional] 

## Example

```python
from pyaries.models.admin_status_readiness import AdminStatusReadiness

# TODO update the JSON string below
json = "{}"
# create an instance of AdminStatusReadiness from a JSON string
admin_status_readiness_instance = AdminStatusReadiness.from_json(json)
# print the JSON string representation of the object
print AdminStatusReadiness.to_json()

# convert the object into a dict
admin_status_readiness_dict = admin_status_readiness_instance.to_dict()
# create an instance of AdminStatusReadiness from a dict
admin_status_readiness_form_dict = admin_status_readiness.from_dict(admin_status_readiness_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


