# WalletRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | Time of record creation | [optional] 
**key_management_mode** | **str** | Mode regarding management of wallet key | 
**settings** | **object** | Settings for this wallet. | [optional] 
**state** | **str** | Current record state | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 
**wallet_id** | **str** | Wallet record ID | 

## Example

```python
from pyaries.models.wallet_record import WalletRecord

# TODO update the JSON string below
json = "{}"
# create an instance of WalletRecord from a JSON string
wallet_record_instance = WalletRecord.from_json(json)
# print the JSON string representation of the object
print WalletRecord.to_json()

# convert the object into a dict
wallet_record_dict = wallet_record_instance.to_dict()
# create an instance of WalletRecord from a dict
wallet_record_form_dict = wallet_record.from_dict(wallet_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


