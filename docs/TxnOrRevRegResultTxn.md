# TxnOrRevRegResultTxn

Revocation registry definition transaction to endorse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Transaction type | [optional] 
**connection_id** | **str** | The connection identifier for thie particular transaction record | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**endorser_write_txn** | **bool** | If True, Endorser will write the transaction after endorsing it | [optional] 
**formats** | **List[Dict[str, str]]** |  | [optional] 
**messages_attach** | **List[object]** |  | [optional] 
**meta_data** | **object** |  | [optional] 
**signature_request** | **List[object]** |  | [optional] 
**signature_response** | **List[object]** |  | [optional] 
**state** | **str** | Current record state | [optional] 
**thread_id** | **str** | Thread Identifier | [optional] 
**timing** | **object** |  | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**transaction_id** | **str** | Transaction identifier | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.txn_or_rev_reg_result_txn import TxnOrRevRegResultTxn

# TODO update the JSON string below
json = "{}"
# create an instance of TxnOrRevRegResultTxn from a JSON string
txn_or_rev_reg_result_txn_instance = TxnOrRevRegResultTxn.from_json(json)
# print the JSON string representation of the object
print TxnOrRevRegResultTxn.to_json()

# convert the object into a dict
txn_or_rev_reg_result_txn_dict = txn_or_rev_reg_result_txn_instance.to_dict()
# create an instance of TxnOrRevRegResultTxn from a dict
txn_or_rev_reg_result_txn_form_dict = txn_or_rev_reg_result_txn.from_dict(txn_or_rev_reg_result_txn_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


