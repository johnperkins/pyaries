# IndyCredPrecis


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_info** | [**IndyCredInfo**](IndyCredInfo.md) |  | [optional] 
**interval** | [**IndyNonRevocationInterval**](IndyNonRevocationInterval.md) |  | [optional] 
**presentation_referents** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.indy_cred_precis import IndyCredPrecis

# TODO update the JSON string below
json = "{}"
# create an instance of IndyCredPrecis from a JSON string
indy_cred_precis_instance = IndyCredPrecis.from_json(json)
# print the JSON string representation of the object
print IndyCredPrecis.to_json()

# convert the object into a dict
indy_cred_precis_dict = indy_cred_precis_instance.to_dict()
# create an instance of IndyCredPrecis from a dict
indy_cred_precis_form_dict = indy_cred_precis.from_dict(indy_cred_precis_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


