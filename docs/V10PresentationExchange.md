# V10PresentationExchange


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_present** | **bool** | Prover choice to auto-present proof as verifier requests | [optional] 
**auto_verify** | **bool** | Verifier choice to auto-verify proof presentation | [optional] 
**connection_id** | **str** | Connection identifier | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**error_msg** | **str** | Error message | [optional] 
**initiator** | **str** | Present-proof exchange initiator: self or external | [optional] 
**presentation** | [**IndyProof**](IndyProof.md) |  | [optional] 
**presentation_exchange_id** | **str** | Presentation exchange identifier | [optional] 
**presentation_proposal_dict** | [**PresentationProposal**](PresentationProposal.md) |  | [optional] 
**presentation_request** | [**IndyProofRequest**](IndyProofRequest.md) |  | [optional] 
**presentation_request_dict** | [**PresentationRequest**](PresentationRequest.md) |  | [optional] 
**role** | **str** | Present-proof exchange role: prover or verifier | [optional] 
**state** | **str** | Present-proof exchange state | [optional] 
**thread_id** | **str** | Thread identifier | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 
**verified** | **str** | Whether presentation is verified: true or false | [optional] 
**verified_msgs** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.v10_presentation_exchange import V10PresentationExchange

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationExchange from a JSON string
v10_presentation_exchange_instance = V10PresentationExchange.from_json(json)
# print the JSON string representation of the object
print V10PresentationExchange.to_json()

# convert the object into a dict
v10_presentation_exchange_dict = v10_presentation_exchange_instance.to_dict()
# create an instance of V10PresentationExchange from a dict
v10_presentation_exchange_form_dict = v10_presentation_exchange.from_dict(v10_presentation_exchange_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


