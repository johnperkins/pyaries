# Filter


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**const** | **object** | Const | [optional] 
**enum** | **List[object]** |  | [optional] 
**exclusive_maximum** | **object** | ExclusiveMaximum | [optional] 
**exclusive_minimum** | **object** | ExclusiveMinimum | [optional] 
**format** | **str** | Format | [optional] 
**max_length** | **int** | Max Length | [optional] 
**maximum** | **object** | Maximum | [optional] 
**min_length** | **int** | Min Length | [optional] 
**minimum** | **object** | Minimum | [optional] 
**var_not** | **bool** | Not | [optional] 
**pattern** | **str** | Pattern | [optional] 
**type** | **str** | Type | [optional] 

## Example

```python
from pyaries.models.filter import Filter

# TODO update the JSON string below
json = "{}"
# create an instance of Filter from a JSON string
filter_instance = Filter.from_json(json)
# print the JSON string representation of the object
print Filter.to_json()

# convert the object into a dict
filter_dict = filter_instance.to_dict()
# create an instance of Filter from a dict
filter_form_dict = filter.from_dict(filter_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


