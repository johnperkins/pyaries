# IndyPrimaryProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eq_proof** | [**IndyEQProof**](IndyEQProof.md) |  | [optional] 
**ge_proofs** | [**List[IndyGEProof]**](IndyGEProof.md) | Indy GE proofs | [optional] 

## Example

```python
from pyaries.models.indy_primary_proof import IndyPrimaryProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyPrimaryProof from a JSON string
indy_primary_proof_instance = IndyPrimaryProof.from_json(json)
# print the JSON string representation of the object
print IndyPrimaryProof.to_json()

# convert the object into a dict
indy_primary_proof_dict = indy_primary_proof_instance.to_dict()
# create an instance of IndyPrimaryProof from a dict
indy_primary_proof_form_dict = indy_primary_proof.from_dict(indy_primary_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


