# IndyProofIdentifier


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_def_id** | **str** | Credential definition identifier | [optional] 
**rev_reg_id** | **str** | Revocation registry identifier | [optional] 
**schema_id** | **str** | Schema identifier | [optional] 
**timestamp** | **int** | Timestamp epoch | [optional] 

## Example

```python
from pyaries.models.indy_proof_identifier import IndyProofIdentifier

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofIdentifier from a JSON string
indy_proof_identifier_instance = IndyProofIdentifier.from_json(json)
# print the JSON string representation of the object
print IndyProofIdentifier.to_json()

# convert the object into a dict
indy_proof_identifier_dict = indy_proof_identifier_instance.to_dict()
# create an instance of IndyProofIdentifier from a dict
indy_proof_identifier_form_dict = indy_proof_identifier.from_dict(indy_proof_identifier_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


