# KeylistUpdate


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**updates** | [**List[KeylistUpdateRule]**](KeylistUpdateRule.md) | List of update rules | [optional] 

## Example

```python
from pyaries.models.keylist_update import KeylistUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of KeylistUpdate from a JSON string
keylist_update_instance = KeylistUpdate.from_json(json)
# print the JSON string representation of the object
print KeylistUpdate.to_json()

# convert the object into a dict
keylist_update_dict = keylist_update_instance.to_dict()
# create an instance of KeylistUpdate from a dict
keylist_update_form_dict = keylist_update.from_dict(keylist_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


