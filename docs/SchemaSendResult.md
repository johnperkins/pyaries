# SchemaSendResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**var_schema** | [**ModelSchema**](ModelSchema.md) |  | [optional] 
**schema_id** | **str** | Schema identifier | 

## Example

```python
from pyaries.models.schema_send_result import SchemaSendResult

# TODO update the JSON string below
json = "{}"
# create an instance of SchemaSendResult from a JSON string
schema_send_result_instance = SchemaSendResult.from_json(json)
# print the JSON string representation of the object
print SchemaSendResult.to_json()

# convert the object into a dict
schema_send_result_dict = schema_send_result_instance.to_dict()
# create an instance of SchemaSendResult from a dict
schema_send_result_form_dict = schema_send_result.from_dict(schema_send_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


