# ClaimFormat


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jwt** | **object** |  | [optional] 
**jwt_vc** | **object** |  | [optional] 
**jwt_vp** | **object** |  | [optional] 
**ldp** | **object** |  | [optional] 
**ldp_vc** | **object** |  | [optional] 
**ldp_vp** | **object** |  | [optional] 

## Example

```python
from pyaries.models.claim_format import ClaimFormat

# TODO update the JSON string below
json = "{}"
# create an instance of ClaimFormat from a JSON string
claim_format_instance = ClaimFormat.from_json(json)
# print the JSON string representation of the object
print ClaimFormat.to_json()

# convert the object into a dict
claim_format_dict = claim_format_instance.to_dict()
# create an instance of ClaimFormat from a dict
claim_format_form_dict = claim_format.from_dict(claim_format_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


