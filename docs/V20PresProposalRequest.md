# V20PresProposalRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_present** | **bool** | Whether to respond automatically to presentation requests, building and presenting requested proof | [optional] 
**comment** | **str** | Human-readable comment | [optional] 
**connection_id** | **str** | Connection identifier | 
**presentation_proposal** | [**V20PresProposalByFormat**](V20PresProposalByFormat.md) |  | 
**trace** | **bool** | Whether to trace event (default false) | [optional] 

## Example

```python
from pyaries.models.v20_pres_proposal_request import V20PresProposalRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresProposalRequest from a JSON string
v20_pres_proposal_request_instance = V20PresProposalRequest.from_json(json)
# print the JSON string representation of the object
print V20PresProposalRequest.to_json()

# convert the object into a dict
v20_pres_proposal_request_dict = v20_pres_proposal_request_instance.to_dict()
# create an instance of V20PresProposalRequest from a dict
v20_pres_proposal_request_form_dict = v20_pres_proposal_request.from_dict(v20_pres_proposal_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


