# pyaries.DiscoverFeaturesV20Api

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**discover_features20_queries_get**](DiscoverFeaturesV20Api.md#discover_features20_queries_get) | **GET** /discover-features-2.0/queries | Query supported features
[**discover_features20_records_get**](DiscoverFeaturesV20Api.md#discover_features20_records_get) | **GET** /discover-features-2.0/records | Discover Features v2.0 records


# **discover_features20_queries_get**
> V20DiscoveryExchangeResult discover_features20_queries_get(connection_id=connection_id, query_goal_code=query_goal_code, query_protocol=query_protocol)

Query supported features

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.DiscoverFeaturesV20Api(api_client)
    connection_id = 'connection_id_example' # str | Connection identifier, if none specified, then the query will provide features for this agent. (optional)
    query_goal_code = 'query_goal_code_example' # str | Goal-code feature-type query (optional)
    query_protocol = 'query_protocol_example' # str | Protocol feature-type query (optional)

    try:
        # Query supported features
        api_response = api_instance.discover_features20_queries_get(connection_id=connection_id, query_goal_code=query_goal_code, query_protocol=query_protocol)
        print("The response of DiscoverFeaturesV20Api->discover_features20_queries_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DiscoverFeaturesV20Api->discover_features20_queries_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connection_id** | **str**| Connection identifier, if none specified, then the query will provide features for this agent. | [optional] 
 **query_goal_code** | **str**| Goal-code feature-type query | [optional] 
 **query_protocol** | **str**| Protocol feature-type query | [optional] 

### Return type

[**V20DiscoveryExchangeResult**](V20DiscoveryExchangeResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **discover_features20_records_get**
> V20DiscoveryExchangeListResult discover_features20_records_get(connection_id=connection_id)

Discover Features v2.0 records

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.DiscoverFeaturesV20Api(api_client)
    connection_id = 'connection_id_example' # str | Connection identifier (optional)

    try:
        # Discover Features v2.0 records
        api_response = api_instance.discover_features20_records_get(connection_id=connection_id)
        print("The response of DiscoverFeaturesV20Api->discover_features20_records_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DiscoverFeaturesV20Api->discover_features20_records_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connection_id** | **str**| Connection identifier | [optional] 

### Return type

[**V20DiscoveryExchangeListResult**](V20DiscoveryExchangeListResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

