# pyaries.PresentProofV20Api

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_proof_request**](PresentProofV20Api.md#create_proof_request) | **POST** /present-proof-2.0/create-request | Creates a presentation request not bound to any proposal or connection
[**delete_record**](PresentProofV20Api.md#delete_record) | **DELETE** /present-proof-2.0/records/{pres_ex_id} | Remove an existing presentation exchange record
[**get_matching_credentials**](PresentProofV20Api.md#get_matching_credentials) | **GET** /present-proof-2.0/records/{pres_ex_id}/credentials | Fetch credentials from wallet for presentation request
[**get_record**](PresentProofV20Api.md#get_record) | **GET** /present-proof-2.0/records/{pres_ex_id} | Fetch a single presentation exchange record
[**get_records**](PresentProofV20Api.md#get_records) | **GET** /present-proof-2.0/records | Fetch all present-proof exchange records
[**report_problem**](PresentProofV20Api.md#report_problem) | **POST** /present-proof-2.0/records/{pres_ex_id}/problem-report | Send a problem report for presentation exchange
[**send_presentation**](PresentProofV20Api.md#send_presentation) | **POST** /present-proof-2.0/records/{pres_ex_id}/send-presentation | Sends a proof presentation
[**send_proposal**](PresentProofV20Api.md#send_proposal) | **POST** /present-proof-2.0/send-proposal | Sends a presentation proposal
[**send_request**](PresentProofV20Api.md#send_request) | **POST** /present-proof-2.0/records/{pres_ex_id}/send-request | Sends a presentation request in reference to a proposal
[**send_request_free**](PresentProofV20Api.md#send_request_free) | **POST** /present-proof-2.0/send-request | Sends a free presentation request not bound to any proposal
[**verify_presentation**](PresentProofV20Api.md#verify_presentation) | **POST** /present-proof-2.0/records/{pres_ex_id}/verify-presentation | Verify a received presentation


# **create_proof_request**
> V20PresExRecord create_proof_request(body=body)

Creates a presentation request not bound to any proposal or connection

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    body = pyaries.V20PresCreateRequestRequest() # V20PresCreateRequestRequest |  (optional)

    try:
        # Creates a presentation request not bound to any proposal or connection
        api_response = api_instance.create_proof_request(body=body)
        print("The response of PresentProofV20Api->create_proof_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->create_proof_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V20PresCreateRequestRequest**](V20PresCreateRequestRequest.md)|  | [optional] 

### Return type

[**V20PresExRecord**](V20PresExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_record**
> object delete_record(pres_ex_id)

Remove an existing presentation exchange record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier

    try:
        # Remove an existing presentation exchange record
        api_response = api_instance.delete_record(pres_ex_id)
        print("The response of PresentProofV20Api->delete_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->delete_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_matching_credentials**
> List[IndyCredPrecis] get_matching_credentials(pres_ex_id, count=count, extra_query=extra_query, referent=referent, start=start)

Fetch credentials from wallet for presentation request

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier
    count = 'count_example' # str | Maximum number to retrieve (optional)
    extra_query = 'extra_query_example' # str | (JSON) object mapping referents to extra WQL queries (optional)
    referent = 'referent_example' # str | Proof request referents of interest, comma-separated (optional)
    start = 'start_example' # str | Start index (optional)

    try:
        # Fetch credentials from wallet for presentation request
        api_response = api_instance.get_matching_credentials(pres_ex_id, count=count, extra_query=extra_query, referent=referent, start=start)
        print("The response of PresentProofV20Api->get_matching_credentials:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->get_matching_credentials: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 
 **count** | **str**| Maximum number to retrieve | [optional] 
 **extra_query** | **str**| (JSON) object mapping referents to extra WQL queries | [optional] 
 **referent** | **str**| Proof request referents of interest, comma-separated | [optional] 
 **start** | **str**| Start index | [optional] 

### Return type

[**List[IndyCredPrecis]**](IndyCredPrecis.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_record**
> V20PresExRecord get_record(pres_ex_id)

Fetch a single presentation exchange record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier

    try:
        # Fetch a single presentation exchange record
        api_response = api_instance.get_record(pres_ex_id)
        print("The response of PresentProofV20Api->get_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->get_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 

### Return type

[**V20PresExRecord**](V20PresExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_records**
> V20PresExRecordList get_records(connection_id=connection_id, role=role, state=state, thread_id=thread_id)

Fetch all present-proof exchange records

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    connection_id = 'connection_id_example' # str | Connection identifier (optional)
    role = 'role_example' # str | Role assigned in presentation exchange (optional)
    state = 'state_example' # str | Presentation exchange state (optional)
    thread_id = 'thread_id_example' # str | Thread identifier (optional)

    try:
        # Fetch all present-proof exchange records
        api_response = api_instance.get_records(connection_id=connection_id, role=role, state=state, thread_id=thread_id)
        print("The response of PresentProofV20Api->get_records:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->get_records: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connection_id** | **str**| Connection identifier | [optional] 
 **role** | **str**| Role assigned in presentation exchange | [optional] 
 **state** | **str**| Presentation exchange state | [optional] 
 **thread_id** | **str**| Thread identifier | [optional] 

### Return type

[**V20PresExRecordList**](V20PresExRecordList.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **report_problem**
> object report_problem(pres_ex_id, body=body)

Send a problem report for presentation exchange

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier
    body = pyaries.V20PresProblemReportRequest() # V20PresProblemReportRequest |  (optional)

    try:
        # Send a problem report for presentation exchange
        api_response = api_instance.report_problem(pres_ex_id, body=body)
        print("The response of PresentProofV20Api->report_problem:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->report_problem: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 
 **body** | [**V20PresProblemReportRequest**](V20PresProblemReportRequest.md)|  | [optional] 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_presentation**
> V20PresExRecord send_presentation(pres_ex_id, body=body)

Sends a proof presentation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier
    body = pyaries.V20PresSpecByFormatRequest() # V20PresSpecByFormatRequest |  (optional)

    try:
        # Sends a proof presentation
        api_response = api_instance.send_presentation(pres_ex_id, body=body)
        print("The response of PresentProofV20Api->send_presentation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->send_presentation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 
 **body** | [**V20PresSpecByFormatRequest**](V20PresSpecByFormatRequest.md)|  | [optional] 

### Return type

[**V20PresExRecord**](V20PresExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_proposal**
> V20PresExRecord send_proposal(body=body)

Sends a presentation proposal

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    body = pyaries.V20PresProposalRequest() # V20PresProposalRequest |  (optional)

    try:
        # Sends a presentation proposal
        api_response = api_instance.send_proposal(body=body)
        print("The response of PresentProofV20Api->send_proposal:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->send_proposal: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V20PresProposalRequest**](V20PresProposalRequest.md)|  | [optional] 

### Return type

[**V20PresExRecord**](V20PresExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_request**
> V20PresExRecord send_request(pres_ex_id, body=body)

Sends a presentation request in reference to a proposal

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier
    body = pyaries.V20PresentationSendRequestToProposal() # V20PresentationSendRequestToProposal |  (optional)

    try:
        # Sends a presentation request in reference to a proposal
        api_response = api_instance.send_request(pres_ex_id, body=body)
        print("The response of PresentProofV20Api->send_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->send_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 
 **body** | [**V20PresentationSendRequestToProposal**](V20PresentationSendRequestToProposal.md)|  | [optional] 

### Return type

[**V20PresExRecord**](V20PresExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **send_request_free**
> V20PresExRecord send_request_free(body=body)

Sends a free presentation request not bound to any proposal

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    body = pyaries.V20PresSendRequestRequest() # V20PresSendRequestRequest |  (optional)

    try:
        # Sends a free presentation request not bound to any proposal
        api_response = api_instance.send_request_free(body=body)
        print("The response of PresentProofV20Api->send_request_free:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->send_request_free: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V20PresSendRequestRequest**](V20PresSendRequestRequest.md)|  | [optional] 

### Return type

[**V20PresExRecord**](V20PresExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_presentation**
> V20PresExRecord verify_presentation(pres_ex_id)

Verify a received presentation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.PresentProofV20Api(api_client)
    pres_ex_id = 'pres_ex_id_example' # str | Presentation exchange identifier

    try:
        # Verify a received presentation
        api_response = api_instance.verify_presentation(pres_ex_id)
        print("The response of PresentProofV20Api->verify_presentation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling PresentProofV20Api->verify_presentation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pres_ex_id** | **str**| Presentation exchange identifier | 

### Return type

[**V20PresExRecord**](V20PresExRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

