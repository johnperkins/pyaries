# OobRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attach_thread_id** | **str** | Connection record identifier | [optional] 
**connection_id** | **str** | Connection record identifier | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**invi_msg_id** | **str** | Invitation message identifier | 
**invitation** | [**InvitationMessage**](InvitationMessage.md) |  | 
**oob_id** | **str** | Oob record identifier | 
**our_recipient_key** | **str** | Recipient key used for oob invitation | [optional] 
**role** | **str** | OOB Role | [optional] 
**state** | **str** | Out of band message exchange state | 
**their_service** | [**ServiceDecorator**](ServiceDecorator.md) |  | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.oob_record import OobRecord

# TODO update the JSON string below
json = "{}"
# create an instance of OobRecord from a JSON string
oob_record_instance = OobRecord.from_json(json)
# print the JSON string representation of the object
print OobRecord.to_json()

# convert the object into a dict
oob_record_dict = oob_record_instance.to_dict()
# create an instance of OobRecord from a dict
oob_record_form_dict = oob_record.from_dict(oob_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


