# PresentationRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**request_presentationsattach** | [**List[AttachDecorator]**](AttachDecorator.md) |  | 

## Example

```python
from pyaries.models.presentation_request import PresentationRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PresentationRequest from a JSON string
presentation_request_instance = PresentationRequest.from_json(json)
# print the JSON string representation of the object
print PresentationRequest.to_json()

# convert the object into a dict
presentation_request_dict = presentation_request_instance.to_dict()
# create an instance of PresentationRequest from a dict
presentation_request_form_dict = presentation_request.from_dict(presentation_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


