# KeylistQueryPaginate


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**limit** | **int** | Limit for keylist query | [optional] 
**offset** | **int** | Offset value for query | [optional] 

## Example

```python
from pyaries.models.keylist_query_paginate import KeylistQueryPaginate

# TODO update the JSON string below
json = "{}"
# create an instance of KeylistQueryPaginate from a JSON string
keylist_query_paginate_instance = KeylistQueryPaginate.from_json(json)
# print the JSON string representation of the object
print KeylistQueryPaginate.to_json()

# convert the object into a dict
keylist_query_paginate_dict = keylist_query_paginate_instance.to_dict()
# create an instance of KeylistQueryPaginate from a dict
keylist_query_paginate_form_dict = keylist_query_paginate.from_dict(keylist_query_paginate_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


