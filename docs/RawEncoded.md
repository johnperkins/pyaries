# RawEncoded


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**encoded** | **str** | Encoded value | [optional] 
**raw** | **str** | Raw value | [optional] 

## Example

```python
from pyaries.models.raw_encoded import RawEncoded

# TODO update the JSON string below
json = "{}"
# create an instance of RawEncoded from a JSON string
raw_encoded_instance = RawEncoded.from_json(json)
# print the JSON string representation of the object
print RawEncoded.to_json()

# convert the object into a dict
raw_encoded_dict = raw_encoded_instance.to_dict()
# create an instance of RawEncoded from a dict
raw_encoded_form_dict = raw_encoded.from_dict(raw_encoded_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


