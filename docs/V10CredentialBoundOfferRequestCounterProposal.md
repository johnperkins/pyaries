# V10CredentialBoundOfferRequestCounterProposal

Optional counter-proposal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**cred_def_id** | **str** |  | [optional] 
**credential_proposal** | [**CredentialPreview**](CredentialPreview.md) |  | [optional] 
**issuer_did** | **str** |  | [optional] 
**schema_id** | **str** |  | [optional] 
**schema_issuer_did** | **str** |  | [optional] 
**schema_name** | **str** |  | [optional] 
**schema_version** | **str** |  | [optional] 

## Example

```python
from pyaries.models.v10_credential_bound_offer_request_counter_proposal import V10CredentialBoundOfferRequestCounterProposal

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialBoundOfferRequestCounterProposal from a JSON string
v10_credential_bound_offer_request_counter_proposal_instance = V10CredentialBoundOfferRequestCounterProposal.from_json(json)
# print the JSON string representation of the object
print V10CredentialBoundOfferRequestCounterProposal.to_json()

# convert the object into a dict
v10_credential_bound_offer_request_counter_proposal_dict = v10_credential_bound_offer_request_counter_proposal_instance.to_dict()
# create an instance of V10CredentialBoundOfferRequestCounterProposal from a dict
v10_credential_bound_offer_request_counter_proposal_form_dict = v10_credential_bound_offer_request_counter_proposal.from_dict(v10_credential_bound_offer_request_counter_proposal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


