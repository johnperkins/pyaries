# TAAInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aml_record** | [**AMLRecord**](AMLRecord.md) |  | [optional] 
**taa_accepted** | [**TAAAcceptance**](TAAAcceptance.md) |  | [optional] 
**taa_record** | [**TAARecord**](TAARecord.md) |  | [optional] 
**taa_required** | **bool** |  | [optional] 

## Example

```python
from pyaries.models.taa_info import TAAInfo

# TODO update the JSON string below
json = "{}"
# create an instance of TAAInfo from a JSON string
taa_info_instance = TAAInfo.from_json(json)
# print the JSON string representation of the object
print TAAInfo.to_json()

# convert the object into a dict
taa_info_dict = taa_info_instance.to_dict()
# create an instance of TAAInfo from a dict
taa_info_form_dict = taa_info.from_dict(taa_info_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


