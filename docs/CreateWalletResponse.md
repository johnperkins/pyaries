# CreateWalletResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **str** | Time of record creation | [optional] 
**key_management_mode** | **str** | Mode regarding management of wallet key | 
**settings** | **object** | Settings for this wallet. | [optional] 
**state** | **str** | Current record state | [optional] 
**token** | **str** | Authorization token to authenticate wallet requests | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 
**wallet_id** | **str** | Wallet record ID | 

## Example

```python
from pyaries.models.create_wallet_response import CreateWalletResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CreateWalletResponse from a JSON string
create_wallet_response_instance = CreateWalletResponse.from_json(json)
# print the JSON string representation of the object
print CreateWalletResponse.to_json()

# convert the object into a dict
create_wallet_response_dict = create_wallet_response_instance.to_dict()
# create an instance of CreateWalletResponse from a dict
create_wallet_response_form_dict = create_wallet_response.from_dict(create_wallet_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


