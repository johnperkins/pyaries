# V20CredExRecordDetailCredExRecord

Credential exchange record

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_issue** | **bool** | Issuer choice to issue to request in this credential exchange | [optional] 
**auto_offer** | **bool** | Holder choice to accept offer in this credential exchange | [optional] 
**auto_remove** | **bool** | Issuer choice to remove this credential exchange record when complete | [optional] 
**by_format** | [**V20CredExRecordByFormat**](V20CredExRecordByFormat.md) |  | [optional] 
**connection_id** | **str** | Connection identifier | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**cred_ex_id** | **str** | Credential exchange identifier | [optional] 
**cred_issue** | [**V20CredExRecordCredIssue**](V20CredExRecordCredIssue.md) |  | [optional] 
**cred_offer** | [**V20CredExRecordCredOffer**](V20CredExRecordCredOffer.md) |  | [optional] 
**cred_preview** | [**V20CredExRecordCredPreview**](V20CredExRecordCredPreview.md) |  | [optional] 
**cred_proposal** | [**V20CredExRecordCredProposal**](V20CredExRecordCredProposal.md) |  | [optional] 
**cred_request** | [**V20CredExRecordCredRequest**](V20CredExRecordCredRequest.md) |  | [optional] 
**error_msg** | **str** | Error message | [optional] 
**initiator** | **str** | Issue-credential exchange initiator: self or external | [optional] 
**parent_thread_id** | **str** | Parent thread identifier | [optional] 
**role** | **str** | Issue-credential exchange role: holder or issuer | [optional] 
**state** | **str** | Issue-credential exchange state | [optional] 
**thread_id** | **str** | Thread identifier | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.v20_cred_ex_record_detail_cred_ex_record import V20CredExRecordDetailCredExRecord

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExRecordDetailCredExRecord from a JSON string
v20_cred_ex_record_detail_cred_ex_record_instance = V20CredExRecordDetailCredExRecord.from_json(json)
# print the JSON string representation of the object
print V20CredExRecordDetailCredExRecord.to_json()

# convert the object into a dict
v20_cred_ex_record_detail_cred_ex_record_dict = v20_cred_ex_record_detail_cred_ex_record_instance.to_dict()
# create an instance of V20CredExRecordDetailCredExRecord from a dict
v20_cred_ex_record_detail_cred_ex_record_form_dict = v20_cred_ex_record_detail_cred_ex_record.from_dict(v20_cred_ex_record_detail_cred_ex_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


