# WriteLedgerRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ledger_id** | **str** |  | [optional] 

## Example

```python
from pyaries.models.write_ledger_request import WriteLedgerRequest

# TODO update the JSON string below
json = "{}"
# create an instance of WriteLedgerRequest from a JSON string
write_ledger_request_instance = WriteLedgerRequest.from_json(json)
# print the JSON string representation of the object
print WriteLedgerRequest.to_json()

# convert the object into a dict
write_ledger_request_dict = write_ledger_request_instance.to_dict()
# create an instance of WriteLedgerRequest from a dict
write_ledger_request_form_dict = write_ledger_request.from_dict(write_ledger_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


