# PerformRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Menu option name | [optional] 
**params** | **Dict[str, str]** | Input parameter values | [optional] 

## Example

```python
from pyaries.models.perform_request import PerformRequest

# TODO update the JSON string below
json = "{}"
# create an instance of PerformRequest from a JSON string
perform_request_instance = PerformRequest.from_json(json)
# print the JSON string representation of the object
print PerformRequest.to_json()

# convert the object into a dict
perform_request_dict = perform_request_instance.to_dict()
# create an instance of PerformRequest from a dict
perform_request_form_dict = perform_request.from_dict(perform_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


