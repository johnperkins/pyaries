# V20DiscoveryRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connection_id** | **str** | Connection identifier | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**disclosures** | [**Disclosures**](Disclosures.md) |  | [optional] 
**discovery_exchange_id** | **str** | Credential exchange identifier | [optional] 
**queries_msg** | [**Queries**](Queries.md) |  | [optional] 
**state** | **str** | Current record state | [optional] 
**thread_id** | **str** | Thread identifier | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.v20_discovery_record import V20DiscoveryRecord

# TODO update the JSON string below
json = "{}"
# create an instance of V20DiscoveryRecord from a JSON string
v20_discovery_record_instance = V20DiscoveryRecord.from_json(json)
# print the JSON string representation of the object
print V20DiscoveryRecord.to_json()

# convert the object into a dict
v20_discovery_record_dict = v20_discovery_record_instance.to_dict()
# create an instance of V20DiscoveryRecord from a dict
v20_discovery_record_form_dict = v20_discovery_record.from_dict(v20_discovery_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


