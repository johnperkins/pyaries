# InputDescriptors


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**constraints** | [**Constraints**](Constraints.md) |  | [optional] 
**group** | **List[str]** |  | [optional] 
**id** | **str** | ID | [optional] 
**metadata** | **object** | Metadata dictionary | [optional] 
**name** | **str** | Name | [optional] 
**purpose** | **str** | Purpose | [optional] 
**var_schema** | [**SchemasInputDescriptorFilter**](SchemasInputDescriptorFilter.md) |  | [optional] 

## Example

```python
from pyaries.models.input_descriptors import InputDescriptors

# TODO update the JSON string below
json = "{}"
# create an instance of InputDescriptors from a JSON string
input_descriptors_instance = InputDescriptors.from_json(json)
# print the JSON string representation of the object
print InputDescriptors.to_json()

# convert the object into a dict
input_descriptors_dict = input_descriptors_instance.to_dict()
# create an instance of InputDescriptors from a dict
input_descriptors_form_dict = input_descriptors.from_dict(input_descriptors_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


