# V10PresentationExchangePresentation

(Indy) presentation (also known as proof)

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifiers** | [**List[IndyProofIdentifier]**](IndyProofIdentifier.md) | Indy proof.identifiers content | [optional] 
**proof** | [**IndyProofProof**](IndyProofProof.md) |  | [optional] 
**requested_proof** | [**IndyProofRequestedProof**](IndyProofRequestedProof.md) |  | [optional] 

## Example

```python
from pyaries.models.v10_presentation_exchange_presentation import V10PresentationExchangePresentation

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationExchangePresentation from a JSON string
v10_presentation_exchange_presentation_instance = V10PresentationExchangePresentation.from_json(json)
# print the JSON string representation of the object
print V10PresentationExchangePresentation.to_json()

# convert the object into a dict
v10_presentation_exchange_presentation_dict = v10_presentation_exchange_presentation_instance.to_dict()
# create an instance of V10PresentationExchangePresentation from a dict
v10_presentation_exchange_presentation_form_dict = v10_presentation_exchange_presentation.from_dict(v10_presentation_exchange_presentation_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


