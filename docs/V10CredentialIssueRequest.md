# V10CredentialIssueRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | **str** | Human-readable comment | [optional] 

## Example

```python
from pyaries.models.v10_credential_issue_request import V10CredentialIssueRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialIssueRequest from a JSON string
v10_credential_issue_request_instance = V10CredentialIssueRequest.from_json(json)
# print the JSON string representation of the object
print V10CredentialIssueRequest.to_json()

# convert the object into a dict
v10_credential_issue_request_dict = v10_credential_issue_request_instance.to_dict()
# create an instance of V10CredentialIssueRequest from a dict
v10_credential_issue_request_form_dict = v10_credential_issue_request.from_dict(v10_credential_issue_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


