# InvitationRecordInvitation

Out of band invitation message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] 
**accept** | **List[str]** | List of mime type in order of preference | [optional] 
**handshake_protocols** | **List[str]** |  | [optional] 
**label** | **str** | Optional label | [optional] 
**requestsattach** | [**List[AttachDecorator]**](AttachDecorator.md) | Optional request attachment | [optional] 
**services** | **List[object]** |  | [optional] 

## Example

```python
from pyaries.models.invitation_record_invitation import InvitationRecordInvitation

# TODO update the JSON string below
json = "{}"
# create an instance of InvitationRecordInvitation from a JSON string
invitation_record_invitation_instance = InvitationRecordInvitation.from_json(json)
# print the JSON string representation of the object
print InvitationRecordInvitation.to_json()

# convert the object into a dict
invitation_record_invitation_dict = invitation_record_invitation_instance.to_dict()
# create an instance of InvitationRecordInvitation from a dict
invitation_record_invitation_form_dict = invitation_record_invitation.from_dict(invitation_record_invitation_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


