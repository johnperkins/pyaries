# SchemaInputDescriptor


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**required** | **bool** | Required | [optional] 
**uri** | **str** | URI | [optional] 

## Example

```python
from pyaries.models.schema_input_descriptor import SchemaInputDescriptor

# TODO update the JSON string below
json = "{}"
# create an instance of SchemaInputDescriptor from a JSON string
schema_input_descriptor_instance = SchemaInputDescriptor.from_json(json)
# print the JSON string representation of the object
print SchemaInputDescriptor.to_json()

# convert the object into a dict
schema_input_descriptor_dict = schema_input_descriptor_instance.to_dict()
# create an instance of SchemaInputDescriptor from a dict
schema_input_descriptor_form_dict = schema_input_descriptor.from_dict(schema_input_descriptor_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


