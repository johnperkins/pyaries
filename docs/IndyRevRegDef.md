# IndyRevRegDef


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_def_id** | **str** | Credential definition identifier | [optional] 
**id** | **str** | Indy revocation registry identifier | [optional] 
**revoc_def_type** | **str** | Revocation registry type (specify CL_ACCUM) | [optional] 
**tag** | **str** | Revocation registry tag | [optional] 
**value** | [**IndyRevRegDefValue**](IndyRevRegDefValue.md) |  | [optional] 
**ver** | **str** | Version of revocation registry definition | [optional] 

## Example

```python
from pyaries.models.indy_rev_reg_def import IndyRevRegDef

# TODO update the JSON string below
json = "{}"
# create an instance of IndyRevRegDef from a JSON string
indy_rev_reg_def_instance = IndyRevRegDef.from_json(json)
# print the JSON string representation of the object
print IndyRevRegDef.to_json()

# convert the object into a dict
indy_rev_reg_def_dict = indy_rev_reg_def_instance.to_dict()
# create an instance of IndyRevRegDef from a dict
indy_rev_reg_def_form_dict = indy_rev_reg_def.from_dict(indy_rev_reg_def_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


