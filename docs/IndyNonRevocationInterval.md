# IndyNonRevocationInterval


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**var_from** | **int** | Earliest time of interest in non-revocation interval | [optional] 
**to** | **int** | Latest time of interest in non-revocation interval | [optional] 

## Example

```python
from pyaries.models.indy_non_revocation_interval import IndyNonRevocationInterval

# TODO update the JSON string below
json = "{}"
# create an instance of IndyNonRevocationInterval from a JSON string
indy_non_revocation_interval_instance = IndyNonRevocationInterval.from_json(json)
# print the JSON string representation of the object
print IndyNonRevocationInterval.to_json()

# convert the object into a dict
indy_non_revocation_interval_dict = indy_non_revocation_interval_instance.to_dict()
# create an instance of IndyNonRevocationInterval from a dict
indy_non_revocation_interval_form_dict = indy_non_revocation_interval.from_dict(indy_non_revocation_interval_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


