# V20CredRequestRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**holder_did** | **str** | Holder DID to substitute for the credentialSubject.id | [optional] 

## Example

```python
from pyaries.models.v20_cred_request_request import V20CredRequestRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredRequestRequest from a JSON string
v20_cred_request_request_instance = V20CredRequestRequest.from_json(json)
# print the JSON string representation of the object
print V20CredRequestRequest.to_json()

# convert the object into a dict
v20_cred_request_request_dict = v20_cred_request_request_instance.to_dict()
# create an instance of V20CredRequestRequest from a dict
v20_cred_request_request_form_dict = v20_cred_request_request.from_dict(v20_cred_request_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


