# pyaries.CredentialsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_record**](CredentialsApi.md#delete_record) | **DELETE** /credential/{credential_id} | Remove credential from wallet by id
[**delete_w3c_credential**](CredentialsApi.md#delete_w3c_credential) | **DELETE** /credential/w3c/{credential_id} | Remove W3C credential from wallet by id
[**get_credential_mime_types**](CredentialsApi.md#get_credential_mime_types) | **GET** /credential/mime-types/{credential_id} | Get attribute MIME types from wallet
[**get_record**](CredentialsApi.md#get_record) | **GET** /credential/{credential_id} | Fetch credential from wallet by id
[**get_records**](CredentialsApi.md#get_records) | **GET** /credentials | Fetch credentials from wallet
[**get_revocation_status**](CredentialsApi.md#get_revocation_status) | **GET** /credential/revoked/{credential_id} | Query credential revocation status by id
[**get_w3c_credential**](CredentialsApi.md#get_w3c_credential) | **GET** /credential/w3c/{credential_id} | Fetch W3C credential from wallet by id
[**get_w3c_credentials**](CredentialsApi.md#get_w3c_credentials) | **POST** /credentials/w3c | Fetch W3C credentials from wallet


# **delete_record**
> object delete_record(credential_id)

Remove credential from wallet by id

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialsApi(api_client)
    credential_id = 'credential_id_example' # str | Credential identifier

    try:
        # Remove credential from wallet by id
        api_response = api_instance.delete_record(credential_id)
        print("The response of CredentialsApi->delete_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialsApi->delete_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credential_id** | **str**| Credential identifier | 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_w3c_credential**
> object delete_w3c_credential(credential_id)

Remove W3C credential from wallet by id

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialsApi(api_client)
    credential_id = 'credential_id_example' # str | Credential identifier

    try:
        # Remove W3C credential from wallet by id
        api_response = api_instance.delete_w3c_credential(credential_id)
        print("The response of CredentialsApi->delete_w3c_credential:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialsApi->delete_w3c_credential: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credential_id** | **str**| Credential identifier | 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_credential_mime_types**
> AttributeMimeTypesResult get_credential_mime_types(credential_id)

Get attribute MIME types from wallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialsApi(api_client)
    credential_id = 'credential_id_example' # str | Credential identifier

    try:
        # Get attribute MIME types from wallet
        api_response = api_instance.get_credential_mime_types(credential_id)
        print("The response of CredentialsApi->get_credential_mime_types:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialsApi->get_credential_mime_types: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credential_id** | **str**| Credential identifier | 

### Return type

[**AttributeMimeTypesResult**](AttributeMimeTypesResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_record**
> IndyCredInfo get_record(credential_id)

Fetch credential from wallet by id

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialsApi(api_client)
    credential_id = 'credential_id_example' # str | Credential identifier

    try:
        # Fetch credential from wallet by id
        api_response = api_instance.get_record(credential_id)
        print("The response of CredentialsApi->get_record:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialsApi->get_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credential_id** | **str**| Credential identifier | 

### Return type

[**IndyCredInfo**](IndyCredInfo.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_records**
> CredInfoList get_records(count=count, start=start, wql=wql)

Fetch credentials from wallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialsApi(api_client)
    count = 'count_example' # str | Maximum number to retrieve (optional)
    start = 'start_example' # str | Start index (optional)
    wql = 'wql_example' # str | (JSON) WQL query (optional)

    try:
        # Fetch credentials from wallet
        api_response = api_instance.get_records(count=count, start=start, wql=wql)
        print("The response of CredentialsApi->get_records:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialsApi->get_records: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **str**| Maximum number to retrieve | [optional] 
 **start** | **str**| Start index | [optional] 
 **wql** | **str**| (JSON) WQL query | [optional] 

### Return type

[**CredInfoList**](CredInfoList.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_revocation_status**
> CredRevokedResult get_revocation_status(credential_id, var_from=var_from, to=to)

Query credential revocation status by id

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialsApi(api_client)
    credential_id = 'credential_id_example' # str | Credential identifier
    var_from = 'var_from_example' # str | Earliest epoch of revocation status interval of interest (optional)
    to = 'to_example' # str | Latest epoch of revocation status interval of interest (optional)

    try:
        # Query credential revocation status by id
        api_response = api_instance.get_revocation_status(credential_id, var_from=var_from, to=to)
        print("The response of CredentialsApi->get_revocation_status:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialsApi->get_revocation_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credential_id** | **str**| Credential identifier | 
 **var_from** | **str**| Earliest epoch of revocation status interval of interest | [optional] 
 **to** | **str**| Latest epoch of revocation status interval of interest | [optional] 

### Return type

[**CredRevokedResult**](CredRevokedResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_w3c_credential**
> VCRecord get_w3c_credential(credential_id)

Fetch W3C credential from wallet by id

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialsApi(api_client)
    credential_id = 'credential_id_example' # str | Credential identifier

    try:
        # Fetch W3C credential from wallet by id
        api_response = api_instance.get_w3c_credential(credential_id)
        print("The response of CredentialsApi->get_w3c_credential:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialsApi->get_w3c_credential: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credential_id** | **str**| Credential identifier | 

### Return type

[**VCRecord**](VCRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_w3c_credentials**
> VCRecordList get_w3c_credentials(count=count, start=start, wql=wql, body=body)

Fetch W3C credentials from wallet

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.CredentialsApi(api_client)
    count = 'count_example' # str | Maximum number to retrieve (optional)
    start = 'start_example' # str | Start index (optional)
    wql = 'wql_example' # str | (JSON) WQL query (optional)
    body = pyaries.W3CCredentialsListRequest() # W3CCredentialsListRequest |  (optional)

    try:
        # Fetch W3C credentials from wallet
        api_response = api_instance.get_w3c_credentials(count=count, start=start, wql=wql, body=body)
        print("The response of CredentialsApi->get_w3c_credentials:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling CredentialsApi->get_w3c_credentials: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **str**| Maximum number to retrieve | [optional] 
 **start** | **str**| Start index | [optional] 
 **wql** | **str**| (JSON) WQL query | [optional] 
 **body** | [**W3CCredentialsListRequest**](W3CCredentialsListRequest.md)|  | [optional] 

### Return type

[**VCRecordList**](VCRecordList.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

