# V20DiscoveryRecordQueriesMsg

Queries message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**queries** | [**List[QueryItem]**](QueryItem.md) |  | [optional] 

## Example

```python
from pyaries.models.v20_discovery_record_queries_msg import V20DiscoveryRecordQueriesMsg

# TODO update the JSON string below
json = "{}"
# create an instance of V20DiscoveryRecordQueriesMsg from a JSON string
v20_discovery_record_queries_msg_instance = V20DiscoveryRecordQueriesMsg.from_json(json)
# print the JSON string representation of the object
print V20DiscoveryRecordQueriesMsg.to_json()

# convert the object into a dict
v20_discovery_record_queries_msg_dict = v20_discovery_record_queries_msg_instance.to_dict()
# create an instance of V20DiscoveryRecordQueriesMsg from a dict
v20_discovery_record_queries_msg_form_dict = v20_discovery_record_queries_msg.from_dict(v20_discovery_record_queries_msg_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


