# V20DiscoveryRecordDisclosures

Disclosures message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**disclosures** | **List[object]** | List of protocol or goal_code descriptors | 

## Example

```python
from pyaries.models.v20_discovery_record_disclosures import V20DiscoveryRecordDisclosures

# TODO update the JSON string below
json = "{}"
# create an instance of V20DiscoveryRecordDisclosures from a JSON string
v20_discovery_record_disclosures_instance = V20DiscoveryRecordDisclosures.from_json(json)
# print the JSON string representation of the object
print V20DiscoveryRecordDisclosures.to_json()

# convert the object into a dict
v20_discovery_record_disclosures_dict = v20_discovery_record_disclosures_instance.to_dict()
# create an instance of V20DiscoveryRecordDisclosures from a dict
v20_discovery_record_disclosures_form_dict = v20_discovery_record_disclosures.from_dict(v20_discovery_record_disclosures_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


