# IndyCredPrecisInterval

Non-revocation interval from presentation request

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**var_from** | **int** | Earliest time of interest in non-revocation interval | [optional] 
**to** | **int** | Latest time of interest in non-revocation interval | [optional] 

## Example

```python
from pyaries.models.indy_cred_precis_interval import IndyCredPrecisInterval

# TODO update the JSON string below
json = "{}"
# create an instance of IndyCredPrecisInterval from a JSON string
indy_cred_precis_interval_instance = IndyCredPrecisInterval.from_json(json)
# print the JSON string representation of the object
print IndyCredPrecisInterval.to_json()

# convert the object into a dict
indy_cred_precis_interval_dict = indy_cred_precis_interval_instance.to_dict()
# create an instance of IndyCredPrecisInterval from a dict
indy_cred_precis_interval_form_dict = indy_cred_precis_interval.from_dict(indy_cred_precis_interval_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


