# WalletList


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[WalletRecord]**](WalletRecord.md) | List of wallet records | [optional] 

## Example

```python
from pyaries.models.wallet_list import WalletList

# TODO update the JSON string below
json = "{}"
# create an instance of WalletList from a JSON string
wallet_list_instance = WalletList.from_json(json)
# print the JSON string representation of the object
print WalletList.to_json()

# convert the object into a dict
wallet_list_dict = wallet_list_instance.to_dict()
# create an instance of WalletList from a dict
wallet_list_form_dict = wallet_list.from_dict(wallet_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


