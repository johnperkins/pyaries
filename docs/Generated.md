# Generated


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**master_secret** | **str** |  | [optional] 
**number** | **str** |  | [optional] 
**remainder** | **str** |  | [optional] 

## Example

```python
from pyaries.models.generated import Generated

# TODO update the JSON string below
json = "{}"
# create an instance of Generated from a JSON string
generated_instance = Generated.from_json(json)
# print the JSON string representation of the object
print Generated.to_json()

# convert the object into a dict
generated_dict = generated_instance.to_dict()
# create an instance of Generated from a dict
generated_form_dict = generated.from_dict(generated_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


