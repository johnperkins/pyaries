# V20PresFormat


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attach_id** | **str** | Attachment identifier | 
**format** | **str** | Attachment format specifier | 

## Example

```python
from pyaries.models.v20_pres_format import V20PresFormat

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresFormat from a JSON string
v20_pres_format_instance = V20PresFormat.from_json(json)
# print the JSON string representation of the object
print V20PresFormat.to_json()

# convert the object into a dict
v20_pres_format_dict = v20_pres_format_instance.to_dict()
# create an instance of V20PresFormat from a dict
v20_pres_format_form_dict = v20_pres_format.from_dict(v20_pres_format_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


