# DIDEndpointWithType


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**did** | **str** | DID of interest | 
**endpoint** | **str** | Endpoint to set (omit to delete) | [optional] 
**endpoint_type** | **str** | Endpoint type to set (default &#39;Endpoint&#39;); affects only public or posted DIDs | [optional] 

## Example

```python
from pyaries.models.did_endpoint_with_type import DIDEndpointWithType

# TODO update the JSON string below
json = "{}"
# create an instance of DIDEndpointWithType from a JSON string
did_endpoint_with_type_instance = DIDEndpointWithType.from_json(json)
# print the JSON string representation of the object
print DIDEndpointWithType.to_json()

# convert the object into a dict
did_endpoint_with_type_dict = did_endpoint_with_type_instance.to_dict()
# create an instance of DIDEndpointWithType from a dict
did_endpoint_with_type_form_dict = did_endpoint_with_type.from_dict(did_endpoint_with_type_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


