# pyaries.TrustpingApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**send_ping**](TrustpingApi.md#send_ping) | **POST** /connections/{conn_id}/send-ping | Send a trust ping to a connection


# **send_ping**
> PingRequestResponse send_ping(conn_id, body=body)

Send a trust ping to a connection

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.TrustpingApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    body = pyaries.PingRequest() # PingRequest |  (optional)

    try:
        # Send a trust ping to a connection
        api_response = api_instance.send_ping(conn_id, body=body)
        print("The response of TrustpingApi->send_ping:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling TrustpingApi->send_ping: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **body** | [**PingRequest**](PingRequest.md)|  | [optional] 

### Return type

[**PingRequestResponse**](PingRequestResponse.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

