# V10CredentialProblemReportRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | 

## Example

```python
from pyaries.models.v10_credential_problem_report_request import V10CredentialProblemReportRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialProblemReportRequest from a JSON string
v10_credential_problem_report_request_instance = V10CredentialProblemReportRequest.from_json(json)
# print the JSON string representation of the object
print V10CredentialProblemReportRequest.to_json()

# convert the object into a dict
v10_credential_problem_report_request_dict = v10_credential_problem_report_request_instance.to_dict()
# create an instance of V10CredentialProblemReportRequest from a dict
v10_credential_problem_report_request_form_dict = v10_credential_problem_report_request.from_dict(v10_credential_problem_report_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


