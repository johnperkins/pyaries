# RevRegIssuedResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **int** | Number of credentials issued against revocation registry | [optional] 

## Example

```python
from pyaries.models.rev_reg_issued_result import RevRegIssuedResult

# TODO update the JSON string below
json = "{}"
# create an instance of RevRegIssuedResult from a JSON string
rev_reg_issued_result_instance = RevRegIssuedResult.from_json(json)
# print the JSON string representation of the object
print RevRegIssuedResult.to_json()

# convert the object into a dict
rev_reg_issued_result_dict = rev_reg_issued_result_instance.to_dict()
# create an instance of RevRegIssuedResult from a dict
rev_reg_issued_result_form_dict = rev_reg_issued_result.from_dict(rev_reg_issued_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


