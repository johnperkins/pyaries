# MediationRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connection_id** | **str** |  | 
**created_at** | **str** | Time of record creation | [optional] 
**endpoint** | **str** |  | [optional] 
**mediation_id** | **str** |  | [optional] 
**mediator_terms** | **List[str]** |  | [optional] 
**recipient_terms** | **List[str]** |  | [optional] 
**role** | **str** |  | 
**routing_keys** | **List[str]** |  | [optional] 
**state** | **str** | Current record state | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.mediation_record import MediationRecord

# TODO update the JSON string below
json = "{}"
# create an instance of MediationRecord from a JSON string
mediation_record_instance = MediationRecord.from_json(json)
# print the JSON string representation of the object
print MediationRecord.to_json()

# convert the object into a dict
mediation_record_dict = mediation_record_instance.to_dict()
# create an instance of MediationRecord from a dict
mediation_record_form_dict = mediation_record.from_dict(mediation_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


