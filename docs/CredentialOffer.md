# CredentialOffer


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**credential_preview** | [**CredentialPreview**](CredentialPreview.md) |  | [optional] 
**offersattach** | [**List[AttachDecorator]**](AttachDecorator.md) |  | 

## Example

```python
from pyaries.models.credential_offer import CredentialOffer

# TODO update the JSON string below
json = "{}"
# create an instance of CredentialOffer from a JSON string
credential_offer_instance = CredentialOffer.from_json(json)
# print the JSON string representation of the object
print CredentialOffer.to_json()

# convert the object into a dict
credential_offer_dict = credential_offer_instance.to_dict()
# create an instance of CredentialOffer from a dict
credential_offer_form_dict = credential_offer.from_dict(credential_offer_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


