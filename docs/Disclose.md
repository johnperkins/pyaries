# Disclose


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**protocols** | [**List[ProtocolDescriptor]**](ProtocolDescriptor.md) | List of protocol descriptors | 

## Example

```python
from pyaries.models.disclose import Disclose

# TODO update the JSON string below
json = "{}"
# create an instance of Disclose from a JSON string
disclose_instance = Disclose.from_json(json)
# print the JSON string representation of the object
print Disclose.to_json()

# convert the object into a dict
disclose_dict = disclose_instance.to_dict()
# create an instance of Disclose from a dict
disclose_form_dict = disclose.from_dict(disclose_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


