# SchemaSendRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | **List[str]** | List of schema attributes | 
**schema_name** | **str** | Schema name | 
**schema_version** | **str** | Schema version | 

## Example

```python
from pyaries.models.schema_send_request import SchemaSendRequest

# TODO update the JSON string below
json = "{}"
# create an instance of SchemaSendRequest from a JSON string
schema_send_request_instance = SchemaSendRequest.from_json(json)
# print the JSON string representation of the object
print SchemaSendRequest.to_json()

# convert the object into a dict
schema_send_request_dict = schema_send_request_instance.to_dict()
# create an instance of SchemaSendRequest from a dict
schema_send_request_form_dict = schema_send_request.from_dict(schema_send_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


