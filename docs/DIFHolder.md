# DIFHolder


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**directive** | **str** | Preference | [optional] 
**field_id** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.dif_holder import DIFHolder

# TODO update the JSON string below
json = "{}"
# create an instance of DIFHolder from a JSON string
dif_holder_instance = DIFHolder.from_json(json)
# print the JSON string representation of the object
print DIFHolder.to_json()

# convert the object into a dict
dif_holder_dict = dif_holder_instance.to_dict()
# create an instance of DIFHolder from a dict
dif_holder_form_dict = dif_holder.from_dict(dif_holder_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


