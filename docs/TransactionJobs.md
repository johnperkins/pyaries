# TransactionJobs


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_my_job** | **str** | My transaction related job | [optional] 
**transaction_their_job** | **str** | Their transaction related job | [optional] 

## Example

```python
from pyaries.models.transaction_jobs import TransactionJobs

# TODO update the JSON string below
json = "{}"
# create an instance of TransactionJobs from a JSON string
transaction_jobs_instance = TransactionJobs.from_json(json)
# print the JSON string representation of the object
print TransactionJobs.to_json()

# convert the object into a dict
transaction_jobs_dict = transaction_jobs_instance.to_dict()
# create an instance of TransactionJobs from a dict
transaction_jobs_form_dict = transaction_jobs.from_dict(transaction_jobs_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


