# EndpointsResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**my_endpoint** | **str** | My endpoint | [optional] 
**their_endpoint** | **str** | Their endpoint | [optional] 

## Example

```python
from pyaries.models.endpoints_result import EndpointsResult

# TODO update the JSON string below
json = "{}"
# create an instance of EndpointsResult from a JSON string
endpoints_result_instance = EndpointsResult.from_json(json)
# print the JSON string representation of the object
print EndpointsResult.to_json()

# convert the object into a dict
endpoints_result_dict = endpoints_result_instance.to_dict()
# create an instance of EndpointsResult from a dict
endpoints_result_form_dict = endpoints_result.from_dict(endpoints_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


