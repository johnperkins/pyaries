# V20PresRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**formats** | [**List[V20PresFormat]**](V20PresFormat.md) |  | 
**request_presentationsattach** | [**List[AttachDecorator]**](AttachDecorator.md) | Attachment per acceptable format on corresponding identifier | 
**will_confirm** | **bool** | Whether verifier will send confirmation ack | [optional] 

## Example

```python
from pyaries.models.v20_pres_request import V20PresRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresRequest from a JSON string
v20_pres_request_instance = V20PresRequest.from_json(json)
# print the JSON string representation of the object
print V20PresRequest.to_json()

# convert the object into a dict
v20_pres_request_dict = v20_pres_request_instance.to_dict()
# create an instance of V20PresRequest from a dict
v20_pres_request_form_dict = v20_pres_request.from_dict(v20_pres_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


