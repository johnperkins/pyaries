# V10CredentialExchangeCredentialOfferDict

Credential offer message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**credential_preview** | [**CredentialPreview**](CredentialPreview.md) |  | [optional] 
**offersattach** | [**List[AttachDecorator]**](AttachDecorator.md) |  | 

## Example

```python
from pyaries.models.v10_credential_exchange_credential_offer_dict import V10CredentialExchangeCredentialOfferDict

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialExchangeCredentialOfferDict from a JSON string
v10_credential_exchange_credential_offer_dict_instance = V10CredentialExchangeCredentialOfferDict.from_json(json)
# print the JSON string representation of the object
print V10CredentialExchangeCredentialOfferDict.to_json()

# convert the object into a dict
v10_credential_exchange_credential_offer_dict_dict = v10_credential_exchange_credential_offer_dict_instance.to_dict()
# create an instance of V10CredentialExchangeCredentialOfferDict from a dict
v10_credential_exchange_credential_offer_dict_form_dict = v10_credential_exchange_credential_offer_dict.from_dict(v10_credential_exchange_credential_offer_dict_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


