# IndyCredRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**blinded_ms** | **object** | Blinded master secret | 
**blinded_ms_correctness_proof** | **object** | Blinded master secret correctness proof | 
**cred_def_id** | **str** | Credential definition identifier | 
**nonce** | **str** | Nonce in credential request | 
**prover_did** | **str** | Prover DID | [optional] 

## Example

```python
from pyaries.models.indy_cred_request import IndyCredRequest

# TODO update the JSON string below
json = "{}"
# create an instance of IndyCredRequest from a JSON string
indy_cred_request_instance = IndyCredRequest.from_json(json)
# print the JSON string representation of the object
print IndyCredRequest.to_json()

# convert the object into a dict
indy_cred_request_dict = indy_cred_request_instance.to_dict()
# create an instance of IndyCredRequest from a dict
indy_cred_request_form_dict = indy_cred_request.from_dict(indy_cred_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


