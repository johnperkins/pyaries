# RemoveWalletRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wallet_key** | **str** | Master key used for key derivation. Only required for             unmanaged wallets. | [optional] 

## Example

```python
from pyaries.models.remove_wallet_request import RemoveWalletRequest

# TODO update the JSON string below
json = "{}"
# create an instance of RemoveWalletRequest from a JSON string
remove_wallet_request_instance = RemoveWalletRequest.from_json(json)
# print the JSON string representation of the object
print RemoveWalletRequest.to_json()

# convert the object into a dict
remove_wallet_request_dict = remove_wallet_request_instance.to_dict()
# create an instance of RemoveWalletRequest from a dict
remove_wallet_request_form_dict = remove_wallet_request.from_dict(remove_wallet_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


