# V10CredentialExchange


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_issue** | **bool** | Issuer choice to issue to request in this credential exchange | [optional] 
**auto_offer** | **bool** | Holder choice to accept offer in this credential exchange | [optional] 
**auto_remove** | **bool** | Issuer choice to remove this credential exchange record when complete | [optional] 
**connection_id** | **str** | Connection identifier | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**credential** | [**IndyCredInfo**](IndyCredInfo.md) |  | [optional] 
**credential_definition_id** | **str** | Credential definition identifier | [optional] 
**credential_exchange_id** | **str** | Credential exchange identifier | [optional] 
**credential_id** | **str** | Credential identifier | [optional] 
**credential_offer** | [**IndyCredAbstract**](IndyCredAbstract.md) |  | [optional] 
**credential_offer_dict** | [**CredentialOffer**](CredentialOffer.md) |  | [optional] 
**credential_proposal_dict** | [**CredentialProposal**](CredentialProposal.md) |  | [optional] 
**credential_request** | [**IndyCredRequest**](IndyCredRequest.md) |  | [optional] 
**credential_request_metadata** | **object** | (Indy) credential request metadata | [optional] 
**error_msg** | **str** | Error message | [optional] 
**initiator** | **str** | Issue-credential exchange initiator: self or external | [optional] 
**parent_thread_id** | **str** | Parent thread identifier | [optional] 
**raw_credential** | [**IndyCredential**](IndyCredential.md) |  | [optional] 
**revoc_reg_id** | **str** | Revocation registry identifier | [optional] 
**revocation_id** | **str** | Credential identifier within revocation registry | [optional] 
**role** | **str** | Issue-credential exchange role: holder or issuer | [optional] 
**schema_id** | **str** | Schema identifier | [optional] 
**state** | **str** | Issue-credential exchange state | [optional] 
**thread_id** | **str** | Thread identifier | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.v10_credential_exchange import V10CredentialExchange

# TODO update the JSON string below
json = "{}"
# create an instance of V10CredentialExchange from a JSON string
v10_credential_exchange_instance = V10CredentialExchange.from_json(json)
# print the JSON string representation of the object
print V10CredentialExchange.to_json()

# convert the object into a dict
v10_credential_exchange_dict = v10_credential_exchange_instance.to_dict()
# create an instance of V10CredentialExchange from a dict
v10_credential_exchange_form_dict = v10_credential_exchange.from_dict(v10_credential_exchange_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


