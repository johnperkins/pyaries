# DIFPresSpec


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issuer_id** | **str** | Issuer identifier to sign the presentation, if different from current public DID | [optional] 
**presentation_definition** | [**PresentationDefinition**](PresentationDefinition.md) |  | [optional] 
**record_ids** | **object** | Mapping of input_descriptor id to list of stored W3C credential record_id | [optional] 
**reveal_doc** | **object** | reveal doc [JSON-LD frame] dict used to derive the credential when selective disclosure is required | [optional] 

## Example

```python
from pyaries.models.dif_pres_spec import DIFPresSpec

# TODO update the JSON string below
json = "{}"
# create an instance of DIFPresSpec from a JSON string
dif_pres_spec_instance = DIFPresSpec.from_json(json)
# print the JSON string representation of the object
print DIFPresSpec.to_json()

# convert the object into a dict
dif_pres_spec_dict = dif_pres_spec_instance.to_dict()
# create an instance of DIFPresSpec from a dict
dif_pres_spec_form_dict = dif_pres_spec.from_dict(dif_pres_spec_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


