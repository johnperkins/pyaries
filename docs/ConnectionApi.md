# pyaries.ConnectionApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accept_invitation**](ConnectionApi.md#accept_invitation) | **POST** /connections/{conn_id}/accept-invitation | Accept a stored connection invitation
[**accept_request**](ConnectionApi.md#accept_request) | **POST** /connections/{conn_id}/accept-request | Accept a stored connection request
[**create_invitation**](ConnectionApi.md#create_invitation) | **POST** /connections/create-invitation | Create a new connection invitation
[**create_static_connection**](ConnectionApi.md#create_static_connection) | **POST** /connections/create-static | Create a new static connection
[**delete_connection**](ConnectionApi.md#delete_connection) | **DELETE** /connections/{conn_id} | Remove an existing connection record
[**establish_inbound**](ConnectionApi.md#establish_inbound) | **POST** /connections/{conn_id}/establish-inbound/{ref_id} | Assign another connection as the inbound connection
[**get_connection**](ConnectionApi.md#get_connection) | **GET** /connections/{conn_id} | Fetch a single connection record
[**get_connection_endpoint**](ConnectionApi.md#get_connection_endpoint) | **GET** /connections/{conn_id}/endpoints | Fetch connection remote endpoint
[**get_connections**](ConnectionApi.md#get_connections) | **GET** /connections | Query agent-to-agent connections
[**get_metadata**](ConnectionApi.md#get_metadata) | **GET** /connections/{conn_id}/metadata | Fetch connection metadata
[**receive_invitation**](ConnectionApi.md#receive_invitation) | **POST** /connections/receive-invitation | Receive a new connection invitation
[**set_metadata**](ConnectionApi.md#set_metadata) | **POST** /connections/{conn_id}/metadata | Set connection metadata


# **accept_invitation**
> ConnRecord accept_invitation(conn_id, mediation_id=mediation_id, my_endpoint=my_endpoint, my_label=my_label)

Accept a stored connection invitation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    mediation_id = 'mediation_id_example' # str | Identifier for active mediation record to be used (optional)
    my_endpoint = 'my_endpoint_example' # str | My URL endpoint (optional)
    my_label = 'my_label_example' # str | Label for connection (optional)

    try:
        # Accept a stored connection invitation
        api_response = api_instance.accept_invitation(conn_id, mediation_id=mediation_id, my_endpoint=my_endpoint, my_label=my_label)
        print("The response of ConnectionApi->accept_invitation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->accept_invitation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **mediation_id** | **str**| Identifier for active mediation record to be used | [optional] 
 **my_endpoint** | **str**| My URL endpoint | [optional] 
 **my_label** | **str**| Label for connection | [optional] 

### Return type

[**ConnRecord**](ConnRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **accept_request**
> ConnRecord accept_request(conn_id, my_endpoint=my_endpoint)

Accept a stored connection request

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    my_endpoint = 'my_endpoint_example' # str | My URL endpoint (optional)

    try:
        # Accept a stored connection request
        api_response = api_instance.accept_request(conn_id, my_endpoint=my_endpoint)
        print("The response of ConnectionApi->accept_request:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->accept_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **my_endpoint** | **str**| My URL endpoint | [optional] 

### Return type

[**ConnRecord**](ConnRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_invitation**
> InvitationResult create_invitation(alias=alias, auto_accept=auto_accept, multi_use=multi_use, public=public, body=body)

Create a new connection invitation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    alias = 'alias_example' # str | Alias (optional)
    auto_accept = True # bool | Auto-accept connection (defaults to configuration) (optional)
    multi_use = True # bool | Create invitation for multiple use (default false) (optional)
    public = True # bool | Create invitation from public DID (default false) (optional)
    body = pyaries.CreateInvitationRequest() # CreateInvitationRequest |  (optional)

    try:
        # Create a new connection invitation
        api_response = api_instance.create_invitation(alias=alias, auto_accept=auto_accept, multi_use=multi_use, public=public, body=body)
        print("The response of ConnectionApi->create_invitation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->create_invitation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alias** | **str**| Alias | [optional] 
 **auto_accept** | **bool**| Auto-accept connection (defaults to configuration) | [optional] 
 **multi_use** | **bool**| Create invitation for multiple use (default false) | [optional] 
 **public** | **bool**| Create invitation from public DID (default false) | [optional] 
 **body** | [**CreateInvitationRequest**](CreateInvitationRequest.md)|  | [optional] 

### Return type

[**InvitationResult**](InvitationResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_static_connection**
> ConnectionStaticResult create_static_connection(body=body)

Create a new static connection

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    body = pyaries.ConnectionStaticRequest() # ConnectionStaticRequest |  (optional)

    try:
        # Create a new static connection
        api_response = api_instance.create_static_connection(body=body)
        print("The response of ConnectionApi->create_static_connection:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->create_static_connection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ConnectionStaticRequest**](ConnectionStaticRequest.md)|  | [optional] 

### Return type

[**ConnectionStaticResult**](ConnectionStaticResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_connection**
> object delete_connection(conn_id)

Remove an existing connection record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier

    try:
        # Remove an existing connection record
        api_response = api_instance.delete_connection(conn_id)
        print("The response of ConnectionApi->delete_connection:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->delete_connection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **establish_inbound**
> object establish_inbound(conn_id, ref_id)

Assign another connection as the inbound connection

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    ref_id = 'ref_id_example' # str | Inbound connection identifier

    try:
        # Assign another connection as the inbound connection
        api_response = api_instance.establish_inbound(conn_id, ref_id)
        print("The response of ConnectionApi->establish_inbound:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->establish_inbound: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **ref_id** | **str**| Inbound connection identifier | 

### Return type

**object**

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_connection**
> ConnRecord get_connection(conn_id)

Fetch a single connection record

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier

    try:
        # Fetch a single connection record
        api_response = api_instance.get_connection(conn_id)
        print("The response of ConnectionApi->get_connection:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->get_connection: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 

### Return type

[**ConnRecord**](ConnRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_connection_endpoint**
> EndpointsResult get_connection_endpoint(conn_id)

Fetch connection remote endpoint

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier

    try:
        # Fetch connection remote endpoint
        api_response = api_instance.get_connection_endpoint(conn_id)
        print("The response of ConnectionApi->get_connection_endpoint:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->get_connection_endpoint: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 

### Return type

[**EndpointsResult**](EndpointsResult.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_connections**
> ConnectionList get_connections(alias=alias, connection_protocol=connection_protocol, invitation_key=invitation_key, invitation_msg_id=invitation_msg_id, my_did=my_did, state=state, their_did=their_did, their_public_did=their_public_did, their_role=their_role)

Query agent-to-agent connections

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    alias = 'alias_example' # str | Alias (optional)
    connection_protocol = 'connection_protocol_example' # str | Connection protocol used (optional)
    invitation_key = 'invitation_key_example' # str | invitation key (optional)
    invitation_msg_id = 'invitation_msg_id_example' # str | Identifier of the associated Invitation Mesage (optional)
    my_did = 'my_did_example' # str | My DID (optional)
    state = 'state_example' # str | Connection state (optional)
    their_did = 'their_did_example' # str | Their DID (optional)
    their_public_did = 'their_public_did_example' # str | Their Public DID (optional)
    their_role = 'their_role_example' # str | Their role in the connection protocol (optional)

    try:
        # Query agent-to-agent connections
        api_response = api_instance.get_connections(alias=alias, connection_protocol=connection_protocol, invitation_key=invitation_key, invitation_msg_id=invitation_msg_id, my_did=my_did, state=state, their_did=their_did, their_public_did=their_public_did, their_role=their_role)
        print("The response of ConnectionApi->get_connections:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->get_connections: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alias** | **str**| Alias | [optional] 
 **connection_protocol** | **str**| Connection protocol used | [optional] 
 **invitation_key** | **str**| invitation key | [optional] 
 **invitation_msg_id** | **str**| Identifier of the associated Invitation Mesage | [optional] 
 **my_did** | **str**| My DID | [optional] 
 **state** | **str**| Connection state | [optional] 
 **their_did** | **str**| Their DID | [optional] 
 **their_public_did** | **str**| Their Public DID | [optional] 
 **their_role** | **str**| Their role in the connection protocol | [optional] 

### Return type

[**ConnectionList**](ConnectionList.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_metadata**
> ConnectionMetadata get_metadata(conn_id, key=key)

Fetch connection metadata

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    key = 'key_example' # str | Key to retrieve. (optional)

    try:
        # Fetch connection metadata
        api_response = api_instance.get_metadata(conn_id, key=key)
        print("The response of ConnectionApi->get_metadata:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->get_metadata: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **key** | **str**| Key to retrieve. | [optional] 

### Return type

[**ConnectionMetadata**](ConnectionMetadata.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **receive_invitation**
> ConnRecord receive_invitation(alias=alias, auto_accept=auto_accept, mediation_id=mediation_id, body=body)

Receive a new connection invitation

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    alias = 'alias_example' # str | Alias (optional)
    auto_accept = True # bool | Auto-accept connection (defaults to configuration) (optional)
    mediation_id = 'mediation_id_example' # str | Identifier for active mediation record to be used (optional)
    body = pyaries.ReceiveInvitationRequest() # ReceiveInvitationRequest |  (optional)

    try:
        # Receive a new connection invitation
        api_response = api_instance.receive_invitation(alias=alias, auto_accept=auto_accept, mediation_id=mediation_id, body=body)
        print("The response of ConnectionApi->receive_invitation:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->receive_invitation: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alias** | **str**| Alias | [optional] 
 **auto_accept** | **bool**| Auto-accept connection (defaults to configuration) | [optional] 
 **mediation_id** | **str**| Identifier for active mediation record to be used | [optional] 
 **body** | [**ReceiveInvitationRequest**](ReceiveInvitationRequest.md)|  | [optional] 

### Return type

[**ConnRecord**](ConnRecord.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_metadata**
> ConnectionMetadata set_metadata(conn_id, body=body)

Set connection metadata

### Example

* Api Key Authentication (AuthorizationHeader):
```python
from __future__ import print_function
import time
import os
import pyaries
from pyaries.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = pyaries.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: AuthorizationHeader
configuration.api_key['AuthorizationHeader'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['AuthorizationHeader'] = 'Bearer'

# Enter a context with an instance of the API client
with pyaries.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pyaries.ConnectionApi(api_client)
    conn_id = 'conn_id_example' # str | Connection identifier
    body = pyaries.ConnectionMetadataSetRequest() # ConnectionMetadataSetRequest |  (optional)

    try:
        # Set connection metadata
        api_response = api_instance.set_metadata(conn_id, body=body)
        print("The response of ConnectionApi->set_metadata:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ConnectionApi->set_metadata: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **conn_id** | **str**| Connection identifier | 
 **body** | [**ConnectionMetadataSetRequest**](ConnectionMetadataSetRequest.md)|  | [optional] 

### Return type

[**ConnectionMetadata**](ConnectionMetadata.md)

### Authorization

[AuthorizationHeader](../README.md#AuthorizationHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

