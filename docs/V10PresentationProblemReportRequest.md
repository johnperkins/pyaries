# V10PresentationProblemReportRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | 

## Example

```python
from pyaries.models.v10_presentation_problem_report_request import V10PresentationProblemReportRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationProblemReportRequest from a JSON string
v10_presentation_problem_report_request_instance = V10PresentationProblemReportRequest.from_json(json)
# print the JSON string representation of the object
print V10PresentationProblemReportRequest.to_json()

# convert the object into a dict
v10_presentation_problem_report_request_dict = v10_presentation_problem_report_request_instance.to_dict()
# create an instance of V10PresentationProblemReportRequest from a dict
v10_presentation_problem_report_request_form_dict = v10_presentation_problem_report_request.from_dict(v10_presentation_problem_report_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


