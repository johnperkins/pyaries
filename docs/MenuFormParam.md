# MenuFormParam


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default** | **str** | Default parameter value | [optional] 
**description** | **str** | Additional descriptive text for menu form parameter | [optional] 
**name** | **str** | Menu parameter name | 
**required** | **bool** | Whether parameter is required | [optional] 
**title** | **str** | Menu parameter title | 
**type** | **str** | Menu form parameter input type | [optional] 

## Example

```python
from pyaries.models.menu_form_param import MenuFormParam

# TODO update the JSON string below
json = "{}"
# create an instance of MenuFormParam from a JSON string
menu_form_param_instance = MenuFormParam.from_json(json)
# print the JSON string representation of the object
print MenuFormParam.to_json()

# convert the object into a dict
menu_form_param_dict = menu_form_param_instance.to_dict()
# create an instance of MenuFormParam from a dict
menu_form_param_form_dict = menu_form_param.from_dict(menu_form_param_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


