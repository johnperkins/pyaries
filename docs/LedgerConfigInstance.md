# LedgerConfigInstance


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**genesis_file** | **str** | genesis_file | [optional] 
**genesis_transactions** | **str** | genesis_transactions | [optional] 
**genesis_url** | **str** | genesis_url | [optional] 
**id** | **str** | ledger_id | [optional] 
**is_production** | **bool** | is_production | [optional] 

## Example

```python
from pyaries.models.ledger_config_instance import LedgerConfigInstance

# TODO update the JSON string below
json = "{}"
# create an instance of LedgerConfigInstance from a JSON string
ledger_config_instance_instance = LedgerConfigInstance.from_json(json)
# print the JSON string representation of the object
print LedgerConfigInstance.to_json()

# convert the object into a dict
ledger_config_instance_dict = ledger_config_instance_instance.to_dict()
# create an instance of LedgerConfigInstance from a dict
ledger_config_instance_form_dict = ledger_config_instance.from_dict(ledger_config_instance_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


