# V20CredExRecordListResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**List[V20CredExRecordDetail]**](V20CredExRecordDetail.md) | Credential exchange records and corresponding detail records | [optional] 

## Example

```python
from pyaries.models.v20_cred_ex_record_list_result import V20CredExRecordListResult

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExRecordListResult from a JSON string
v20_cred_ex_record_list_result_instance = V20CredExRecordListResult.from_json(json)
# print the JSON string representation of the object
print V20CredExRecordListResult.to_json()

# convert the object into a dict
v20_cred_ex_record_list_result_dict = v20_cred_ex_record_list_result_instance.to_dict()
# create an instance of V20CredExRecordListResult from a dict
v20_cred_ex_record_list_result_form_dict = v20_cred_ex_record_list_result.from_dict(v20_cred_ex_record_list_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


