# ActionMenuFetchResultResult

Action menu

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**description** | **str** | Introductory text for the menu | [optional] 
**errormsg** | **str** | An optional error message to display in menu header | [optional] 
**options** | [**List[MenuOption]**](MenuOption.md) | List of menu options | 
**title** | **str** | Menu title | [optional] 

## Example

```python
from pyaries.models.action_menu_fetch_result_result import ActionMenuFetchResultResult

# TODO update the JSON string below
json = "{}"
# create an instance of ActionMenuFetchResultResult from a JSON string
action_menu_fetch_result_result_instance = ActionMenuFetchResultResult.from_json(json)
# print the JSON string representation of the object
print ActionMenuFetchResultResult.to_json()

# convert the object into a dict
action_menu_fetch_result_result_dict = action_menu_fetch_result_result_instance.to_dict()
# create an instance of ActionMenuFetchResultResult from a dict
action_menu_fetch_result_result_form_dict = action_menu_fetch_result_result.from_dict(action_menu_fetch_result_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


