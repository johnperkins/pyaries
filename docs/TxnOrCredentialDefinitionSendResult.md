# TxnOrCredentialDefinitionSendResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sent** | [**CredentialDefinitionSendResult**](CredentialDefinitionSendResult.md) |  | [optional] 
**txn** | [**TransactionRecord**](TransactionRecord.md) |  | [optional] 

## Example

```python
from pyaries.models.txn_or_credential_definition_send_result import TxnOrCredentialDefinitionSendResult

# TODO update the JSON string below
json = "{}"
# create an instance of TxnOrCredentialDefinitionSendResult from a JSON string
txn_or_credential_definition_send_result_instance = TxnOrCredentialDefinitionSendResult.from_json(json)
# print the JSON string representation of the object
print TxnOrCredentialDefinitionSendResult.to_json()

# convert the object into a dict
txn_or_credential_definition_send_result_dict = txn_or_credential_definition_send_result_instance.to_dict()
# create an instance of TxnOrCredentialDefinitionSendResult from a dict
txn_or_credential_definition_send_result_form_dict = txn_or_credential_definition_send_result.from_dict(txn_or_credential_definition_send_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


