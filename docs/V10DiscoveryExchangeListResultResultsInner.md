# V10DiscoveryExchangeListResultResultsInner

Discover Features v1.0 exchange record

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connection_id** | **str** | Connection identifier | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**disclose** | [**V10DiscoveryRecordDisclose**](V10DiscoveryRecordDisclose.md) |  | [optional] 
**discovery_exchange_id** | **str** | Credential exchange identifier | [optional] 
**query_msg** | [**V10DiscoveryRecordQueryMsg**](V10DiscoveryRecordQueryMsg.md) |  | [optional] 
**state** | **str** | Current record state | [optional] 
**thread_id** | **str** | Thread identifier | [optional] 
**trace** | **bool** | Record trace information, based on agent configuration | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 

## Example

```python
from pyaries.models.v10_discovery_exchange_list_result_results_inner import V10DiscoveryExchangeListResultResultsInner

# TODO update the JSON string below
json = "{}"
# create an instance of V10DiscoveryExchangeListResultResultsInner from a JSON string
v10_discovery_exchange_list_result_results_inner_instance = V10DiscoveryExchangeListResultResultsInner.from_json(json)
# print the JSON string representation of the object
print V10DiscoveryExchangeListResultResultsInner.to_json()

# convert the object into a dict
v10_discovery_exchange_list_result_results_inner_dict = v10_discovery_exchange_list_result_results_inner_instance.to_dict()
# create an instance of V10DiscoveryExchangeListResultResultsInner from a dict
v10_discovery_exchange_list_result_results_inner_form_dict = v10_discovery_exchange_list_result_results_inner.from_dict(v10_discovery_exchange_list_result_results_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


