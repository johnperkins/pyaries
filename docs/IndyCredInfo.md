# IndyCredInfo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attrs** | **Dict[str, str]** | Attribute names and value | [optional] 
**cred_def_id** | **str** | Credential definition identifier | [optional] 
**cred_rev_id** | **str** | Credential revocation identifier | [optional] 
**referent** | **str** | Wallet referent | [optional] 
**rev_reg_id** | **str** | Revocation registry identifier | [optional] 
**schema_id** | **str** | Schema identifier | [optional] 

## Example

```python
from pyaries.models.indy_cred_info import IndyCredInfo

# TODO update the JSON string below
json = "{}"
# create an instance of IndyCredInfo from a JSON string
indy_cred_info_instance = IndyCredInfo.from_json(json)
# print the JSON string representation of the object
print IndyCredInfo.to_json()

# convert the object into a dict
indy_cred_info_dict = indy_cred_info_instance.to_dict()
# create an instance of IndyCredInfo from a dict
indy_cred_info_form_dict = indy_cred_info.from_dict(indy_cred_info_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


