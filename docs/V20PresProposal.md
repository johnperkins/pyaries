# V20PresProposal


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**formats** | [**List[V20PresFormat]**](V20PresFormat.md) |  | 
**proposalsattach** | [**List[AttachDecorator]**](AttachDecorator.md) | Attachment per acceptable format on corresponding identifier | 

## Example

```python
from pyaries.models.v20_pres_proposal import V20PresProposal

# TODO update the JSON string below
json = "{}"
# create an instance of V20PresProposal from a JSON string
v20_pres_proposal_instance = V20PresProposal.from_json(json)
# print the JSON string representation of the object
print V20PresProposal.to_json()

# convert the object into a dict
v20_pres_proposal_dict = v20_pres_proposal_instance.to_dict()
# create an instance of V20PresProposal from a dict
v20_pres_proposal_form_dict = v20_pres_proposal.from_dict(v20_pres_proposal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


