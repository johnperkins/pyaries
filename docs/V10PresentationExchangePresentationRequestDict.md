# V10PresentationExchangePresentationRequestDict

Presentation request message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message identifier | [optional] 
**type** | **str** | Message type | [optional] [readonly] 
**comment** | **str** | Human-readable comment | [optional] 
**request_presentationsattach** | [**List[AttachDecorator]**](AttachDecorator.md) |  | 

## Example

```python
from pyaries.models.v10_presentation_exchange_presentation_request_dict import V10PresentationExchangePresentationRequestDict

# TODO update the JSON string below
json = "{}"
# create an instance of V10PresentationExchangePresentationRequestDict from a JSON string
v10_presentation_exchange_presentation_request_dict_instance = V10PresentationExchangePresentationRequestDict.from_json(json)
# print the JSON string representation of the object
print V10PresentationExchangePresentationRequestDict.to_json()

# convert the object into a dict
v10_presentation_exchange_presentation_request_dict_dict = v10_presentation_exchange_presentation_request_dict_instance.to_dict()
# create an instance of V10PresentationExchangePresentationRequestDict from a dict
v10_presentation_exchange_presentation_request_dict_form_dict = v10_presentation_exchange_presentation_request_dict.from_dict(v10_presentation_exchange_presentation_request_dict_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


