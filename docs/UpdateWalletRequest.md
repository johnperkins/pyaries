# UpdateWalletRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_url** | **str** | Image url for this wallet. This image url is publicized            (self-attested) to other agents as part of forming a connection. | [optional] 
**label** | **str** | Label for this wallet. This label is publicized            (self-attested) to other agents as part of forming a connection. | [optional] 
**wallet_dispatch_type** | **str** | Webhook target dispatch type for this wallet.             default - Dispatch only to webhooks associated with this wallet.             base - Dispatch only to webhooks associated with the base wallet.             both - Dispatch to both webhook targets. | [optional] 
**wallet_webhook_urls** | **List[str]** | List of Webhook URLs associated with this subwallet | [optional] 

## Example

```python
from pyaries.models.update_wallet_request import UpdateWalletRequest

# TODO update the JSON string below
json = "{}"
# create an instance of UpdateWalletRequest from a JSON string
update_wallet_request_instance = UpdateWalletRequest.from_json(json)
# print the JSON string representation of the object
print UpdateWalletRequest.to_json()

# convert the object into a dict
update_wallet_request_dict = update_wallet_request_instance.to_dict()
# create an instance of UpdateWalletRequest from a dict
update_wallet_request_form_dict = update_wallet_request.from_dict(update_wallet_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


