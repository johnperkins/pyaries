# RevRegWalletUpdatedResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accum_calculated** | **object** | Calculated accumulator for phantom revocations | [optional] 
**accum_fixed** | **object** | Applied ledger transaction to fix revocations | [optional] 
**rev_reg_delta** | **object** | Indy revocation registry delta | [optional] 

## Example

```python
from pyaries.models.rev_reg_wallet_updated_result import RevRegWalletUpdatedResult

# TODO update the JSON string below
json = "{}"
# create an instance of RevRegWalletUpdatedResult from a JSON string
rev_reg_wallet_updated_result_instance = RevRegWalletUpdatedResult.from_json(json)
# print the JSON string representation of the object
print RevRegWalletUpdatedResult.to_json()

# convert the object into a dict
rev_reg_wallet_updated_result_dict = rev_reg_wallet_updated_result_instance.to_dict()
# create an instance of RevRegWalletUpdatedResult from a dict
rev_reg_wallet_updated_result_form_dict = rev_reg_wallet_updated_result.from_dict(rev_reg_wallet_updated_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


