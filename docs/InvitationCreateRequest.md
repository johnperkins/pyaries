# InvitationCreateRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accept** | **List[str]** | List of mime type in order of preference that should be use in responding to the message | [optional] 
**alias** | **str** | Alias for connection | [optional] 
**attachments** | [**List[AttachmentDef]**](AttachmentDef.md) | Optional invitation attachments | [optional] 
**handshake_protocols** | **List[str]** |  | [optional] 
**mediation_id** | **str** | Identifier for active mediation record to be used | [optional] 
**metadata** | **object** | Optional metadata to attach to the connection created with the invitation | [optional] 
**my_label** | **str** | Label for connection invitation | [optional] 
**protocol_version** | **str** | OOB protocol version | [optional] 
**use_public_did** | **bool** | Whether to use public DID in invitation | [optional] 

## Example

```python
from pyaries.models.invitation_create_request import InvitationCreateRequest

# TODO update the JSON string below
json = "{}"
# create an instance of InvitationCreateRequest from a JSON string
invitation_create_request_instance = InvitationCreateRequest.from_json(json)
# print the JSON string representation of the object
print InvitationCreateRequest.to_json()

# convert the object into a dict
invitation_create_request_dict = invitation_create_request_instance.to_dict()
# create an instance of InvitationCreateRequest from a dict
invitation_create_request_form_dict = invitation_create_request.from_dict(invitation_create_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


