# AdminMediationDeny


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mediator_terms** | **List[str]** | List of mediator rules for recipient | [optional] 
**recipient_terms** | **List[str]** | List of recipient rules for mediation | [optional] 

## Example

```python
from pyaries.models.admin_mediation_deny import AdminMediationDeny

# TODO update the JSON string below
json = "{}"
# create an instance of AdminMediationDeny from a JSON string
admin_mediation_deny_instance = AdminMediationDeny.from_json(json)
# print the JSON string representation of the object
print AdminMediationDeny.to_json()

# convert the object into a dict
admin_mediation_deny_dict = admin_mediation_deny_instance.to_dict()
# create an instance of AdminMediationDeny from a dict
admin_mediation_deny_form_dict = admin_mediation_deny.from_dict(admin_mediation_deny_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


