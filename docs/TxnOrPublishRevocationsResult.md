# TxnOrPublishRevocationsResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sent** | [**PublishRevocations**](PublishRevocations.md) |  | [optional] 
**txn** | [**TransactionRecord**](TransactionRecord.md) |  | [optional] 

## Example

```python
from pyaries.models.txn_or_publish_revocations_result import TxnOrPublishRevocationsResult

# TODO update the JSON string below
json = "{}"
# create an instance of TxnOrPublishRevocationsResult from a JSON string
txn_or_publish_revocations_result_instance = TxnOrPublishRevocationsResult.from_json(json)
# print the JSON string representation of the object
print TxnOrPublishRevocationsResult.to_json()

# convert the object into a dict
txn_or_publish_revocations_result_dict = txn_or_publish_revocations_result_instance.to_dict()
# create an instance of TxnOrPublishRevocationsResult from a dict
txn_or_publish_revocations_result_form_dict = txn_or_publish_revocations_result.from_dict(txn_or_publish_revocations_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


