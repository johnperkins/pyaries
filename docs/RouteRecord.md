# RouteRecord


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connection_id** | **str** |  | [optional] 
**created_at** | **str** | Time of record creation | [optional] 
**recipient_key** | **str** |  | 
**record_id** | **str** |  | [optional] 
**role** | **str** |  | [optional] 
**state** | **str** | Current record state | [optional] 
**updated_at** | **str** | Time of last record update | [optional] 
**wallet_id** | **str** |  | [optional] 

## Example

```python
from pyaries.models.route_record import RouteRecord

# TODO update the JSON string below
json = "{}"
# create an instance of RouteRecord from a JSON string
route_record_instance = RouteRecord.from_json(json)
# print the JSON string representation of the object
print RouteRecord.to_json()

# convert the object into a dict
route_record_dict = route_record_instance.to_dict()
# create an instance of RouteRecord from a dict
route_record_form_dict = route_record.from_dict(route_record_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


