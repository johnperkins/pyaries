# KeylistUpdateRule


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | **str** | Action for specific key | 
**recipient_key** | **str** | Key to remove or add | 

## Example

```python
from pyaries.models.keylist_update_rule import KeylistUpdateRule

# TODO update the JSON string below
json = "{}"
# create an instance of KeylistUpdateRule from a JSON string
keylist_update_rule_instance = KeylistUpdateRule.from_json(json)
# print the JSON string representation of the object
print KeylistUpdateRule.to_json()

# convert the object into a dict
keylist_update_rule_dict = keylist_update_rule_instance.to_dict()
# create an instance of KeylistUpdateRule from a dict
keylist_update_rule_form_dict = keylist_update_rule.from_dict(keylist_update_rule_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


