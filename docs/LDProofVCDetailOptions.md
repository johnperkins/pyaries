# LDProofVCDetailOptions


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**challenge** | **str** | A challenge to include in the proof. SHOULD be provided by the requesting party of the credential (&#x3D;holder) | [optional] 
**created** | **str** | The date and time of the proof (with a maximum accuracy in seconds). Defaults to current system time | [optional] 
**credential_status** | [**CredentialStatusOptions**](CredentialStatusOptions.md) |  | [optional] 
**domain** | **str** | The intended domain of validity for the proof | [optional] 
**proof_purpose** | **str** | The proof purpose used for the proof. Should match proof purposes registered in the Linked Data Proofs Specification | [optional] 
**proof_type** | **str** | The proof type used for the proof. Should match suites registered in the Linked Data Cryptographic Suite Registry | 

## Example

```python
from pyaries.models.ld_proof_vc_detail_options import LDProofVCDetailOptions

# TODO update the JSON string below
json = "{}"
# create an instance of LDProofVCDetailOptions from a JSON string
ld_proof_vc_detail_options_instance = LDProofVCDetailOptions.from_json(json)
# print the JSON string representation of the object
print LDProofVCDetailOptions.to_json()

# convert the object into a dict
ld_proof_vc_detail_options_dict = ld_proof_vc_detail_options_instance.to_dict()
# create an instance of LDProofVCDetailOptions from a dict
ld_proof_vc_detail_options_form_dict = ld_proof_vc_detail_options.from_dict(ld_proof_vc_detail_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


