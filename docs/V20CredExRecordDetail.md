# V20CredExRecordDetail


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cred_ex_record** | [**V20CredExRecord**](V20CredExRecord.md) |  | [optional] 
**indy** | [**V20CredExRecordIndy**](V20CredExRecordIndy.md) |  | [optional] 
**ld_proof** | [**V20CredExRecordLDProof**](V20CredExRecordLDProof.md) |  | [optional] 

## Example

```python
from pyaries.models.v20_cred_ex_record_detail import V20CredExRecordDetail

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredExRecordDetail from a JSON string
v20_cred_ex_record_detail_instance = V20CredExRecordDetail.from_json(json)
# print the JSON string representation of the object
print V20CredExRecordDetail.to_json()

# convert the object into a dict
v20_cred_ex_record_detail_dict = v20_cred_ex_record_detail_instance.to_dict()
# create an instance of V20CredExRecordDetail from a dict
v20_cred_ex_record_detail_form_dict = v20_cred_ex_record_detail.from_dict(v20_cred_ex_record_detail_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


