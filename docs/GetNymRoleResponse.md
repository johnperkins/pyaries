# GetNymRoleResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role** | **str** | Ledger role | [optional] 

## Example

```python
from pyaries.models.get_nym_role_response import GetNymRoleResponse

# TODO update the JSON string below
json = "{}"
# create an instance of GetNymRoleResponse from a JSON string
get_nym_role_response_instance = GetNymRoleResponse.from_json(json)
# print the JSON string representation of the object
print GetNymRoleResponse.to_json()

# convert the object into a dict
get_nym_role_response_dict = get_nym_role_response_instance.to_dict()
# create an instance of GetNymRoleResponse from a dict
get_nym_role_response_form_dict = get_nym_role_response.from_dict(get_nym_role_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


