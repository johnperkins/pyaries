# DIFProofProposal


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**input_descriptors** | [**List[InputDescriptors]**](InputDescriptors.md) |  | [optional] 
**options** | [**DIFOptions**](DIFOptions.md) |  | [optional] 

## Example

```python
from pyaries.models.dif_proof_proposal import DIFProofProposal

# TODO update the JSON string below
json = "{}"
# create an instance of DIFProofProposal from a JSON string
dif_proof_proposal_instance = DIFProofProposal.from_json(json)
# print the JSON string representation of the object
print DIFProofProposal.to_json()

# convert the object into a dict
dif_proof_proposal_dict = dif_proof_proposal_instance.to_dict()
# create an instance of DIFProofProposal from a dict
dif_proof_proposal_form_dict = dif_proof_proposal.from_dict(dif_proof_proposal_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


