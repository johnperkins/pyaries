# RevRegsCreated


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rev_reg_ids** | **List[str]** |  | [optional] 

## Example

```python
from pyaries.models.rev_regs_created import RevRegsCreated

# TODO update the JSON string below
json = "{}"
# create an instance of RevRegsCreated from a JSON string
rev_regs_created_instance = RevRegsCreated.from_json(json)
# print the JSON string representation of the object
print RevRegsCreated.to_json()

# convert the object into a dict
rev_regs_created_dict = rev_regs_created_instance.to_dict()
# create an instance of RevRegsCreated from a dict
rev_regs_created_form_dict = rev_regs_created.from_dict(rev_regs_created_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


