# MenuOption


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | Additional descriptive text for menu option | [optional] 
**disabled** | **bool** | Whether to show option as disabled | [optional] 
**form** | [**MenuForm**](MenuForm.md) |  | [optional] 
**name** | **str** | Menu option name (unique identifier) | 
**title** | **str** | Menu option title | 

## Example

```python
from pyaries.models.menu_option import MenuOption

# TODO update the JSON string below
json = "{}"
# create an instance of MenuOption from a JSON string
menu_option_instance = MenuOption.from_json(json)
# print the JSON string representation of the object
print MenuOption.to_json()

# convert the object into a dict
menu_option_dict = menu_option_instance.to_dict()
# create an instance of MenuOption from a dict
menu_option_form_dict = menu_option.from_dict(menu_option_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


