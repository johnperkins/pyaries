# IndyProofProofProofsProof


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**non_revoc_proof** | [**IndyNonRevocProof**](IndyNonRevocProof.md) |  | [optional] 
**primary_proof** | [**IndyPrimaryProof**](IndyPrimaryProof.md) |  | [optional] 

## Example

```python
from pyaries.models.indy_proof_proof_proofs_proof import IndyProofProofProofsProof

# TODO update the JSON string below
json = "{}"
# create an instance of IndyProofProofProofsProof from a JSON string
indy_proof_proof_proofs_proof_instance = IndyProofProofProofsProof.from_json(json)
# print the JSON string representation of the object
print IndyProofProofProofsProof.to_json()

# convert the object into a dict
indy_proof_proof_proofs_proof_dict = indy_proof_proof_proofs_proof_instance.to_dict()
# create an instance of IndyProofProofProofsProof from a dict
indy_proof_proof_proofs_proof_form_dict = indy_proof_proof_proofs_proof.from_dict(indy_proof_proof_proofs_proof_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


