# V20CredIssueRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | **str** | Human-readable comment | [optional] 

## Example

```python
from pyaries.models.v20_cred_issue_request import V20CredIssueRequest

# TODO update the JSON string below
json = "{}"
# create an instance of V20CredIssueRequest from a JSON string
v20_cred_issue_request_instance = V20CredIssueRequest.from_json(json)
# print the JSON string representation of the object
print V20CredIssueRequest.to_json()

# convert the object into a dict
v20_cred_issue_request_dict = v20_cred_issue_request_instance.to_dict()
# create an instance of V20CredIssueRequest from a dict
v20_cred_issue_request_form_dict = v20_cred_issue_request.from_dict(v20_cred_issue_request_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


