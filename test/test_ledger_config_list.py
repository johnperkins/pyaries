# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.ledger_config_list import LedgerConfigList  # noqa: E501
from pyaries.rest import ApiException

class TestLedgerConfigList(unittest.TestCase):
    """LedgerConfigList unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test LedgerConfigList
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `LedgerConfigList`
        """
        model = pyaries.models.ledger_config_list.LedgerConfigList()  # noqa: E501
        if include_optional :
            return LedgerConfigList(
                ledger_config_list = [
                    pyaries.models.ledger_config_instance.LedgerConfigInstance(
                        genesis_file = '', 
                        genesis_transactions = '', 
                        genesis_url = '', 
                        id = '', 
                        is_production = True, )
                    ]
            )
        else :
            return LedgerConfigList(
                ledger_config_list = [
                    pyaries.models.ledger_config_instance.LedgerConfigInstance(
                        genesis_file = '', 
                        genesis_transactions = '', 
                        genesis_url = '', 
                        id = '', 
                        is_production = True, )
                    ],
        )
        """

    def testLedgerConfigList(self):
        """Test LedgerConfigList"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
