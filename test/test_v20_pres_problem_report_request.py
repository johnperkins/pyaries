# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.v20_pres_problem_report_request import V20PresProblemReportRequest  # noqa: E501
from pyaries.rest import ApiException

class TestV20PresProblemReportRequest(unittest.TestCase):
    """V20PresProblemReportRequest unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V20PresProblemReportRequest
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V20PresProblemReportRequest`
        """
        model = pyaries.models.v20_pres_problem_report_request.V20PresProblemReportRequest()  # noqa: E501
        if include_optional :
            return V20PresProblemReportRequest(
                description = ''
            )
        else :
            return V20PresProblemReportRequest(
                description = '',
        )
        """

    def testV20PresProblemReportRequest(self):
        """Test V20PresProblemReportRequest"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
