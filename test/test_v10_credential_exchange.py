# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.v10_credential_exchange import V10CredentialExchange  # noqa: E501
from pyaries.rest import ApiException

class TestV10CredentialExchange(unittest.TestCase):
    """V10CredentialExchange unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V10CredentialExchange
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V10CredentialExchange`
        """
        model = pyaries.models.v10_credential_exchange.V10CredentialExchange()  # noqa: E501
        if include_optional :
            return V10CredentialExchange(
                auto_issue = False, 
                auto_offer = False, 
                auto_remove = False, 
                connection_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                created_at = '2021-12-31 23:59:59+00:00', 
                credential = pyaries.models.indy_cred_info.IndyCredInfo(
                    attrs = {
                        'key' : 'alice'
                        }, 
                    cred_def_id = 'WgWxqztrNooG92RXvxSTWv:3:CL:20:tag', 
                    cred_rev_id = '12345', 
                    referent = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                    rev_reg_id = 'WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0', 
                    schema_id = 'WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0', ), 
                credential_definition_id = 'WgWxqztrNooG92RXvxSTWv:3:CL:20:tag', 
                credential_exchange_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                credential_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                credential_offer = pyaries.models.indy_cred_abstract.IndyCredAbstract(
                    cred_def_id = 'WgWxqztrNooG92RXvxSTWv:3:CL:20:tag', 
                    key_correctness_proof = pyaries.models.indy_key_correctness_proof.IndyKeyCorrectnessProof(
                        c = '0', 
                        xr_cap = [
                            [
                                ''
                                ]
                            ], 
                        xz_cap = '0', ), 
                    nonce = '0', 
                    schema_id = 'WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0', ), 
                credential_offer_dict = pyaries.models.credential_offer.CredentialOffer(
                    @id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                    @type = 'https://didcomm.org/my-family/1.0/my-message-type', 
                    comment = '', 
                    credential_preview = pyaries.models.credential_preview.CredentialPreview(
                        @type = 'issue-credential/1.0/credential-preview', 
                        attributes = [
                            pyaries.models.cred_attr_spec.CredAttrSpec(
                                mime_type = 'image/jpeg', 
                                name = 'favourite_drink', 
                                value = 'martini', )
                            ], ), 
                    offers~attach = [
                        pyaries.models.attach_decorator.AttachDecorator(
                            @id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                            byte_count = 1234, 
                            data = pyaries.models.attach_decorator_data.AttachDecoratorData(
                                base64 = 'ey4uLn0=', 
                                json = {"sample": "content"}, 
                                jws = pyaries.models.attach_decorator_data_jws.AttachDecoratorDataJWS(
                                    header = pyaries.models.attach_decorator_data_jws_header.AttachDecoratorDataJWSHeader(
                                        kid = 'did:sov:LjgpST2rjsoxYegQDRm7EL#keys-4', ), 
                                    protected = 'ey4uLn0', 
                                    signature = 'ey4uLn0', 
                                    signatures = [
                                        pyaries.models.attach_decorator_data1_jws.AttachDecoratorData1JWS(
                                            header = pyaries.models.attach_decorator_data_jws_header.AttachDecoratorDataJWSHeader(
                                                kid = 'did:sov:LjgpST2rjsoxYegQDRm7EL#keys-4', ), 
                                            protected = 'ey4uLn0', 
                                            signature = 'ey4uLn0', )
                                        ], ), 
                                links = [
                                    'https://link.to/data'
                                    ], 
                                sha256 = '617a48c7c8afe0521efdc03e5bb0ad9e655893e6b4b51f0e794d70fba132aacb', ), 
                            description = 'view from doorway, facing east, with lights off', 
                            filename = 'IMG1092348.png', 
                            lastmod_time = '2021-12-31 23:59:59+00:00', 
                            mime_type = 'image/png', )
                        ], ), 
                credential_proposal_dict = pyaries.models.credential_proposal.CredentialProposal(
                    @id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                    @type = 'https://didcomm.org/my-family/1.0/my-message-type', 
                    comment = '', 
                    cred_def_id = 'WgWxqztrNooG92RXvxSTWv:3:CL:20:tag', 
                    credential_proposal = pyaries.models.credential_preview.CredentialPreview(
                        @type = 'issue-credential/1.0/credential-preview', 
                        attributes = [
                            pyaries.models.cred_attr_spec.CredAttrSpec(
                                mime_type = 'image/jpeg', 
                                name = 'favourite_drink', 
                                value = 'martini', )
                            ], ), 
                    issuer_did = 'WgWxqztrNooG92RXvxSTWv', 
                    schema_id = 'WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0', 
                    schema_issuer_did = 'WgWxqztrNooG92RXvxSTWv', 
                    schema_name = '', 
                    schema_version = '1.0', ), 
                credential_request = pyaries.models.indy_cred_request.IndyCredRequest(
                    blinded_ms = pyaries.models.blinded_ms.blinded_ms(), 
                    blinded_ms_correctness_proof = pyaries.models.blinded_ms_correctness_proof.blinded_ms_correctness_proof(), 
                    cred_def_id = 'WgWxqztrNooG92RXvxSTWv:3:CL:20:tag', 
                    nonce = '0', 
                    prover_did = 'WgWxqztrNooG92RXvxSTWv', ), 
                credential_request_metadata = None, 
                error_msg = 'Credential definition identifier is not set in proposal', 
                initiator = 'self', 
                parent_thread_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                raw_credential = pyaries.models.indy_credential.IndyCredential(
                    cred_def_id = 'WgWxqztrNooG92RXvxSTWv:3:CL:20:tag', 
                    rev_reg = pyaries.models.rev_reg.rev_reg(), 
                    rev_reg_id = 'WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0', 
                    schema_id = 'WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0', 
                    signature = pyaries.models.signature.signature(), 
                    signature_correctness_proof = pyaries.models.signature_correctness_proof.signature_correctness_proof(), 
                    values = {
                        'key' : pyaries.models.indy_attr_value.IndyAttrValue(
                            encoded = '-1', 
                            raw = '', )
                        }, 
                    witness = pyaries.models.witness.witness(), ), 
                revoc_reg_id = '', 
                revocation_id = '', 
                role = 'issuer', 
                schema_id = 'WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0', 
                state = 'credential_acked', 
                thread_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                trace = True, 
                updated_at = '2021-12-31 23:59:59+00:00'
            )
        else :
            return V10CredentialExchange(
        )
        """

    def testV10CredentialExchange(self):
        """Test V10CredentialExchange"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
