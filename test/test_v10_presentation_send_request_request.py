# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.v10_presentation_send_request_request import V10PresentationSendRequestRequest  # noqa: E501
from pyaries.rest import ApiException

class TestV10PresentationSendRequestRequest(unittest.TestCase):
    """V10PresentationSendRequestRequest unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V10PresentationSendRequestRequest
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V10PresentationSendRequestRequest`
        """
        model = pyaries.models.v10_presentation_send_request_request.V10PresentationSendRequestRequest()  # noqa: E501
        if include_optional :
            return V10PresentationSendRequestRequest(
                auto_verify = False, 
                comment = '', 
                connection_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                proof_request = pyaries.models.indy_proof_request.IndyProofRequest(
                    name = 'Proof request', 
                    non_revoked = pyaries.models.indy_proof_request_non_revoked.IndyProofRequestNonRevoked(
                        from = 1640995199, 
                        to = 1640995199, ), 
                    nonce = '1', 
                    requested_attributes = {
                        'key' : pyaries.models.indy_proof_req_attr_spec.IndyProofReqAttrSpec(
                            name = 'favouriteDrink', 
                            names = [
                                'age'
                                ], 
                            restrictions = [
                                {
                                    'key' : 'WgWxqztrNooG92RXvxSTWv:3:CL:20:tag'
                                    }
                                ], )
                        }, 
                    requested_predicates = {
                        'key' : pyaries.models.indy_proof_req_pred_spec.IndyProofReqPredSpec(
                            name = 'index', 
                            p_type = '>=', 
                            p_value = 56, )
                        }, 
                    version = '1.0', ), 
                trace = False
            )
        else :
            return V10PresentationSendRequestRequest(
                connection_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6',
                proof_request = pyaries.models.indy_proof_request.IndyProofRequest(
                    name = 'Proof request', 
                    non_revoked = pyaries.models.indy_proof_request_non_revoked.IndyProofRequestNonRevoked(
                        from = 1640995199, 
                        to = 1640995199, ), 
                    nonce = '1', 
                    requested_attributes = {
                        'key' : pyaries.models.indy_proof_req_attr_spec.IndyProofReqAttrSpec(
                            name = 'favouriteDrink', 
                            names = [
                                'age'
                                ], 
                            restrictions = [
                                {
                                    'key' : 'WgWxqztrNooG92RXvxSTWv:3:CL:20:tag'
                                    }
                                ], )
                        }, 
                    requested_predicates = {
                        'key' : pyaries.models.indy_proof_req_pred_spec.IndyProofReqPredSpec(
                            name = 'index', 
                            p_type = '>=', 
                            p_value = 56, )
                        }, 
                    version = '1.0', ),
        )
        """

    def testV10PresentationSendRequestRequest(self):
        """Test V10PresentationSendRequestRequest"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
