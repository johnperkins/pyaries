# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.taa_record import TAARecord  # noqa: E501
from pyaries.rest import ApiException

class TestTAARecord(unittest.TestCase):
    """TAARecord unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test TAARecord
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `TAARecord`
        """
        model = pyaries.models.taa_record.TAARecord()  # noqa: E501
        if include_optional :
            return TAARecord(
                digest = '', 
                text = '', 
                version = ''
            )
        else :
            return TAARecord(
        )
        """

    def testTAARecord(self):
        """Test TAARecord"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
