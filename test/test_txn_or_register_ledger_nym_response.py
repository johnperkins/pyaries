# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.txn_or_register_ledger_nym_response import TxnOrRegisterLedgerNymResponse  # noqa: E501
from pyaries.rest import ApiException

class TestTxnOrRegisterLedgerNymResponse(unittest.TestCase):
    """TxnOrRegisterLedgerNymResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test TxnOrRegisterLedgerNymResponse
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `TxnOrRegisterLedgerNymResponse`
        """
        model = pyaries.models.txn_or_register_ledger_nym_response.TxnOrRegisterLedgerNymResponse()  # noqa: E501
        if include_optional :
            return TxnOrRegisterLedgerNymResponse(
                success = True, 
                txn = pyaries.models.transaction_record.TransactionRecord(
                    _type = '101', 
                    connection_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                    created_at = '2021-12-31 23:59:59+00:00', 
                    endorser_write_txn = True, 
                    formats = [
                        {"attach_id":"3fa85f64-5717-4562-b3fc-2c963f66afa6","format":"dif/endorse-transaction/request@v1.0"}
                        ], 
                    messages_attach = [
                        {"@id":"143c458d-1b1c-40c7-ab85-4d16808ddf0a","data":{"json":"{\"endorser\": \"V4SGRU86Z58d6TV7PBUe6f\",\"identifier\": \"LjgpST2rjsoxYegQDRm7EL\",\"operation\": {\"data\": {\"attr_names\": [\"first_name\", \"last_name\"],\"name\": \"test_schema\",\"version\": \"2.1\",},\"type\": \"101\",},\"protocolVersion\": 2,\"reqId\": 1597766666168851000,\"signatures\": {\"LjgpST2rjsox\": \"4ATKMn6Y9sTgwqaGTm7py2c2M8x1EVDTWKZArwyuPgjU\"},\"taaAcceptance\": {\"mechanism\": \"manual\",\"taaDigest\": \"f50fe2c2ab977006761d36bd6f23e4c6a7e0fc2feb9f62\",\"time\": 1597708800,}}"},"mime-type":"application/json"}
                        ], 
                    meta_data = {"context":{"param1":"param1_value","param2":"param2_value"},"post_process":[{"topic":"topic_value","other":"other_value"}]}, 
                    signature_request = [
                        {"author_goal_code":"aries.transaction.ledger.write","context":"did:sov","method":"add-signature","signature_type":"<requested signature type>","signer_goal_code":"aries.transaction.endorse"}
                        ], 
                    signature_response = [
                        {"context":"did:sov","message_id":"3fa85f64-5717-4562-b3fc-2c963f66afa6","method":"add-signature","signer_goal_code":"aries.transaction.refuse"}
                        ], 
                    state = 'active', 
                    thread_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                    timing = {"expires_time":"2020-12-13T17:29:06+0000"}, 
                    trace = True, 
                    transaction_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                    updated_at = '2021-12-31 23:59:59+00:00', )
            )
        else :
            return TxnOrRegisterLedgerNymResponse(
        )
        """

    def testTxnOrRegisterLedgerNymResponse(self):
        """Test TxnOrRegisterLedgerNymResponse"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
