# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.v20_discovery_exchange_result import V20DiscoveryExchangeResult  # noqa: E501
from pyaries.rest import ApiException

class TestV20DiscoveryExchangeResult(unittest.TestCase):
    """V20DiscoveryExchangeResult unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V20DiscoveryExchangeResult
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V20DiscoveryExchangeResult`
        """
        model = pyaries.models.v20_discovery_exchange_result.V20DiscoveryExchangeResult()  # noqa: E501
        if include_optional :
            return V20DiscoveryExchangeResult(
                results = pyaries.models.v20_discovery_record.V20DiscoveryRecord(
                    connection_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                    created_at = '2021-12-31 23:59:59+00:00', 
                    disclosures = pyaries.models.disclosures.Disclosures(
                        @id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                        @type = 'https://didcomm.org/my-family/1.0/my-message-type', 
                        disclosures = [
                            None
                            ], ), 
                    discovery_exchange_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                    queries_msg = pyaries.models.queries.Queries(
                        @id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                        @type = 'https://didcomm.org/my-family/1.0/my-message-type', 
                        queries = [
                            pyaries.models.query_item.QueryItem(
                                feature_type = 'protocol', 
                                match = '', )
                            ], ), 
                    state = 'active', 
                    thread_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                    trace = True, 
                    updated_at = '2021-12-31 23:59:59+00:00', )
            )
        else :
            return V20DiscoveryExchangeResult(
        )
        """

    def testV20DiscoveryExchangeResult(self):
        """Test V20DiscoveryExchangeResult"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
