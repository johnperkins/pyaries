# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.indy_rev_reg_def import IndyRevRegDef  # noqa: E501
from pyaries.rest import ApiException

class TestIndyRevRegDef(unittest.TestCase):
    """IndyRevRegDef unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test IndyRevRegDef
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `IndyRevRegDef`
        """
        model = pyaries.models.indy_rev_reg_def.IndyRevRegDef()  # noqa: E501
        if include_optional :
            return IndyRevRegDef(
                cred_def_id = 'WgWxqztrNooG92RXvxSTWv:3:CL:20:tag', 
                id = 'WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0', 
                revoc_def_type = 'CL_ACCUM', 
                tag = '', 
                value = pyaries.models.indy_rev_reg_def_value.IndyRevRegDefValue(
                    issuance_type = 'ISSUANCE_ON_DEMAND', 
                    max_cred_num = 10, 
                    public_keys = pyaries.models.indy_rev_reg_def_value_public_keys.IndyRevRegDefValuePublicKeys(
                        accum_key = pyaries.models.indy_rev_reg_def_value_public_keys_accum_key.IndyRevRegDefValuePublicKeysAccumKey(
                            z = '1 120F522F81E6B7 1 09F7A59005C4939854', ), ), 
                    tails_hash = 'H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV', 
                    tails_location = '', ), 
                ver = '1.0'
            )
        else :
            return IndyRevRegDef(
        )
        """

    def testIndyRevRegDef(self):
        """Test IndyRevRegDef"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
