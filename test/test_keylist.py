# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.keylist import Keylist  # noqa: E501
from pyaries.rest import ApiException

class TestKeylist(unittest.TestCase):
    """Keylist unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test Keylist
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `Keylist`
        """
        model = pyaries.models.keylist.Keylist()  # noqa: E501
        if include_optional :
            return Keylist(
                results = [
                    pyaries.models.route_record.RouteRecord(
                        connection_id = '', 
                        created_at = '2021-12-31 23:59:59+00:00', 
                        recipient_key = '', 
                        record_id = '', 
                        role = '', 
                        state = 'active', 
                        updated_at = '2021-12-31 23:59:59+00:00', 
                        wallet_id = '', )
                    ]
            )
        else :
            return Keylist(
        )
        """

    def testKeylist(self):
        """Test Keylist"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
