# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.v10_credential_proposal_request_mand import V10CredentialProposalRequestMand  # noqa: E501
from pyaries.rest import ApiException

class TestV10CredentialProposalRequestMand(unittest.TestCase):
    """V10CredentialProposalRequestMand unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V10CredentialProposalRequestMand
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V10CredentialProposalRequestMand`
        """
        model = pyaries.models.v10_credential_proposal_request_mand.V10CredentialProposalRequestMand()  # noqa: E501
        if include_optional :
            return V10CredentialProposalRequestMand(
                auto_remove = True, 
                comment = '', 
                connection_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                cred_def_id = 'WgWxqztrNooG92RXvxSTWv:3:CL:20:tag', 
                credential_proposal = pyaries.models.credential_preview.CredentialPreview(
                    @type = 'issue-credential/1.0/credential-preview', 
                    attributes = [
                        pyaries.models.cred_attr_spec.CredAttrSpec(
                            mime_type = 'image/jpeg', 
                            name = 'favourite_drink', 
                            value = 'martini', )
                        ], ), 
                issuer_did = 'WgWxqztrNooG92RXvxSTWv', 
                schema_id = 'WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0', 
                schema_issuer_did = 'WgWxqztrNooG92RXvxSTWv', 
                schema_name = 'preferences', 
                schema_version = '1.0', 
                trace = True
            )
        else :
            return V10CredentialProposalRequestMand(
                connection_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6',
                credential_proposal = pyaries.models.credential_preview.CredentialPreview(
                    @type = 'issue-credential/1.0/credential-preview', 
                    attributes = [
                        pyaries.models.cred_attr_spec.CredAttrSpec(
                            mime_type = 'image/jpeg', 
                            name = 'favourite_drink', 
                            value = 'martini', )
                        ], ),
        )
        """

    def testV10CredentialProposalRequestMand(self):
        """Test V10CredentialProposalRequestMand"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
