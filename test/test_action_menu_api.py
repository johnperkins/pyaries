# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import pyaries
from pyaries.api.action_menu_api import ActionMenuApi  # noqa: E501
from pyaries.rest import ApiException


class TestActionMenuApi(unittest.TestCase):
    """ActionMenuApi unit test stubs"""

    def setUp(self):
        self.api = pyaries.api.action_menu_api.ActionMenuApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_close_active_menu(self):
        """Test case for close_active_menu

        Close the active menu associated with a connection  # noqa: E501
        """
        pass

    def test_fetch_active_menu(self):
        """Test case for fetch_active_menu

        Fetch the active menu  # noqa: E501
        """
        pass

    def test_perform_action(self):
        """Test case for perform_action

        Perform an action associated with the active menu  # noqa: E501
        """
        pass

    def test_request_active_menu(self):
        """Test case for request_active_menu

        Request the active menu  # noqa: E501
        """
        pass

    def test_send_menu(self):
        """Test case for send_menu

        Send an action menu to a connection  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
