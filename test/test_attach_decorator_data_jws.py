# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.attach_decorator_data_jws import AttachDecoratorDataJWS  # noqa: E501
from pyaries.rest import ApiException

class TestAttachDecoratorDataJWS(unittest.TestCase):
    """AttachDecoratorDataJWS unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test AttachDecoratorDataJWS
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `AttachDecoratorDataJWS`
        """
        model = pyaries.models.attach_decorator_data_jws.AttachDecoratorDataJWS()  # noqa: E501
        if include_optional :
            return AttachDecoratorDataJWS(
                header = pyaries.models.attach_decorator_data_jws_header.AttachDecoratorDataJWSHeader(
                    kid = 'did:sov:LjgpST2rjsoxYegQDRm7EL#keys-4', ), 
                protected = 'ey4uLn0', 
                signature = 'ey4uLn0', 
                signatures = [
                    pyaries.models.attach_decorator_data1_jws.AttachDecoratorData1JWS(
                        header = pyaries.models.attach_decorator_data_jws_header.AttachDecoratorDataJWSHeader(
                            kid = 'did:sov:LjgpST2rjsoxYegQDRm7EL#keys-4', ), 
                        protected = 'ey4uLn0', 
                        signature = 'ey4uLn0', )
                    ]
            )
        else :
            return AttachDecoratorDataJWS(
        )
        """

    def testAttachDecoratorDataJWS(self):
        """Test AttachDecoratorDataJWS"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
