# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.v20_pres_format import V20PresFormat  # noqa: E501
from pyaries.rest import ApiException

class TestV20PresFormat(unittest.TestCase):
    """V20PresFormat unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V20PresFormat
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V20PresFormat`
        """
        model = pyaries.models.v20_pres_format.V20PresFormat()  # noqa: E501
        if include_optional :
            return V20PresFormat(
                attach_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                format = 'dif/presentation-exchange/submission@v1.0'
            )
        else :
            return V20PresFormat(
                attach_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6',
                format = 'dif/presentation-exchange/submission@v1.0',
        )
        """

    def testV20PresFormat(self):
        """Test V20PresFormat"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
