# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import pyaries
from pyaries.api.ledger_api import LedgerApi  # noqa: E501
from pyaries.rest import ApiException


class TestLedgerApi(unittest.TestCase):
    """LedgerApi unit test stubs"""

    def setUp(self):
        self.api = pyaries.api.ledger_api.LedgerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_accept_taa(self):
        """Test case for accept_taa

        Accept the transaction author agreement  # noqa: E501
        """
        pass

    def test_fetch_taa(self):
        """Test case for fetch_taa

        Fetch the current transaction author agreement, if any  # noqa: E501
        """
        pass

    def test_get_did_endpoint(self):
        """Test case for get_did_endpoint

        Get the endpoint for a DID from the ledger.  # noqa: E501
        """
        pass

    def test_get_did_nym_role(self):
        """Test case for get_did_nym_role

        Get the role from the NYM registration of a public DID.  # noqa: E501
        """
        pass

    def test_get_did_verkey(self):
        """Test case for get_did_verkey

        Get the verkey for a DID from the ledger.  # noqa: E501
        """
        pass

    def test_ledger_multiple_config_get(self):
        """Test case for ledger_multiple_config_get

        Fetch the multiple ledger configuration currently in use  # noqa: E501
        """
        pass

    def test_ledger_multiple_get_write_ledger_get(self):
        """Test case for ledger_multiple_get_write_ledger_get

        Fetch the current write ledger  # noqa: E501
        """
        pass

    def test_register_nym(self):
        """Test case for register_nym

        Send a NYM registration to the ledger.  # noqa: E501
        """
        pass

    def test_rotate_public_did_keypair(self):
        """Test case for rotate_public_did_keypair

        Rotate key pair for public DID.  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
