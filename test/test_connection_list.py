# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.connection_list import ConnectionList  # noqa: E501
from pyaries.rest import ApiException

class TestConnectionList(unittest.TestCase):
    """ConnectionList unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test ConnectionList
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `ConnectionList`
        """
        model = pyaries.models.connection_list.ConnectionList()  # noqa: E501
        if include_optional :
            return ConnectionList(
                results = [
                    pyaries.models.conn_record.ConnRecord(
                        accept = 'auto', 
                        alias = 'Bob, providing quotes', 
                        connection_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                        connection_protocol = 'connections/1.0', 
                        created_at = '2021-12-31 23:59:59+00:00', 
                        error_msg = 'No DIDDoc provided; cannot connect to public DID', 
                        inbound_connection_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                        invitation_key = 'H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV', 
                        invitation_mode = 'once', 
                        invitation_msg_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                        my_did = 'WgWxqztrNooG92RXvxSTWv', 
                        request_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                        rfc23_state = 'invitation-sent', 
                        routing_state = 'active', 
                        state = 'active', 
                        their_did = 'WgWxqztrNooG92RXvxSTWv', 
                        their_label = 'Bob', 
                        their_public_did = '2cpBmR3FqGKWi5EyUbpRY8', 
                        their_role = 'requester', 
                        updated_at = '2021-12-31 23:59:59+00:00', )
                    ]
            )
        else :
            return ConnectionList(
        )
        """

    def testConnectionList(self):
        """Test ConnectionList"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
