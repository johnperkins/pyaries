# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.indy_non_revocation_interval import IndyNonRevocationInterval  # noqa: E501
from pyaries.rest import ApiException

class TestIndyNonRevocationInterval(unittest.TestCase):
    """IndyNonRevocationInterval unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test IndyNonRevocationInterval
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `IndyNonRevocationInterval`
        """
        model = pyaries.models.indy_non_revocation_interval.IndyNonRevocationInterval()  # noqa: E501
        if include_optional :
            return IndyNonRevocationInterval(
                var_from = 1640995199, 
                to = 1640995199
            )
        else :
            return IndyNonRevocationInterval(
        )
        """

    def testIndyNonRevocationInterval(self):
        """Test IndyNonRevocationInterval"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
