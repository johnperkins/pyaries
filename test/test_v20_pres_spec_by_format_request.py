# coding: utf-8

"""
    Aries Cloud Agent

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: v0.7.5
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import pyaries
from pyaries.models.v20_pres_spec_by_format_request import V20PresSpecByFormatRequest  # noqa: E501
from pyaries.rest import ApiException

class TestV20PresSpecByFormatRequest(unittest.TestCase):
    """V20PresSpecByFormatRequest unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test V20PresSpecByFormatRequest
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `V20PresSpecByFormatRequest`
        """
        model = pyaries.models.v20_pres_spec_by_format_request.V20PresSpecByFormatRequest()  # noqa: E501
        if include_optional :
            return V20PresSpecByFormatRequest(
                dif = pyaries.models.dif_pres_spec.DIFPresSpec(
                    issuer_id = '', 
                    presentation_definition = pyaries.models.presentation_definition.PresentationDefinition(
                        format = pyaries.models.claim_format.ClaimFormat(
                            jwt = pyaries.models.jwt.jwt(), 
                            jwt_vc = pyaries.models.jwt_vc.jwt_vc(), 
                            jwt_vp = pyaries.models.jwt_vp.jwt_vp(), 
                            ldp = pyaries.models.ldp.ldp(), 
                            ldp_vc = pyaries.models.ldp_vc.ldp_vc(), 
                            ldp_vp = pyaries.models.ldp_vp.ldp_vp(), ), 
                        id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                        input_descriptors = [
                            pyaries.models.input_descriptors.InputDescriptors(
                                constraints = pyaries.models.constraints.Constraints(
                                    fields = [
                                        pyaries.models.dif_field.DIFField(
                                            filter = pyaries.models.filter.Filter(
                                                const = pyaries.models.const.const(), 
                                                enum = [
                                                    None
                                                    ], 
                                                exclusive_maximum = pyaries.models.exclusive_maximum.exclusiveMaximum(), 
                                                exclusive_minimum = pyaries.models.exclusive_minimum.exclusiveMinimum(), 
                                                max_length = 1234, 
                                                maximum = pyaries.models.maximum.maximum(), 
                                                min_length = 1234, 
                                                minimum = pyaries.models.minimum.minimum(), 
                                                not = False, 
                                                pattern = '', 
                                                type = '', ), 
                                            id = '', 
                                            path = [
                                                ''
                                                ], 
                                            predicate = 'required', 
                                            purpose = '', )
                                        ], 
                                    is_holder = [
                                        pyaries.models.dif_holder.DIFHolder(
                                            directive = 'required', 
                                            field_id = [
                                                '3fa85f64-5717-4562-b3fc-2c963f66afa6'
                                                ], )
                                        ], 
                                    limit_disclosure = '', 
                                    status_active = 'required', 
                                    status_revoked = 'required', 
                                    status_suspended = 'required', 
                                    subject_is_issuer = 'required', ), 
                                group = [
                                    ''
                                    ], 
                                id = '', 
                                metadata = pyaries.models.metadata.metadata(), 
                                name = '', 
                                purpose = '', 
                                schema = pyaries.models.schemas_input_descriptor_filter.SchemasInputDescriptorFilter(
                                    oneof_filter = True, 
                                    uri_groups = [
                                        [
                                            pyaries.models.schema_input_descriptor.SchemaInputDescriptor(
                                                required = True, 
                                                uri = '', )
                                            ]
                                        ], ), )
                            ], 
                        name = '', 
                        purpose = '', 
                        submission_requirements = [
                            pyaries.models.submission_requirements.SubmissionRequirements(
                                count = 1234, 
                                from = '', 
                                from_nested = [
                                    pyaries.models.submission_requirements.SubmissionRequirements(
                                        count = 1234, 
                                        from = '', 
                                        max = 1234, 
                                        min = 1234, 
                                        name = '', 
                                        purpose = '', 
                                        rule = 'all', )
                                    ], 
                                max = 1234, 
                                min = 1234, 
                                name = '', 
                                purpose = '', 
                                rule = 'all', )
                            ], ), 
                    record_ids = {"<input descriptor id_1>":["<record id_1>","<record id_2>"],"<input descriptor id_2>":["<record id>"]}, 
                    reveal_doc = {"@context":["https://www.w3.org/2018/credentials/v1","https://w3id.org/security/bbs/v1"],"@explicit":true,"@requireAll":true,"credentialSubject":{"@explicit":true,"@requireAll":true,"Observation":[{"effectiveDateTime":{},"@explicit":true,"@requireAll":true}]},"issuanceDate":{},"issuer":{},"type":["VerifiableCredential","LabReport"]}, ), 
                indy = pyaries.models.indy_pres_spec.IndyPresSpec(
                    requested_attributes = {
                        'key' : pyaries.models.indy_requested_creds_requested_attr.IndyRequestedCredsRequestedAttr(
                            cred_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                            revealed = True, )
                        }, 
                    requested_predicates = {
                        'key' : pyaries.models.indy_requested_creds_requested_pred.IndyRequestedCredsRequestedPred(
                            cred_id = '3fa85f64-5717-4562-b3fc-2c963f66afa6', 
                            timestamp = 1640995199, )
                        }, 
                    self_attested_attributes = {
                        'key' : 'self_attested_value'
                        }, 
                    trace = False, ), 
                trace = True
            )
        else :
            return V20PresSpecByFormatRequest(
        )
        """

    def testV20PresSpecByFormatRequest(self):
        """Test V20PresSpecByFormatRequest"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
